# Tema 6 - Comandos de filtrado - Entrada y Salida estándar

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Canales estándar](#canales-estándar)
- [Redirigiendo canales estándar](#redirigiendo-canales-estándar)
- [Enviar datos entre comandos (pipes)](#enviar-datos-entre-comandos-pipes)
- [Comandos de filtrado](#comandos-de-filtrado)
    - [Salida y concatenación de ficheros de texto](#salida-y-concatenación-de-ficheros-de-texto)
    - [Principio y final](#principio-y-final)
    - [Procesado de texto carácter a carácter](#procesado-de-texto-carácter-a-carácter)
        - [tr](#tr)
    - [Procesado de texto línea a línea](#procesado-de-texto-línea-a-línea)
        - [nl](#nl)
        - [od](#od)
        - [split](#split)
        - [wc](#wc)
    - [Gestión de datos](#gestión-de-datos)
        - [sort](#sort)
        - [uniq](#uniq)
    - [Columnas y campos](#columnas-y-campos)
        - [cut](#cut)
        - [paste](#paste)
    - [Algoritmo MD5](#algoritmo-md5)
    - [Reforzando la seguridad en algoritmos hash](#reforzando-la-seguridad-en-algoritmos-hash)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)
- [Resumen](#resumen)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos

* Redirección de los canales de entrada y salida en la shell.
* Conocer los comandos más importantes para operaciones de filtrado.

## Canales estándar

Muchos comandos de Linux están diseñados para leer datos de entrada, manipularlos y mostrar los resultados de esa operación. Por ejemplo:

	$ grep xyz

con este comando podemos escribir líneas de texto con el teclado, y `grep` sólo dejará pasar aquellas que cumplan con el criterio de búsqueda, es decir, aquellas que contengan la secuencia de caracteres `xyz`:

	$ grep xyz
	abd def
	xyz 123
	xyz 123
	aaa bbb
	XXXYYYZZZ
	...

Decimos que `grep` lee los datos desde su «entrada estándar» (el teclado), y los envía hacia su «salida estándar» (la pantalla).
El tercer canal es la «salida de error estándar». Mientras que los resultados los muestra por su salida estándar, `grep` envía hacia la salida de error cualquier mensaje de error.

<p align="center"><img width="600" src="img/T08_01.png"></p>

La shell puede redireccionar eses canales estándar para comandos individuales, sin que los programas noten ningún cambio. La salida estándar puede ser redirigida hacia un fichero, que aunque no exista, será creado. El canal de entrada también puede ser redirigido, y un programa puede recibir su entrada desde un fichero.

## Redirigiendo canales estándar

Podemos redirigir el canal de la salida estándar usando el operador de la shell `>`. En el siguiente ejemplo, la salida del comando `ls -laF` es redirigido hacia el fichero `filelist`:

	$ ls -laF >filelist

Si el fichero `filelist` no existe, será creado. Y si existe un fichero con ese nombre, se sobreescribe su contenido. De hecho, la shell crea dicho fichero aunque el comando contenga un error y no se pueda ejecutar correctamente.

> Si queremos evitar la sobreescritura de ficheros existentes, debemos activar la opción `noclobber` (`set -o noclobber`). En este caso, la redirección hacia un fichero existente, provocará un error.

Podemos visualizar el contenido del fichero `filelist` usando el comando `less`:

	$ less filelist

*¿Hay algo extraño en el contenido del fichero anterior?*

Si lo que queremos es añadir la salida de un comando a un fichero existente sin sustituir su contenido anterior, usaremos el operador `>>`. Si el fichero no existe, también se crea.

	$ date >> filelist
	$ less filelist

Otro método para redirigir la salida estándar de un comando es mediante el uso de los acentos "`". Esto se denomina **sustitución de comando**. La salida estándar del comando entrecomillado será insertada en la línea de comandos. Por ejemplo:

```
$ cat fechas
22/12 Comprar regalos
23/12 Comprar árbol de navidad
24/12 Nochebuena
$ date +%d/%m
23/12
$ grep ^`date +%d/%m` fechas
...
```

> Una sintaxis más adecuada para \`date\` es `$(date)`. Sin embargo, esto sólo está soportado por las shells más modernas.

Podemos usar el operador `<` para redirigir el canal de entrada estándar. Se leerá el contenido del fichero indicado en lugar de la entrada de teclado.

	$ wc -l </etc/passwd
	36

> No hay operador de redirección `<<` para concatenar múltiples ficheros de entrada; para enviar el contenido de varios ficheros como entrada de otro, debemos hacer uso de `cat`
>	$ cat filea fileb filec | wc -l

El operador `|` lo veremos más adelante. La mayoría de los programas aceptan uno o más nombres de fichero como argumentos en la línea de comandos.

> Sin embargo, usamos el operador `<<` para recoger líneas de entrada para un comando hasta que se introduce el comando indicado. Por ejemplo:

```
$ grep Linux <<END
Las rosas son rojas,
las violetas azules,
Linux es bonito,
yo sé que es verdad.
END
```

> Si indicamos la cadena final del *documento* sin comillas, las variables serán evaluadas, y la sustitución de comandos (\`...\` o `$()`) se ejecutarán sobre las líneas del *documento*. Pero con las comillas, el *documento* será procesado tal cual. Compara las salidas:

```
$ cat <<EOF
Hoy es: `date`
EOF

$ cat <<"EOF"
Hoy es: `date`
EOF
```

Por supuesto, la entrada y salida estándar pueden redireccionarse al mismo tiempo. La salida del contador de palabras se escribe en un fichero llamado `wordcount`:

```
$ wc -w < frog.txt > wordcount
$ cat wordcount
```

Además de los canales de entrada y salida estándar, también disponemos del canal de salida estándar. Si ocurre algún error durante la ejecución del comando, los mensajes de error se envían hacia dicha salida. De esta forma, se verán los mensajes de error aunque hayamos redireccionado la salida estándar.
Si queremos redireccionar la salida de error debemos indicarlo con el número de canal (2), algo que no era necesario para las salidas anteriores (0 y 1).
Usamos el operador `>&` para redireccionar un canal hacia otro:

	$ make >make.log 2>&1

> El orden es muy importante !
	make >make.log 2>&1
	make 2>&1 >make.log

## Enviar datos entre comandos (pipes)

La redirección de la salida suele ser usada para almacenar el resultado de un programa y continuar el proceso con un comando diferente. Sin embargo, este tipo de intercambio puede resultar tedioso, además de tener que deshacerse más tarde de aquellos ficheros que ya han sido utilizados.
Por supuesto, Linux ofrece un método para unir comandos directamente a través de conexiones, llamadas *tuberías*, de tal forma que la salida de un programa se convierte en la entrada de otro.

	$ ls -la | less

Las *tuberías* entre comandos se pueden encadenar entre varios comandos, donde incluso el resultado final se envía hacia un fichero:

	$ cut -d: -f1 /etc/passwd | sort | pr -2 >userlist

Esta línea extrae todos los nombres de usuario presentes en la primera columna del fichero `/etc/passwd`, los ordena alfabéticamente y los escribe en el fichero `userlist` en un formato de dos columnas.
A veces puede resultar útil almacenar un flujo de datos en el interior de una *tubería* en un punto intermedio, por ejemplo, porque el resultado intermedio en esa etapa es útil para diferentes tareas. El comando `tee` copia los datos y los envía a la salida estándar y a un fichero de forma simultánea.

<p align="center"><img width="600" src="img/T08_01.png"></p>

El comando `tee` sin opciones crea el fichero indicado, o lo sobreescribe si existe; con la opción `-a` (append), la salida puede añadirse a un fichero existente:

	$ ls -laF | tee lista | less

En este ejemplo, el contenido del directorio actual se escribe tanto al fichero `lista` como en la pantalla.

> El fichero `lista` no se muestra en la salida de `ls`, porque es creado posteriormente por el comando `tee`.

## Comandos de filtrado

Uno de los principios básicos de UNIX (y por lo tanto de Linux) es el concepto de *kit de herramientas*. El sistema incorpora un gran número de programas del sistema, donde cada uno de ellos ejecuta una tarea simple (al menos, conceptualmente hablando). Esos programas se pueden usar como bloques de construcción para a su vez construir otros programas, y ahorrar a los autores el desarrollo de las funciones requeridas. Por ejemplo, no todos los programas incorporan su propia rutina de ordenamiento, ya que muchos de ellos utilizan el comando `sort` proporcionado por Linux.

Esta estructura modular tiene varias ventajas:

* Alivia el trabajo a los programadores, que no necesitan desarrollar (o añadir) nuevas rutinas de ordenamiento cada vez que escriben un programa.
* Si se soluciona algún *bug* en el comando `sort`, o se le añade alguna función adicional, todos los programas que lo usan se benefician de ella, además de que en la mayoría de los casos no es necesario realizar ningún cambio en los programas que hacen uso del comando.

Las herramientas que toman su entrada de la entrada estándar y escriben su salida a la salida estándar, son denominados *comandos de filtrado*, o *filtros*. Si no hay redirecciones, un filtro leerá su entrada desde el teclado. Para finalizar la entrada desde el teclado, se debe usar la secuencia `CTRL-D`, que se interpreta como `EOF` (fin de fichero) por el driver del terminal.

### Salida y concatenación de ficheros de texto

El comando `cat` (con**cat**enar), en realidad está diseñado para unir varios ficheros en uno sólo. Si le pasamos tan sólo un nombre de fichero, se mostrará el contenido de ese fichero por la salida estándar. Y si no le pasamos ningún parámetro, `cat` lee su entrada estándar, y esto aunque pueda parecer de poca utilidad, `cat` ofrece opciones para numerar líneas, añadir finales de línea, hacer visibles caracteres especiales, o comprimir grupos de líneas en blanco en una sola.

> En este momento debemos resaltar que sólo los ficheros de texto tienen un buen comportamiento en pantalla cuando se usan con `cat`. Si se aplica el comando a otro tipo de ficheros (por ejemplo: `/bin/cat`), es más que probable -al menos en una terminal de texto- que el prompt de la shell se transforme en un montón de caracteres no legibles. Si esto sucede, se puede restaurar el conjunto normal de caracteres usando el comando `reset`. Por supuesto, si se redirige la salida hacia un fichero no tendremos este problema.

> Es habitual que se haga un uso abusivo del comando `cat`. En la mayoría de los casos, los comandos utilizados pueden leer de ficheros y no tan sólo lo hacen de su entrada estándar, por lo tanto no es necesario usar `cat` para pasar un fichero a la entrada estándar. Un comando como `cat data.txt | grep foo` no es necesario si se puede escribir de forma más sencilla: `grep foo data.txt`. Incluso aunque `grep` sólo fuera capaz de leer de su entrada estándar, `grep foo <data.txt` sería más corto y no implicaría el uso del comando `cat`.

El comando `tac`, es un *cat hacia atrás*, y hace el mismo trabajo que éste: lee una serie de ficheros, o su entrada estándar, y muestra las líneas que ha leído, pero en orden inverso. Sin embargo, es hasta aquí donde llegan las semejanzas: `tac` no soporta las mismas opciones que `cat`, e incorpora las suyas propias. Por ejemplo, se puede usar la opción `-s` para configurar un separador alternativo que el programa usará para revertir la entrada -normalmente el separador es el carácter de nueva línea, por lo que la salida es dada la vuelta línea a línea.

	$ echo A:B:C:D | tac -s :

La salida anterior puede parecer un poco extraña, pero puede ser explicada de la siguiente manera:
La entrada está formada por cuatro partes `A:`, `B:`, `C:` y `D\n` (el separadar `:`, se entiende que pertenece a la parte que le precede, y el último carácter de nueva línea lo ha insertado el comando `echo`. Estas cuatro partes se colocan en sentido contrario, `D\n` es el primero y luego van las otras tres, ya que cada una de ellas posee un carácter de separación. De esta forma el prompt de la shell queda junto a la salida. Si usamos la opción `-b`, se considera que el separador pertenece a la parte que le sigue en lugar de la que le precede.

### Principio y final

Hay situaciones en las que sólo nos interesa una parte del fichero. Las primeras líneas para comprobar que es el fichero correcto, o en particular con ficheros de *log*, las últimas entradas.	Los comandos `head` y `tail` ejecutan exactamente esto -por defecto las 10 primeras (y últimas) líneas de cada uno de los ficheros pasados como argumentos (o incluso con la entrada estándar).	La opción `-n` le permite especificar un número diferente de líneas; `head -n 20` devuelve las 20 primeras líneas de su entrada estándar, y `tail -n 5 data.txt`, las cinco últimas del fichero `data.txt`.

> La tradición dice que se puede especificar el número *n* de líneas deseadas, directamente con `-n`. Oficialmente, esto no es admitido, pero las versiones Linux de `head` y `tail` sí lo soportan.

Se puede usar la opción `-c` para especificar que la cuenta debe hacerse en bytes, no en líneas: `head -c 20` muestra los primeros 20 bytes, sin importar cuántas líneas ocupen.

	$ head -c 20 /etc/passwd

El comando `head` soporta el símbolo menos: `head -c -20` mostrará todo menos los últimos 20 bytes.

	$ head -c -20 /etc/passwd

> A modo de venganza, `tail` puede hacer algo que `head` no soporta. Si el número de líneas comienza con *+*, muestra todo empezando en la línea indicada: `$ tail -n +3 file`

El comando `tail` también soporta la opción `-f`. Esto provoca que `tail` espere aún después de haber encontrado el fin de fichero, para mostrar los datos que se pueden ir añadiendo más tarde. Esto es muy útil para echarles un vistazo en tiempo real a los ficheros de *log*. Si le pasamos varios ficheros a `tail -f`, coloca una línea de cabecera enfrente de cada bloque de salida.

### Procesado de texto carácter a carácter

#### tr

El comando `tr` es usado para reemplazar caracteres simples por otros diferentes en un texto, o para eliminarlos, `tr` es un comando de filtro en el sentido más estricto, no toma como argumentos los nombres de los ficheros, sino que trabaja con los canales estándar. Para sustituciones, la sintaxis es:

	tr <s1> <s2>

Los dos parámetros son cadenas de caracteres describiendo la sustitución. En el caso más simple, el primer carácter en «s1» será sustituido por el primer carácter en «s2», el segundo de «s1» por el segundo de «s2», y así sucesivamente.

Si «s1» tiene mayor longitud que «s2», los caracteres de exceso de «s1» son sustituidos por el último carácter de «s2»; y si «s2» es más largo que «s1», los caracteres extra de «s2» son ignorados. Por ejemplo:

	tr Aeiu aey <ejemplo.txt >nuevo.txt

Este comando lee el fichero `ejemplo.txt` y reemplaza las “A” por “a”, las “E” por “e”, y todas las “i” y “u”, por “y”. El resultado se almacena en `nuevo.txt`. Es posible expresar secuencias de caracteres por rangos, de la forma «m-n», donde «m» debe preceder a «n» en el orden del alfabeto.

Con la opción `-c`, `tr` no reemplaza el contenido de «s1», sino su complementario, es decir, todos los caracteres que NO están en «s1». Por ejemplo:

	tr -c A-Za-z ' ' <ejemplo.txt >nuevo.txt

reemplaza todas las «no-letras» del fichero `ejemplo.txt`, por espacios. Para eliminar caracteres, sólo se necesita especificar «s1», el comando:

	tr -d a-z <ejemplo.txt >nuevo.txt

elimina todas las letras minúsculas de `ejemplo.txt`.

Se pueden reemplazar varias ocurrencias seguidas de un carácter por uno solo:

	tr -s '\n' <ejemplo.txt >nuevo.txt

elimina las secuencias de líneas vacías, mediante el reemplazamiento de caracteres de nueva línea por uno sólo.	La opción `-s` (squeeze, “exprimir”) también hace posible sustituir dos caracteres de entrada diferentes por dos idénticos, y luego reemplazarlos por uno solo (al igual que hacía la opción `-s` con un sólo argumento):

	tr -s AE X <ejemplo.txt >nuevo.txt

convierte todos los caracteres «A» y «E» (y las secuencias de ellos) en una «X».

### Procesado de texto línea a línea

#### nl

El comando `nl` se utiliza para numerar las líneas. Si no se especifica nada más, se enumeran las líneas de su entrada de forma consecutiva (las líneas con contenido):

	$ nl /etc/passwd

Esto también lo podría realizar el comando `cat -b`. Aunque `nl` permite un mayor control sobre el proceso de numeración de las líneas:

	$ nl -b a -n rz -w 5 -v 1000 -i 10 frog.txt

La opción `-b a`, causa que todas las líneas sean numeradas, no sólo las «no vacías». `-n rz` formatea los números de línea rellenando con ceros por la derecha. `-w 5`, crea una columna de 5 posiciones. `-v 1000` comienza la numeración en 1000, y `-i 10` incrementa el número de línea de 10 en 10 por cada línea.

Además, `nl` también puede manejar números de línea por página. Esto se organiza usando las cadenas «mágicas» `\:\:\:` o `\:`, como se muestra en el ejemplo:

	$ cat nl-test.txt

Cada página tiene una cabecera y un pie de página, así como un cuerpo conteniendo el texto. La cabecera se introduce usando `\:\:\:`, y separado del cuerpo usando `\:\:`. El cuerpo finaliza con `\:`. Por defecto, `nl` numera las líneas de cada una de las páginas empezando en 1; la cabecera y el pie de página no se numeran.

#### od

El comando `od` nos puede facilitar la labor de investigación y revisión del contenido de un fichero. Estas tareas pueden incluír el trabajo con una imagen o con un fichero de texto que ha sido modificado por una aplicación. Esta utilidad permite visualizar el contenido del fichero en formato octal (base 8), hexadecimal (base 16), decimal (base 10) y ASCII.

Por defecto, la salida se muestra en formato octal.

```
	$ cat cientouno.txt
	101
	ciento uno
	one hundred one
	cento un
	cent un
	centouno
	$ od cientouno.txt
	0000000 030061 005061 064543 067145 067564 072440 067556 067412
	0000020 062556 064040 067165 071144 062145 067440 062556 061412
	0000040 067145 067564 072440 005156 062543 072156 072440 005156
	0000060 062543 072156 072557 067556 000012
	0000071
```

La primera columna de la salida del comando `od` es un índice para cada una de las líneas. Por ejemplo, en la tercera línea, `0000040` indica que esa línea comienza en el byte 40 en octal (decimal 32). En la última fila aparece el índice 71 en octal, que corresponde con el tamaño total del fichero (57 en decimal, que será el resultado visible con el comando `ls` por ejemplo).

Podemos mejorar la lectura de la salida de este comando con la opción `-cb`, por ejemplo:

```
	$ od -cb cientouno.txt
	0000000   1   0   1  \n   c   i   e   n   t   o       u   n   o  \n   o
        	061 060 061 012 143 151 145 156 164 157 040 165 156 157 012 157
	0000020   n   e       h   u   n   d   r   e   d       o   n   e  \n   c
        	156 145 040 150 165 156 144 162 145 144 040 157 156 145 012 143
	0000040   e   n   t   o       u   n  \n   c   e   n   t       u   n  \n
        	145 156 164 157 040 165 156 012 143 145 156 164 040 165 156 012
	0000060   c   e   n   t   o   u   n   o  \n
        	143 145 156 164 157 165 156 157 012
	0000071
```

#### split

El comando `split` permite dividir un fichero en partes de menor tamaño. Se puede dividir usando el tamaño, los bytes que ocupa o las líneas. Es importante remarcar que el fichero original queda intacto, y se crean tantos ficheros nuevos como las opciones indiquen.

Veamos un ejemplo, haciendo uso del fichero `cientouno.txt`:

```
	$ split -l 3 cientouno.txt split101
	$ ls split101*
	split101aa split101ab
	$ cat split101aa
	101
	ciento uno
	one hundred one
	$ cat split101ab
	cento un
	cent un
	centouno
```

#### wc

El nombre del comando `wc` es una abreviatura de «word count». Y no sólo sirve para contar palabras, también cuenta los caracteres y las líneas de entrada (ficheros y entrada estándar). Desde el punto de vista de `wc`, una «palabra» es una secuencia de una o más letras. Sin ninguna opción, se muestran los tres valores correspondientes a líneas, palabras y caracteres.

	$ wc frog.txt

y con alguna de las opciones activadas se pueden limitar los resultados:

	$ ls | wc -l

### Gestión de datos

#### sort

El comando `sort` permite ordenar las líneas de ficheros de texto según un criterio. Por defecto, es por orden alfabético ascendente (A a la Z). Se pueden ordenar no sólo teniendo en cuenta toda la línea, sino sólo una determinada columna o campo de una tabla. Los campos son numerados empezando en 1. Con la opción `-k 2`, el primer campo no se tiene en cuenta, y es el segundo campo el que se ordena. Si hay valores iguales en este campo, se sigue comparando con los siguientes campos, a no ser que se indique un campo de fin `-k 2,3`.
El caracter espacio sirve como separador de campos. Y si hay varios espacios seguidos, tan solo se toma el primero como separador, y el resto pertenecen al campo.

	$ sort -k 2,2 participantes.dat
	$ sort -r -k 2,2 participantes.dat

Con la opción `-t` podemos seleccionar un caracter arbitrario en lugar del separador espacio. Esto es una buena idea, debido a que los campos pueden contener espacios. La nueva versión `participantes0.dat` es menos legible, pero más manejable.

Ahora es más fácil ordenar, por ejemplo por número de participante:

	$ sort -t -k4 participantes0.dat

Por supuesto, la ordenación se realiza con criterios de alfabeto. Esto se puede arreglar con la opción `-n`:

	$ sort -t: -k4 -n participantes0.dat

#### uniq

El comando `uniq` hace el importante trabajo de dejar pasar sólo la primera de una secuencia de líneas iguales en la entrada (o la última, como prefiera!). Lo que se considera como igual, puede ser detallado en las opciones. Sólo admite un fichero de entrada, y si se indica otro, será donde se almacene el resultado. `uniq` trabaja mucho mejor si las líneas de entrada están ordenadas, por lo que todas las líneas iguales estarán unas detrás de otras. Si no es el caso, no está garantizado que cada línea se muestre una sola vez en la salida.

	$ cat uniq-test
	$ uniq uniq-test
	$ sort -u uniq-test

### Columnas y campos

#### cut

Mientras que puedes localizar y extraer líneas de un texto usando `grep`, el comando `cut` trabaja a través de un fichero de texto «por columna». Y lo hace de dos formas:

1. Una posibilidad es el tratamiento absoluto de las columnas. Esas columnas corresponden a caracteres simples en una línea. Para extraer tales columnas, el número de columna debe ser indicado después de la opción `-c`. Para cortar varias columnas en un sólo paso, se pueden especificar como una lista separada por comas. Incluso se pueden indicar rangos de columnas.

	```
	$ cut -c 12,1-5 participantes.dat
	```

	En el ejemplo anterior, se extraen la primera letra del nombre propio y las cinco letras del apellido. La salida contiene las columnas en el mismo orden que en la entrada, incluso si los rangos se solapan, cada carácter de entrada se muestra una sóla vez.<br/>

	```
	$ cut -c 1-2,2-6,2-7 participantes.dat
	```

2. Y el segundo método es extraer campos relativos, los cuales son delimitados por separadores. Debemos usar la opción `-f` y el número de campo deseado. Se aplican las mismas normas que antes. El uso de estas opciones es mutuamente exclusivo. El separador por defecto es tabulador, hay que especificar el uso de uno distinto:

	```
	$ cut -d: -f 1,4 participantes0.dat
	```

#### paste

El comando `paste` une las líneas de los ficheros especificados. Se suele utilizar junto a `cut`. No es un comando de filtrado. Con un símbolo «-» lee de su entrada estándar. Su salida se envía siempre a la salida estándar.	Este comando trabaja por líneas, si se le pasan dos ficheros, la primera línea del primer fichero se une con la primera línea del segundo fichero, usando un tabulador como separador. Para indicar un separador diferente, usamos la opción `-d`.

	$ cut -d: -f4 participantes0.dat >numero.dat
	$ cut -d: -f1-3,5 participantes0.dat | paste -d: numero.dat - >p-numero.dat
	$ cat p-numero.dat

Ahora el fichero puede ser ordenado por el número de participante.

Con la opción `-s`, los ficheros son procesados en secuencia. Primero todas las líneas del primer fichero son unidas en una sola línea con el tabulador en medio, luego todas las del segundo fichero en la segunda línea, etc.

	$ cat lista1
	$ cat lista2
	$ paste -s lista*

### Algoritmo MD5

La utilidad `md5sum` está basada en el algoritmo de resumen de mensaje MD5. Fue creado con fines criptográficos, aunque en la actualidad tiene demasiadas vulnerabilidades que desaconsejan su uso. Sin embardo, puede resultar útil para comprobar la integridad de un fichero.

	$ md5sum cientouno.txt
	8d25223a82e86ca603f61160b4fab3a2  cientouno.txt

`md5sum` genera un hash de 128 bits. Cuando copiamos este fichero en otro equipo a través de la red, podemos ejecutar de nuevo el comando `md5sum` en el equipo remoto para comprobar dicho valor.

### Reforzando la seguridad en algoritmos hash

SHA es una familia de funciones hash, que al igual que MD5 pueden ser usadas para verificar la integridad de un fichero. Dependiendo de nuestra distribución tendremos un número diferente de binarios, que podemos consultar con el siguiente comando:

	$ ls -1 /usr/bin/sha???sum
	/usr/bin/sha224sum
	/usr/bin/sha256sum
	/usr/bin/sha384sum
	/usr/bin/sha512sum

	$ sha256sum cientouno.txt
	b194009a8af27a14f28e341ebd8e989694bb519f789d862448760fd86e34607e  cientouno.txt
	$ sha512sum cientouno.txt
	d3891ee7a2118989e3cda2ca93531fb57f5213f908739ed3fc6be024b96c7f45feb498109d177bd860b014e1d6e5c5850c149d324d9781fbb2e722728422f732  cientouno.txt

## Comandos vistos en el tema

Comando | Descripción
-- | --
cat | Concatena ficheros (aparte de hacer otras cosas ;-) )
cut | Extrae campos o columnas de su entrada
expand | Reemplaza tabuladores por espacios equivalentes
head | Muestra el principio de un fichero
paste | Une líneas de diferentes ficheros de entrada
reset | Resetea el juego de caracteres de la terminal
sort | Ordena su entrada por línea
tac | Muestra un fichero de fin a principio
tail | Muestra el final de un fichero
uniq | Reemplaza secuencias de líneas idénticas por una sola
wc | Cuenta los caracteres, palabras y líneas desde su entrada

## Resumen

* `wc` puede usarse para contar las líneas, palabras y caracteres de la entrada estándar.
* `sort` es un programa versátil para ordenar.
* El comando `cut`, corta rangos específicos de columnas o campos de cada línea de su entrada.
* Con `paste`, se pueden unir las líneas de los ficheros.
