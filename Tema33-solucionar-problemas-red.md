# Tema 33 - Solucionar problemas de red

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Introducción](#introducción)
- [Problemas locales](#problemas-locales)
- [Comprobando la conectividad con ping](#comprobando-la-conectividad-con-ping)
- [Comprobando las rutas usando traceroute y tracepath](#comprobando-las-rutas-usando-traceroute-y-tracepath)
- [Comprobando servicios con netstat y nmap](#comprobando-servicios-con-netstat-y-nmap)
- [Comprobando sockets](#comprobando-sockets)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Conocer las estrategias para resolver problemas de red
* Ser capaz de usar herramientas como `ping`, `traceroute`, y `netstat` para el análisis de los problemas
* Ser capaz de solucionar errores simples de la configuración de la red 

## Introducción

Los administradores aman esto: tan pronto te has sentado cómodamente en frente de tu ordenador con una taza de café caliente, leyendo las últimas novedades en *LWN.net*, aparece de repente un usuario gritando: “No tengo red !!” ¿Qué puedo hacer?. Las redes de ordenadores son un tema complejo, y no debemos sorprendernos de situaciones en las que «todo lo que puede fallar, fallará». En este tema veremos las estrategias y herramientas más importantes para encontrar y reparar los problemas de red.

## Problemas locales

Lo primero que debemos comprobar es que la tarjeta de red esté presente y reconocida, aunque tampoco debemos olvidar de echar un vistazo a la parte posterior del equipo y comprobar que no se ha soltado el cable de red por la acción de «el robot de limpieza». Ejecutamos el comando `ipconfig -a`, así nos muestra todas las tarjetas de red dentro de nuestro equipo aunque no estén configuradas. Al menos nos deberían aparecer `lo` y `ethx`. Si no aparecen, quizás hayamos encontrado el primer problema: algo ocurre con el driver, o que la tarjeta no se reconoce.

> Si, en lugar de `eth0`, sólo ves algo como `eth1`, es posible que la tarjeta de red haya sido reemplazada, y *udev* le haya asignado un nuevo nombre de interfaz, teniendo en cuenta su dirección MAC. La solución es eliminar la línea que hace referencia al viejo dispositivo de:<br>
	# /etc/udev/rules/70-persistent-net.rules
y corregir el nombre de la interfaz en la línea del nuevo dispositivo, y luego reiniciar *udev*.

> Si la salida de `ifconfig` no muestra nada que se parezca a nuestra interfaz de red, debemos comprobar con `lsmod` si el driver está cargado. Si no sabemos qué driver buscar, podemos observar la salida de `lspci -k`, y localizar nuestra tarjeta de red. En nuestro caso se está usando el driver *pcnet32*.

## Comprobando la conectividad con ping

Si el comando `ifconfig` nos muestra una dirección IP, una máscara de red, y la dirección de difusión, entonces debemos comprobar la conectividad. El comando `ping`, toma una dirección IP (o un nombre DNS) y le intenta enviar un datagrama ICMP *echo request* al host en cuestión. Ese host nos debería responder con un datagrama ICMP *echo reply*. Primero, lo haremos con nuestro equipo:

	$ ping 127.0.0.1
	PING 127.0.0.1 (127.0.0.1) 56(84) bytes of data.
	64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=0.065 ms
	...

La salida nos indica que el «otro host» puede ser alcanzado. El comando `ping` envía paquetes hasta que pulsemos CTRL+C, por lo que podemos usar la opción `-c` para indicar un número determinado de paquetes a transmitir.

> ¿Qué significa «56(84) bytes of data»? Sencillo: una cabecera de un datagrama IP sin opciones tiene 20 bytes de longitud. A esto le añadimos la cabecera de ICMP *echo request* de 8 bytes. Esto explica la diferencia entre 56 y 84. El número 56 resulta de que el comando `ping` se asegura de que al menos se envían 64 bytes en el campo de datos, dentro de cada datagrama IP. Incluso, si existe espacio suficiente para insertar una estructura *timeval de C*, la usará para medir el tiempo que tarda en retornar el paquete.

El siguiente paso sería hacer un *ping* a la dirección IP de nuestra tarjeta de red. La salida debería ser similar a la anterior.

> Si hemos llegado hasta aquí sin errores es que la parte de red de nuestro equipo funciona correctamente. Los orígenes de los errores, pueden estar entonces en la propia red, o en alguna parte de la pila de protocolos de nuestro equipo.

El siguiente *ping* a ejecutar debemos hacerlo a nuestra pasarela por defecto, o a otro nodo en nuestra red local. Si no funciona, podría estar mal la máscara de red, posiblemente en el otro host. Otras posibilidades incluyen problemas de hardware, como pliegues en el cable o un conector roto. Ahora podríamos realizar *ping* a equipos fuera de nuestra red local. Si obtenemos respuesta, es buena señal, en caso contrario, podríamos estar ante un problema de enrutado, o ante la presencia de algún cortafuegos que impide el paso de señales ICMP. El comando `ping` soporta una gran cantidad de opciones que aumentan las posibilidades de chequeo. Las más importantes para las labores de test, son `-f` (flood ping) para comprobar problemas de red intermitentes, y `-s` para indicar el tamaño de los datagramas.

En el caso del comando `ping6`, el proceso es más complejo ya que es necesario indicar la interfaz por la que enviar los paquetes:

	$ ping -c 4 fe80::a00:27ff:fe8d:c04d%eth0

> Los sistemas actuales suelen protegerse de ataques DOS desactivando el soporte para los paquetes ICMP, por lo que no debemos extrañarnos si un equipo remoto no responde.

## Comprobando las rutas usando traceroute y tracepath

Si no damos llegado a un host fuera de nuestra red local usando `ping`, puede ser debido a un problema de enrutado. Los programas `traceroute` y `tracepath` nos pueden ayudar.

> El típico caso es que podamos alcanzar todos los hosts de la red local, pero ninguno fuera de ella. Puede ser debido a que la ruta por defecto no apunta a nuestro router. Tendremos que verificar que *route* apunta a la ruta por defecto. Si un *ping* a la pasarela por defecto funciona, pero un *ping* a un equipo detrás de él no funciona, entonces algo le pasa a la pasarela. Debemos comprobar si otros equipos pueden alcanzar algun host detrás de la pasarela, y que nuestro host sea alcanzable desde la pasarela. Sin olvidarnos que la pasarela puede tener un filtro de paquetes ICMP.

`traceroute` es básicamente una extensión de `ping`. No sólo comprueba que un nodo esté vivo, sino que muestra la ruta del datagrama a través de la red. Sigue la pista de los routers por los que pasa el datagrama y la calidad de la conexión de dichos routers. `traceroute` envía paquetes ICMP con valores bajos en el campo TTL de tal modo que tendrán una vida corta en su trayecto. El primer router en el camino decrementa el valor de TTL en una unidad, por lo que es desechado, y se le envía una señal al emisor (ICMP time exceeded) con la dirección IP del router.

Los segundos datagramas son desechados por el segundo router, y así continúa. De esta forma podemos trazar la ruta seguida por los datagramas hasta el destino. Al llegar al destino, el nodo no envía *TIME EXCEEDED* sino *PORT UNREACHABLE*.

	$ traceroute ftp.rediris.es
	$ traceroute www.melisa.gal

Cada línea de la salida corresponde a un grupo de tres datagramas. Se muestra el nodo que ha enviado el mensaje *TIME EXCEEDED*, así como el tiempo de transmisión de los tres datagramas. Un asterisco indica que no se ha recibido respuesta para el datagrama, normalmente después de cinco segundos.

Puede ocurrir que algunos cortafuegos bloqueen los datagramas con puertos de destino «poco probables».

`tracepath` realiza básicamente la misma función que `traceroute`, pero usa paquetes UDP en lugar de ICMP. De esta forma se consigue un mejor comportamiento al intentar traspasar los routers, por lo que suele ser la opción usada después de un fallo en `traceroute`.
	
	$ tracepath www.kernel.org

Otro beneficio del uso de paquetes UDP es que no es necesario tener privilegios de *root* para enviar paquetes UDP, por lo que cualquier usuario puede usar el comando `tracepath`.

## Comprobando servicios con netstat y nmap

El comando `netstat` nos puede proporcionar una gran cantidad de información, y por defecto muestra todas las conexiones abiertas en el sistema:

```
# netstat
Active Internet connections (w/o servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State      
tcp        0      0 localhost.localdo:58386 packet04.centos.or:http TIME_WAIT  
tcp        0      0 localhost.localdoma:ssh _gateway:38216          ESTABLISHED
tcp        0      0 localhost.localdo:42862 ftp.cica.es:http        TIME_WAIT  
udp        0      0 localhost.locald:bootpc _gateway:bootps         ESTABLISHED
Active UNIX domain sockets (w/o servers)
Proto RefCnt Flags       Type       State         I-Node   Path
unix  3      [ ]         DGRAM                    11288    /run/systemd/notify
unix  2      [ ]         DGRAM                    11290    /run/systemd/cgroups-agent
. . . 
```

Se puede limitar la gran cantidad de información que se muestra haciendo uso de las opciones `-t` para conexiones TCP y `-u` para las UDP.

```
$ netstat -t
Active Internet connections (w/o servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State      
tcp        0      0 localhost.localdoma:ssh _gateway:38216          ESTABLISHED
```

Con la opción `-l` obtenemos una lista de las aplicaciones que están a la espera de conexiones en sus respectivos puertos:

```
$ netstat -l
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State      
tcp        0      0 0.0.0.0:sunrpc          0.0.0.0:*               LISTEN     
tcp        0      0 0.0.0.0:ssh             0.0.0.0:*               LISTEN     
tcp6       0      0 [::]:sunrpc             [::]:*                  LISTEN     
tcp6       0      0 [::]:ssh                [::]:*                  LISTEN     
udp        0      0 localhost:323           0.0.0.0:*                     
. . .
```

Otra funcionalidad de interés es la opción `-s` que muestra estadísticas para los diferentes tipos de paquetes que se han usado a través de la red.

```
$ netstat -s
Ip:
    Forwarding: 2
    5834 total packets received
    1 with invalid addresses
    0 forwarded
    0 incoming packets discarded
    5833 incoming packets delivered
    5477 requests sent out
    139 dropped because of missing route
Icmp:
    1 ICMP messages received
    0 input ICMP message failed
    ICMP input histogram:
        destination unreachable: 1
    0 ICMP messages sent
    0 ICMP messages failed
    ICMP output histogram:
IcmpMsg:
        InType3: 1
Tcp:
    36 active connection openings
    5 passive connection openings
    0 failed connection attempts
    1 connection resets received
    1 connections established
    5648 segments received
    5266 segments sent out
    3 segments retransmitted
    0 bad segments received
    0 resets sent
Udp:
    184 packets received
    0 packets to unknown port received
    0 packet receive errors
    208 packets sent
    0 receive buffer errors
    0 send buffer errors
UdpLite:
TcpExt:
    35 TCP sockets finished time wait in fast timer
    19 delayed acks sent
    4787 packet headers predicted
    46 acknowledgments not containing data payload received
    515 predicted acknowledgments
    3 congestion windows recovered without slow start after partial ack
    TCPTimeouts: 3
    TCPRcvCoalesce: 17
    TCPAutoCorking: 11
    TCPSynRetrans: 3
    TCPOrigDataSent: 556
    TCPDelivered: 592
IpExt:
    InOctets: 9666859
    OutOctets: 290085
    InNoECTPkts: 12265
MPTcpExt:
```

Si deseamos ejecutar un servicio, pero los hosts cliente reciben un mensaje de que la conexión fue rechazada:

	Unable to connect to remote host: Connection refused

podemos comprobar que el servicio está a la escucha de conexiones, por ejemplo con el programa `netstat`:

	$ netstat -tul
	Active Internet connections (only servers)
	Proto Recv-Q Send-Q Local Address           Foreign Address         State      
	tcp        0      0 *:40617                 *:*                     LISTEN     
	tcp        0      0 *:sunrpc                *:*                     LISTEN     
	tcp        0      0 localhost:ipp           *:*                     LISTEN 

La opción `-l` muestra sólo los programas que se encuentran escuchando. Con `-t` y `-u` sólo mostramos los servicios basados en TCP y UDP.

En la salida tenemos las siguientes columnas:

* **Proto** El protocolo (tcp, udp, raw...) usado por el socket.
* **Recv-Q** El número de bytes de datos que han sido recibidos pero que no han sido gestionados por la aplicación.
* **Send-Q** El número de bytes de salida que no han sido aceptados con ACK por el host remoto.
* **Local Address** Dirección local y número de puerto del socket. Un asterisco indica que se está escuchando en todas las direcciones posibles, es decir, tanto en 127.0.0.1 como en las direcciones IP de la tarjeta de red.
* **Foreign Address** La dirección y el número de puerto en el host remoto.
* **State** El estado del socket. Los sockets *raw* y *udp* no suelen tener estado. Se usa para los sockets TCP.

Si nuestro servicio no aparece en la salida de netstat `-tul`, indica que el programa no se está ejecutando. Si el servicio aparece en la lista, una posibilidad es que los clientes sean rechazados por un cortafuegos antes de alcanzarlo. Por otro lado, también es posible que el puerto en cuestión esté bloqueado por otro programa que por cualquier razón no funciona correctamente. En este caso usaremos `netstat -tulp` para mostrar el ID de proceso del programa que está sirviendo en el puerto.

> Si quitamos la opción `-l`, obtenemos una lista de las conexiones activas, es decir, aquellas en las que nuestro equipo actúa como servidor, o como cliente.

`netstat` asume que tienes acceso al menos a una shell, si no tienes privilegios de *root*, en el ordenador donde se ejecuta el programa. Pero, si queremos realizar un escaneo de puertos desde el exterior, debemos usar el programa `nmap` en busca de puertos TCP y UDP abiertos, filtrados e inutilizados en un equipo de la red. En el caso más simple, introducimos `nmap` junto con la dirección IP o nombre del ordenador a ser examinado (ponte cómodo)

	# nmap ftp.cixug.es

Si se puede alcanzar un servicio, se indica como abierto. Si devuelven un mensaje de error, se etiqueta como cerrado, mientras que los puertos que no «reaccionan», simplemente porque los paquetes entrantes se reenvían o se desechan por ser un cortafuegos, y no hay ningún mensaje de error, se designan como *filtrados*.

## Comprobando sockets

Con el comando `netstat` se obtiene gran cantidad de información, pero puede resultar complejo el determinar que aplicaciones están a la escucha y en qué puerto. El comando `ss` nos puede ayudar.

Con este comando podemos conocer la relación que existe entre los procesos del sistema y los *sockets* de red que están activos.

```
$ ss -anpt
State                Recv-Q               Send-Q                             Local Address:Port                              Peer Address:Port                
LISTEN               0                    128                                      0.0.0.0:111                                    0.0.0.0:*                   
LISTEN               0                    128                                      0.0.0.0:22                                     0.0.0.0:*                   
ESTAB                0                    0                                      10.0.2.15:22                                    10.0.2.2:38216               
LISTEN               0                    128                                         [::]:111                                       [::]:*                   
LISTEN               0                    128                                         [::]:22                                        [::]:*                   ```

## La utilidad netcat

La herramienta `netcat` puede leer y escribir en cualquier puerto de red, y se convierte en una auténtica *navaja suiza* para un entorno de redes. Entre sus usos se incluye la posibilidad de establecer una comunicación cliente/servidor entre dos equipos de nuestra red.

Por ejemplo, para aceptar conexiones entrante en el puerto TCP 2000, ejecutamos el siguiente comando:

	$ nc -l 2000

Luego, desde el equipo remoto nos conectamos al servidor:

	$ nc 192.168.1.77 2000

Una vez establecida la conexión cualquier entrada del cliente se visualizará en el lado del servidor.

## Comprobar DNS con host y dig

Si la conexión a un host a través de su nombre no se puede realizar, mientras que sí lo hacemos usando su dirección IP, el DNS puede tener algún problema. A la inversa, nuestro equipo puede tardar mucho tiempo en conectar, porque el equipo remoto intenta localizar un nombre para nuestra dirección IP y se encuentra con algún problema. Para comprobar DNS podemos usar los programas `host` y `dig`. `host` es un programa muy simple, que acepta un nombre DNS y devuelve la dirección IP que se obtiene del mismo:

	$ host www.abeancos.gal
	www.abeancos.gal is an alias for pataca.comunidadeozulo.org.
	pataca.comunidadeozulo.org has address 159.89.22.90

Y también funciona en sentido contrario:

	$ host 159.89.22.90
	90.22.89.159.in-addr.arpa domain name pointer pataca.comunidadeozulo.org.

Podemos comparar la salida de varios servidores DNS especificando las direcciones IP, como parte de la consulta:

	$ host www.abeancos.gal 9.9.9.9
	[...]

de esta forma podemos comprobar si un servidor DNS nos devuelve un valor correcto.

El programa `dig` realiza la misma función que `host`, pero de forma más detallada:

	$ dig www.abeancos.gal

para resolver direcciones IP a nombres, se debe indicar la opción `-x`:

	$ dig -x 159.89.22.90

para realizar la consulta a un servidor DNS concreto, precedemos su dirección con una @:

	$ dig www.abeancos.gal @9.9.9.9

o bien, un tipo determinado de registro:

	$ dig abeancos.gal MX

## Otras herramientas para realizar diagnósticos

* telnet: El comando `telnet` es usado para iniciar sesión en un host remoto usando el protocolo *telnet*, o bien de forma general, contactar un puerto TCP arbitrario. Está obsoleto por sus fallos de seguridad, pero nos puede servir para comprobar el estado de un socket.

	$ telnet microsoft.com 80
	Trying 207.46.197.32...
	Connected to microsoft.com.
	Escape character is '^]'.
	GET / HTTP/1.0
	......
	$ telnet microsoft.com 22

* tcpdump: Este programa es un sniffer que analiza los paquetes que *viajan* a través de una interfaz de red. La tarjeta de red es activada en modo *promiscuo*, de modo que lee todos los paquetes, no sólo aquellos que van dirigidos a dicha interfaz. 

Un ejemplo:

	# tcpdump -ni eth1

*abrir el navegador web y comprobar la salida del comando `tcpdump`*

* wireshark también es un sniffer, sin embargo, incorpora más prestaciones y dispone de una interfaz gráfica.

## Comandos vistos en el tema

| Comando | Descripción |
| --- | --- |
| host | Busca información en los DNS |
| nmap | Escáner de puertos |
| ping | Comprueba la conectividad de la red usando ICMP |
| tcpdump | Esnifer de red, lee y analiza el tráfico de la red |
| telnet | Abre conexiones en servicios TCP |
| tracepath | Traza la ruta a un host |
| traceroute | Analiza el enrutado TCP/IP a un host |
