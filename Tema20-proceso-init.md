# Tema 20 - SysV-Init y el proceso init

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [El proceso init](#el-proceso-init)
- [El Boot Script](#el-boot-script)
- [Niveles de ejecución](#niveles-de-ejecución)
- [Init scripts](#init-scripts)
- [Modo monousuario](#modo-monousuario)
- [Upstart](#upstart)
- [Apagando el sistema](#apagando-el-sistema)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)
- [Resumen](#resumen)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Comprender la infraestructura SysV-Init y systemd
* Conocer la sintaxis y estructura de /etc/inittab
* Comprender los runlevels y los scripts de inicio

## El proceso init

Una vez que el kernel ha inicializado el hardware, lanza el primer proceso. Este proceso es llamado «proceso init», y siempre tiene un PID 1. Su trabajo es finalizar la inicialización del sistema y supervisar las operaciones siguientes del sistema. Existen dos demonios de inicio que debemos conocer:

* **SysVinit** Aunque ya se encuentra en desuso en la mayoría de las distribuciones actuales, aún se puede encontrar en algunos servidores.
* **systemd** Desde el año 2010 su popularidad ha crecido de forma importante entre las principales distribuciones. Su gran diferencia es que inicia los servicios en paralelo.

> El proceso «init» tiene una característica especial, que es el único proceso que no puede ser abortado usando `kill -9`.

Para comprobar el sistema utilizado por nuestra distribución, en primer lugar debemos localizar el programa `init`:
	
	$ which init
	/sbin/init

Una vez localizado, comprobamos con `readlink -f` si está apuntando a otro programa diferente.

	$ readlink -f /sbin/init
	/lib/systemd/systemd

Una forma de verificar que estamos usando *systemd* es consultar el nombre del proceso con PID 1.

```
$ ps -p 1
  PID TTY          TIME CMD
    1 ?        00:01:29 systemd
```

Para más información, consultar el [Tema 41](Tema41-systemd.md) de este temario.

«init SysV» toma el nombre de que sigue el ejemplo del *Unix System V*. Su característica principal son los «niveles de ejecución» (runlevels), que describen el estado del sistema y los servicios que se ofrecen. El proceso «init» también se encarga de que los usuarios puedan iniciar sesión en consolas virtuales, terminales conectadas directamente, etc. Toda la configuración está almacenda en el fichero `/etc/inittab`.

Un método para comprobar las distribuciones que siguen haciendo uso de SysVinit es consultar el sitio [distrowatch](http://distrowatch.com/search.php), y realizar una búsqueda con el filtro adecuado.

```
# Standard runlevel
id:5:initdefault

# First script to be executed
si::bootwait:/etc/init.d/boot

# runlevels
l0:0:wait:/etc/init.d/rc 0
l1:1:wait:/etc/init.d/rc 1
l2:2:wait:/etc/init.d/rc 2
#l4:4:wait:/etc/init.d/rc 4
l5:5:wait:/etc/init.d/rc 5
l6:6:wait:/etc/init.d/rc 6

ls:S:wait:/etc/init.d/rc S
~~:S:respawn:/sbin/sulogin

# Ctrl-Alt-Del
ca::ctrlaltdel:/sbin/shutdown -r -t 4 now

# Terminals
1:2345:respawn:/sbin/mingetty --noclear tty1
2:2345:respawn:/sbin/mingetty tty2
3:2345:respawn:/sbin/mingetty tty3

# Serial terminal
# S0:12345:respawn:/sbin/agetty -L 9600 ttyS0 vt102

# Modem
# mo:235:respawn:/usr/sbin/mgetty -s 38400 modem
```

El fichero `/etc/inittab` está formada por líneas que se dividen en cuatro columnas, separadas por «dos puntos» (:).

* **label** su propósito es identificar la línea. Podemos colocar una combinación cualquiera de cuatro caracteres.
* **runlevels** indica que la línea se aplica a dicho runlevel.
* **acción** especifica cómo manejar la línea. Las opciones más importantes son:
  * **respawn** el proceso indicado en esta línea será iniciado de nuevo, una vez que ha finalizado.
  * **wait** el proceso indicado es iniciado cuando el sistema cambia al nivel de ejecución en cuestión, y el proceso «init» espera que finalice.
  * **bootwait** el proceso indicado será ejecutado durante el arranque del sistema. «init» espera a que finalice. El campo de runlevel se ignora.
  * **initdefault** el campo runlevel de esta línea indica el nivel de ejecución que el	sistema intentará alcanzar después de reiniciar.
  * **ctrlaltdel** especifica lo que debe hacer el sistema si se le envía una señal *SIGINT* al proceso «init». Normalmente, ejecuta algún tipo de shutdown.
  * **comando** el comando que será ejecutado.

> Si hacemos cambios en `/etc/inittab`, no se reflejan automáticamente, debemos ejecutar `telinit q`.

## El Boot Script

En el SysV-Init, el proceso «init» inicia un script de shell (en Debian, `/etc/init.d/rcS`). El nombre exacto lo podemos encontrar en `/etc/inittab`. El «boot script» ejecuta tareas tales como el chequeo y la correción de los sistemas de ficheros listados en `/etc/fstab`, inicializa el reloj de Linux, y otros requisitos importantes para una operación estable del sistema. A continuación, se cargan los módulos de kernel que se necesiten, se montan los sistemas de ficheros, etc.

## Niveles de ejecución

Después de ejecutar el «boot script», el proceso «init» intenta situar el sistema en uno de los niveles de ejecución. Exactamente, en el indicado por el fichero `/etc/inittab`, o por el prompt del arranque del sistema y pasado a «init» por el kernel. Los niveles de ejecución han sido estandarizados como sigue:

| Nivel | Descripción |
| --- | --- |
| 1 | modo monousuario sin red |
| 2 | modo multiusuario sin servicios de red |
| 3 | modo multiusuario con servicios de red |
| 4 | sin uso |
| 5 | igual que el 3, pero con login GUI |
| 6 | reinicio |
| 0 | apagado |

El sistema se ejecuta a través del nivel de ejecución S (o 's') durante el arranque, antes de cambiar a un nivel 2 hasta 5. Cuando se inicia el sistema, los niveles de ejecución preferidos son del 3 al 5 (el nivel de ejecución 5 es el típico para estaciones de trabajo, y el 3 para servidores). Durante el uso normal del sistema, podemos modificar el nivel de ejecución usando el comando `telinit` (sólo puede ser ejecutado por root).

	# telinit 5

Todos los servicios que no son necesarios en dicho nivel de ejecución son parados, mientras que se pueden iniciar los presentes en ese nivel de ejecución.

El comando `runlevel` muestra el nivel de ejecución actual y anterior.

	# runlevel
	N 2

En este caso, el sistema se encuentra en el runlevel 2, y el anterior indica que el sistema acaba de iniciarse. Es posible crear nuevos niveles de ejecución «bajo demanda» con entradas en `/etc/inittab` del tipo:

	xy:AB:ondemand...

hacen que podamos usar:

	# telinit A

al cambiar a estos niveles de ejecución, el nivel actual no cambia.

## Init scripts

Los servicios disponibles en los diferentes niveles de ejecución son iniciados y parados usando los scripts del directorio `/etc/init.d`. Todos esos scripts se llaman «init scripts». Por supuesto, no es preciso ejecutar dichos scripts de forma manual, sino que el sistema los inicia cuando cambiamos de nivel. Por ejemplo, para cada nivel «x», hay un directorio `/etc/rcx.d`. Los servicios para un nivel son activados mediante la llamada a los enlaces simbólicos en ese directorio que comienzan por la letras «S», en orden alfabético con el parámetro *start*. Debido a que los enlaces contienen dos números después de la «S» se puede indicar el orden en que serán ejecutados.

De forma similar, para desactivar los servicios en un nivel de ejecución, todos los enlaces simbólicos con la letra «K» son ejecutados en orden alfabético con el parámetro *stop*. Si un servicio que está en ejecución, y que también lo debe estar en el siguiente nivel, puede provocar un reinicio no esperado. Entonces, antes de ejecutar un enlace «S», el script `rc` comprueba si existe un enlace «S» para el mismo serivicio en el nivel anterior. En Debian, cuando se cambia de nivel de ejecución, se ejecutan todos los enlaces que comiencen por «K», pasándole el parámetro *stop*, y a los enlaces que comienzan por «S» se les pasa un parámetro *start*. 

> Para configurar los servicios en un nivel de ejecución, o para crear uno nuevo, se pueden «manipular» los enlaces simbólicos directamente. Sin embargo, la mayoría de distribuciones no lo recomiendan.

Red Hat usa un programa llamado `chkconfig` para configurar los niveles de ejecución. Por ejemplo, `chkconfig quota 35`, añade el servicio `quota` en los niveles 3 y 5. SUSE utiliza un programa llamado `insserv` para colocar cada servicio en su nivel de ejecución. Usa la información almacenada en los «init scripts» para calcular el orden de inicio y parada de los servicios en cada nivel de ejecución. En Debian tampoco se deben crear a mano los enlaces, debemos utilizar el comando `update-rc.d`. Por ejemplo, con el comando:

	# update-rc.d mypackage defaults

el script `/etc/init.d/mypackage` se iniciará en los niveles 2,3,4 y 5, y se parará en los niveles 0,1 y 6. Si no se indica nada, `update-rc.d` usa la secuencia 20 para calcular la posición del servicio.

## Modo monousuario

En el modo monousuario (runlevel S), sólo el administrador puede trabajar en la consola del sistema. No es posible cambiar a otras consolas virtuales. Este modo es el utilizado para los trabajos de administración, especialmente si es preciso reparar el sistema de ficheros, o para configurar el sistema de cuotas. Para salir de este modo, Debian GNU/Linux recomienda un reinicio antes del cambio de nivel (`telinit 2`). Esto se debe a que el cambio a modo monousuario para todos los procesos no necesarios, incluídos algunos procesos básicos en segundo plano que fueron activados en el nivel «S» después del arranque del sistema, por ello, resulta imprudente pasar del nivel «S» a uno multiusuario.

## Upstart

Mientras que el sistema tradicional System-V incorpora un esquema «síncrono», ya que los cambios son producidos por acciones del usuario, y los pasos ejecutados durante un cambio de estado, se ejecutan secuencialmente, *Upstart* usa una filosofía «basada en eventos». Esto significa que el sistema va reaccionar ante eventos externos (como la conexión de un dispositivo USB). Upstart guarda compatibilidad con System-V init, por lo menos en lo que se refiere a la reutilización de los scripts sin hacer cambios en ellos. Y también pretende acelerar el proceso de arranque al ser capaz de iniciar servicios en paralelo.

La configuración de *Upstart* está basada en la idea de «jobs», que heredan el papel de los «scripts init». *Upstart* distingue entre «tareas», que son trabajos que tienen una duración limitada y se paran por si mismos, y «servicios», que son trabajos que se ejecutan por tiempo ilimitado en segundo plano. Los trabajos son configurados a través de ficheros situados en el directorio `/etc/init`. Los nombres corresponden al trabajo con un sufijo `.conf`.

```
	# rsyslog - system logging daemon
	# rsyslog is an enhanced multi-threaded replacement for the traditional
	# syslog daemon, logging messages from applications

	description     "system logging daemon"

	start on filesystem
	stop on runlevel [06]

	expect fork
	respawn
```

Uno de los objetivos de *Upstart* es evitar la gran cantidad de código redundante que está presente en los «scripts init». Por ejemplo, en el fichero de configuración de *rsyslog*, podemos observar que se indica en modo de inicio del trabajo (exec rsyslogd -c4). Además, el servicio debe reiniciarse en caso de parada (respawn), y como se debe gestionar el proceso (expect fork), de tal modo que se debe ejecutar en segundo plano como un proceso hijo, y luego salir. Mientras que el modelo clásico System-V, el administrador debe asignar un orden «global» para el inicio en un nivel de ejecución dado, con *Upstart* los trabajos deciden por si mismos (localmente) donde ubicarse dentro de la red de dependencias. Las líneas `start on...` y `stop on...` indican los eventos que llevan al arranque o parada del trabajo.

Durante el arranque del sistema, *Upstart* crea el evento «startup» tan pronto como pueda, provocando la ejecución de otros trabajos hasta completar todas las dependencias. En *Upstart*, el comando `initctl` se usa para interactuar con el proceso «init»:

```
	# initctl list
	[...]
	# initctl stop rsyslog
	rsyslog stop/waiting
	# initctl status rsyslog
	rsyslog stop/waiting
	# initctl start rsyslog
	rsyslog start/running, process 2846
	# initctl restart rsyslog
```

## Apagando el sistema

Un sistema Linux no tiene porque apagarse, a no ser que haya habido cambios en el kernel, o hayamos añadido o reemplazado hardware defectuoso en el ordenador. La primera opción es evitable usando la infraestructura «kexec». Y en la segunda, si disponemos del hardware adecuado, podemos reemplazar componentes con el sistema en funcionamiento.

Hay varias formas de apagar o reiniciar el sistema:
* Pulsando el botón de apagado. Sólo usar en caso de ver fuego !
* Usar el comando `shutdown`. Es el método recomendado para apagar o reiniciar el sistema..
* El comando `telinit 0` puede usarse para cambiar al nivel 0. Equivale a un `shutdown`.
* Usar el comando `halt`. Es una orden directa al kernel para parar el sistema, pero muchas distribuciones hacen que `halt` llame a `shutdown` si el sistema no se encuentra ya en los niveles 0 o 6.

> Todos estos comandos están restringidos al administrador.

Normalmente, usaremos la segunda opción, el comando `shutdown`. Se asegura que todos los usuarios que tienen sesión abierta son advertidos de la parada del sistema, y se impiden nuevos inicios de sesión:

	# shutdown -h +10

este comando apagará el sistema en 10 minutos. Con la opción `-r` se reiniciará. Y sin ninguna opción, entrará en el modo monousuario después de transcurrido el tiempo indicado. También podemos indicar un momento absoluto:

	# shutdown -h 12:00

La palabra *now* equivale a +0, y debemos usarlo en caso de que no haya nadie usando el sistema.

También podemos indicar un mensaje, que se le mostrará a todos los usuarios conectados:

	# shutdown -h 12:00 ' El sistema se apagará por tareas de mantenimiento '

Si hemos ejecutado el comando `shutdown`, y luego queremos cancelarlo, podemos usar la opción `-c`:

	# shutdown -c “Se pospone el apagado”

El método utilizado por el comando `shutdown` para enviar el mensaje a todos los usuarios, es mediante la ejecución del comando `wall` (write to all):

	$ wall “Pinchos en la cafetería a las 13:30!”

Si enviamos el mensaje de difusión desde un usuario normal, será recibido por todos los usuarios que no han bloqueado la recepción de dichos mensajes con el comando `mesg n`. Para que llegue a todos los usuarios del sistema, debe ser enviado desde la cuenta de *root*. Si somos *root*, se le puede pasar como parámetro el nombre de un fichero, que será leido y su contenido será el cuerpo del mensaje:

	# echo “Pasteles en la cafetería a las 13:30!” > aviso.txt
	# wall aviso.txt

Como *root*, podemos eliminar el «banner» “Broadcast message...” con la opción «-n».

## Comandos vistos en el tema

| Comando | Descripción |
| --- | --- |
| halt | Para el sistema | 
| reboot | Reinicia el sistema |
| runlevel | Muestra los niveles actual y anterior |
| shutdown | Apaga el sistema, o lo reinicia, con un cierto retardo y con mensajes de advertencia a los usuarios conectados |

## Resumen

* Después de arrancar, el kernel inicializa el sistema y ejecuta el programa `/sbin/init` como el primer proceso.
* El proceso «init» toma el control del sistema, y activa los servicios en segundo plano, gestiona las terminales, consolas virtuales, etc.
* El sistema distingue entre diferentes niveles de ejecución.
* El modo monousuario está disponible para realizar tareas administrativas de larga duración.
* El comando `shutdown` es la opción recomendada para apagar o reiniciar el sistema.
