# Tema 29 - Sistema de registro (logging)

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Historia del registro de eventos en Linux](#historia-del-registro-de-eventos-en-linux)
- [El problema](#el-problema)
- [Registro con rsyslogd](#registro-con-rsyslogd)
    - [syslogd](#syslogd)
    - [Configurando rsyslogd](#configurando-rsyslogd)
- [Envío de mensajes a un servidor](#envío-de-mensajes-a-un-servidor)
- [El programa logrotate](#el-programa-logrotate)
- [Kernel logging](#kernel-logging)
- [Registro con systemd-journald](#registro-con-systemd-journald)
    - [Configurando systemd-journald](#configurando-systemd-journald)
- [Búsqueda en Journal](#búsqueda-en-journal)
- [Capas de registro](#capas-de-registro)
- [Hacer persistente el journal](#hacer-persistente-el-journal)
- [Consultar entradas journal](#consultar-entradas-journal)
- [Mantenimiento del journal](#mantenimiento-del-journal)
- [Consultar diferentes ficheros journal](#consultar-diferentes-ficheros-journal)
- [Crear entradas journal](#crear-entradas-journal)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)
- [Resumen](#resumen)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Usar y configurar el demonio *syslog*.
* Ser capaz de gestionar ficheros de log usando `logrotate`.
* Comprender como el kernel de Linux maneja los mensajes de registro.

## Historia del registro de eventos en Linux

A lo largo de los años ha habido varios proyectos libres para proporcionar un servicio de registro para los sistemas Linux. Los más destacables son los siguientes:

* `sysklogd`. La aplicación *syslog* original, que incluye dos programas: el `syslogd` para monitorizar los eventos del sistema y de las aplicaciones, y `klogd` para gestionar los eventos del núcleo Linux.
* `syslogd-ng`. Añade características avanzadas, como filtrado de mensajes y la capacidad del envío de mensajes a equipos remotos.
* `rsyslog`. El proyecto se centra en la velocidad (la *r* viene de *rocket fast*), y pronto se convierten en un estándar en diferentes distribuciones Linux.
* `systemd-journald`. Como parte del ecosistema *systemd*, varias distribuciones lo incorporan como sistema de registro. En lugar de usar el protocolo *syslog*, emplea un modo diferente para informar y almacenar los eventos del sistema y de las aplicaciones.

## El problema

Los programas necesitan comunicarse con los usuarios, porque han completado una tarea, o se ha producido un error. Los programas que operan bajo una interfaz gráfica, lo pueden realizar mediante mensajes en cuadros de diálogo, pero el kernel, y los servicios que están ejecutándose en segundo plano, no están conectados a terminales de usuario, y además, en un entorno multiusuario no podemos escribir en la consola un mensaje de sistema, porque puede ser leído por el usuario equivocado, y no queda guardada una copia de dicho mensaje.

## Registro con rsyslogd

### syslogd

La solución a este problema es el demonio *syslogd*, que acepta mensajes a través del socket `/dev/log`. Normalmente, se inicia al arrancar el sistema, y cuando recibe un mensaje lo escribe en un fichero, o lo envía a través de la red a un ordenador central. El administrador decide qué hacer con los mensajes individuales. El fichero `/etc/rsyslog.conf` especifica a dónde van los mensajes.

```
kern.warn:*.err;authpriv.none		/dev/tty10
*.emerg					*
*.crit					/var/log/warn
*.*;mail.nones;news.none		-/var/log/messages
```

La primera columna determina el tipo de mensaje que será seleccionado, y la segunda columna a dónde deben ir esos mensajes. El formato de la primera columna es:
	<recurso>.<prioridad>[;<recurso>.<prioridad>]...

donde `<recurso>` podría tratarse de un servidor de correo, el propio kernel, o programas gestionando el control de acceso al sistema. Un asterisco, sirve como comodín para cualquier programa. No es fácil añadir nuevos programas, pero podemos hacer uso de *local0* a *local7*. Y la prioridad indica la importancia del mensaje.

Código | Recurso | Descripción
--- | --- | --- 
0 | kern | Mensajes del kernel
1 | user | Mensajes relacionados con los usuarios
2 | mail | Mensajes del subsistema de correo
3 | daemon | Mensajes de programas demonio (segundo plano)
4 | auth | Mensajes de seguridad o identificación
5 | syslog | Mensajes de syslogd
6 | lpr | Mensajes del subsistema de impresión
7 | news | Mensajes del subsistema de noticias Usenet
8 | uucp | Mensajes del subsistema UUCP (Unix-to-Unix Copy)
9 | cron | Mensajes del planificador *cron*
10 | authpriv | Mensajes del subsistema de seguridad
11 | ftp | Mensajes del demonio FTP
12 | ntp | Mensajes de NTP (Network Time Protocol)
13 | security | Mensajes de *audit*
14 | console | Mensajes de alerta
15 | solaris-cron | Otro tipo de mensaje de tareas de planificación
16-23 | local0-7 | Mensajes locales de libre uso

Cada evento está etiquetado con una prioridad, que define la importancia relativa del mensaje para la estabilidad del sistema. En la siguiente tabla se muestran los valores definidos en el protocolo *syslog*:

Código | Prioridad | Descripción
--- | --- | ---
0 | emerg | Mensaje final antes de un fallo general del sistema
1 | alert | Mensajes que requieren atención inmediata
2 | crit | Mensajes de error crítico, pero no requiere una atención inmediata
3 | err | Mensajes de error de todos los tipos
4 | warning | Avisos sobre problemas que no revisten gravedad
5 | notice | Situaciones *notables* durante las operaciones habituales del sistema
6 | info | Operaciones normales del sistema
7 | debug | Mensajes sobre el estado interno de un programa
- | none | Sin prioridad. Sirve para excluir todos los mensajes de un cierto recurso


> ¿Quién determina el recurso y prioridad asociado a un mensaje? La solución es simple: «El que utiliza la función syslog()», o sea el programador de la aplicación en cuestión, es el que debe asignar el recurso y la prioridad a sus mensajes. La mayoría de programas permiten que el administrador, al menos, pueda redefinir el recurso del mensaje.

Un criterio de selección como `mail.info`, incluye a todos los mensajes con prioridad *info* y superiores, del subsistema de correo. Si sólo queremos capturar los mensajes de una determinada prioridad, debemos escribir `mail=info`. El asterisco equivale a cualquier prioridad, o lo que es lo mismo: *debug*.

El signo de admiración significa negación. Por ejemplo, `mail.!info` no selecciona los mensajes de correo con una prioridad *info* o superior. Ambos operadores pueden combinarse; `mail=!info`, para no seleccionar (sólo esos) los mensajes del sistema de correo con prioridad *info*. También se permiten expresiones del tipo `mail,news.info`; esta expresión selecciona los mensajes de prioridad *info* y superior, que pertenecen a los recursos de correo y noticias.

En la columna de la derecha, los destinos de los mensajes, que pueden ser tratados de diferentes formas:

* Enviados a un fichero. El nombre del fichero puede ser indicado como ruta absoluta, y si tiene un guión delante, el fichero no se escribirá inmediatamente a disco. Pudiendo llegar a perderse mensajes ante un fallo general en el sistema. Suelen tratarse de mensajes con prioridad inferior a *notice*.
* Pueden ser enviados a una tubería con nombre (FIFO), que se debe preceder por el símbolo «|», y con ruta completa, por ejemplo, `/dev/xconsole`.
* Envío directo a usuarios. Indicando el nombre del usuario, o usuarios, enviándolo a las terminales de los usuarios, si están conectados.
* Puede ser enviado a través de la red, a otro demonio *syslogd*. Indicamos el nombre o la dirección IP precedido por una arroba. Es útil para fallos que impiden la escritura en disco, rastrear a crackers, o tratamiento posterior de mensajes en una red local. En el equipo destino el demonio debe ser ejecutado con la opción `-r`, para que acepte mensajes entrantes.
* A todos los usuarios conectados, mediante «\*».

Cuando realizamos una instalación, ya tenemos en ejecución a *syslogd*, y un fichero de configuración `/etc/syslog.conf`. Si queremos registrar más mensajes debido a cualquier tipo de error, debemos editar el fichero de configuración, y enviar una señal `SIGHUP` a *syslogd* para que vuelva a leer el fichero de configuración.

> Podemos testear el mecanismo *syslogd*, usando el programa `logger`. Una llamada del tipo:<br>
 `$ logger -p local0.err -t TEST “Hola syslogd !”`<br>
 produce un mensaje de registro del tipo:<br>
 `Aug 7 18:54:34 debian TEST: Hola syslogd !`

### Configurando rsyslogd

La aplicación *rsyslogd* utiliza todas las características del protocolo original *syslog*, incluyendo el formato de configuración y las acciones de registro. En esta sección se verá la configuración de *rsyslogd* y donde encontrar los ficheros generados.

La aplicación `rsyslogd` utiliza el fichero de configuración `/etc/rsyslogd.conf`, y en algunas distribuciones, hay ficheros `.conf` en el directorio `/etc/rsyslog.d` para la definición de eventos a los que atender y como gestionarlos.

El formato de una regla *rsyslogd* es:

	facility.priority action

La entrada `facility` usa uno de los valores descritos más arriba como recursos del sistema. La `priority` usa una de las claves que definen la gravedad del evento. Una vez definida una prioridad, *syslogd* registrará todos los eventos con una prioridad igual o superior.

La entrada `kern.crit` registrará todos los eventos del núcleo con una prioridad *critical*, *alert* o *emergency*. Para registrar solo aquellos mensajes con una prioridad específica, se debe utilizar la siguiente notación:

	kern.=crit

También se pueden utilizar caracteres comodín tanto para el campo *recurso* como el campo *prioridad*. La siguiente entrada registrará todos los eventos con una prioridad de *emergencia*:

	*.emerg

La entrada `action` define la acción a realizar por `rsyslogd` al recibir el mensaje. Existen seis acciones posibles:

* Envío a un fichero *regular*
* Derivar el mensaje hacia una aplicación
* Mostrar el mensaje en una terminal o en la consola del sistema
* Enviar el mensaje a un *host* remoto
* Enviar el mensaje a una lista de usuarios
* Enviar el mensaje a todos los usuarios conectados

A continuación, analizaremos las entradas de los ficheros de configuración de dos distribuciones de uso extendido, como son Ubuntu 18.04 y CentOS 7.

En el caso de Ubuntu, el fichero de configuración contiene las siguientes entradas:

```
auth,authpriv.*          /var/log/auth.log
*.*;auth,authpriv.none  -/var/log/syslog
kern.*                  -/var/log/kern.log
mail.*                  -/var/log/mail.log
mail.err                 /var/log/mail.err
*.emerg                  :omusrmsg:*
```

La primera entrada define la regla que gestiona todos los mensajes de tipo `auth` y `authpriv`. Es un ejemplo de como indicar varios recursos separándolos por comas. También se usa un asterisco como comodín para la prioridad, por lo que se registrarán todas las prioridades, y que serán guardadas en el fichero `/var/log/auth.log`.

La segunda entrada define la regla para gestionar todos los eventos (*.*), excepto las prioridades de `auth` y `authpriv` (es decir, ya se han registrado en la entrada anterior). El término `none` indica que no se ha de usar ninguna de las prioridades (justo lo contrario que el asterisco). El signo menos (-) que se sitúa antes del nombre del fichero hace que `rsyslogd` no ejecute la función `fsync` (liberar el buffer del kernel) después de cada operación de escritura, incrementando de este modo el rendimiento del sistema de registro. Por otro lado, en caso de fallo del sistema se podrán perder mensajes de registro que no han llegado al disco.

La última entrada declara la regla para la gestión de todos los eventos de emergencia. El comando `omusrmsg` indica que se debe enviar el mensaje a un usuario del sistema. En este caso, se hace uso del asterisco, por lo que se enviará una copia a todos los usuarios del sistema.

En el caso de la distribución CentOS 7, el fichero de configuración tendría un aspecto similar al siguiente:

```
*.info;mail.none;authpriv.none;cron.none                /var/log/messages
authpriv.*                                              /var/log/secure
mail.*                                                 -/var/log/maillog
cron.*                                                  /var/log/cron
*.emerg                                                 :omusrmsg:*
uucp,news.crit                                          /var/log/spooler
local7.*                                                /var/log/boot.log
```

Destacar que los sistemas basados en Red Hat hacen uso del fichero `/var/log/messages` para los mensajes de información y el fichero `/var/log/secure` para los de seguridad.

## Envío de mensajes a un servidor

Uno de los servidores más frecuente en un centro de datos es el *central logging host* que se encarga de recibir y almacenar los registros de diferentes clientes. A través de `rsyslog` es posible convertir a nuestro sistema en cliente de dicho sistema de registro remoto.

Para enviar todos nuestros mensajes al servidor central, debemos editar el fichero de configuración `/etc/rsyslogd.conf`. Debemos añadir una línea al final del fichero que siga el formato que ya conocemos `facility.priority action`. Como hemos decidido enviar todos nuestros mensajes de registro, usaremos la entrada `*.*` en la primera parte de la línea. Sin embargo, la segunda parte (`action`) tiene una sintaxis diferente:

	TCP|UDP[(Z#)]HOST:[PORT#]

La analizamos con detalle:

* `TCP|UDP` Podemos seleccionar entre los dos protocolos para transportar los mensajes al servidor central. Como sabemos, UDP puede provocar la pérdida de datos, por lo que debemos seleccionar TCP para asegurar el envío de mensajes de registro importantes. Para seleccionar UDP debemos usar el símbolo @, y para TCP, @@.
* `[(z#)]` Este campo es opcional. La `z` implica el uso de `zlib` para comprimir los datos antes de enviarlos por la red, y el símbolo `#` indica el nivel de compresión, que puede ir desde el 1 (la menor) hasta el 9 (la mayor).
* `HOST` En este campo se indica el nombre del servidor en formato FQDN, o una dirección IP. Si se usa una IPv6 se debe encerrar entre corchetes.
* `[PORT#]` También es opcional, y hace referencia al puerto del servidor remoto en el que escuchará por las peticiones de tráfico entrante.

Vemos un ejemplo. Supongamos que deseamos enviar nuestros registros al servidor remoto `logserver.example.com`, y que espera por tráfico TCP en el puerto 6514. Para reducir el uso de la red, decidimos aplicar una compresión con su nivel máximo. Por lo tanto, en cada cliente debemos añadir la siguiente línea al final del fichero `/etc/rsyslog.conf`:

	*.*  @@(z9)logserver.example.com:6514

## El programa logrotate

Dependiendo del número de usuarios y del número y tipo de servicios en ejecución, los ficheros de *log* pueden crecer de forma muy rápida. Para mantener el sistema limpio de «basura», en primer lugar deberíamos colocar el directorio `/var/log`, en un partición propia. Y por otro lado, existe cierto software que se encarga de hacer revisiones periódicas, y según varios criterios, como pueden ser el tamaño, trunca los ficheros de registro y los archiva.	Este proceso se llama «rotación», y uno de los programas encargados es `logrotate`. No es un «demonio», sino que se suele ejecutar una vez al día a través de *cron*.

`logrotate` se configura usando el fichero `/etc/logrotate.conf`, y los ficheros que están dentro del directorio `/etc/logrotate.d`. En el fichero `/etc/logrotate.conf` configuramos los parámetros generales, que pueden ser modificados por los ficheros del directorio `/etc/logrotate.d`. `logrotate` examina todos los ficheros indicados en los ficheros de configuración.

El fichero `logrotate.conf` suele incorporar suficientes comentarios que nos ayudarán a conocer cada una de las opciones. Por ejemplo, `dateext` obliga a `logrotate` a usar la fecha actual en el nombre de los ficheros archivados. En caso de no hacer uso de la opción anterior, los nuevos ficheros tendrán un número como extensión, siendo el numero más alto el fichero más antiguo.

	mail.log
	mail.log.1
	mail.log.2

En el caso de los ficheros del directorio `logrotate.d` un ejemplo puede ser el `/etc/logrotate.d/apt`:

	/var/log/apt/term.log {
	  rotate 6
	  monthly
	  compress
	  missingok
	  notifempty
	}

El fichero `/var/log/apt/term.log` se gestiona del siguiente modo: Se mantienen las 6 versiones anteriores, comprimidas con gzip, y ejecutando dicha tarea la primera vez que se ejecuta `logrotate` cada mes. No hace nada si el fichero de log no existe, y si está vacío no se rota.

Las directivas más comunes de `logrotate` son las siguientes:

Directiva | Descripción
--- | ---
`hourly` | El fichero de *log* se procesa cada hora. En este caso, la planificación del *cron* tendrá que ser modificada.
`daily` | El fichero de *log* se procesa cada día.
`weekly n` | El fichero de *log* se procesa cada semana, el día `n`, donde 0 es igual a Domingo, y así hasta el 6 para Sábado. El 7 es un número especial que indica una rotación cada uno de los días de la semana.
`monthly` | El fichero de *log* es procesado la primera vez que `logrotate` se ejecuta en el mes actual.
`size n` | La rotación se basa en el tamaño y no en la fecha, donde `n` indica el tamaño del fichero que dispara la rotación. El número `n` irá seguido de k, M o G).
`rotate n` | Los ficheros procesados más de `n` veces son borrados o enviados por correo, según indiquen otras directivas. En caso de que `n` sea igual a cero, los ficheros procesados se eliminan en lugar de guardarse. Digamos que no existe la rotación.
`dateformat format-string` | Modifica la configuración de `dateext` usando el formato `format-string`.
`missingok` | Si el fichero de *log* no existe, no se enviará un mensaje de error y se continúa con el siguiente fichero a procesar.
`notifempty` | Si el fichero de *log* está vacío, no se procesa dicho fichero y se continúa con el siguiente.

Para consultar el resto de directivas, podemos consultar el manual con el comando `man logrotate`.

> Algunas distribuciones mantienen un fichero con el estado de `logrotate` en el directorio `/var/lib/logrotate`, que será de gran ayuda para resolver incidencias o consultar las últimas rotaciones. En CentOS el fichero es `logrotate.status`, y en Ubuntu `status`.

## Kernel logging

El kernel no envía los mensajes a *syslogd*, sino que los coloca en un buffer interno. Existe un programa (`klogd`) que se usa para leer el fichero `/proc/kmsg`, y enviar los mensajes hacia *syslogd*. Como durante el arranque todavía no están disponibles estos programas, y no se pueden manejar los mensajes directamente, el comando `dmesg` hace posible el acceso al buffer de forma retroactiva, y así poder examinar el log del arranque del sistema.

	# dmesg > boot.msg
	# dmesg -c	(borramos el buffer)	
	# dmesg -n 1 	(sólo se envían los mensajes emerg a la	consola directamente)

En cualquier caso, todos los mensajes se escriben en `/proc/kmsg`.

## Registro con systemd-journald

El sistema *systemd* incluuye su propia herramienta de gestión del registro. Se debe destacar que se conoce como *journal utility* en lugar de *logging*. Esto implica que *systemd-jouranld* hace uso de un método totalmente diferente para el almacenamiento de los mensajes que como lo hacía el protocolo *syslog*.

### Configurando systemd-journald

El servicio `systemd-journald` lee su configuración desde el fichero de configuración `/etc/systemd/journald.conf`. Cuando se examina el contenido de este fichero podemos comprobar el modo de funcionamento de la aplicación y como se gestionan diferentes propiedades como el tamaño del fichero *journal*.

Las directivas más comunes son la siguientes:

Directiva | Descripción
--- | ---
`Storage=` | Soporta los valores `auto`, `persistent`, `volatile` o `none`. Especifica el modo de gestionar los mensajes de eventos. Por defecto, `auto`.
`Compress=` | Admite los valores `yes` o `no`. Los ficheros se comprimen si el valor es `yes` (que es el valor por defecto).
`ForwardToSyslog=` | Admite `yes` o `no`. Si está activado (yes - valor por defecto), cualquier mensaje recibido se encaminará hacia un programa *syslog* presente en el sistema, como puede ser `rsyslogd`.
`ForwardToWall=` | Admites `yes` o `no`. Si está activado (yes - valor por defecto), los mensajes recibidos son enviados al *muro* (wall) a los usuarios conectados.
`MaxFileSec=` | Se indicará un número seguido de una unidad de tiempo (`month`, `week` o `day`) que es la cantidad de tiempo antes de que se procese el fichero de *journal*. No será necesario si se indica un límite de tamaño. Para desactivar esta característica, usamos el valor `0`. El valor por defecto es `1month`.
`RuntimeKeepFree=` | Se indicará un número seguido de una unidad (como `K`, `M` o `G`) que refleja la cantidad de espacio libre que `systemd-journald` debe mantener disponible cuando hace uso de un almacenamiento volátil (por defecto, es el 15%).
`RuntimeMaxFileSize=` | Se añadirá un número seguido de una unidad (como `K`, `M` o `G`) que fija la cantidad de espacio en disco que `systemd-journald` puede llegar a consumir.
`RuntimeMaxUse=` | Se añadirá un número seguido de una unidad (como `K`, `M` o `G`) que configura la cantidad de espacio en disco que `systemd-journald` puede llegar a utilizar cuando hace uso de medios volátiles (por defecto es el 10%).
`SystemKeepFree=` | Un número seguido de una unidad (como `K`, `M` o `G`) que configura la cantidad de espacio en disco que `systemd-journald` debe mantener disponible cuando hace uso de almacenamientos persistentes (por defecto es el 15% del espacio actual).
`SystemMaxFileSize=` | Un número seguido de una unidad (como `K`, `M` o `G`) que es el espacio total que `systemd-journald` puede consumir si es persistente.
`SystemMaxUse=` | Un número seguido de una unidad (como `K`, `M` o `G`) que es el espacio en dicsco que `systemd-journald` puede consumir cuando utiliza un medio persistente (por defecto, el 10%).

El cambio de un medio volátil a uno persistente se verá más adelante. Ahora, debemos explicar con más detalle los valores que admite la directiva `Storage`:

* `auto`. Provoca que `systemd-journald` localice el directorio `/var/log/journal` y almacene en el mismo los mensajes con los eventos. Si el directorio no existe, los mensajes se guardarán en el directorio temporal `/run/log/journal`, que será eliminado cuando el sistema se apague.
* `persistent`. Hace que `systemd-journald` cree de forma automática el directorio `/var/log/journal` en caso de no existir y guardará ahí los mensajes de los eventos.
* `volatile`. Fuerza a que `systemd-journald` guarde solo los mensajes en el directorio temporal `/run/log/journal`.
* `none`. Los mensajes serán descartados.

## Búsqueda en Journal

Dependiendo de la configuración de `systemd-journald` podemos tener uno o varios ficheros de *journal* en nuestro sistema. Por ejemplo, si tenemos establecida la variable `Storage` al valor `persistent`, podemos hacer uso de la directiva `SplitMode` para dividir el fichero *journal* en diferentes ficheros activos (uno por usuario, y otro de sistema).

La ubicación del directorio va depender de si el *journal* es persistente o no. En cualquier caso, el nombre del fichero de *journal* del sistema es `system.journal`, y los de usuario tendrán el formato `user-UID.journal`.

Estos ficheros se rotan de forma automática cuando se alcanza un tamaño, o una fecha determinada, dependiendo de las directivas del fichero `journal.conf`. Después de la rotación, se renombran y se archivan. Los nombres de los ficheros archivados comienzan por `system` o `user-UID`, y continúa con el carácter `@` seguido por un conjunto de letras y números, y finaliza con la extensión `.journal`.

```
$ ls /var/log/journal/5c7d0092bbe64997ac70211cb41d5527/
system@7298c3d3116c45998328c0521b68c783-0000000002285b1c-0005b9c7a50d3392.journal     system@7298c3d3116c45998328c0521b68c783-00000000027885b8-0005b9ca69cb5a22.journal
[...]
user-1000.journal
```

> En algunos sistemas, es posible ejecutar la rotación de formal manual con el comando `journalctl --rotate`.

## Capas de registro

En caso de ser necesario, se pueden mantener activas más de una aplicación de registro. Por ejemplo, es posible mantener activo un sistema como `rsyslog` y `systemd-journald` al mismo tiempo.

* Cliente Journal. Este método permite que un programa *syslog* actúe como un cliente *journal*, que se encargue de leer entradas almacenadas en un fichero *journal*. Suele ser la opción recomendada, ya que evita la pérdida de mensajes relevantes durante el incio del sistema, justo antes de que *syslog* se active.
Aunque suele estar activado para `rsyslog`, podemos consultar el fichero `/etc/rsyslog.conf` para verificarlo. Es necesario que los módulos `imuxsock` y/o `imjournal` sean cargados a través de `module(load=<name>`.
* Envío a syslog. En este método se utiliza el fichero `/run/systemd/journal/syslog`. Los mensajes se envían al fichero (socket) donde un programa *syslog* puede leerlos.
Para habilitar este método, debemos modificar el fichero de configuración *journal*, `/etc/systemd/journald.conf`, y establecer la directiva `ForwardToSyslog` a `yes`. Para aplicar los cambios, no podemos utilizar la opción `systemctl reload`, sino que debemos ejecutar el siguiente comando con privilegios de administrador:

	systemctl restart systemd-journald

## Hacer persistente el journal

En algunas distribuciones, las entradas *journal* se almacenan en el directorio `/run/log/journal`, que se eliminará cuando el equipo se apague. Los administradores pueden establecer la directiva `Storage` en el fichero `journald.conf` al valor `persistent`.

Cuando se carga esta configuración, `systemd-journald` crea de forma automática el directorio `/var/log/journal`, mueve el fichero *journal* a su nueva ubicación, y comienza con el guardado de las nuevas entradas en él.

Sin embargo, si utilizamos el valor `auto` para la directiva `Storage`, tendremos que crear el directorio `/var/log/journal` y asignar los permisos adecuados. Por supuesto, se recomienda la opción en que `systemd-journald` hace este trabajo por nosotros (persistent).

## Consultar entradas journal

`systemd-journald` no almacena las entradas en ficheros de texto, sino que hace uso de su propio formato binario de forma similar a una base de datos. Esto hace que la visualización sea más compleja, pero por otro lado, aumenta la velocidad de búsqueda.

El comando `journalctl` será nuestra herramienta para ejecutar las consultas. El formato básico es:

	journalctl [OPTIONS...] [MATCHES...]

Las opciones gestionan como se devuelven los datos que filtra `MATCHES`. Las opciones más comunes son las siguientes:

Formato corto | Formato largo | Descripción
--- | --- | ---
-a | --all | Muestra todos los campos, incluyendo los no legibles.
-e | --pager-end | Se desplaza hasta el final del *journal* y muestra las entradas.
-k | --dmesg | Solo muestra entradas del *kernel*.
-n number | --lines=number | Muestra las `number` entradas más recientes.
-r | --reverse | Invierte el orden de las entradas al mostrarlas.
-S date | --since=date | Muestra las entradas desde la fecha `date`.
-U date | --until=date | Muestra las entradas hasta la fecha `date`.
-u unit o pattern | --unit=unit o pattern | Muestra todas las entradas para la unidad `unit` o aquellas que coinciden con el patrón `pattern`.

Para visualizar los 10 últimos mensajes en Ubuntu, ejecutamos el siguiente comando:

	$ sudo journalctl -n 10 --no-pager

Si necesitamos localizar entradas relacionadas con un evento específico, usaremos los filtros y opciones adecuadas para poder obtenerlos. Por ejemplo, con la siguiente instrucción se filtran las entradas del día de hoy para el servicio `ssh.service`:

	$ sudo journalctl --since=today _SYSTEMD_UNIT=ssh.service

## Mantenimiento del journal

A mayores de las tareas anteriores, en las que haciamos nuestro *journal* persistente y vigilamos el espacio en disco ocupado, debemos realizar otras tareas de mantenimiento de nuestros ficheros de *journal*.

Podemos comprobar el espacio en disco ocupado por el/los fichero/s de *journal* a través del comando `journalctl --disk-usage`.

	$ journalctl --disk-usage
	Archived and active journals take up 4.0G in the file system.

Además del proceso automático de limpieza del espacio en disco que se puede configurar desde el fichero de configuración `journald.conf`, `systemd-journald` también permite realizar esta tarea de forma manual gracias a las opciones *vacuum* del comando `journalctl`:

	--vacuum-size
	--vacuum-time

Tal y como se puede esperar, la opción `vacuum-size` elimina los ficheros de *journal* hasta que el espacio ocupado en disco sea igual al valor indicado. Debemos tener en cuenta que esta operación solo afecta a los ficheros *archivados*, y no a los activos. Y por otro lado, la opción `--vacuum-time`, indicará la entrada más antigua a partir de la cual se eliminarán las entradas anteriores. Las unidades se miden con los siguientes indicadores; `s`, `min`, `h`, `days`, `months`, `weeks` o `years`. Al igual que en la opción anterior solo tiene efecto sobre los ficheros *archivados*.

```
$ journalctl --disk-usage
Archived and active journals take up 4.0G in the file system.

$ sudo journalctl --vacuum-size=500M
[...]
Deleted archived journal /var/log/journal/d3f77387667547eb966609d515335ba2/system@f69b509bb8f34fe18680c6aeefe10c15-00000000015f879a-0005ba09b863a9bc.journal (8.0M).
Deleted archived journal /var/log/journal/d3f77387667547eb966609d515335ba2/user-1000@56ac44c5757c4d4181caa150af932da2-00000000015f65b9-0005ba09a6742022.journal (128.0M).
Vacuuming done, freed 3.5G of archived journals from /var/log/journal/d3f77387667547eb966609d515335ba2.

$ journalctl --disk-usage
Archived and active journals take up 432.0M in the file system.
```

## Consultar diferentes ficheros journal

Si es preciso consultar entradas que están guardadas en ficheros *archivados*, debemos tener en cuenta las siguientes opciones de `journalctl`.

Por defecto, `journalctl` realiza la búsqueda en los ficheros de los directorios `/run/log/journal` o `/var/log/journal`, pero podemos indicar otros directorios diferentes a través de las opciones `-D nombre-directorio` o `--directory nombre-directorio`.

Si el fichero que deseamos consultar tiene un nombre diferente a `system.journal` o `user-UID`, usaremos la opción `--file=<patrón>` del comando `journalctl`.

Si nuestro sistema ha sido *rescatado* de una caída y tenemos que consultar entradas de dos o más ficheros de *journal*, podemos unirlos con las opciones `-m` o `--merge` de la utilidad `journalctl`. Es importante mencionar que esta unión tan solo se realiza en la salida que podemos visualizar, y no en los propios ficheros.

## Crear entradas journal

De forma similar al comando `logger`, es posible añadir entradas desde la línea de comandos o a través de scripts con la herramienta `systemd-cat`. Para poder utilizarla, debemos enviar la salida estándar hacia dicho utilidad:

	comando | systemd-cat

Por ejemplo:

	$ echo "Prueba de systemd-cat" | systemd-cat

	$ journalctl --no-pager | grep systemd-cat
	Ene 11 19:35:04 centurion unknown[672453]: Prueba de systemd-cat

Si nuestro sistema soporta un programa *syslog* para operar como cliente a través del módulo `imuxsock`, podremos utilizar `logger` para enviar entradas tipo *journal*. Por ejemplo, como se muestra a continuación:

	$ logger "Prueba de logger"

	$ journalctl -r
	-- Logs begin at Fri 2021-01-11 15:14:54 CET, end at Fri 2021-01-11 19:37:39 CET. --
	Ene 11 19:37:39 centurion rafael[672476]: Prueba de logger
	[...]

## Comandos vistos en el tema

| Comando | Descripción |
| --- | --- |
| klogd | Acepta mensajes de log del kernel. |
| Logger | Añade entradas a los ficheros del sistema de log. |
| logrotate | Gestiona, trunca y 'rota' los ficheros de log. |
| logsurfer | Busca en los ficheros de log eventos importantes. |
| syslogd | Maneja los mensajes de log del sistema. |

## Resumen

* El demonio *syslogd* puede aceptar mensajes de registro desde diferentes componentes del sistema, o enviarlos hacia los usuarios y otros equipos.
* Los mensajes de registro pueden pertenecer a diferentes recursos, y tener varias prioridades.
* El comando `logger` envía mensajes al demonio *syslogd*.
* Normalmente, los ficheros de registro se guardan en `/var/log`.
* `logrotate` puede ser usado para gestionar los ficheros de registro.
