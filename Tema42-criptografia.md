# Tema 42 - Criptografía

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Introducción](#introducción)
- [Conceptos clave](#conceptos-clave)
- [Protegiendo los datos](#protegiendo-los-datos)
- [Firmando transmisiones](#firmando-transmisiones)
- [GPG](#gpg)
    - [Creación de claves](#creación-de-claves)
    - [Importar las claves](#importar-las-claves)
    - [Cifrando y descifrando](#cifrando-y-descifrando)
    - [Firma y comprobación de mensajes](#firma-y-comprobación-de-mensajes)
    - [Revocación de clave](#revocación-de-clave)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introducción

El propósito principal de la criptografía es la codificación de los datos para que se mantengan en secreto. En la criptografía, el *texto plano* (legible por seres humanos) se convierte en *texto cifrado* (que no pueden leer humanos ni máquinas) haciendo uso de algoritmos criptográficos. La conversión de texto plano en texo cifrado, se denomina *cifrado*, y el proceso contrario, *descifrado*.

Los algoritmos hacen uso de *claves* para las operaciones de cifrado y descifrado. Cuando se comparte la información cifrada también se deben compartir esas *claves*.

## Conceptos clave

Es imprescindible conocer el funcionamiento de las *claves* en los procesos de cifrado. Hay dos tipos; privadas y público/privadas.

* Claves privadas. *Claves simétricas* cifran los datos a través de un algoritmo y una única clave. El texto plano es cifrado/descifrado con la misma clave, que se suele proteger con una contraseña conocida como *passphrase*. La gran deseventaja es que es necesario compartir la clave privada, y por el contrario este método resulta muy rápido en el proceso de cifrado/descifrado.
* Par de claves público/privadas. *Claves asimétricas*, cifran los datos a través de un algoritmo y dos claves. Normalmente, la clave pública se usa para cifrar los datos y la clave privada para descifrar. La clave privada se puede proteger con una *passphrase* y se debe mantener en secreto. La clave pública se puede compartir entre los participantes.

## Protegiendo los datos

Otro importante concepto es el *hashing*, que no es más que un algoritmo matemático de un solo sentido que convierte un texto plano en una cadena de longitud fija. Al tratarse de un algoritmo de un solo sentido, no se puede ejecutar un proceso de *de-hash*. El texto creado en este proceso también es conocido como *message digest*, hash, fingerprint o signature.

La gran utilidad de un mensaje *digest* es que se puede utilizar para comparar datos. Por ejemplo, si dos ficheros `file1` y `file2` generan el mismo *hash*, podemos afirmar que su contenido es idéntico.

Un mensaje *digest* cifrado es creado con un fichero de texto plano junto con una llave privada. Este tipo de *hashing* es robusto ante diferentes ataques y se suele utilizar en aplicaciones Linux como OpenSSH.

## Firmando transmisiones

Otra implementación práctica del *hashing* en la *firma digital*. Una firma digital es un *token* criptográfico que proporciona autenticación y verificación de datos. Está formado por un mensaje resumen *digest* del fichero original en texto plano, que es cifrado con la llave privada del usuario y enviada con el texto cifrado.

El destinatario realiza el descifrado de la firma digital con la llave pública del emisor, por lo que estará disponible el resumen del mensaje original (digest). El destinatario también descifra el *texto cifrado* y crea un resumen de los datos en texto plano. En este momento, el destinatario puede comparar el nuevo resumen creado con el recibido en el mensaje del emisor. Si coinciden, la firma digital está verificada, y podemos confiar en que el envío se realizó desde el emisor, y que los datos no han sido modificados durante el envío.

## GPG

Con GPG (GNU Privacy Guard) podremos cifrar el contenido de ficheros y realizar firmas digitales para incrementar la seguridad de los mismos. GPG está basado en *Pretty Good Privacy*, y es una herramienta de cifrado muy popular que suele estar instalada por defecto. Sin embargo, si no está disponible en nuestro sistema, se puede realizar la instalación de los paquetes `gpg` o `gnupg2`.

### Creación de claves

Para poder comenzar a usar GPG, es necesario crear un par de llaves público/privada, donde la pública está disponible para todo el mundo y la privada se debe mantener en secreto.

	$ gpg --gen-key

GPG nos realizará una serie de preguntas, entre las que se incluyen nuestro nombre completo y dirección de correo electrónico, así como una *frase de paso*. Será necesario que recordemos la dirección de correo y la *frase de paso*, porque la dirección de correo identifica nuestra llave pública, y la *frase de paso* nos permite el acceso a la privada.

> Durante el proceso de creación de las llaves es necesario contar con la entropía suficiente en nuestro sistema, y así poder generar la aletoriedad precisa. Un método sencillo es el uso de la utilidad `rngd`, disponible en los paquetes `rng-utils` o `rng-tools`. Antes de ejecutar el comando `gpg --gen-key`, ejecutamos `rngd -r /dev/urandom` con privilegios de administrador. De forma alternativa, podemos hacer uso de la herramienta `haveged`.

Una vez finalizado el proceso de creación de las llaves, estas quedarán guardadas en un fichero llamado `keyring` en el directorio `~/.gnupg`.

Para que alguien pueda cifrar un fichero que luego tengamos que descifrar, debemos enviarle una copia de nuestra llave pública. Este proceso se conoce como *exportar la llave*, y copiarla dentro de un fichero con el siguiente comando:

	gpg --export EMAIL-ADDRESS > fichero.pub

El campo *EMAIL-ADDRESS* identifica nuestra llave pública en el anillo de llaves (keyring), y el *fichero* puede ser elegido a nuestra voluntad. En este momento, podemos enviar ese fichero a cualquier persona que deseemos que nos envíe información cifrada.

> El fichero generado con la llave pública esta guardado con un formato binario. Si presenta problemas a la hora de enviarlo por correo electrónico o descargarlo desde una web, podemos volver a generarlo con la opción `--armor`.

### Importar las claves

Cuando recibimos la llave pública de otra persona, debemos incorporarla a nuestro anillo, en un proceso que es conocido como *importación*, y se puede conseguir con el siguiente comando:
	
	gpg --import fichero.pub

Después de realizar la importación, es una buena idea comprobar la lista de llaves presentes en nuestro anillo:

	gpg --list-keys

> GPG posee un agente secreto, `gpg-agent`, y su función es la gestión de las llaves privadas de forma independiente a cualquier protocolo. Es un *demonio* que se inicia bajo demanda de la utilidad `gpg`, de tal forma que cuando GPG necesita una llave privada, pregunta por la misma al agente. El agente mantiene las llaves que se han usado previamente en la memoria RAM. Si la llave solicitada no está cargada en memoria, el agente se encarga de añadirla preguntando por la *frase de paso*.

### Cifrando y descifrando

Una vez cargada la llave pública en nuestro anillo, podemos comenzar el proceso de cifrado. Para cifrar el fichero haciendo uso de la llave pública, ejecutamos el siguiente comando:

	gpg --out FICHERO-CIFRADO --recipient EMAIL-ADDRESS --encrypt FICHERO-ORIGINAL

Como se ha visto en el proceso de exportación, la dirección de correo electrónico identifica la llave pública en nuestro anillo.

El receptor del fichero cifrado, puede descifrarlo haciendo uso de su llave privada con el siguiente comando:

	gpg --out FICHERO-CIFRADO --decrypt FICHERO-ORIGINAL

### Firma y comprobación de mensajes

El proceso de cifrado protege la privacidad del documento, pero no lo protege de posibles cambios durante el envío. En este momento es cuando entra en escena la firma digital de un fichero cifrado. Este proceso crea un *sello de tiempo* y certifica el fichero, que hace que si el fichero se modifica durante el envío, la utilidad `gpg` mostrará una alerta cuando el receptor revise el mensaje.

Para firmar un fichero debemos usar la opción `--sign`, y si queremos enviarlo en formato ASCII, añadimos el modificador `--clearsign`. El proceso de firma digital es algo diferente al de cifrado. Para cifrar la firma digital, `gpg` usa la llave privada. A continuación, se muestra un ejemplo:

	$ cat secret.txt
	Hola, este fichero lo he enviado yo.
	Firmado,
	Bob.

	$ gpg --out tosign --recipiento bob@example.com --encrypt secret.txt

	$ gpg --output signed --sign tosign

Cuando el destinatario recibe el mensaje cifrado y firmado, puede verificar que procede del emisor y que no ha sufrido cambios durante el envío:

	gpg --verify signed

> Como el fichero ha sido cifrado con la llave privada, debemos estar en posesión de su llave pública.

El proceso de comprobación de firma y descifrado debemos realizarlo en dos etapas. Primero, debemos descifrar y comprobar la firma.

	$ ls signed
	signed

	$ gpg --out mensaje.gpg --verify signed

Después de la comprobación anterior, debemos descifrar el mensaje original como se hizo en secciones anteriores:

	$ gpg --out mensaje.txt --decrypt mensaje.gpg

	$ cat mensaje.txt
	Hola, este fichero lo he enviado yo.
	Firmado,
	Bob.

> Si queremos firmar un mensaje sin cifrarlo, debemos usar la opción `--detach-sign`. A continuación, enviaremos el fichero de firma junto con el mensaje o fichero.

### Revocación de clave

Si nuestra clave privada se ha visto comprometida, o la hemos perdido, debemos realizar una revocación de la llave pública. Este proceso está formado por los siguientes pasos:

1. Generar el certificado de revocación.
2. Importar el certificado anterior en nuestro anillo.
3. Hacer disponible el certificado a aquellas personas que dispongan de nuestra llave pública.

Para poder generar el certificado de revocacion, usaremos las opciones `--gen-revoke` o `-generate-revocation` del comando `gpg`. A continuación, lo importamos a nuestro anillo con la opción `--import`.

Y, por último, lo pondremos a disposición de nuestros contactos para que puedan incorporarlo a sus anillos. Si hemos usado un servidor de claves, debemos subirlo a dicho servior.
