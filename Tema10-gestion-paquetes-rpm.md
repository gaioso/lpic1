# Tema 10 - Gestión de paquetes con RPM y YUM

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Introducción](#introducción)
- [Gestión de paquetes usando rpm](#gestión-de-paquetes-usando-rpm)
    - [Instalación y actualización](#instalación-y-actualización)
    - [Desinstalación](#desinstalación)
    - [Consultas a la base de datos](#consultas-a-la-base-de-datos)
    - [Comprobación de paquetes](#comprobación-de-paquetes)
    - [El programa rpm2cpio](#el-programa-rpm2cpio)
- [YUM](#yum)
    - [Repositorios de paquetes](#repositorios-de-paquetes)
    - [Instalar y desinstalar paquetes usando YUM](#instalar-y-desinstalar-paquetes-usando-yum)
    - [Información sobre paquetes](#información-sobre-paquetes)
    - [Descarga de paquetes](#descarga-de-paquetes)
- [ZYpp](#zypp)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos

* Conocer los conceptos básicos de RPM.
* Ser capaz de usar *rpm* para la gestión de paquetes.
* Ser capaz de usar *YUM*.

## Introducción

El «Red Hat Package Manager»(RPM) es una herramienta de gestión de paquetes software. Soporta la instalación y desinstalación de paquetes, mientras que se asegura que diferentes paquetes no entren en conflicto, y que las dependencias entre ellos son tenidas en cuenta. Además, permite realizar consultas sobre los paquetes y asegurarse de la integridad de los mismos.	El núcleo de RPM es una base de datos. Se añaden a la base cuando son instalados y se eliminan cuando son desinstalados. El formato RPM es usado por varias distribuciones (desde la propia Red Hat, Novell/SUSE, TurboLinux, Mandriva, Fedora...).

Y un paquete *rpm* cualesquiera no puede ser instalado en todas las distribuciones. Dicho paquete contiene software compilado, y puede haber variaciones entre las diferentes distribuciones. RPM fue desarrollado originalmente por Red Hat, y se llamó «Red Hat Package Manager», pero luego fue usado por otras distribuciones, por lo que se rebautizó «RPM Package Manager». Un paquete RPM tiene un nombre compuesto, por ejemplo:

	openssh-3.5p1-107.i586.rpm

que consiste en el nombre del paquete (openssh-3.5p1-107), la arquitectura (i586) y el sufijo «.rpm». El nombre del paquete se usa para identificar internamente el paquete una vez instalado. Y está formado por el nombre del software (openssh), la versión asignada por los desarroladores originales (3.5p1), seguido del número de liberación (107) asignada por el constructor del paquete (el distribuidor).

El «RPM Package Manager» es invocado usando el comando `rpm`, seguido por un modo básico. Los modos más importantes se verán más adelante, pero aquellos más complejos, como la inicialización de la base de datos, o la construcción y firma de paquetes, están fuera del alcance de este curso.

## Gestión de paquetes usando rpm

En la siguiente tabla se muestra un resumen de las principales opciones del comando `rpm`:

Formato corto | Formato largo | Descripción
--- | --- | ---
-e | --erase | Elimina el paquete
-F | --freshen | Actualiza un paquete solo si existe una versión previa
-i | --install | Instala el paquete
-q | --query | Consulta si el paquete está instalado
-U | --upgrade | Instala o actualiza el paquete
-V | --verify | Comprueba si existen los ficheros del paquete y su integridad

Instalación y actualización. Un paquete RPM se instala con el modo `-i`, seguido de la ruta del fichero del paquete, por ejemplo:

	# rpm -i /tmp/aircrack-ng-1.0-2.fc12.i686.rpm

También podemos indicar la ruta a una dirección HTTP o FTP, para instalar paquetes desde servidores remotos. Está permitido especificar varios paquetes al mismo tiempo:

	# rpm -i /tmp/*.rpm

Adicionalmente, hay dos modos relacionados, que son `-U` (upgrade) y `-F` (freshen). La primera elimina cualquier versión anterior de un paquete e instala la nueva versión, mientras que el segundo instala el paquete SÓLO si existe una versión anterior instalada (que será eliminada).

### Instalación y actualización

Después de estas opciones, podemos indicar que se muestre una barra de progreso (`-h` «hash mark»). Otra opción es `--test`, que no realiza la instalación, sino que tan solo comprueba posibles conflictos (debe utilizarse junto con `-i`). Cuando ocurre un conflicto, el paquete no se instala. Algunos de estos conflictos son:

* Ya existe ese paquete instalado.
* Se requiere realizar la instalación de un paquete ya instalado en una versión diferente (-i), o existe una versión más actual (-U).
* La instalación pretende escribir un fichero que pertenece a un paquete diferente.
* Un paquete necesita un paquete diferente que no está instalado, o que está a punto de ser instalado.

### Desinstalación

Los paquetes pueden ser desinstalados usando el modo `-e` («erase»):

	# rpm -e zsh
	# rpm -q zsh

Debemos indicar el nombre del paquete en lugar del nombre fichero, ya que RPM no recuerda este último. También podemos abreviar el nombre del paquete si es el único existente, e includo hacer uso de las opciones `--test` y `--nodeps`. En principio, se eliminan todos los ficheros del paquete, a no ser que existan ficheros de configuración que hayan sido modificados, que se guardarán con la extensión `.rpmsave`.

### Consultas a la base de datos

El modo para obtener información acerca de los paquetes es `-q` (query).

	$ rpm -q zsh
	zsh-5.5.1-6.el8_1.2.x86_64
	$ rpm -q docker
	package docker is not installed

Las opciones `-qi` nos devolverán información ampliada sobre el paquete.

```
$ rpm -qi zsh
Name        : zsh
Version     : 5.5.1
Release     : 6.el8_1.2
Architecture: x86_64
Install Date: Thu Nov 12 19:01:04 2020
Group       : Unspecified
Size        : 7546254
License     : MIT
Signature   : RSA/SHA256, Sat Mar 21 01:07:41 2020, Key ID 05b555b38483c65d
Source RPM  : zsh-5.5.1-6.el8_1.2.src.rpm
Build Date  : Fri Mar 20 18:38:18 2020
Build Host  : x86-01.mbox.centos.org
Relocations : (not relocatable)
Packager    : CentOS Buildsys <bugs@centos.org>
Vendor      : CentOS
URL         : http://zsh.sourceforge.net/
Summary     : Powerful interactive shell
Description :
The zsh shell is a command interpreter usable as an interactive login
shell and as a shell script command processor.  Zsh resembles the ksh
shell (the Korn shell), but includes many enhancements.  Zsh supports
command line editing, built-in spelling correction, programmable
command completion, shell functions (with autoloading), a history
mechanism, and more.
```

De la información anterior se puede obtener el número de versión, fecha de instalación y mucho más. Sin embargo, nos falta algún dato como la lista de dependencias. Para obtener esta información debemos hacer uso de las opciones `-qR`:

	$ rpm -qR zsh
	/bin/sh
	[...]
	libc.so.6(GLIBC_2.7)(64bit)
	[...]
	rtld(GNU_HASH)

Para obtener la lista de ficheros que forman parte de un paquete, usamos las opciones `-qc`:

	$ rpm -qc zsh
	/etc/skel/.zshrc
	/etc/zlogin
	/etc/zlogout
	/etc/zprofile
	/etc/zshenv
	/etc/zshrc

De forma inversa, para averiguar a que paquete pertenece un determinado fichero.

	$ rpm -qf /usr/bin/ssh
	openssh-clients-8.0p1-4.el8_1.x86_64

Para obtener una lista completa de los paquetes instalados:

	$ rpm -qa

	$ rpm -qa | grep cups

Otra consulta que puede resultar de interés es la que comprueba a que paquete pertenece un determinado fichero:

	$ rpm -q --whatprovides /usr/bin/zsh
	zsh-5.5.1-6.el8_1.2.x86_64

### Comprobación de paquetes

Cuando descargamos un paquete debemos tener en cuenta que la descarga se pudo realizar de forma incorrecta, o incluso que el paquete pueda estar falsificado (troyano). RPM nos protege de ambos escenarios, mediante la siguiente opción:

	$ rpm --checksig /tmp/zsh-5.5.1-6.el8_1.2.x86_64.rpm

> Para descargar un paquete sin realizar la instalación del mismo, podemos hacer uso de la utilidad `yumdownloader` que forma parte del paquete `yum-utils`.

Se comprueba el checksum MD5 que el paquete contiene en su interior, además su firma interna (SHA1) es comparada con la clave pública del gestor del paquete. Por supuesto, debemos tener la clave pública del distribuidor en nuestro sistema.

En cuanto a la verificación posterior.

	# rpm -V zsh

compara ciertos valores de la base de datos con los ficheros del sistema. Aunque tampoco nos proporciona seguridad, ya que un atacante podría modificar tanto el fichero como la base de datos, pero nos puede servir para detectar errores en ficheros del sistema provocados, por ejemplo, por un corte de luz.

> De forma manual e intencionada, podemos modificar los permisos de alguno de los ficheros que forman parte del paquete `zsh`, y ejecutar la verificación.

### El programa rpm2cpio

Los paquetes RPM son básicamente archivos *cpio* con una cabecera. Podemos, de este modo, extraer los ficheros individuales de un paquete RPM sin tener que instalar el paquete primero. Simplemente, convertimos el paquete RPM a un archivo *cpio* usando el programa `rpm2cpio`, y lo enviamos hacia `cpio`. Para ello, utilizaremos una tubería.

Con este primer comando, podemos visualizar el contenido del fichero *rpm*:

	$ rpm2cpio zsh-5.5.1-6.el8_1.2.x86_64.rpm | cpio -t
	./etc/zlogin
	[...]
	./usr/share/zsh/site-functions
	15153 blocks

Si lo que deseamos es realizar una extracción completa de los ficheros que integran el archivo, ejecutaremos el siguiente comando:
 
	$ rpm2cpio zsh-5.5.1-6.el8_1.2.x86_64.rpm | cpio -idv

Aunque también nos puede interesar tan solo uno de los ficheros del archivo *rpm*:

	$ rpm2cpio zsh-5.5.1-6.el8_1.2.x86_64.rpm | cpio -idv ./etc/zlogin

Y, de forma alternativa, con los siguientes comandos también es posible obtener los ficheros internos al archivo *rpm*:

	$ rpm2cpio zsh-5.5.1-6.el8_1.2.x86_64.rpm > zsh.cpio
	$ cpio -idv < zsh.cpio

## YUM

El programa `rpm` tiene límites. Por ejemplo, no facilita la búsqueda de paquetes que se pueden instalar, hay que indicarle exactamente el fichero a instalar. **YUM** (YellowDog Update Manager) permite el acceso a repositorios disponibles tanto en Internet com en CD-ROM (es similar a un `apt-get`). Dispone incluso de una shell (`yum shell`).

Este comando hace uso del directorio `/etc/yum.repos.d/` para guardar los ficheros donde se guardan las direcciones de los repositorios remotos. En una distribución de CentOS, versión 8, el contenido del directorio es el siguiente:

	$ ls /etc/yum.repos.d/
	CentOS-AppStream.repo  CentOS-Debuginfo.repo  CentOS-HA.repo          CentOS-Sources.repo     CentOS-fasttrack.repo  epel-testing-modular.repo
	CentOS-Base.repo       CentOS-Devel.repo      CentOS-Media.repo       CentOS-Vault.repo       epel-modular.repo      epel-testing.repo
	CentOS-CR.repo         CentOS-Extras.repo     CentOS-PowerTools.repo  CentOS-centosplus.repo  epel-playground.repo   epel.repo

El programa `yum` es muy versátil y a continuación se muestran algunos de los comandos que podemos usar:

Comando | Descripción
--- | ---
`check-update` | Consulta las actualizaciones del repositorio para los paquetes instalados
`clean` | Elimina los ficheros temporales que se descargaron durante la instalación
`deplist` | Muestra las dependencias de un paquete
`groupinstall` | Instala el grupo de paquetes
`info` | Muestra la información del paquete
`install` | Instala el paquete
`list` | Muestra información sobre los paquetes instalados
`localinstall` | Instala un paquete desde un fichero RPM
`localupdate` | Actualiza el sistema desde un fichero RPM
`provides` | Muestra el paquete al que pertenece un fichero
`reinstall` | Vuelve instalar el paquete
`remove` | Elimina el paquete del sistema
`resolvedep` | Muestra los paquetes relacionados con la dependencia
`search` | Busca entre los nombres y descripciones de los paquetes el texto indicado
`shell` | Inicia el modo línea de comandos
`update` | Actualiza los paquetes indicados a la última versión disponible en el repositorio
`upgrade` | Actualiza los paquetes indicados y elimina los paquetes obsoletos

### Repositorios de paquetes

Un repositorio es un conjunto de paquetes que están disponibles a través de la red, y que permite la instalación de los mismos a través de Yum.

	$ yum repolist

muestra una lista de los repositorios configurados.

	$ yum repolist disabled

muestra una lista de los repositorios deshabilitados.

Para habilitar un repositorio, usamos la opción `--enablerepo`, seguido del «repo id» de la lista. Esto sólo funciona en conexión con un comando `yum`; la repolist permanece intacta:

	$ yum --enablerepo=epel repolist

### Instalar y desinstalar paquetes usando YUM

Para instalar un nuevo paquete usando YUM, debemos conocer su nombre. YUM comprueba en los repositorios activos un nombre similar, resuelve las dependencias, descarga el paquete, y los que sean necesarios, y por último, los instala.

	# yum install hello
	# yum install openssh

Eliminar paquetes es así de simple:

	# yum remove hello

esta opción también desinstala los paquetes de los que depende, y no sean requeridos por otros paquetes.

Podemos actualizar paquetes:

	# yum update hello

para actualizar todos los paquetes instalados en el sistema:	

	# yum update

Una de las características más útiles de `yum` es la capacidad de agrupar paquetes para su distribución. En lugar de tener que descargar todos los paquetes necesarios para un determinado entorno (servidor web, base de datos, etc.), es posible descargar el paquete que los agrupa a todos ellos. Usaremos el comando `yum grouplist` para ver la lista de los grupos disponibles, y `yum groupinstall <nombre>` para su instalación.

> Recientemente, el programa `dnf` ha ganado popularidad y está incluído en la distribución Fedora Linux como sustituto de `yum`. Incorpora muchas mejoras, como por ejemplo la velocidad a la hora de resolver las búsquedas de dependencias.

### Información sobre paquetes

El comando `yum list` está disponible para saber si un paquete existe:

	$ yum list gcc
	$ yum list “gcc*”

los «Installed Packages» están disponibles en el sistema local, mientras que los «Available Packages» pueden ser localizados en los repositorios.

	$ yum list installed “gcc*”
	$ yum list available “gcc*”

El comando:

	$ yum list updates

muestra todos los paquetes instalados para los cuales existe una actualización disponible en los repositorios.

	$ yum list recent

indica los paquetes que se han incorporado recientemente al repositorio.

	$ yum info hello

muestra información adicional sobre un paquete. A diferencia de «rpm -qi», también muestra información de paquetes no instalados, pero disponibles en los repositorios.

	$ yum search mysql

busca todos los paquetes, en cuyo nombre o descripción se encuentra la cadena indicada. Para examinar las dependencias de un paquete:

	$ yum deplist gcc

### Descarga de paquetes

Si lo que queremos es descargar un paquete de un repositorio, pero sin instalarlo, podemos usar el programa `yumdownloader`.

	$ yumdownloader --destdir /tmp hello

busca en los repositorios el paquete `hello`, y lo descarga en el directorio `/tmp`.

La opción `--resolve` descargaría también las dependencias necesarias, y que no estuvieran presentes en el equipo. Y la opción `--source`, descarga las fuentes en lugar de los binarios.

	$ yumdownloader --source --destdir /tmp hello
	
## ZYpp

La distribución OpenSUSE usa el gestor de paquetes RPM y el software se distribuye en ficheros `.rpm` pero no hace uso de `yum` ni `dnf`. En su lugar, ha creado su propia herramienta de gestión de paquetes, llamada *ZYpp* (también conocida por `libzypp`). El comando `zypper` permite realizar las operaciones de consulta, instalación y retirada de paquetes software de nuestro sistema.

Comando | Descripción
--- | ---
`help` | Muestra información general o sobre un determinado comando
`install` | Instala el paquete
`info` | Muestra información sobre el paquete
`list-updates` | Muestra todas las actualizaciones disponibles para los paquetes instalados desde el repositorio
`lr` | Muestra información del repositorio
`packages` | Muestra todos los paquetes disponibles, o bien los de un repositorio determinado
`what-provides` | Muestra el paquete al que pertenece un fichero
`refresh` | Actualiza la información del repositorio
`remove` | Elimina un paquete del sistema
`search` | Busca un paquete determinado
`update` | Actualiza todos los paquetes a su última versión del repositorio, o tan solo el que se indique
`verify` | Comprueba que los paquetes instalados cumplan sus dependencias

Para la instalación de paquetes, `zypper` se comporta de forma similar a `yum`.

	$ sudo zypper install emacs
	[...]
	The following 31 NEW packages are going to be installed:
	  efont-unicode-bitmap-fonts emacs emacs-info emacs-x11 etags fonts-config fribidi ft2demos gconf2 gconf2-branding-openSUSE gconf2-lang gconf-polkit
	  ImageMagick-config-6-SUSE intlfonts-euro-bitmap-fonts libfontenc1 libgif6 libgthread-2_0-0 libHalf11 libIex-2_1-11 libIlmImf-Imf_2_1-21 libIlmThread-2_1-11 libm17n0
	  libMagickCore-6_Q16-1 libMagickWand-6_Q16-1 libotf0 libXaw3d8 m17n-db mkfontdir mkfontscale xorg-x11-fonts xorg-x11-fonts-core

	The following 3 recommended packages were automatically selected:
	  gconf2-lang mkfontdir mkfontscale
	[...]

El comando `info` lo podremos usar para visualizar información sobre un paquete determinado.

```
$ zypper info emacs
[...]
Información de Paquete emacs:
-----------------------------
Repositorio      : openSUSE-Leap-42.3-Update
Nome             : emacs                    
Versión          : 24.3-28.1                
Arq              : x86_64                   
Vendedor         : openSUSE                 
Tamaño Instalado : 63,9 MiB                 
Instalado        : Si                       
Estado           : actualizado              
Source package   : emacs-24.3-28.1.src      
Resumo           : GNU Emacs Base Package   
Descrición       :                          
    Basic package for the GNU Emacs editor. Requires emacs-x11 or
    emacs-nox.
```

La utilidad `zypper` hace un esfuerzo para facilitar la transición desde herramientas de gestión anteriores. Por ejemplo, si hacemos uso del método `what-provides` para buscar el paquete al que pertenece un determinado fichero, nos mostrará información sobre el nuevo método a utilizar:

```
$ which emacs
/usr/bin/emacs
$
$ zypper what-provides /usr/bin/emacs
Command 'what-provides' is replaced by 'search --provides --match-exact'.
See 'help search' for all available options.
Loading repository data...
Advertencia: Repository 'openSUSE-Leap-42.3-Update' appears to be outdated. Consider using a different mirror or server.
Advertencia: Repository 'openSUSE-Leap-42.3-Update-Non-Oss' appears to be outdated. Consider using a different mirror or server.
Reading installed packages...

E  | Nome  | Resumo                 | Tipo   
---+-------+------------------------+--------
i+ | emacs | GNU Emacs Base Package | Paquete
```

Si necesitamos buscar ayuda sobre el uso de la utilidad, tenemos disponible el comando `help`, que también puede ser usado para una opción concreta de la utilidad con `zypper help <comando>`.
También permite el uso de nombres cortos para alguno de sus comandos; `in` para `install`, `re` para `remove` y `se` para `search`.

	$ zypper se nmap

Y por último, con el comando `remove` podemos eliminar uno de los paquetes instalados en el sistema:

	$ sudo zypper remove emacs

## Comandos vistos en el tema

Comando | Descripción
--| --
cpio | Gestor de archivos
rpm | Herramienta de gestión de paquetes usada por varias distribuciones Linux.
rpm2cpio | Convierte paquetes RPM a archivos cpio.
yum | Potente herramienta de gestión de paquetes.
