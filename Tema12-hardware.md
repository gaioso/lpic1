# Tema 12 - Hardware

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [La BIOS](#la-bios)
    - [El típico problema del límite del cilindro 1024](#el-típico-problema-del-límite-del-cilindro-1024)
    - [Orden de arranque](#orden-de-arranque)
- [Arranque UEFI](#arranque-uefi)
- [Tarjetas PCI](#tarjetas-pci)
- [Dispositivos USB](#dispositivos-usb)
- [Interfaz GPIO](#interfaz-gpio)
- [Periféricos.](#periféricos)
- [El directorio /dev](#el-directorio-dev)
- [El directorio /proc](#el-directorio-proc)
    - [Peticiones de interrupción](#peticiones-de-interrupción)
    - [Puertos I/O](#puertos-io)
    - [Acceso directo a memoria (DMA)](#acceso-directo-a-memoria-dma)
- [El directorio /sys.](#el-directorio-sys)
- [Gestión de dispositivos](#gestión-de-dispositivos)
    - [Localizar dispositivos](#localizar-dispositivos)
- [Módulos hardware](#módulos-hardware)
    - [Ver módulos instalados](#ver-módulos-instalados)
    - [Información del módulo](#información-del-módulo)
    - [Instalar nuevos módulos](#instalar-nuevos-módulos)
    - [Eliminar módulos](#eliminar-módulos)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)
- [Resumen](#resumen)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Conceptos básicos del soporte hardware de Linux para PC's.
* Familiarizarse con términos como BIOS, UEFI o PCI.
* Saber cómo gestiona Linux los dispositivos de almacenamiento basados en ATA, SATA, y SCSI.

## La BIOS

Hoy en día podemos encontrar sistemas Linux en multitud de hardware, desde pc's hasta televisores, pasando por routers, pda's, teléfonos móviles, etc. Linux es capaz de trabajar con dispositivos hardware poco usuales, o poco frecuentes. A nosotros nos interesa un sistema Linux bajo una arquitectura hardware compatible con IBM x86 e Intel. La arquitectura x86 posee uno o más procesadores, conectados a través de un chipset con la memoria RAM, la interfaz I/O, controladoras IDE, ATA, SCSI y otras interfaces como USB, Ethernet, etc. y con los conectores de teclado, ratón, puerto serie y paralelo. Todo ello está montado sobre una placa base, proporcionada por multitud de fabricantes, para diferentes procesadores, como pueden ser Intel, AMD, de 32/64 bits, etc.

La BIOS (Basic Input/Output System) es el software que está más cercano al hardware de un PC. Está almacenado sobre una memoria ROM en la placa base. Se encarga del primer arranque que se realiza en la placa. Era usada por los sistemas operativos antiguos (MS-DOS) para interactuar con el hardware a través de llamadas al sistema, sin embargo, en la actualidad, los sistemas modernos, como Linux, tienen su propia capacidad de trabajo con el hardware, ignorando la BIOS. Por lo tanto, no es necesario configurar de manera extendida la BIOS para trabajar con Linux, simplemente debemos tener presente que se pueden habilitar o deshabilitar ciertos dispositivos en la BIOS para poder trabajar con ellos posteriormente.

La mayoría de las BIOS tienen una interfaz de usuario, a la cual podemos acceder en el momento de encendido del equipo de varias formas, dependiendo del fabricante, la mas usual, es pulsar la tecla *Supr*, o *F1*/*F2* en otros casos, justo después del arranque del equipo. Una de las configuraciones más obvias en la fecha y hora almacenada en el reloj hardware. Se debe configurar con el valor correspondiente a nuestra zona horaria, o bien en UTC, y luego decirle a Linux que tipo de hora utilizar. La BIOS debe reconocer al menos al disco que tiene el sistema operativo al cual debe pasarle el control. Puede ocurrir que haya discos modernos que alguna BIOS antigua no reconozca, no pasará nada, ya que Linux lo reconocerá una vez que se inicie, siempre que dicho disco no contenga el sistema operativo de arranque.

### El típico problema del límite del cilindro 1024

Las versiones antiguas de BIOS sólo eran capaces de direccionar hasta el cilindro 1024. Por lo tanto, había sistemas que no podían arrancar al situarse el sector de arranque, más allá del cilindro 1024. Hay varias posibles soluciones:

* Configurar en la BIOS el disco como LBA.
* Usar otro disco.
* Colocar la partición `/boot` cerca del inicio físico del disco (20-30 Mb son suficientes).

### Orden de arranque

La BIOS comprueba varios dispositivos para arrancar desde alguno de ellos. Lo normal es seguir un orden, desde el primer disco fijo, pasando por el disquete y siguiendo por el CD-ROM, por cuestiones de seguridad es importante que este orden no se pueda modificar facilmente (contraseña en la BIOS), ya que puede poner en peligro la seguridad del sistema al arrancar con otro sistema operativo desde un *live-cd*, por ejemplo.

Cuando se inicia el sistema desde un disco, debemos indicar cual es ese disco, así como la partición dentro del mismo, para que BIOS pueda cargar el programa de gestión de arranque (boot loader). Esto se consigue mediante la activación del *master boot record* (MBR).

MBR es el primer sector de la primera partición del disco de inicio en el sistema. BIOS busca el MBR y carga el programa en la memoria. Debido al tamaño de dicho sector, este programa será muy simple y no puede realizar demasiadas tareas. Su tarea principal será indicar la localización del fichero con el núcleo del sistema operativo, que estará guardado en un sector de una partición diferente.

> El *boot loader* no tiene porque apuntar a un *kernel*, sino que puede cargar un segundo gestor de arranque que incorpore muchas más opciones al disponer de más espacio en disco para el código.

## Arranque UEFI

A pesar de todas las limitaciones de BIOS, ha sido el sistema usado de forma mayoritaria en los equipos compatibles con IBM. En estos momentos, y debido a la complejidad que han adquirido los sistemas operativos llega el momento de implementar un nuevo método de arranque del sistema.

Intel ha creado *Extensible Firmware Interface* en 1998 para solucionar algunas de las limitaciones de BIOS. Fue en el año 2005 cuando otros fabricantes apoyaron este desarrollo y se presenta *Unified EFI* (UEFI) para que se convirtiera en un estándar. En la actualidad, su uso está extendido a lo largo de la mayoría de equipos de escritorio y servidores compatibles con IBM.

A diferencia de BIOS, no hace uso de un único sector de arranque sino que especifica una partición especial del disco, denominada *EFI System Partition* (ESP), donde se almacenan los programas de arranque. Esto permite el uso de programas de cualquier tamaño, así como la capacidad de guardar diferentes programas de arranque para múltiples sistemas operativos.

Esta partición (ESP) hace uso del sistema de ficheros FAT para almacenar los programas. En los sistemas Linux, la partición ESP se suele montar en `/boot/efi`, y los ficheros se guardan con la extensión `.efi`, como por ejemplo `linux.efi`.

## Tarjetas PCI

El PC tiene una historia de unos 25 años, y los cambios en el hardware han sido radicales. Una de las características más importantes en los PC's de los 80 era la capacidad de expansión con tarjetas para diversas funciones, fabricadas por terceros, el bus ISA. Y pronto se quedó obsoleta y lenta para los ordenadores más modernos. Así surgió el bus PCI, a principios de los 90, que todavía continúa en uso hoy en día. El bus PCI no sólo permite transferencia de datos a alta velocidad entre la CPU, memoria y los periféricos, sino que admite reconocimiento de hardware, ya que cada tarjeta PCI contiene un código almacenando su tipo, fabricante y modelo. Esta información puede ser obtenida a través del comanso `lspci`. El PCI ID nos da idea de la posición del dispositivo en el bus.

	$ lspci

Estos datos son utilizados por diferentes distribuciones para seleccionar y configurar los drivers de la base de datos. Asimismo, la salida del comando `lspci` resultará de gran utilidad para la resolución de problemas tales como que una tarjeta no sea reconocida por Linux.

Opción | Descripción
--- | ---
-A | Define el método de acceso a la información de PCI
-b | Muestra la información de conexión desde el punto de vista de la tarjeta
-k | Muestra los módulos del kernel para cada tarjeta PCI
-m | Muestra la información de fabricante y dispositivo en números en lugar de texto
-q | Consulta en la base de datos centralizada de PCI por la tarjetas instaladas
-t | Muestra un diágrama de árbol que muestra las conexiones entre tarjetas y buses
-v | Muestra información adicional (*verbose*) sobre las tarjetas
-x | Muestra un volcado en hexadecimal de la información de la tarjeta

El estándar *PCI Express* (PCIe) es el de uso mayoritario a día de hoy en la mayoría de servidores y estaciones de trabajo.

## Dispositivos USB

«Universal Serial Bus», ha sido concebido para sustituir a interfaces como PS/2, puerto paralelo, puerto serie, etc., y conectarlos todos a través del puerto USB. Es un cable asimétrico, con los extremos diferentes, y permite la conexión y desconexión en caliente, es decir mientras el sistema está ejecutándose. Los nuevos PC'S tienen varios controladores USB, que cada uno de ellos puede llegar a controlar 127 dispositivos. Podemos hacer uso de concentradores USB para conectar diferentes dispositivos al mismo puerto.

Las primeras velocidades eran de 1,5Mb/s y 12 Mbit/s. La más baja es más que suficiente para teclados, modems analógicos, sonido, etc, y la más alta para dispositivos de red como ethernet de 10Mb/s, o para cámaras digitales o escáneres que pueden enviar unos cuantos megas de datos. Para otros dispositivos, como discos duros, camaras de alta resolución o reproductores MP3, se quedó lenta y llegó el USB 2.0 con una tasa máxima de 480Mb/s. Los kernels actuales soportan las tres velocidades. La velocidad real de un puerto USB 2.0 es de unos 53Mb/s. Con discos USB, la velocidad de transferencia suele ser de unos 35Mb. USB 3.0 fue lanzado en agosto de 2008, y presenta una velocidad de transferencia de 5Gb/s.

La velocidad de transferencia siempre es la del menor de los conectados, bien sea el dispositivo o el puerto. Hay concentradores USB2.0 que pueden realizar una conversión de velocidad, y así enviar los datos a alta velocidad de dispositivos lentos conectados a él. A cada uno de los dispositivos conectados se le asigna un número entre 1 y 127, como número de dispositivo, y luego también se lee su descriptor, que contiene información sobre el tipo de dispositivo, velocidad soportada, etc.

	# lsusb
	# lsusb -v

## Interfaz GPIO

La interfaz *General Purpose Input/Output* (GPIO) se ha vuelto muy popular en los sistemas Linux de formato reducido, y que se usan normalmente para el control de dispositivos en proyectos de automatización. Aquí se incluyen los sistemas populares como Raspberry Pi o BeagleBone.

La interfaz GPIO proporciona varias lineas digitales de entrada y salida que se puden controlar de forma individual. Resultando de gran utilidad para la gestión de dispositivos como relés, luces, sensores y motores.

## Periféricos.

Linux soporta una gran cantidad de periféricos usando para ellos drivers que se utilizan como módulos cargables. Se guardan en `/lib/modules`, y pueden cargarse de forma manual usando el comando `modprobe`.

	# modprobe bla

Si tiene dependencias, también se cargan los módulos dependientes.

El comando `lsmod` muestra los módulos actualmente cargados.

	# lsmod
	# modprobe -r foo

Al igual que con el comando anterior, si hay dependencias, también las elimina.

Es raro que tengamos que usar manualmente `modprobe`, ya lo hace el kernel de forma automática. Lo básico se encuentra en `/etc/modprobe.d/aliases`:

	alias “nombre driver" dispositivo

	alias block-major-3-* ide-generic
	alias char-major-10-1 psmouse

	$ ls -l /dev/hda1 /dev/psaux

## El directorio /dev

Después de que el *kernel* sea capaz de comunicarse con un dispositivo debe gestionar la transferencia de datos desde, y hacia, el mismo. Esto se realiza mediante los *fichero de dispositivo*, que el *kernel* crea en el directorio especial `/dev`.

Para realizar operaciones de lectura y escritura en un determinado dispositivo, Linux se encarga de todas las tareas de bajo nivel, facilitando el manejo de dichas operaciones a las aplicaciones del sistema a través de simples operaciones de lectura y/o escritura en dicho fichero.

Cuando se añaden nuevos dispositivos a nuestro equipo, Linux creará un nuevo fichero en el directorio `/dev` para que las aplicaciones puedan interactuar con dicho dispositivo.

Hay dos tipos de ficheros de dispositivo en Linux, según se realice la transferencia de datos al dispositivo:

* **Ficheros de carácter** - La transferencia de datos se realiza con un carácter en cada operación. Esto se suele dar en dispositivos *serie* como terminales de usuario o USB's.
* **Ficheros de bloque** - La transferencia se realiza en bloques de datos. En este caso, estaremos hablando de dispositivos que son capaces de gestionar grandes cantidades de datos, como discos duros o tarjetas de red.

El tipo de dispositivo aparece reflejado en la primera letra de la lista de permisos de la salida del comando `ls`:

```
$ ls -al /dev/sd* /dev/tty*
brw-rw----. 1 root disk    8,  0 Nov 19 18:46 /dev/sda
brw-rw----. 1 root disk    8,  1 Nov 19 18:46 /dev/sda1
brw-rw----. 1 root disk    8, 16 Nov 19 18:46 /dev/sdb
brw-rw----. 1 root disk    8, 17 Nov 19 18:46 /dev/sdb1
crw-rw-rw-. 1 root tty     5,  0 Nov 19 18:46 /dev/tty
crw--w----. 1 root tty     4,  0 Nov 19 18:46 /dev/tty0
[...]
```

## El directorio /proc

El directorio `/proc` será una de las herramientas más útiles que tendremos a nuestra disposición para resolver problemas de hardware en nuestro sistema. Se trata de un directorio virtual, que el kernel desplega con información sobre la configuración del hardware y su estado.

El contenido de los ficheros almacenados en este directorio se actualiza de forma dinámica por parte del kernel con el estado del hardware, por lo que con tan solo leer el contenido de los mismos podemos hacer un seguimiento de los componentes del sistema.

### Peticiones de interrupción

Las peticiones de interrupción (IRQs) permiten a los dispositivos solicitar el envío de datos a la CPU. Las IRQs actuales de un sistema se pueden consultar en el fichero `/proc/interrupts`.

```
$ cat /proc/interrupts 
           CPU0       
  0:        123   IO-APIC   2-edge      timer
  1:          9   IO-APIC   1-edge      i8042
  [...]
```

Algunas de las IRQs anteriores están reservadas para uso exclusivo por parte de ciertos dispositivos, como el 0 para el *timer* y el 1 para el teclado.

### Puertos I/O

Los puertos de entrada/salida son direcciones de memoria para que la CPU se comunique con el dispositivo. Podemos consultar el estado de los puertos a través del fichero `/proc/ioports`.

```
$ cat /proc/ioports 
0000-0000 : PCI Bus 0000:00
  0000-0000 : dma1
  0000-0000 : pic1
  [...]
```

Cada dispositivo tendrá asignado su puerto correspondiente de forma exclusiva, y en caso de que se produjera un conflicto, se pueden modificar los ajustes de forma manual a través del comando `setpci`.

### Acceso directo a memoria (DMA)

El uso de puertos para enviar datos a la CPU puede resultar lento. Para aumentar la velocidad, muchos dispositivos usan los canales de *acceso directo a memoria*, que se encargan del envío directo de datos desde el dispositivo a la memoria sin esperar por la CPU. Luego, la CPU podrá leer esas posiciones de memoria con los datos enviados. Se puede consultar el estado de los canales en uso a través del fichero `/proc/dma`.

```
$ cat /proc/dma
 4: cascade
```

La salida anterior indica que tan solo se está usando el canal 4.

## El directorio /sys.

Hasta la versión 2.4 del kernel, el directorio `/proc` era la única forma de acceder a los detalles del kernel y la configuración del sistema. Pero este directorio había crecido con demasiado contenido y se decidió mover todo aquello que no fuera información relativa a los procesos. El *pseudo sistema* de ficheros *sysfs* se monta en `/sys`, y está disponible a partir de la versión 2.6. `/sys/bus` permite el acceso a dispositivos dependiendo de su tipo de conexión. `/sys/devices` lo hace en un orden diferente.

	$ ls -ldi /sys/devices/pci0000:00/0000:00:06.0/usb1
	$ ls -ldi /sys/bus/usb/devices/usb1

*Mismo inodo. Es el mismo directorio.*

	$ ls /sys/bus/usb/devices/usb1
	$ cat /sys/bus/usb/devices/usb1/product

## Gestión de dispositivos

Linux proporciona una gran variedad de herramientas para la gestión de los diferentes dispositivos conectados a nuestro sistema, así como el seguimiento de su activida y la resolución de los problemas que se puedan presentar.

### Localizar dispositivos

Una de las tareas básicas de un administrador de sistemas es la identificación de los diferentes dispositivos conectados al sistema.

El comando `lsdev` muestra información acerca del hardware instalado en el sistema Linux, y para ello hace uso de los ficheros virtuales `/proc/interrupts`, `/proc/ioports` y `/proc/dma`.

```

```

El comando `lsblk` muestra información sobre los dispositivos de bloque instalados en el sistema.

```

```

La salida de este comando también muestra los dispositivos relacionados, como pueden ser los volúmenes LVM y el disco duro físico asociado. La salida del comando `lsblk` se puede ajustar para mostrar información adicional o filtrar por un determinado subconjunto de datos. La opción `-S` tan solo muestra información de los dispositivos de bloque tipo SCSI.

```
$ lsblk -S
NAME HCTL       TYPE VENDOR   MODEL            REV  TRAN
sda  2:0:0:0    disk VBOX     HARDDISK         1.0  spi
sdb  2:0:1:0    disk VBOX     HARDDISK         1.0  spi
```

## Módulos hardware

El núcleo Linux necesita controladores para poder comunicarse con los dispositivos instalados en el sistema. Por supuesto, no tiene sentido que se compilen todos los controladores de todo el hardware conocido en el propio kernel, ya que lo haría extremadamente grande. Para solucionar esta situación, Linux hace uso de los *módulos*, que son ficheros con un controlador específico que puede ser enlazado con el kernel en tiempo de ejecución. Si el kernel se configura para que pueda cargar los módulos del hardware, entonces los ficheros deben estar disponibles en el propio sistema. 

Los ficheros de los módulos se pueden distribuír en formato código fuente, por lo que será necesario realizar su compilación previa, o bien en formato binario por lo que el núcleo ya los podrá usar de forma dinámica. Los ficheros con extensión `.ko` identifican a los módulos con código objeto.

La localización por defecto para guardar los ficheros de los módulos es el directorio `/lib/modules`. Aquí es donde herramientas como `insmod` o `modprobe` buscarán los ficheros objeto de los módulos.

La lista de los módulos que se deben cargar durante el inicio del sistema se guarda en el fichero `/etc/modules`. Algunos de estos módulos tienen dependencias con otros módulos que se deben cargar con anterioridad. Estas relaciones se definen en el fichero `modules.dep`, que se guarda en el directorio `/lib/modules/<version>` (donde <version> es la versión del kernel). El formato de cada línea es el siguiente:

	modulefilename: dependencyfilename1 dependencyfilename2 ...

Cuando usamos `modules_install` para instalar los módulos, se llama a la utilidad `depmod` para averiguar sus dependencias y crear el fichero `modules.dep` de forma automática.

### Ver módulos instalados

Tenemos a nuestra disposición un conjunto de comandos que nos ayudan a consultar y resolver los problemas relacionados con los módulos.

El primero de estos comandos es `lsmod`, que nos muestra todos los comandos instalados en nuestro sistema.

	$ lsmod
	Module                  Size  Used by
	ppp_mppe               16384  0
	libarc4                16384  1 ppp_mppe
	[...]

En la salida también podemos consultar si un módulo está siendo usado por otro módulo. Nos puede resultar de utilidad para resolver problemas de tipo hardware.

### Información del módulo

Si precisamos obtener información sobre uno de los módulos, debemos usar el comando `modinfo`.

	$ modinfo bluetooth
	filename:       /lib/modules/5.4.0-53-generic/kernel/net/bluetooth/bluetooth.ko
	alias:          net-pf-31
	license:        GPL
	version:        2.22
	description:    Bluetooth Core ver 2.22
	[...]

### Instalar nuevos módulos

Si necesitamos instalar un nuevo módulo de forma manual, disponemos de dos comandos:
	
* `insmod`
* `modprobe`

El comando `insmod` funciona de forma básica, y debemos indicar el nombre exacto del fichero del módulo. Por ejemplo, en un sistema Ubuntu, los controladores se encuentran en el siguiente directorio:

	/lib/modules/5.4.0-53-generic/kernel/drivers/bluetooth/

Este directorio pertenece a la versión actual del núcleo (5.4.0-53), y dentro del mismo se encuentran los ficheros de los módulos para diferentes tipos de Bluetooth:

	$ ls -l
	total 748
	-rw-r--r-- 1 root root  24993 Out 21 11:00 ath3k.ko
	-rw-r--r-- 1 root root  13281 Out 21 11:00 bcm203x.ko
	[...]

Cada fichero con extensión `.ko` pertenece a un módulo para cada controlador que se puede instalar en el kernel 5.4.0-53. Para instalar el módulo debemos indicar el nombre del fichero, y en algunos casos, la ruta completa:

	$ sudo insmod lib/modules/5.4.0-53-generic/kernel/drivers/bluetooth/btintel.ko

La contraparte de esta utilidad es que si el módulo a instalar tiene dependencias sin resolver, dicha instalación no se completará. Para resolver esta situación, debemos hacer uso del comando `modprobe` que resolverá las posibles dependencias por nosotros.

Otra ventaja de `modprobe` es que reconoce los nombres de los módulos, y por lo tanto será capaz de localizar el fichero correspondiente.

> El siguiente comando se ha ejecutado en CentOS 8

	$ sudo modprobe -iv btusb
	insmod /lib/modules/4.18.0-193.28.1.el8_2.x86_64/kernel/net/rfkill/rfkill.ko.xz
	insmod /lib/modules/4.18.0-193.28.1.el8_2.x86_64/kernel/crypto/ecdh_generic.ko.xz 
	[...]

### Eliminar módulos

Normalmente, no habrá ningún problema en tener módulos instalados en nuestro sistema si el dispositivo hardware no está conectado. El kernel ignora dicho módulo. Sin embargo, algunos administradores desearán mantener su kernel lo más ligero posible, y podrán eliminar eses módulos innecesarios con el comando `rmmod`. A este comando debemos indicarle el nombre del módulo a desinstalar.

Sin embargo, el comando `modprobe` también se puede usar para este propósito, haciendo uso de la opción `-r`.

> El siguiente comando se ha ejecutado en CentOS 8

	$ sudo modprobe -rv btusb
	rmmod btusb
	rmmod btintel
	rmmod btbcm
	rmmod btrtl
	rmmod bluetooth
	rmmod ecdh_generic
	
## Comandos vistos en el tema

Comando | Descripción
--- | ---
lsmod | Muestra los módulos cargados por el kernel
lspci | Muestra información de los dispositivos en el bus PCI
lsusb | Muestra todos los dispositivos conectados al bus USB
modprobe | Carga módulos del kernel, teniendo en cuenta las dependencias

## Resumen

* La BIOS gestiona el proceso de arranque de un sistema Linux, pero luego no se utiliza.
* Las configuraciones típicas de la BIOS para Linux, incluyen, por ejemplo, la fecha y la hora, parámetros de disco y el orden de arranque.
* El comando `lspci` muestra información sobre el bus PCI y los dispositivos conectados a él.
