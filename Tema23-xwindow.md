# Tema 23 - Instalar y configurar X11

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Introducción](#introducción)
- [La interfaz de usuario](#la-interfaz-de-usuario)
- [Arquitectura X11](#arquitectura-x11)
    - [X.Org](#xorg)
    - [Wayland](#wayland)
- [Gestión de la GUI](#gestión-de-la-gui)
    - [Propiedades estándar](#propiedades-estándar)
- [El sistema de inicio de sesión](#el-sistema-de-inicio-de-sesión)
- [Gestores de escritorio](#gestores-de-escritorio)
    - [GNOME](#gnome)
    - [KDE Plasma](#kde-plasma)
    - [Cinnamon](#cinnamon)
    - [MATE](#mate)
    - [Xfce](#xfce)
- [Acceso remoto con X11](#acceso-remoto-con-x11)
    - [Conexiones X11 remotas](#conexiones-x11-remotas)
    - [Conexiones X11 tunelizadas](#conexiones-x11-tunelizadas)
- [Software de acceso remoto](#software-de-acceso-remoto)
    - [VNC](#vnc)
    - [Xrdp](#xrdp)
    - [NX](#nx)
    - [SPICE](#spice)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introducción

Una interfaz gráfica de usuario (GUI en inglés), está formada por una serie de aplicaciones que permite al usuario interactuar con el sistema a través de iconos, ventanas y otros elementos visuales. A pesar de que seamos capaces de administrar nuestro sistema a través de la línea de comandos, debemos poseer conocimientos de uso de diferentes GUI's para poder administrarlas y gestionar su seguridad, por ejemplo.

## La interfaz de usuario

El escritorio que visualizamos en nuestra pantalla es tan solo una de las piezas que componen el puzzle que conforma la intefaz de usuario. Tal y como se aprecia en el siguiente esquema, el gestor de ventanas es un intermediario en dicho escenario, y se encarga de la comunicación con el servidor gráfico. Cada gestor de escritorio tiene su gestor de ventanas por defecto, como Mutter, Muffin, Marco o Metacity.

El servidor gráfico es un programa que hace uso de un protocolo de comunicación para enviar las peticiones de la UI al sistema operativo, y viceversa. Este protocolo es capaz de funcionar sobre la red de datos.

Una pieza que forma parte del servidor, es el *compositor*. Se encarga de gestionar diferentes elementos dentro de una ventana para enviar la imagen generada al cliente.

## Arquitectura X11

El *sistema X Window* (o simplemente *X*) es el servidor gráfico que se utiliza en Linux. Aunque fue desarrollado en los año 80, su uso llega hasta nuestros días.

Inicialmente, solo existía un paquete que implementara las X; XFree86. Fue en el 2004 cuando debido a un cambio en su licencia, varias distribuciones Linux migraron a la implementación ofrecida por la fundación X.org. El servidor X.Org implementa la versión 11 del estándar X Window, incluyendo el formato de los ficheros de configuración del paquete original XFree86. Sin embargo, algunas distribuciones crearon su propia adaptación del servidor X.Org, lo que originó multitud de nombres, como X.org-X11, X, X11 y X.Org Server.

Recientemente, se está extendiendo el uso de un nuevo servidor, llamado *Wayland*. Este paquete proporciona diferentes características avanzadas que ofrecen soporte para nuevo hardware e implementa medidas de seguridad. Wayland sigue ganando seguidores en el mundo Linux y está llamado a convertirse en el servidor por defecto en la mayoría de distribuciones Linux.

### X.Org

El paquete *X.Org* mantiene información en un fichero de configuración de la tarjeta gráfica, pantalla y dispositivos de entrada, haciendo uso del formato XFree86. El fichero principal de configuración es `/etc/X11/xorg.conf`. En la actualidad no se suele hacer uso de este fichero; en su lugar, los dispositivos y aplicaciones guardan sus propios ficheros en el directorio `/etc/X11/xorg.conf.d`. No debe sorprendernos que no seamos capaces de localizar dichos ficheros, ya que el servidor X.Org puede crearlos en tiempo de ejecución, una vez que se inicia.

En ocasiones puede que la detección automática del hardware puede fallar y es necesario realizar ajustes en la configuración X11. Para poder crear un fichero de configuración de forma manual, en primer lugar debemos detener el servidor X con el comando `telinit 3`, y a través de los permisos de administrador crear el fichero con el comando `Xorg -configure`. El nuevo fichero `xorg.conf.new` se guarda en el directorio actual, por lo que debemos realizar las acciones necesarias para copiarlo en el directorio correcto.

El fichero `xorg.conf` tiene diferentes secciones con información de configuración:

* Input Device: Configura el teclado y ratón
* Monitor: Aplica la configuración de la pantalla
* Modes: Define los modos de vídeo
* Device: Configura la tarjeta gráfica
* Screen: Aplica la resolución y la profundidad de color de la pantalla
* Module: Incluye los módulos que deben cargarse
* Files: Configura las rutas para fuentes, módulos y ficheros de idioma para el teclado
* Server Flags: Configura opciones globales
* Server Layout: Enlaza todos los dispositivos de entra y salida de las sesiones

Debemos tener en cuenta que la mayoría de los gestores de escritorio incluyen aplicaciones para poder modificar los parámetros anteriores a través de cuadros de diálogo.

Si se han producido errores durante su funcionamiento, el servidor X.Org crea el fichero `.xsession-errors` en el directorio personal del usuario. A mayores, disponemos de dos utilidades que nos pueden ayudar; `xdpyinfo` y `xwininfo`. El comando `xdpyinfo` muestra información del servidor X.Org, como los diferentes tipos de pantalla disponibles, los valores por defecto de los parámetros de comunicación, etc. La utilidad `xwininfo` proporciona información de una ventana. Sin opciones, muestra una utilidad interactiva para que hagamos clic en una ventana determinada. Una vez hecho el clic, nos muestra diferente información sobre dicha ventana, como por ejemplo sus dimensiones.

> Si estamos usando Wayland, el comando `xwininfo` no funcionará.

> En una sesión X.Org, es posible reiniciar el servidor a través del atajo de teclado CTRL+ALT+BACKSPACE.

### Wayland

[Wayland](https://wayland.freedesktop.org/) viene a sustituír a X.Org, y ha sido diseñado para ser más simple, más seguro y más sencillo de desarrollar y mantener. Bajo Wayland podemos encontrar el *compositor*, el gestor de ventanas y el propio servidor gráfico.

El protocolo Wayland fue presentado en el año 2009, y hoy se utiliza en diferentes gestores de escritorio como GNOME Shell y KDE Plasma.

El compositor de Wayland se llama *Weston*, aunque tan solo proporciona una funcionalidad básica. Fue creado para que sirva como referencia en la creación de otros compositores, por lo que se puede entender como un ejemplo para nuevas implementaciones. De todo esto, se deduce que se puede *sobreescribir* con otros compositores, que es precisamente lo que suelen hacer los gestores de escritorio, por ejemplo, Kwin y Mutter gestionan sus propias tareas de compositor.

> Si queremos ejecutar aplicaciones desarrolladas para X11 que no tengan soporte para Wayland, podemos hacerlo a través de XWayland, que está disponible en el paquete Weston.

## Gestión de la GUI

Algunos sistemas operativos poseen una GUI con pocas opciones de personalización. No es el caso de Linux, donde las GUI's permiten un alto de grado de configuración. A continuación se hará un repaso por aquellos componentes de un entorno GUI estándar.

### Propiedades estándar

Una GUI está formada por una serie de componentes que operan en conjunto para ofrecer la capacidad gráfica de la UI. Uno de dichos componentes en el entorno de escritorio, que proporciona un diseño para la GUI. Está formado por las siguientes secciones y funciones:

* Ajustes del escritorio. Está formado por diferentes aplicaciones que permiten cambios en el entorno del escritorio.
* Gestor de visualización. Si en nuestro sistema disponemos de diferentes gestores de escritorio, en la ventana de inicio de sesión podremos seleccionar uno de ellos a través del gestor de visualización.
* Gestor de ficheros. Esta aplicación permite una gestión básica de los ficheros a través de elementos gráficos.
* Iconos. Un icono es una representación gráfica de un fichero o programa.
* Barra de favoritos. Esta área de la ventana contiene los iconos de uso más frecuente. Pueden añadirse y eliminarse de forma manual, o bien de modo automático por el sistema según se vayan usando.
* Lanzador. Este programa permite la búsqueda de ficheros y programas.
* Menús. Esta área será accesible a través de un icono, y suele contener una lista de ficheros y/o programas.
* Paneles. Los paneles son áreas rectangulares que se sitúan en la parte superior y/o inferior de la ventana principal, y suelen mostrar la hora del sistema o el área de notificaciones.
* Bandeja del sistema. Suele ser un menú especial que se sitúa en un panel y que proporciona acceso al cierre de sesión, apagado, ajustes de sonido, etc.
* Widgets. Son elementos que proporcionan funcionalidades en el escritorio, como por ejemplo un gestor de notas.
* Gestor de ventanas. Gestiona el modo en el que se presentan las ventanas en el escritorio. 

## El sistema de inicio de sesión

Por defecto, cuando un sistema Linux se inicia, aparece una consola de texto para poder identificarse. Esto es así en servidores, pero en equipos de escritorio, los usuarios prefieren un entorno gráfico donde poder identificarse.

El gestor de visualización (*display manager*) es el responsable de esta funcionalidad. Toda distribución incorpora un paquete que se ejecuta durante el arranque para mostrar esta ventana de inicio de sesión. Dicho gestor hace uso del protocolo XDCMP (*X Display Manager Control Protocol*) para la gestión de dicho proceso.

El paquete XDM (*X Display Manager*) es el gestor que incorpora Linux de forma básica. Suele mostrar un nombre de usuario por defecto para que pueda introducir su contraseña, y una vez autorizado por el sistema, se iniciará el servidor X junto con su entorno de escritorio seleccionado.

Este gestor puede ser adaptado a través del fichero de configuración `/etc/X11/xdm/xdm-config`.

La mayoría de los gestores de ventanas incorporan su propio gestor de visualizacion (*display manager*) que aumenta las capacidades del paquete XDM. Algunos de los más populares, son los siguientes:

* KDM. El *display manager* usado en el entorno KDE.
* GDM. Usado en el entorno GNOME.
* LightDM. Utilizado en escritorios ligeros, como Xfce.

## Gestores de escritorio

A diferencia de otros sistemas operativos, Linux ofrece una amplia variedad de entornos de escritorio. Aunque las distribuciones suelen incorporar tan solo un entorno, es posible la instalación de entornos adicionales en nuestro sistema.

A continuación se muestran algunos de los más usados en las distribuciones Linux más importantes.

### GNOME

El entorno de escritorio GNOME fue creado en los años 90, y desde entonces se hizo muy popular entre distribuciones importantes como CentOS o Ubuntu. En el año 2011 se produce un cambio radical en el diseño del entorno, naciendo la versión 3, también conocida como *GNOME Shell*. Dichos cambios no fueron recibidos de igual forma por todos los usuarios, y debido al rechazo de parte de ellos han surgido diferentes subproyectos.

### KDE Plasma

El entorno *Kool Desktop Environment* (KDE) fue presentado en el año 1996, y publicada su primera versión en 1998. KDE posee una amplia variedad de proyectos de aplicaciones alrededor del propio escritorio. En el año 2009, el entorno de escritorio se convierte en KDE Plasma.

### Cinnamon

Este entorno de escritorio fue presentado en el año 2011, cuando muchos usuarios reaccionaron en contra de la versión 3 de GNOME. Los desarrolladores de Linux Mint crearon Cinnamon como un *fork* de GNOME 3.

### MATE

Este entorno de escritorio también se inicia en el año 2011, tras la publicación de GNOME 3. Fue creada por un usuario de Arch Linux que reside en Argentina.  Para los usuarios más acostumbrados a entornos como GNOME 2, MATE les resultará muy familiar y con una transición cómoda.

### Xfce

Una de las principales características de todo gestor de escritorio es que requiere de elevados recursos hardware, tanto CPU como memoria RAM. Linux se suele adaptar a todo tipo de equipos informáticos, incluyendo aquellos que ya no soportan otros sistemas operativos, como Windows o MacOS. Como suele ocurrir en los entornos libres, Linux ofrece una amplia oferta de escritorios ligeros, que pueden ejecutarse en equipos con recursos reducidos.

El escritorio Xfce fue desarrollado en 1996 como una extensión de CDE (Common Desktop Environment) haciendo uso del conjunto de herramientas XForms (de ahí el origen de «Xf» en su nombre).

## Acceso remoto con X11

El sistema X11 utiliza un sistema cliente/servidor clásico, y en la mayoría de las ocasiones tanto el cliente como el servidor se ejecutarán en el mismo equipo, pero no tiene porque ser así. Hay diferentes técnicas para implementar conexiones remotas a entornos de escritorio X11.

### Conexiones X11 remotas

El método más sencillo para ejecutar un escritorio X11 remoto es hacer uso del propio protocolo a través de la red hacia el cliente remoto. Por ejemplo, en el siguiente escenario se contempla que el equipo `ws1` está ubicado en un red remota, pero necesitamos ejecutar una aplicación gráfica haciendo uso de nuestro equipo local, `ws2`. Debemos seguir los siguientes pasos:

1. Iniciamos sesión en `ws2` a través del gestor de escritorio disponible.
2. Arrancamos una sesión de terminal.
3. Ejecutamos `xhost +ws1`, donde `ws1` puede el nombre de host o dirección IP del equipo remoto. El comando `xhost` permite la recepción de datos desde el equipo remoto hacia nuestro host.
4. Iniciamos sesión SSH en el host `ws1`.
5. En la línea de comandos, ejecutamos `export DISPLAY=ws2:0:0`, donde `ws2` es el nombre de host o dirección IP de nuestro equipo local. Este comando redirige cualquier salida gráfica en `ws1` hacia el servidor X de `ws2`.
6. Ejecutamos cualqueir aplicación gráfica en `ws1`, que debe aparecer en una nueva ventana del host `ws1`.

> El comando `xhost` hace uso de los puertos 6000-6063, por lo que debemos tenerlo en cuenta a la hora de ser filtrado en algún cortafuegos.

### Conexiones X11 tunelizadas

Otro método que proporciona conexiones GUI remotas a través de un túnel seguro es *X11 forwarding*.

En primer lugar debemos comprobar que se permita *X11 forwarding* en nuestro sistema, a través del fichero de configuración de openSSH, `/etc/ssh/sshd_config`. La directiva `X11Forwarding` debe establecerse a `yes` en el fichero de configuración del equipo remoto.

Después de realizar las modificaciones anteriores, ejecutamos el comando `ssh -X usuario@host-remoto`.

## Software de acceso remoto

Aunque la opción de hacer uso de las capacidades de X11 están disponibles, pueden resultar complejas de poner en marcha en algunos entornos. Como alternativa, disponemos de aplicaciones de acceso remoto que evitan realizar las tareas de configuración más complejas.

Este software hace uso del modelo cliente/servidor, y todo lo que debemos hacer es instalar el cliente en nuestro equipo local, y el servidor en el equipo remoto.

### VNC

*Virtual Network Computing* (VNC) fue desarrollado por Olivetti & Oracle Research Lab. Algunos de los empleados de la compañía desaparecida trabajan en RealVNC. El software VNC es multiplataforma y hace uso del protocolo Remote Frame Buffer (RFB). Con este protocolo es posible el envío de comandos GUI, como clics de ratón, hacia el servidor. El servidor devuelve *frames* de escritorio de vuelta hacia el monitor del cliente.

El servidor VNC ofrece un servicio GUI en el puerto `5900+n`, donde `n` equivale al número de pantalla. Desde la línea de comandos, el cliente VNC apunta al servidor a través de su nombre de host y puerto TCP.

El servidor VNC también permite la conexión a través de un navegador web con capacidad Java. En este caso, la conexión se realiza a través del puerto `5800+n`.

Algunas de las ventajas del uso de VNC:

* Es flexible a la hora de usar escritorios remotos.
* Es posible la conexión por parte de usuarios simultáneos.
* Están disponibles escritorios tanto estáticos como persistentes.
* Se puede establecer un túnel SSH para cifrar el tráfico.

Y, algunas desventajas:

* El servidor VNC tan solo gestiona movimientos del ratón y pulsaciones de teclado. No permite el envío de fichero ni gestión de audio o impresión.
* Por defecto, no incorpora el cifrado del tráfico y es necesario hacer uso de soluciones adicionales.
* La contraseña del servidor se almacena en un fichero en texto plano.

TigerVNC es una de las implementaciones de VNC más populares en Linux. Al estar disponible también en Windows, es posible la conexión remota a un sistema Linux o Windows.

### Xrdp

Xrdp es una alternativa a VNC. Soporta el protocolo RDP y usa X11rdp o Xvnc para la gestión de la sesión GUI.

Xrdp tan solo dispone de la parte de servidor en una conexión RDP. Permite el acceso desde diferentes clientes RDP, como `rdesktop`, `FreeRDP` y `Microsoft Remote Desktop Connection`.

El nombre del paquete en Linux es `xrdp` y está dispnible en un sistema *systemd*. Puede que no se encuentre disponible en los repositorios estándar de nuestra distribución preferida.

Algunos de los beneficios de Xrdp:

* Al hacer uso de RDP, cifra el tráfico a través de TLS.
* Disponibilidad de una gran variedad de clientes RDP con licencia libre.
* Podemos conectar a una conexión existente para conseguir un escritorio persistente.

En el fichero `/etc/xrdp/xrdp.ini` se pueden modificar algunos de los parámetros de Xrdp. Un ajuste importante es la directiva `security_layer`. Si lo hemos establecido a `negotiate`, el servidor Xrdp negociará con el cliente el método de seguridad a emplear. Tenemos tres métodos disponibles:

* `tls`. Ofrece un cifrado SSL (TLS 1.0) para la autenticación en el servidor y la transferencia de datos.
* `negotiate`. Establece el método de seguridad al más elevado que puede soportar el cliente. Esto puede ser un problema si la conexión se realiza a través de una red pública y el cliente debe usar el método de seguridad estándar RDP.
* `Rdp`. Establece el método de seguridad al estándar RDP. No es seguro ante ataques de *man-in-the-middle*.

### NX

El protocolo NX fue creado por la compañía NoMachine en el año 2001. A pesar de que la versión 3.5 fue liberada con una licencia libre, la versión 4 se ha convertido en privativa. Sin embargo, existen versiones libres disponbiles que se basan en la tecnología NX3, como *FreeNX* y *X2Go*. Están disponibles en diferentes distribuciones Linux, aunque puede que no la hagan en los repositorios por defecto.

Algunos beneficios del uso de productos NX:

* Proporcionan un excelente tiempo de respuesta, incluso en conexiones de baja capacidad con problemas de latencia.
* Son más rápidos que la soluciones basadas en VNC.
* Al usar túneles SSH por defecto, el tráfico está cifrado.
* Soporta varios usuarios simultáneos a través de un único puerto de red.

### SPICE

Otro protocolo para conexiones remotas es *Simple Protocol for Independent Computing Environments* (SPICE). Originalmente fue desarrollado por Qumranet como software privativo, aunque con la compra de esta compañía por parte de Red Hat se ha liberado bajo una licencia libre.

SPICE fue desarrollado para proporcionar conexiones remotas a diferentes máquinas virtuales. En la actualidad, Spice se usa para realizar conexiones con máquinas virtuales KVM.

Spice es independiente de la plataforma y tiene algunas características destacables:

* La parte cliente de Spice usa múltiples conexiones a través de sockets, por lo que es posible disponer de varios clientes.
* La experiencia es similar a una conexión local.
* Spice presenta un bajo consumo de CPU, por lo que es posible disponer de varios servidores con varias máquinas virtuales.
* Spice permite un alto rendimiento en el envío de vídeo de alta calidad.
* Spice soporta migraciones en caliente, por lo que no tendremos interrupciones si la máquina virtual se mueve a un nuevo servidor.

Otro beneficio del uso de Spice son sus características en cuestión de seguridad. Por ejemplo, la autenticación se realiza a través de SASL (con soporte para Kerberos), y los datos se transfieren sobre una capa TLS.
