# Tema 25 - Localización e internacionalización

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Presentación](#presentación)
- [Conjuntos de caracteres](#conjuntos-de-caracteres)
    - [Variables de entorno](#variables-de-entorno)
- [Definición de la localización](#definición-de-la-localización)
    - [Instalación del sistema](#instalación-del-sistema)
- [Modificación de la localización](#modificación-de-la-localización)
    - [Cambio manual de las variables de entorno](#cambio-manual-de-las-variables-de-entorno)
    - [El comando localectl](#el-comando-localectl)
- [Hora y fecha en el sistema](#hora-y-fecha-en-el-sistema)
    - [Zonas horarias](#zonas-horarias)
    - [Ajustando la fecha y la hora](#ajustando-la-fecha-y-la-hora)
        - [Comandos en desuso](#comandos-en-desuso)
        - [El comando timedatectl](#el-comando-timedatectl)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Presentación

En el mundo existen diferentes idiomas. No solo es una cuestión de idiomas, en el que algunos países pueden usar más de uno, sino que también sus habitantes usan diferentes normas para representar los valores numéricos, las monedas y la fecha y hora. Para poder usar nuestro sistema Linux en diferentes países debemos adaptar la configuración a dichas configuraciones regionales.

## Conjuntos de caracteres

Aunque en el interior todos los sistemas informáticos trabajan con ceros y unos, es necesario que pueda comunicarse con los usuarios a través de su idiomas, y es aquí donde entran en juego los juegos de caracteres.

Un *conjunto de caracteres* define un método para interpretar y mostrar los diferentes caracteres en un idioma. Existen diferentes conjuntos para representar dichos caracteres, y en Linux los más frecuentes son los siguientes:

* *ASCII*. El *American Standard Code for Information Interchange* usa 7 bits para representar los caracteres del Inglés.
* *ISO-8859*. La *International Organization for Standardization* (ISO) junto con la *International Electrotechnical Commision* (IEC) han trabajado en la definición de diferentes códigos para la representación de los caracteres internacionales. Existen 15 estándares (desde ISO-8859-1 hasta ISO-8859-15) para la definición de conjuntos de caracteres.
* *Unicode*. El *Unicode Consortium*, formado por diferentes compañias del sector informático, ha creado un estándar internacional que usa un código de 3 bytes capaz de representar cualquier caracter conocido de cualquier país del mundo.
* *UTF*. El *Unicode Transformation Format* (UTF) transforma el valor Unicode en códigos simplificados de 1 byte (UTF-8) o 2 bytes (UTF-16). Dentro de países anglosajones, UTF-8 está reemplazando al estándar ASCII.

Cuando decidamos el uso de un conjunto de caracteres determinado en nuestro sistema Linux, tenemos que ser capaces de configurarlo correctamente, que es lo que veremos en la siguiente sección.

### Variables de entorno

Linux almacena la información de localización en diferentes variables de entorno. Las aplicaciones que necesitan conocer la localización deben obtener los datos necesarios desde diferentes variables de entorno para saber los caracteres a usar.

El comando `locale` nos ayudará a visualizar dichas variables de entorno.

```
$ locale
LANG=gl_ES.UTF-8
LANGUAGE=
LC_CTYPE="gl_ES.UTF-8"
LC_NUMERIC="gl_ES.UTF-8"
LC_TIME="gl_ES.UTF-8"
LC_COLLATE="gl_ES.UTF-8"
LC_MONETARY="gl_ES.UTF-8"
LC_MESSAGES="gl_ES.UTF-8"
LC_PAPER="gl_ES.UTF-8"
LC_NAME="gl_ES.UTF-8"
LC_ADDRESS="gl_ES.UTF-8"
LC_TELEPHONE="gl_ES.UTF-8"
LC_MEASUREMENT="gl_ES.UTF-8"
LC_IDENTIFICATION="gl_ES.UTF-8"
LC_ALL=
```

En el ejemplo anterior, el sistema está configurado para Galego, haciendo uso de UTF-8 para almacenar los caracteres.

Las variables de entorno `LC_` representan una categoría de variables que tienen relación con la configuración local. Para visualizar los valores concretos de las variables de un grupo, debemos usar la opción `-ck` junto con el nombre de dicha categoría.

```
$ locale -ck LC_MONETARY
LC_MONETARY
int_curr_symbol="EUR "
currency_symbol="€"
mon_decimal_point=","
mon_thousands_sep="."
mon_grouping=3;3
positive_sign=""
negative_sign="-"
[...]
```

## Definición de la localización

Linux gestiona la localización a través de tres componentes; el idioma, el país y el conjunto de caracteres. Existen diferentes métodos para modificar estos parámetros.

### Instalación del sistema

En el momento de realizar una nueva instalación de un sistema Linux, tendremos que seleccionar el idioma por defecto. En ese momento, se establecerán las variables de entorno apropiadas para el país e idioma escogido.

## Modificación de la localización

Una vez instalado nuestro sistema, es posible modificar los valores iniciales. Podemos establecer nuevos valores de forma manual a las variables de entorno tipo `LC_`, o bien hacerlo a través del comando `localectl`.

### Cambio manual de las variables de entorno

Para realizar este cambio en el valor de una variable de entorno, lo haremos a través del comando `export` (igual que con cualquier otra variable):

	$ export LC_MONETARY=en_GB.UTF-8

Este cambio es válido para realizar cambios de forma individual, pero resultará complejo si deseamos realizar un cambio completo de localización en nuestro sistema. La variable de entorno `LANG` las controla todas desde un único lugar:

```
$ export LANG=en_GB.UTF-8
$ locale
LANG=en_GB.UTF-8
LC_CTYPE="en_GB.UTF-8"
LC_NUMERIC="en_GB.UTF-8"
LC_TIME="en_GB.UTF-8"
LC_COLLATE="en_GB.UTF-8"
LC_MONETARY="en_GB.UTF-8"
LC_MESSAGES="en_GB.UTF-8"
LC_PAPER="en_GB.UTF-8"
LC_NAME="en_GB.UTF-8"
LC_ADDRESS="en_GB.UTF-8"
LC_TELEPHONE="en_GB.UTF-8"
LC_MEASUREMENT="en_GB.UTF-8"
LC_IDENTIFICATION="en_GB.UTF-8"
LC_ALL=
```

En algunos sistema Linux también será necesaria la actualización de la variable de entorno `LC_ALL`.

> Por supuesto, este cambio no es permanente y solo estará activo durante la sesión actual. Para hacerlo permanente debemos realizar los cambios necesarios en el fichero `.bashrc`.

### El comando localectl

Si nuestra distribución hace uso de *systemd*, entonces podemos hacer uso del comando `localectl`. Por defecto, este comando muestra la configuración actual.

```
$ localectl
   System Locale: LANG=gl_ES.UTF-8
       VC Keymap: n/a
      X11 Layout: es
       X11 Model: pc105
```

El comando `localectl` soporta diferentes opciones, pero laa más comunes sona las que muestran todos los *locales* instalados en nuestro sistema (`list-locales`), y la que modifica el valor actual (`set-locale`).

	$ localectl set-locale LANG=es_GB.UTF-8

> Si es preciso realizar una conversión del juego de caracteres de un fichero, podemos usar el comando `iconv`.

## Hora y fecha en el sistema

La fecha y la hora son cruciales en un sistema Linux para el correcto funcionamiento del mismo. Por ejemplo, es esencial para el seguimiento de los procesos, determinar el inicio/parada de los trabajos y registrar los diferentes eventos que se producen en el sistema. Por lo tanto, es imprescindible que nuestro sistema se encuentre bien sincronizado con la fecha y hora oficial.

### Zonas horarias

Cada país establece una (o varias) zonas horarias, que se basan en la referencia UTC. La mayoría de distribuciones basadas en Linux definen la zona horaria local en el fichero `/etc/timezone`, mientras que los sistemas basados en Red-Hat lo hacen en `/etc/localtime`. Al no ser ficheros en texto plano, no podemos editarlos. En su lugar, debemos copiar una de las plantillas disponibles en el directorio `/usr/share/zoneinfo`.

Para conocer la configuración actual de la zona horaria de nuestro sistema Linux, usaremos el comando `date`, sin opciones:

	$ date
	Dom 1 Ene 2020 12:15:36 CET

La zona horaria se visualiza al final de la salida.

Para modificar la zona horaria en Linux, copiamos (o enlazamos) el fichero apropiado del directorio `/usr/share/zoneinfo` hacia los destinos `/etc/timezone` o `/etc/localtime`. El directorio `/usr/share/zoneinfo` está dividido en subcarpetas basadas en la localización.

> Si desconocemos el nombre exacto de nuestra zona horaria, podemos hacer uso del comando `tzselect`.

Antes de realizar la copia del nuevo fichero de la zona horaria, es necesario eliminar el fichero original:

	$ sudo mv /etc/localtime /etc/localtime.bak
	$ sudo ln -s /usr/share/zoneinfo/US/Pacific /etc/localtime

> Los cambios anteriores afectan al sistema más allá de la sesión actual. En otro caso, debemos hacer uso de la variable `TZ`, que sobreescribe la zona horaria en la sesión actual.

### Ajustando la fecha y la hora

Después de ajustar nuestra zona horaria, es el momento de configurar los valores de fecha y hora correctos.

#### Comandos en desuso

En todas las distribuciones Linux encontramos los siguientes comandos para poder operar con valores de fecha y hora:

* `hwclock`. Muestra o ajusta la hora con el valor del reloj de la BIOS interna o UEFI.
* `date`. Muestra o configura la fecha gestionada por el sistema.

#### El comando timedatectl

En caso de que nuestro sistema haga uso de *systemd*, podremos hacer uso del comando `timedatectl` para la gestión de la fecha y hora de nuestro equipo.

```
$ timedatectl
               Local time: Dom 2021-01-17 12:34:54 CET
           Universal time: Dom 2021-01-17 11:34:54 UTC
                 RTC time: Dom 2021-01-17 11:34:54    
                Time zone: Europe/Madrid (CET, +0100) 
System clock synchronized: yes                        
              NTP service: active                     
          RTC in local TZ: no
```

Con la opción `set-time` podemos ajustar cualquiera de los parámetros:

	$ sudo timedatectl set-time "2019-08-02 06:15:00"

Por supuesto, también podemos sincronizar el reloj hardware de nuestro equipo con el del sistema operativo Linux.

> La mayoría de los sistema conectados a Internet hacen uso del protocolo NTP (Network Time Protocol), gracias al que estará sincronizado con un servidor central.

