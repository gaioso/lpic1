# Tema 28 - Hora del sistema

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Introducción](#introducción)
- [Hora y relojes en Linux](#hora-y-relojes-en-linux)
- [La utilidad date](#la-utilidad-date)
- [La utilidad timedatectl](#la-utilidad-timedatectl)
- [Sincronización de la hora](#sincronización-de-la-hora)
- [Uso del demonio chrony](#uso-del-demonio-chrony)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Conocer como gestiona Linux la fecha y la hora
* Ser capaz de mantener la hora en un ordenador con NTP 

## Introducción

El concepto de *tiempo* es un poco complejo. Nuestra hora *civil*, la hora que usan los trenes, buses y aviones, y que regula la programación de la televisión, deriva de dos fuentes. Los relojes del mundo funcionan siguiendo el *Coordinated Universal Time* (UTC), típicamente con un desplazamiento para la zona horaria (CET se calcula añadiendo una hora a UTC). La segunda fuente son los relojes atómicos, de los cuales existen unos 300 repartidos por más de 50 países.

Por supuesto, el mantenimiento de los relojes es una de las tareas cruciales en cualquier servidor. Algunas aplicaciones deben ejecutarse en un determinado momento, servicios remotos espera que tengamos la hora correctamente sincronizada y el registro de eventos debe guardarse con un sello temporal.

## Hora y relojes en Linux

Cada PC posee un reloj CMOS alimentado por una batería, que es configurado a través de la BIOS, y que sigue funcionando a pesar de que el equipo esté apagado. Linux utiliza este reloj sólo durante el arranque del sistema para configurar el reloj interno del kernel. El reloj del kernel cuenta el tiempo en segundos desde el 1 de enero de 1970, 00:00 UTC. Cuando se arranca el sistema, la fecha y la hora actual se leen del reloj CMOS, convertido a tiempo kernel, y se usa para inicializar el reloj del kernel. Esto es ejecutado por el programa `hwclock`. El sistema necesita saber si el CMOS está configurado en UTC o en la zona horaria local; lo último es necesario saberlo por el bien de otros sistemas operativos presentes en el equipo.

	# hwclock --hctosys -u		reloj CMOS en UTC
	# hwclock --hctosys		reloj CMOS en hora local

Otras opciones de uso frecuente de `hwclock` son las siguientes:

Opción corta | Opción larga | Descripción
N/A | --localtime | Configura el reloj hardware para que se use la hora local estándar
-r | --show | Muestra la hora actual del reloj hardware
-s | --hctosys | Lee la hora actual del reloj hardware, y aplica ese valor al reloj software
-u | --utc | Configura el reloj hardware para usar el estándar UTC
-w | --systohc | Lee la hora actual del reloj software, y la aplica al reloj hardware

El reloj del kernel, se usa, por ejemplo para las marcas de tiempo del sistema de ficheros. Podemos consultarla a través del comando `date`, mientras que `hwclock` sin argumentos nos mostrará la hora de acuerdo con el reloj CMOS.

> En los equipos de 32 bits, (2e32 -1), el último segundo que puede ser representado será a las 3 horas, 14 minutos y 7 segundos del 19 de junio de 2038. Para ese momento todo el que no utilice un equipo de 64 bits, tendrá que usar un kernel modificado.

La zona horaria es configurada en `/etc/timezone`; que contiene una entrada como «Europe/Madrid» referiendose a un fichero situado en `/usr/share/zoneinfo`. Este fichero contiene datos de la zona horaria, tales como el desplazamiento de UTC, horario de verano, etc. `/etc/localtime` es un enlace simbólico al fichero `/etc/timezone`. Los usuarios pueden establecer una zona horaria diferente a través de la variable de entorno *TZ*.

> Linux proporciona varias herramientas para gestionar los ficheros de zonas horarias. El compilador de zona horaria (`zic`) permite crear sus propios ficheros de zona horaria y los convierte al formato requerido por la librería C; `zdump` muestra un fichero de zona horaria (o la mayoría de su contenido) en un formato legible.

## La utilidad date

El reloj software hace uso de los comandos `date` y `timedatectl` para su consulta y/o configuración. Para consultar la hora actual del sistema, debemos escribir el comando `date` sin opciones y sin necesidad de privilegios especiales.

	$ date
	Fri May 31 12:34:23 CET 2020

Para configurar la hora del sistema con el comando `date` tendremos que realizar un esfuerzo mayor. Si trabajamos en la hora local, no son necesarias opciones adicionales, pero si queremos trabajar en UTC, entonces debemos usar las opciones `u`, `--utc` o `--universal`.

> Si nuestro sistema hace uso de NTP no será posible el ajuste con el comando `date`. Para comprobar si se encuentra activo este servicio, podemos comprobar la salida de los siguientes comandos; `systemctl status ntpd`, `systemctl status chronyd` o `systemctl status systemd-timesyncd`, y verificar que ninguno se encuentra activo para así poder hacer uso del comando `date`.

Un ejemplo del cambio de la hora del sistema en CentOS lo observamos en la siguiente instrucción:

```
# date
Fri May 31 14:26:48 EDT 2019
#
# date 05301430
Thu May 30 14:30:00 EDT 2019
#
# date
Thu May 30 14:30:02 EDT 2019
```

> El programa GNU date permite usar un modo más intuitivo para configurar el reloj:<br>
	# date --set="2009-02-03 18:01:30 +0100"
Desde el entorno gráfico podremos configurar el reloj de diferentes formas.

Para configurar el reloj CMOS mientras el sistema está corriendo, primero configuramos el reloj del kernel usando `date`. Luego podemos transferir la hora del kernel al reloj CMOS usando `hwclock --systohc`. También podemos usar la opción `--date` de `hwclock` para configurar el reloj CMOS directamente. En cualquier caso, `hwclock` intentará almacenar las desviaciones del reloj CMOS en `/etc/adjtime`. Los relojes CMOS, por regla general, son muy inexactos.

> Las distribuciones habituales de Linux transfieren la hora del reloj del kernel al reloj CMOS cuando el equipo se apaga. Esto está basado en la premisa de que la hora del kernel puede ser mantenida de forma más exacta usando métodos que veremos a continuación, mientras que el reloj CMOS se dejará llevar hacia quién sabe donde.

## La utilidad timedatectl

El comando `timedatectl` es el preferido para la gestión manual del reloj software. La ejecución del comando sin opciones proporciona una gran cantidad de información.

```
$ timedatectl 
               Local time: Sáb 2021-01-30 13:31:56 CET
           Universal time: Sáb 2021-01-30 12:31:56 UTC
                 RTC time: Sáb 2021-01-30 12:31:56    
                Time zone: Europe/Madrid (CET, +0100) 
System clock synchronized: yes                        
              NTP service: active                     
          RTC in local TZ: no 
```

La salida anterior fue ejecutada en una distribución Ubuntu 20.04, y puede ser diferente en otras distribuciones.

Para realizar cambios en la hora del sistema, debemos ejecutar el comando anterior con privilegios de administrador y hacer uso de la opción `set-time` con la siguiente sintaxis:

	timedatectl set-time "YYYY-MM-DD HH:MM:SS"

Los siguientes comandos se han ejecutado en una distribución CentOS:

```
# date
Sat Jan 30 12:36:00 UTC 2021
# timedatectl set-time "2019-05-31 16:15:00"
Failed to set time: NTP unit is active
# timedatectl set-ntp 0
# timedatectl set-time "2019-05-31 16:15:00"
# date
Fri May 31 16:15:02 UTC 2019
```

El primer intento de cambio ha fallado porque estaba habilitada la sincronización automática, que se ha deshabilitado con el comando `timedatectl set-ntp 0`. Se puede activar de nuevo con el uso del valor `1` en lugar de `0`.

## Sincronización de la hora

Es muy importante cuando estamos trabajando en una red local que todos los equipos tengan la misma hora que el servidor, sobre todo si hacemos uso de NFS, o autenticación como Kerberos. Por lo tanto, parece interesante sincronizar los relojes de todos los equipos, y sobre todo si se realiza de forma automática. Pero aún tendrá más sentido sincronizar los relojes de todos los equipos no sólo con otro de ellos, sino con un elemento externo basado en un reloj atómico. Podríamos usar una señal de radio, o bien hacer uso de servidores de tiempo accesibles a través de Internet. Existen diversos servidores públicos de hora ofrecidos por universidades o ISP's.

NTP hace uso de un esquema que se conoce como *clock stratum*. Los *stratums* se numeran del 0 al 15, siendo los etiquetados como 0 aquellos que poseen la precisión mayor, y suelen ser relojes atómicos. En el siguiente nivel, el 1, se situan aquellos dispositivos conectados directamente a los relojes anteriores, y así continuamos escalando en los siguientes niveles. Como la información que parte del nivel 0 debe viajar a través de los siguientes niveles como tráfico de red, el protocolo NTP deberá ser capaz de corregir las latencias que se generan en dichas redes. 

El demonio del protocolo NTP se llama *ntpd*, Y puede actuar como cliente y comunicarse via NTP con servidores de hora, o actuar como servidor y enviar la hora sincronizada a otros equipos. El demonio *ntpd* está configurado a través del fichero `/etc/ntp.conf`, que puede contener algo similar a las siguientes líneas:

```
driftfile /var/lib/ntp/ntp.drift

# By default, exchange time with everybody, but don't allow configuration.
restrict -4 default kod notrap nomodify nopeer noquery
restrict -6 default kod notrap nomodify nopeer noquery

# Local users may interrogate the ntp server more closely.
restrict 127.0.0.1
restrict ::1

# pool: <http://www.pool.ntp.org/join.html>
server 0.debian.pool.ntp.org iburst dynamic
```

> Encontrar un servidor de hora no es nada sencillo. Un servidor NTP con muchos clientes puede sobrecargar una red, o echar abajo al propio servidor. Por eso se han desarrollado las *NTP pool*, para aquellos equipos que no tienen acceso directo a servidores de tiempo.

> La *NTP pool* está formada por varios servidores de tiempo disponibles públicamente que son accedidos por los clientes a través de un esquema DNS tipo *round-robin*. Esto significa que una dirección como `0.pool.ntp.org` apunta de forma aleatoria a varios cientos de servidores de tiempo públicos en cualquier parte del mundo. Esto significa que los servidores se reparten la carga de trabajo.

> La opción «iburst» (ráfaga) en las directivas «server» provoca que si el servidor no responde, se le envían 8 paquetes en lugar de uno, con una separación de 2 segundos entre ellos.

En la práctica, podríamos especificar tres servidores de tiempo de NTP pool, como a continuación:

	server 0.pool.ntp.org
	server 1.pool.ntp.org
	server 2.pool.ntp.org

Podremos conseguir mejores resultados si nos concentramos en una zona geográfica.

	server 0.europe.pool.ntp.org
	server 0.de.pool.ntp.org

Si nuestro ISP ofrece un servidor de tiempo, podremos usarlo además de dos servidores de la lista.

Las versiones modernas de *ntpd* tienen soporte para la directiva *pool*, que permite el uso directo de *pools* NTP:

	pool es.pool.ntp.org

Pudiendo incluír más de una directiva de este tipo. Si estamos considerando sincronizar toda una red local, debemos configurar uno de los equipos para que se configure con el exterior, y luego el resto de equipos de nuestra red para que se sincronicen con este equipo.

Para poder usar un servidor NTP en nuestra red local, debemos considerar la presencia de más de un único servidor. Al menos dos:

	ntp1.example.com
	ntp2.example.com

Apuntando cada uno de ellos al otro del siguiente modo:

	peer ntp1.example.com   # en ntp1, al revés

Esto significa que los dos servidores se pueden sincronizar uno al otro, y en los clientes:

	server ntp1.example.com iburst
	server ntp2.example.com iburst

Cuando tengamos todo preparado para iniciar el demonio `ntpd`, debemos usar los privilegios del administrador para ejecutar el siguiente comando:

	systemctl start ntpd

Justo después de iniciarse, `ntpd` comenzará el proceso de sincronización de nuestro reloj de sistema. Debemos esperar de 10 a 15 minutos antes de comprobar los resultados a través del comando `ntpstat`.

```
$ ntpstat
synchronised to NTP server (147.156.7.50) at stratum 3 
   time correct to within 243 ms
   polling server every 64 s
```

Además de la comprobación realizada con el comando `ntpstat`, también es posible visualizar una tabla con los servidores que se están consultando y cuando se ha realizado la última sincronización. Esta información la proporciona el comando `ntpq -p`.

```
$ ntpq -p
     remote           refid      st t when poll reach   delay   offset  jitter
==============================================================================
 0.debian.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 1.debian.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 2.debian.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 3.debian.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
+sarah.vandalswe 46.243.26.34     2 u   49   64    7   36.906   -1.936   1.565
-elv06.icfo.es   150.214.94.5     2 u   47   64    7   40.362   -6.799   1.719
-dns01.masbytes. 162.159.200.123  4 u   50   64    7   49.864    1.295   1.087
+bjaaland.red.uv 147.156.1.135    2 u   48   64    7   26.959   -0.957   1.798
-scott.red.uv.es 66.78.223.230    2 u   46   64    7   27.502   -1.060   1.904
-shackleton.red. 66.78.223.230    2 u   46   64    7   27.621   -0.907   1.655
+213.251.52.234  195.66.241.10    2 u   48   64    7   19.395   -2.571   1.558
\*ntp.redimadrid. 193.147.107.33   2 u   47   64    7   19.712   -1.101   1.513
+time.cloudflare 10.40.9.201      3 u   49   64    7   19.113   -0.963   1.754
```

El primer carácter indica el estado del servidor remoto. Por ejemplo, el «\*» indica el servidor primario, el preferido actualmente, y el símobo «+», indica un buen servidor remoto, que podrá ser tenido en cuenta. La columna «refid» es la fuente de dicho servidor, de donde obtiene la hora. «st» es el *stratum* o la distancia al reloj atómico. La columna «when» el instante (en segundos por defecto) que se realizó la última consulta. Y la columna «delay» mide la distancia en milisegundos hasta el servidor.

> De forma alternativa, se puede visualizar el estado de los servidores remotos con el comando `ntpq -c peers`.

El comando `ntpq` soporta diferentes comandos para obtener información de los servidores NTP. Por ejemplo:

```
$ ntpq
ntpq> peers
[...]
ntpq> associations
[...]
ntpq> readvar 34456
[...]
ntpq> readvar 34456 stratum
```

Si disponemos de una red local de gran tamaño, la constante sincronización puede generar multitud de paquetes *ntpd*. En este caso, debemos considerar la posibilidad de configurar un servidor en modo *broadcast*, que periodicamente envíe mensajes a toda la red con la hora real:

	broadcast 192.168.0.255

Enviando de este modo mensajes no solicitados a toda la red 192.168.0.0/24. No debemos olvidarnos de que dichos servidores deben tomar la hora del exterior a través de las directivas «server» o «pool». En los clientes debemos activar la siguiente directiva:

	broadcastclient

En este caso, no son necesarias las directivas «server» ni «pool».

Las correciones dinámicas de `ntp` sólo trabajan para pequeñas desviaciones (en el orden de segundos). Para poder utilizar *ntpd*, primero debe fijar el reloj aproximadamente, y luego `ntp` hará el resto. Para configurar el reloj de forma aproximada, por ejemplo, puede usar `ntpdate`, pasandole un servidor de tiempo como argumento:

	# ntpdate -u 0.es.pool.ntp.org
	# hwclock --systohc

> El demonio `ntpd` se comunica a través del puerto 123, por lo que debemos comprobar que no está bloqueado por el cortafuegos.

## Uso del demonio chrony

El demonio chrony (*chronyd*) incorpora diferentes mejoras respecto a `ntpd`. Es capaz de mantener la hora incluso en sistemas con redes saturadas o que están desconectados durante periodos de tiempo, e incluso en sistemas virtualizados. Es capaz de realizar la sincronización de forma más rápida que `ntpd`, y es fácilmente configurable como servidor local.

Lo podremos encontrar en distribuciones CentOS, así como en otras derivadas de Red Hat. El nombre del paquete es `chrony`, por lo que es posible su instalación desde los repositorios habituales.

El fichero de configuración principal es `chrony.conf` y se puede encontrar en el directorio `/etc/` o en `/etc/chrony`. Entre otras directivas, se encuentran los servidores NTP a utilizar (`server` o `pool`).

Otra directiva que se encuentra en el fichero de configuración es `rtcsync`, que actualiza de forma periódica el reloj hardware. De esta forma evitamos el uso del comando `hwclock --systohc` para sincronizar el reloj hardware con el de sistema.

El servicio se puede gestionar a través de la utilidad `chronyc`, que dispone de comandos similares a los de `ntpd`. Por ejemplo, para visualizar una lista de las fuentes utilizadas por el servicio, ejecutamos el comando `chronyc sources -v`, de forma análoga a `ntpq -p`.

```
$ chrony sources -v
210 Number of sources = 7

  .-- Source mode  '^' = server, '=' = peer, '#' = local clock.
 / .- Source state '\*' = current synced, '+' = combined , '-' = not combined,
| /   '?' = unreachable, 'x' = time may be in error, '~' = time too variable.
||                                                 .- xxxx [ yyyy ] +/- zzzz
||      Reachability register (octal) -.           |  xxxx = adjusted offset,
||      Log2(Polling interval) --.      |          |  yyyy = measured offset,
||                                \     |          |  zzzz = estimated error.
||                                 |    |           \
MS Name/IP address         Stratum Poll Reach LastRx Last sample               
===============================================================================
^? bjaaland.red.uv.es            2   6     1     1  -1889ms[-1889ms] +/-   45ms
^? 178.255.228.77                3   6     1     2  -1889ms[-1889ms] +/-   55ms
^? shackleton.red.uv.es          2   6     1     1  -1889ms[-1889ms] +/-   34ms
^? scott.red.uv.es               0   6     0     -     +0ns[   +0ns] +/-    0ns
^? bjaaland.red.uv.es            0   6     0     -     +0ns[   +0ns] +/-    0ns
^? time.cloudflare.com           0   6     0     -     +0ns[   +0ns] +/-    0ns
^? ntp.redimadrid.es             0   6     0     -     +0ns[   +0ns] +/-    0ns
```

Con el comando `chronyc sourcestats` es posible obtener información detallada sobre las fuentes usadas, y si deseamos comprobar que nuestro reloj de sistema está siendo sincronizado, usaremos el comando `chronyc tracking`.

## Comandos vistos en el tema

Comando | Descripción
 --- | ---
date | Muestra la fecha y la hora
hwclock | Gestiona el reloj CMOS de un equipo
datetimectl | Configura el reloj del sistema
zdump | Muestra las configuraciones de las zonas horarias
