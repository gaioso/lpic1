# Tema 19 - Arranque en Linux

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Introducción](#introducción)
    - [BIOS](#bios)
    - [UEFI](#uefi)
    - [Discos duros: MBR vs. GPT](#discos-duros-mbr-vs-gpt)
    - [Después del «boot loader»](#después-del-«boot-loader»)
- [Análisis del proceso de arranque](#análisis-del-proceso-de-arranque)
- [GRUB Legacy](#grub-legacy)
    - [Configuración de GRUB Legacy](#configuración-de-grub-legacy)
    - [Instalación de GRUB Legacy](#instalación-de-grub-legacy)
- [GRUB 2](#grub-2)
- [Consejos de seguridad](#consejos-de-seguridad)
- [Parámetros del Kernel](#parámetros-del-kernel)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Conocer los gestores de arranque GRUB Legacy y GRUB 2, y saber configurarlos
* Ser capaz de diagnosticar y solucionar problemas del arranque.

## Introducción

Cuando se enciende un equipo con Linux, tiene lugar un complejo proceso, durante el cual el equipo inicializa el hardware y lo verifica antes de pasar el control al sistema operativo. En este tema veremos este proceso en detalle y como poder adaptarlo a nuestras necesidades, así como poder solucionar algunos problemas si fuera necesario. Justo después de que el ordenador se encienda, su firmware -dependiendo de la antigüedad del equipo, BIOS o UEFI- toman el control.

### BIOS

En los sistemas basados en BIOS, se realiza una búsqueda de un sistema operativo en medios como CD-ROM o disco duro, dependiendo del orden configurado. En los discos (duros o disquetes), se leen los primeros 512 bytes. Es el lugar donde se guarda información especial para el arranque del sistema. A esta área se la conoce como **boot sector**; si se trata de un disco duro también es conocido como **master boot record** (MBR).
Los primeros 446 bytes del MBR contienen un programa de inicio mínimo que es responsable de iniciar el sistema operativo -**boot loader**. El resto del sector está ocupado con la tabla de particiones. Estos 446 bytes no son suficientes para el arranque completo, pero si para un pequeño programa capaz de cargar el resto del **boot loader** desde el disco usando la BIOS. En el espacio entre MBR y el comienzo de la primera partición -a partir del sector 63, y hoy en día a partir del 2048 hay espacio suficiente para el resto del **boot loader**.
Los gestores de arranque modernos que se utilizan en Linux (en particular, GRUB), pueden leer los sistemas de ficheros más comunes y son por lo tanto capaces de encontrar el kernel del sistema operativo en una partición Linux, cargarlo en memoria RAM e iniciar el sistema completo.

> GRUB no sólo es capaz de arrancar el sistema, sino que ofrece diferentes opciones de usuario, además de poder iniciar diferentes kernel, o incluso, diferentes sistemas operativos.

### UEFI

Los sistemas basados en UEFI no utilizan sectores de arranque. En su lugar, el firmware UEFI contiene un gestor de arranque con información sobre los sistemas operativos que se guarda en una memoria NVRAM. Los **boot loaders** para los diferentes sistemas operativos del ordenador están guardados como ficheros regulares en un partición EFI, «EFI system partition», donde el firmware puede leer e iniciarlos. El sistema puede encontrar el nombre del **boot loader** deseado en la NVRAM, o bien utilizar el nombre por defecto `/EFI/BOOT/BOOTX64.EFI`. El **boot loader** del sistema operativo se encarga del resto del proceso, de igual modo que en BIOS.

> Según la especificación, la partición ESP debe contener un sistema de ficheros FAT32. Suele ser suficiente con un tamaño de 100MiB, pero algunas UEFI presentan problemas con particiones ESP FAT32 con un tamaño menor de 512MiB. Lo más recomendable es utilizar un tamaño en torno a los 550MiB.

En un sistema Linux, la partición ESP se suele montar en el directorio `/boo/efi`, y los ficheros de gestión del arranque se suelen guardar con la extensión `.efi`.

En UEFI, es necesario el registro de cada gestor de arranque que vaya a mostrarse durante el inicio en el menú. Una vez que el firmware es capaz de ejecutar el gestor de arranque, finaliza su trabajo.

> Para comprobar si nuestro sistema hace uso de UEFI, podemos ejecutar el comando `ls /sys/firmware/efi`. Si nos devuelve un mensaje del tipo `no such file or directory`, nuestro sistema está haciendo uso de BIOS. Y al contrario, si se muestran ficheros, nuestro sistema está usado UEFI.

«UEFI Secure Boot» fue concebido para prevenir una infección por parte de «root kits», que pueden tomar el control del equipo a través del proceso de arranque antes de que el sistema operativo se haya iniciado. De este modo, el firmware rechaza el uso de **boot loaders** que no hayan sido firmados con una clave autorizada. Los **boot loaders** autorizados son responsables de arrancar únicamente aquellos kernels que también haya sido firmados con claves autorizadas. Y los sistemas operativos deben insistir en la correcta firma de los drivers cargados de forma dinámica. El objetivo es que sólo se pueda ejecutar software «trusted», por lo menos en la parte que concierne al sistema operativo.

> Un efecto colateral de este método es la capacidad de excluír ciertos sistemas operativos. En principio, una compañía como Microsoft podría ejercer presión sobre la industria PC para permitir tan solo aquellos **boot loaders** firmados por Microsoft; aunque esto no formará parte de la política oficial de la compañía, para evitar problemas con las agencias antimonopolio. Es más probable que los fabricantes de placas base centren sus esfuerzos en los arranques tipo Windows, y que los arranques para Linux sean más complejos debido a fallos en el firmware.

Linux soporta UEFI Secure Boot de varios modos. Existe un «boot loader» que se llama «Shim» (desarrollado por Matthew Garrett), y otro denominado «PreLoader» (distribuído por la [Linux Foundation](http://blog.hansenpartnership.com/linux-foundation-secure-boot-system-released/)).

### Discos duros: MBR vs. GPT

En Linux no importa demasiado si tenemos un ordenador basado en BIOS con un disco con partición MBR o GPT, o bien un equipo basado en UEFI. Es posible iniciar un equipo BIOS con un disco particionado con GPT, o un UEFI con disco MBR.

### Después del «boot loader»

El «boot loader» carga el kernel Linux y le cede el control. Desde ese mismo momento, tanto el «boot loader» como el firmware ya pierden su papel, y pueden ser eliminados. El kernel será capaz a los drivers necesarios para activar el medio donde se encuentra el sistema de ficheros raíz, como al propio sistema de ficheros (el «boot loader» usaba el firmware para acceder al disco), que se suele encontrar en un disco IDE, SATA o SCSI. Los drivers para poder acceder a la controladora de disco deben estar incorporados al kernel durante la compilación del mismo.

## Análisis del proceso de arranque

Es posible visualizar los mensajes que se envían a la salida estándar durante el arranque, pero la velocidad no permite la consulta detallada de dicha información. Por ello, una vez dentro del sistema, disponemos del comando `dmesg` para consultar esa información con calma. La mayoría de las distribuciones copian eses mensajes a un buffer especial que se denomina *kernel ring buffer*. Este *buffer* es circular y está configurado con un tamaño determinado.

El comando `dmesg` muestra los mensajes más recientes que se encuentran guardados en este *anillo*.

```
$ dmesg
[    0.000000] microcode: microcode updated early to revision 0xde, date = 2020-05-26
[    0.000000] Linux version 5.4.0-54-generic (buildd@lcy01-amd64-024) (gcc version 9.3.0 (Ubuntu 9.3.0-17ubuntu1~20.04)) #60-Ubuntu SMP Fri Nov 6 10:37:59 UTC 2020 (Ubuntu 5.4.0-54.60-generic 5.4.65)
[    0.000000] Command line: BOOT_IMAGE=/boot/vmlinuz-5.4.0-54-generic root=UUID=f6f3a67d-c679-4e3e-b0be-db9060ff575a ro quiet splash vt.handoff=7
[    0.000000] KERNEL supported cpus:
[    0.000000]   Intel GenuineIntel
[    0.000000]   AMD AuthenticAMD
[    0.000000]   Hygon HygonGenuine
[...]
```

También es posible consultar la información de este buffer con el comando `journalctl -k` (si está disponible en nuestra distribución).

```
$ sudo journalctl -k
-- Logs begin at Sat 2020-12-05 18:01:50 UTC, end at Sat 2020-12-05 18:02:35 UTC. --
Dec 05 18:01:50 localhost.localdomain kernel: Linux version 4.18.0-193.28.1.el8_2.x86_64 (mockbuild@kbuilder.bsys.centos.org) (gcc version 8.3.1 20191121 (Red Hat 8.3.1-5) >
Dec 05 18:01:50 localhost.localdomain kernel: Command line: BOOT_IMAGE=(hd0,msdos1)/boot/vmlinuz-4.18.0-193.28.1.el8_2.x86_64 root=UUID=d5f5b677-6350-416d-b1d3-47d723d94d88>
Dec 05 18:01:50 localhost.localdomain kernel: x86/fpu: Supporting XSAVE feature 0x001: 'x87 floating point registers'
[...]
```

## GRUB Legacy

La mayoría de las distribuciones actuales utilizan GRUB como «boot loader» estándar. Presenta varias ventajas frente al obsoleto LILO, como el soporte para una cantidad mayor de sistemas de ficheros. Puede leer el kernel directamente de un fichero como `/boot/vmlinuz`, evitando problemas a la hora de actualizar un nuevo kernel. Además, GRUB ofrece la posibilidad de utilizar su consola para ejecutar diferentes comandos para modificar su configuración o solucionar diversos problemas.
Las versiones más recientes integran la versión 2 de GRUB, mientras que las más antiguas, o bien con destino a entornos empresariales, todavía integran la versión Legacy.
El esquema que sigue GRUB Legacy es el descrito anteriormente. Durante el arranque de un sistema basado en BIOS, la BIOS buscar la primera parte («stage 1») del «boot loader» en el MBR del disco. La «Stage 1» es capaz de encontrar la siguiente fase basándose en la lista de sectores guardados dentro del programa, y las funciones de acceso al disco de la BIOS. Por supuesto, en este caso contamos con una limitación a los primeros 1024 cilindros del disco.
La siguiente etapa suele ser la 1.5, que está guardada en alguna parte entre el MBR y el inicio de la primera partición. En Linux, esta etapa tiene una presencia testimonial, y se encargará de localizar la «stage 2», que suele encontrarse bajo el directorio `/boot/grub`. La «stage 2» puede localizarse en cualquier parte del disco duro. Puede leer sistemas de ficheros, gestionar sus propios ficheros de configuración, mostrar el menú y finalmente cargar el sistema operativo seleccionado.

> Para poder usar GRUB Legacy con discos GPT, necesitamos una partición de arranque tipo BIOS para guardar la etapa 1.5. Existe una versión de GRUB Legacy que puede trabajar con sistemas UEFI, pero es recomendable utilizar otro gestor de arranque.

### Configuración de GRUB Legacy

El fichero de configuración principal para GRUB Legacy suele estar guardado en `/boot/grub/menu.lst`. Contiene los parámetros de configuración general, así como la configuración particular de cada sistema operativo.

```
default 1
timeout 10

title Linux
  kernel (hd0,1)/boot/vmlinuz root=/dev/sda2
  initrd (hd0,1)/boot/initrd
title failsafe
  kernel (hd0,1)/boot/vmlinuz.bak root=/dev/sda2 apm=off acpi=off
  initrd (hd0,1)/initrd.bak
title floppy
  root (fd0)
  chainloader +1
```

* **default** Indica el sistema a iniciar por defecto. GRUB cuenta desde cero.
* **timeout** El tiempo de espera hasta que se active la entrada por defecto (default).
* **title** Nombre a mostrar en el menú de selección. Indica el inicio de una entrada para un sistema operativo en el fichero de configuración.
* **kernel** Indicar el kernel a ser cargado. `(hd0,1)/boot/vmlinux`, por ejemplo, significa que el kernel se encuentra en `/boot/vmlinuz` en la primera partición del disco número cero, que en el ejemplo anterior, para `linux` es `/dev/sda2`. El disco número cero, es el primer disco en el orden de arranque de la BIOS. No se hace distinción entre discos IDE y SCSI, y se comienza a contar en 0. A continuación pueden incluirse diferentes opciones para el kernel.
* **initrd** Especifica la localización del fichero `cpio` usado como «initial RAM disk».
* **root** La partición del sistema para «otros» sistemas operativos. Puede usarse para indicar un dispositivo «arrancable» como un disquete.
* **chainloader +1** Indica el «boot loader» a cargar desde la partición del sistema operativo «foráneo».
* **makeactive** Marca la partición indica como «booteable». Algunos sistemas operativos (no Linux) lo requieren para poder arrancar desde la partición indicada.

### Instalación de GRUB Legacy

Con «instalación» no nos referimos a la instalación de un paquete (como DEB), sino a la instalación del sector de arranque GRUB, o «stage 1» (e incluso la 1.5). Esto es necesario durante la instalación inicial del sistema, que será realizado de forma automática por la distribución.
La instalación se realiza con el comando `grub` (GRUB shell). Es recomendable el uso de un fichero tipo «batch», ya que así evitamos tener que volver al principio de la configuración si cometemos un error.

Algunas distribuciones tienen un fichero preparado para esta tarea:

`# grub --batch --device-map=/boot/grub/device.map < /etc/grub.inst`

La opción `--device-map` crea un fichero `device.map` si no existe.
El fichero `/etc/grub.inst` puede tener el contenido siguiente:

```
root (hd01,)
setup (hd0)
quit
```

`root` se refiere a la partición que contiene el directorio de GRUB (`/boot/grub`). No confundir con la partición `root` que se especifica en las entradas de sistema operativo del fichero de configuración.
`setup` instala GRUB en el dispositivo indicado, en este caso, en el MBR de `hd0`.

En algunas distribuciones disponemos del comando `grub-install` para instalar los componentes de GRUB.

	# grub-install /dev/sda

## GRUB 2

GRUB 2 es implementación completamente nueva del «boot loader» que no se preocupa de la compatibiliade con GRUB Legacy. Fue publicado en 2012, con algunas distribuciones usando versiones anteriores a dicha publicación.
GRUB 2 consiste en varias «stages»:

* Stage 1 (boot.img) está ubicado en el MBR en sistemas basados en BIOS. Con la ayuda de la BIOS puede leer la etapa 1.5.
* Stage 1.5 (core.img) se sitúa entre la MBR y la primera partición (en discos MBR), o bien en la partición de arranque BIOS en un esquema de particionado GPT. Está formada por un sector que está asociado al medio de arranque (disco, CDROM, red...) así como un «kernel» que proporciona una funcionalidad básica para acceso a dispositivos y ficheros, linea de comandos, etc, y una lista de módulos.
* GRUB 2 no incluye una «stage 2» explícita; las funcionalidades avanzadas son proporcionadas por los módulos cargados bajo demanda en la etapa 1.5. Los módulos se pueden encontrar en `/boot/grub`, y el fichero de configuración en `/boot/grub/grub.cfg`.

El fichero de configuración de GRUB 2 es muy diferente al utilizado por GRUB Legacy, y también bastante más complejo. Los autores de GRUB 2 asumen que los gestores del sistema no van editar el fichero manualmente. Para esto, hay un comando llamado `grub-mkconfig` que se encarga de generar dicho fichero. Para realizar esta tarea, utiliza un conjunto de herramientas auxiliares (shell scripts) que están en `/etc/grub.d`, que por ejemplo, buscan kernels en `/boot` para añadirlos al menú de arranque de GRUB. `grub-mkconfig` escribe su resultado a la salida estándar; el comando `update-grub` recoge esa salida y la escrib en el fichero `/boot/grub/grub.cfg`.
Por lo tanto, no debemos modificar a mano el fichero `grub.cfg`, sobre todo, porque nuestra distribución puede hacer uso del comando `update-grub` sin nuestro conocimiento, y deshacer nuestros cambios.

Lo que si podemos hacer, es modificar el fichero `/etc/grub.d/40_custom` para añadir más elementos al menú de GRUB. El comando `grub-mkconfig` copiará literalmente el contenido de este fichero en `grub.cfg`. Como alternativa, podemos nuestra configuración al fichero `/boot/grub.d/custom.cfg`.

Para la gestión de las opciones globales se usa el fichero `/etc/default/grub`. Aunque no se suele modificar este fichero, si es necesario realizar algún ajuste, veremos que las opciones son diferentes respecto a las de GRUB Legacy, por ejemplo se usa `GRUB_TIMEOUT` en lugar de `timeout`.

En GRUB 2, los números de las particiones comienzan en 1, por lo tanto `(hd0,msdos2)` se refiere a la segunda partición MSDOS en el primer disco duro. En lugar de `kernel`, se usa `linux` para iniciar un kernel Linux.

Si nuestro sistema hace uso del firmaware UEFI, el fichero `grub.cfg` se guarda en el directorio `/boot/efi/EFI/distro-name/`.

## Consejos de seguridad

La shell de GRUB ofrece muchas funcionalidades, en particular, acceso al sistema de ficheros sin la contraseña de `root`. Es sencillo modificar los parámetros de arranque para conseguir una shell de `root`. Podemos evitar estes accesos a través de la activación de una contraseña.
Para GRUB Legacy, se introduce la contraseña en el fichero `menu.lst`. Se debe añadir la entrada `password --md5 <contraseña cifrada>` en la sección global. Podemos obtener la contraseña cifrada con el comando `grub-md5-crypt` (o `md5crypt` en una shell de GRUB). A partir de ese momento, será necesaria la contraseña para modificar interactivamente cualquier parámetro a través del menú de GRUB.

> Incluso podemos proteger el acceso a un sistema en particular con la opción `lock` en la sección apropiada del fichero `menu.lst`. GRUB solicitará la contraseña al intentar iniciar dicho sistema, pero el resto se podrán iniciar de forma normal.

## Parámetros del Kernel

Linux acepta una línea de comando desde el «boot loader» y la evalúa durante el procedimiento de inicio del kernel. De este modo, se pueden configurar controladores de dispositivo y modificar diferentes opciones del kernel. Es útil, por ejemplo, para una herramienta de instalación de una distribución para poder trabajar con hardware que presenta algún problema. GRUB permite añadir parámetros a la línea `kernel`.
Además, podemos añadir parámetros de forma interactiva cuando el sistema está arrancando. El menú de GRUB permite la edición de una de sus entradas, si pulsamos la tecla «e» cuando nos presenta el menú.
Existen diferentes tipos de parámetros. El primer grupo modifica valores básicos, como `rw` o `root`. Otro grupo de parámetros sirven para configurar controladores de dispositivo. Y finalmente, hay parámetros para gestionar valores globales de la configuración.
Tan sólo veremos los más usuales. Para conocer el resto, debemos consultar la documentación del kernel.

* **ro** El kernel montará la partición raíz en modo sólo lectura.
* **rw** El kernel montará la partición raíz con la escritura habilitada, aunque se indique lo contrario en el fichero de configuración del «boot loader».
* **init=`<programa>`** Ejecuta el `<programa>` en lugar de `/sbin/init`.
* **`<runlevel>`** Entra en el nivel de ejecución `<runlevel>`, siendo un número entre 1 y 5, generalmente. De otro modo, el nivel de ejecución se toma del fichero `/etc/inittab`.
* **single** Se inicia en modo mono-usuario.
* **maxcpus=`<number>`** En un multiprocesador (o multicore), sólo se utilizarán `<number>` procesadores.
* **mem=<size>** Cantidad de memoria a utilizar, medida en `G`, `M` o  `K`. Un error frecuente suele ser escribir algo como: `mem=512`, que significa 512 bytes!.
* **panic=`<seconds>`** Provoca un reinicio del sistema después de `<seconds>` segundos, en caso de producirse un fallo general (kernel panic).
* **hdx=noprobe** El kernel ignorará totalmente los dispositivos de disco `/dev/hdx`.
* **noapic** y valores similares, como `nousb`, `apm=off` le indican a Linux que no utilice ciertas características.

Si queremos consultar la lista completa de opciones, debemos visitar el [sitio web del kernel](http://www.kernel.org/doc/Documentation/admin-guide/kernel-parameters.txt).

## Comandos vistos en el tema

| Comando | Descripción |
| --- | --- |
| dmesg | Visualiza el contenido del buffer de mensajes del kernel |
| grub-md5-crypt | Establece la contraseña cifrada para GRUB |
