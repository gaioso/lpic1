CREATE TABLE persona (
id INTEGER PRIMARY KEY,
nombre VARCHAR(20),
apellido VARCHAR(20),
nave_id INTEGER
);

CREATE TABLE pelicula (
id INTEGER PRIMARY KEY,
titulo VARCHAR(40),
año INTEGER,
presupuesto INTEGER
);

CREATE TABLE nave (
id INTEGER PRIMARY KEY,
nombre VARCHAR(20)
);

CREATE TABLE personapelicula (
id INTEGER PRIMARY KEY,
persona_id INTEGER,
pelicula_id INTEGER
);
