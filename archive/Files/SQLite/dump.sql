INSERT INTO nave VALUES (1, 'USS Enterprise');
INSERT INTO nave VALUES (2, 'USS Enterprise');
INSERT INTO nave VALUES (3, 'Millenium Falcon');
INSERT INTO nave VALUES (4, 'Death Star');
INSERT INTO nave VALUES (5, 'Serenity');

INSERT INTO pelicula VALUES (1, 'Star Trek',1979,46);
INSERT INTO pelicula VALUES (NULL, 'Star Wars: Generations',1994,35);
INSERT INTO pelicula VALUES (NULL, 'Star Wars 4',1977,11);
INSERT INTO pelicula VALUES (NULL, 'Star Wars 5',1980,33);

INSERT INTO persona VALUES (NULL,'James T.','Kirk',1);
INSERT INTO persona VALUES (NULL,'Willard','Decker',1);
INSERT INTO persona VALUES (NULL,'Jean-Luc','Picard',2);
INSERT INTO persona VALUES (NULL,'Han','Solo',3);
INSERT INTO persona VALUES (NULL,'Wilhuff','Tarkin',4);

INSERT INTO personapelicula VALUES (NULL,1,1);
INSERT INTO personapelicula VALUES (NULL,1,2);
INSERT INTO personapelicula VALUES (NULL,3,2);
INSERT INTO personapelicula VALUES (NULL,4,3);
INSERT INTO personapelicula VALUES (NULL,4,4);
INSERT INTO personapelicula VALUES (NULL,5,3);
