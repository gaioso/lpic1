# Tema 26 - Impresión en Linux

## Presentación

Al igual que ocurre con los sistemas de vídeo, la impresión en Linux puede resultar una actividad muy compleja. Con la alta cantidad de fabricantes y modelos disponibles en el mercado, la tarea de instalar los controladores correctos así como el protocolo de comunicación puede ser una tarea frustrante.

El *Common Unix Printing System* (CUPS) soluciona parte de estos problemas. CUPS ofrece una interfaz para poder trabajar con cualquier tipo de impresora en Linux. Acepta los trabajos de impresión a través del formato PostScript y los envía hacía las colas de impresión.

CUPS usa Ghostscript para convetir el documento PostScript hacia un formato que pueda ser interpretado por las impresoras. Por supuesto, Ghostscript requiere del uso de diferentes controladores de impresora para poder convertir el documento y que pueda imprimirse. CUPS instala diferentes controladores para las impresoras más comunes del mercado y configura los requisitos para usarlas. Los ficheros de configuración se almacenan en el directorio `/etc/cups`.

CUPS ofrece una interfaz web a través de la dirección http://localhost:631/, desde donde se pueden añadir nuevas impresoras, modificar las existentes y comprobar el estado de los trabajos enviados a cada impresora.

Las impresoras también se pueden gestionar a través de otros protocolos, como IPP (Internet Printing Protocol) o SMB (Microsoft Server Message Block).

Aparte de la interfaz web de CUPS, existen una serie de comandos para poder operar con impresoras y colas de impresión:

* `cancel` Cancela una petición de impresión
* `cupsaccept` Activa las peticiones de impresión
* `cupsdisable` Desactiva las peticiones de impresión
* `cupsenable` Habilita la impresora indicada
* `cupsreject` Rechaza las peticiones de impresión

CUPS también acepta los siguientes comandos clásicos:

* `lpc` Inicia, para o pausa la cola de impresión.
* `lpq` Muestra el estado de la cola de impresión, asi como aquellos trabajos a la espera en dicha cola.
* `lpr` Envía un nuevo trabajo a la cola.
* `lprm` Elimina un trabajo de la cola de impresión.

Para comprobar el estado de cualquier cola de impresión, así como el envío de trabajos, ejecutamos los siguientes comandos:

```
$ lpq -P EPSON_ET_3750_Series
EPSON_ET_3750_Series is ready
no entries
$ lpr -P EPSON_ET_3750_Series test.txt
$ lpq -P EPSON_ET_3750_Series
EPSON_ET_3750_Series is ready and printing
Rank    Owner    Job      File(s)       Total Size
active  rich     1        test.txt      1024 bytes
```


