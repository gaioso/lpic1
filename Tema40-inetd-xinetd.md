# Tema 40 - inetd y xinetd

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Introducción](#introducción)
- [Configuración inetd](#configuración-inetd)
- [TCP Wrapper -tcpd](#tcp-wrapper-tcpd)
- [xinetd](#xinetd)
    - [Configuración de xinetd.](#configuración-de-xinetd)
- [Ejecutando xinetd](#ejecutando-xinetd)
- [Procesamiento paralelo de peticiones](#procesamiento-paralelo-de-peticiones)
- [Reemplazando inetd por xinetd](#reemplazando-inetd-por-xinetd)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Saber cómo se inician los servicios usando *inetd* y *xinetd*
* Control de acceso usando «TCP wrapper» y *xinetd*

## Introducción

Cuando un sistema Linux actúa como servidor de red puede ofrecer una amplia variedad de servicios – *TELNET, FTP, POP3, IMAP...* Cada uno de esos servicios es accedido a través de un puerto TCP o UDP específico. Y hay dos métodos básicamente para ofrecer tales servicios. Uno es ejecutar un proceso especializado (un demonio) que espere por conexiones en el puerto en cuestión. Un servidor web, por ejemplo, acepta y procesa conexiones en el puerto TCP 80, mientras que un servidor DNS se encarga del puerto UDP 53. Otra posibilidad es delegar la escucha de varios puertos a un programa, que iniciará otro programa para una petición en alguno de esos puertos, que se encargará de atender dicha petición. El demonio internet, o *inetd* es dicho programa.

¿Por qué usar un programa como *inetd*? Hay algunas ventajas obvias:

* Hay muchos servicios que se usan con poca frecuencia. Sin embargo, un demonio especializado para dicho servicio podría bloquear recursos del sistema incluso cuando el servicio no estuviera en uso.
* El desarrollo de servicios de red sencillos se simplifica radicalmente. Mientras que necesitamos ciertos conocimientos para implementar demonios que sigan todas las reglas, como por ejemplo, no ocupar toda la memoria libre del sistema, los servicios basados en *inetd* pueden limitarse a leer de su entrada estándar y escribir a la salida estándar. *inetd* se encarga de enviar los datos recibidos por el cliente remoto a la entrada estándar del servidor, y enrutar su salida estándar de nuevo hacia el cliente remoto. De esta forma el servidor no se preocupa de nada que tenga que ver con la red.

*inetd* también se puede prestar a implementar servicios que tienen sesiones de larga duración, como pueden ser FTP o SMTP. Por supuesto, estamos hablando de servidores con poca carga. Para servidores tipo HTTP, sería una idea estúpida hacerlo a través de *inetd*, y deberían hacerlo de forma independiente, y no cargar el sistema con un trabajo intermedio por parte de *inetd*.

## Configuración inetd

> Esta sección se mantiene por motivos didácticos, ya que ha sido sustituído por el moderno `xinetd`.

La configuración de *inetd* se guarda en el fichero `/etc/inetd.conf`. Cada línea describe un servicio. Por ejemplo:

	ftp stream tcp nowait root /usr/sbin/ftpd ftpd

La primera palabra identifica el servicio por el nombre o el número de puerto; un servicio debe corresponder a una entrada del fichero `/etc/services` que indica el número de puerto asociado al servicio. La segunda entrada en la línea indica el tipo de socket usado por el servicio. Los valores admitidos inclyen *stream*, *dgram*, *raw*, *rdm*, y *seqpacket*. En la práctica sólo nos encontraremos con *stream* y *dgram*. A continuación tenemos el protocolo que se usará para acceder al servicio. Los nombres de los protocolos deben ser definidos en el fichero `/etc/protocols`. Valores típicos incluyen *tcp* o *udp*, dónde *stream* obliga a usar tcp. Lo mismo ocurre con *dgram* y *udp*.

El cuarto campo contiene *wait* o *nowait*. Esta entrada controla como se usan los sockets *dgram* (UDP); para otro tipo de sockets, se debe especificar *nowait*. 

* *wait* implica que una vez que el servicio es accedido, el puerto en cuestión es considerado 'ocupado' hasta que la petición entrante se haya atendido. Solo después se pueden aceptar nuevas peticiones.
* *nowait* indica que el puerto se libera de forma inmediata, por lo que se pueden manejar nuevas peticiones de una sola vez.

> También podemos usar la opción *nowait.n*, donde *n* indica el número máximo de procesos servidores que inetd creará en 60 segundos. Si *.n* se omite, se usará el valor por defecto, 40.

## TCP Wrapper -tcpd

Un problema de *inetd* son los posibles *intrusos* intentando acceder a los servicios. Cada servicio debería comprobar sus peticiones para diferenciar entre las fiables y las que no lo son. Debido a que muchos servicios no implementan este tipo de control de acceso, se ha creado un servicio central disponible para todos los servicios. El *TCP Wrapper* -tcpd. Si un servicio es accedido, *inetd* inicia en primer lugar la envoltura TCP en lugar del servicio actual. La envoltura TCP registra la conexión usando *syslog*. Luego comprueba si el cliente está autorizado a usar dicho servicio, consultando los ficheros `/etc/hosts.allow` y `/etc/hosts.deny`.	*tcpd* primero comprueba el fichero `/etc/hosts.allow` buscando una entrada específica. Si existe, se acepta el acceso. En otro caso, se busca en el fichero `/etc/hosts.deny`. Si existe, se rechaza el acceso. En otro caso, se acepta el acceso. Si alguno de los ficheros no existiera, se consideran vacíos.

> La filosofía más segura sería denegar el acceso a todo aquello que no tuviera un permiso explícito. Sin embargo, esto no concuerda con la idea de que si tenemos un TCP wrapper sin configurar, debería comportarse de igual manera que si no estuviera presente.

Las entradas de ambos ficheros son muy parecidas, del tipo:

	pop3d : 192.168.10.0/24

La primera entrada contiene el nombre del demonio que debe ser iniciado (el segundo campo del comando en el fichero `/etc/inetd.conf`), o una serie de demonios, separados por comas.

> Si la entrada se debe aplicar a todos los programas, pondremos *ALL*; si se debe aplicar a todos los programas excepto a unos pocos, entonces pondremos: *ALL EXCEPT...*

El segundo campo indica los hosts a los que la entrada hace referencia. En el caso más simple es un nombre de host o una dirección IP. Aquí, *ALL* hace referencia a todos los clientes. También podemos usar la palabra reservada *KNOWN* (todos los hosts que tcpd puede averiguar su nombre a partir de su dirección ip). *UNKNOWN* (aquellos hosts que no puede averiguar su nombre a partir de su dirección ip), y *PARANOID* (aquellos hosts cuyo nombre y resolución de nombre a través de DNS proporciona respuestas conflictivas). Se pueden indicar direcciones completas a través de su dirección de red y máscara. En el ejemplo, se permite o deniega el acceso a todas las estaciones de la red 192.168.10.0/24 que accedan al demonio *POP3*, dependiendo de si esta linea se encuentra el fichero *allow* o *deny*.

Después de indicar el cliente, puede haber más campos separados por comas, con opciones para procesar la conexión. Por ejemplo:

	ALL: ALL: spawn echo “Acceso por %u@%h a %d” >> /var/log/net.log

Podemos usar *spawn* para insertar un comando de shell que será ejecutado en un proceso hijo. Las entradas, y salidas estándar de ese comando están conectadas a `/dev/null`. Antes de que se ejecute el comando, se reemplazan las expresiones «%» que existan en la línea de comando; siguiendo las relaciones que se muestran en la siguiente tabla.

| Código | Significado |
| --- | --- |
| %a | La dirección IP del cliente |
| %c | «Información del cliente»: <usuario>@<host> |
| %d | El nombre del servicio |
| %h | El nombre del cliente (o su IP, si el nombre no se resuelve) |
| %n | El nombre del cliente (o unknown/paranoid, si no se resuelve) |
| %p | El PID del proceso creado con spawn |
| %s | «Información del servidor»: <servicio>@<host> |
| %u | El nombre de usuario del lado del cliente, o unknown |
| %% | Un símbolo de porcentaje ! |

Otras opciones para las reglas de acceso:

* **twist** (seguido de un comando de shell) reemplaza el proceso actual por el comando, y conectando la entrada y salidas estándar al cliente remoto. Por ejemplo:

	in.ftpd : 10.0.0.0/8 : twist /bin/echo 421 Sal de aquí!!

rechaza las conexiones entrantes de FTP desde la red 10.0.0.0/8 con el mensaje indicado, evitando molestar al servidor FTP. Esta opción debe ser colocada al final de una entrada (linea).
* **allow – y deny**, aceptan o rechazan una petición de conexión, sin importar en qué fichero esté la entrada.  Esto hace posible mantener todas las configuraciones en `/etc/hosts.allow` (por ejemplo) en lugar de distribuirlas a través de ambos ficheros. También deben ser colocadas al final de la entrada.
* **umask** – corresponde con el comando `umask` de la shell.
* **setenv** – configura una variable de entorno:
	in.ftpd : 10.0.0.0/8 : setenv HOME /tmp
Algunos demonios «desinfectan» su entorno y eliminan entradas que le parecen extrañas.
* **user** – configura el usuario o el usuario y el grupo para el proceso:
	user nobody
	user nobody.nogroup
Esto es útil porque sino inetd ejecutará todos los demonios como root.
* **banners** – (seguido de un nombre de directorio) (sólo para servicios TCP). Comprueba si el directorio especificado contiene un fichero con el mismo nombre que el demonio, y si es así, copia el contenido de ese fichero al cliente.

Debido a que *inetd*, junto con *tcpd*, no ejecutan el servicio actual cuando se intenta una conexión, sino que simplemente llama a *wrapper* con una serie de argumentos, las entradas en el fichero `/etc/inetd.conf` tienen algunas diferencias:
	ftp stream tcp nowait root /usr/sbin/tcpd ftpd -l -a
Se llama al proceso *tcpd* pero se le pasa `ftpd -l -a` como línea de comandos. `ftpd` es el nombre del comando desde el punto de vista de *tcpd*, que lo usa para localizar el comando que será ejecutado cuando se conceda el acceso. El mismo nombre es usado para buscar entradas en los ficheros `/etc/hosts.allow` y `/etc/hosts.deny`.

Es posible comprobar si un servicio hace uso de la librería *libwrap* mediante el uso del comando `ldd` (datos obtenidos en Debian 10):

	$ ldd `which sshd` | grep libwrap
	libwrap.so.0 => /lib/x86_64-linux-gnu/libwrap.so.0 (0x00007fbb9a862000)

## xinetd

El *super servidor* original ha sido sustituído por una combinación *inetd-tcpd*, llamada *xinetd* (extended Internet daemon). Incorpora todas las funciones en un solo fichero de configuración (supervisión, control de acceso y logging).

### Configuración de xinetd.

El fichero de configuración de *xinetd* es `/etc/xinetd.conf`. Está dividido en secciones, cada una de las cuales comienza con una palabra correspondiente al nombre del servicio (`/etc/services`), y que contiene asignaciones de valores a atributos, como por ejemplo:

	default
	{
		<atributo> <operador> <parámetro> [<parámetro>]
		...
	}

En la práctica, el operador más importante es «=», que asigna un conjunto de valores a un atributo. Los atributos pueden poseer varios valores, y también se soportan los operadores «+=» y «-=», para añadir o eliminar valores. La sección «default» contiene los valores por defecto que se aplicarán a todos los servicios, a menos que sus secciones contengan valores más específicos. Si es así, los valores por defecto son reemplazados o combinados con los más específicos. Cada sección adicional proporciona información mas detallada para un servicio.

Un fichero `/etc/xinetd.conf` podría tener el siguiente aspecto:

```
defaults
{
	log_type		= FILE /var/log/xinetd.log
	log_on_success		= HOST EXIT DURATION
	log_on_failure		= HOST ATTEMPT RECORD
	instances		= 2
}
service telnet
{
	socket_type		= stream
	protocol		= tcp
	wait			= no
	user			= root
	server			= /usr/sin/in.telnetd
	server_args		= -n
	only_from		= localhost
	no_access		=
	}
```

| Atributo | Significado |
| --- | --- |
| type | permite valores como INTERNAL (servicio implementado por xinetd directamente), o UNLISTED para los servicios que no están listados en /etc/services. |
| socket_type | Incluye valores como stream, dgram o raw. |
| protocol | El protocolo usado por el servicio (/etc/protocols) |
| wait | Si es *yes*, es un servicio de un sólo hilo, no se permite iniciar varios servicios al mismo tiempo. |
| user | El usuario, con cuyos privilegios se ejecutará el servicio. |
| instances | Número máximo de copias simultáneas del servicio (si wait=no) |
| server | Nombre de fichero del programa servidor. |
| server_args | Parámetros para el programa servidor. |
| interface | Dirección IP para la interfaz usada por xinetd para escuchar las peticiones. |
| only_from | Sólo los clientes indicados pueden acceder al servicio. |
| no_access | Estos clientes no pueden acceder al servicio. |
| access_times | Intervalos de tiempo en los cuales está disponible el servicio. |
| log_type | El tipo de registro (log) -SYSLOG o FILE |
| log_on_success | Indica la información a almacenar en un intento de conexión con éxito. |
| log_on_failure | Define la información a guardar en un intento de conexión fallido |
| disable | Desactiva el servicio (equivale a comentar la línea en inetd.conf) |
| disabled | Se coloca en la sección defaults, para desactivar varios servicios, por ejemplo «disabled finger ftp» |

## Ejecutando xinetd

*xinetd* acepta varias opciones para controlar la forma de trabajo:

* **-syslog** <categoría> Provoca que *xinetd* envíe sus mensajes de registro al demonio *syslog* con la categoría indicada. Las posibles categorías incluyen *daemon*, *auth*, *user*, y las ocho categorías *local0* a *local7*.
* **-filelog** <fichero> Indica el fichero usado por *xinetd* para escribir sus mensajes de registro. Es mutuamente exclusiva con la anterior.
* **-f** <fichero> Provoca que *xinetd* lea del fichero indicado su configuración. El valor por defecto es `/etc/xinetd.conf`.
* **-inetd\_compat**  Provoca que lea el fichero de configuración de *inetd* (`inetd.conf`), además del suyo propio (`xinetd.conf`).

Es posible controlar *xinetd* a través de sus señales. Las más importantes, y sus efectos, aparecen en la siguiente tabla.

| Señal | Efecto |
| --- | --- |
| SIGHUP | Lee de nuevo su fichero de configuración y finaliza los servidores que no están habilitados en la nueva configuración. Para el resto de servicios, se comprueban las conexiones, y aquellas que no cumplen las nuevas condiciones, se reinician. Si existen más conexiones establecidas que las indicadas en «instances», se finalizan un número aletaorio de las mismas hasta cumplir la condición. |
| SIGQUIT | Finaliza *xinetd* |
| SIGTERM | Cierra todos los servicios activos, y luego *xinetd* |
| SIGUSR1 | Crea un volcado de memoria en `/var/run/xinetd.dmp` |
| SIGIOT | Ejecuta un chequeo interno para asegurarse de que sus estructuras de datos no han sido dañadas. |

## Procesamiento paralelo de peticiones

Los servicios iniciados por *xinetd*, pueden ser subdivididos en dos grupos (veanse los atributos *wait* e *instances*). Si se inicia un nuevo proceso para cada acceso al servicio, el servicio se llama «multihilo». Si un servicio solo acepta una nueva petición después de finalizar la anterior, el servicio se llama de un solo hilo. Los servicios basado en datagramas (UDP) suelen ser de un solo hilo, mientras que los servicios basados en TCP son siempre multihilo.

## Reemplazando inetd por xinetd

Podemos usar en paralelo, tanto *inetd* como *xinetd* (siempre que la distribución lo permita). Sin embargo, debemos asegurarnos de que ambos no manejan los mismos puertos. Existe una herramienta para usar la configuración de *inetd* en *xinetd*, por defecto el paquete *xinetd* contiene el programa `itox`.

	# itox < /etc/inetd.conf > /etc/xinetd.conf

crea un fichero de configuración para *xinetd* basandose en el fichero de configuración de *inetd*. Es importante señalar que sólo los servicios habilitados serán exportados al nuevo fichero de configuración.

## Comandos vistos en el tema

| Comando | Descripción |
| --- | --- |
| inetd | Supervisa los puertos e inicia los servicios |
| tcpd | «TCP wrapper», permite o deniega el acceso en base a las direcciones IP de los clientes |
| xinetd | Versión mejorada de *inetd*, supervisa los puertos e inicia los servicios |


