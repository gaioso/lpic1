# Tema 17 - Archivado y compresión de ficheros

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
    - [Archivado y compresión](#archivado-y-compresión)
    - [Archivando ficheros con `tar`](#archivando-ficheros-con-tar)
- [Copiando con `cpio`](#copiando-con-cpio)
- [Duplicando con dd](#duplicando-con-dd)
    - [Comprimiendo ficheros con `gzip`](#comprimiendo-ficheros-con-gzip)
- [Duplicando con dd](#duplicando-con-dd-1)
    - [Comprimiendo ficheros con `gzip`](#comprimiendo-ficheros-con-gzip-1)
    - [Comprimiendo ficheros con `bzip2`](#comprimiendo-ficheros-con-bzip2)
    - [Comprimiendo con `xz`](#comprimiendo-con-xz)
    - [Archivando y comprimiendo ficheros usando `zip` y `unzip`](#archivando-y-comprimiendo-ficheros-usando-zip-y-unzip)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Comprender los términos «archivado» y «compresión»
* Ser capaz de utilizar `tar`
* Ser capaz de comprimir y descomprimir ficheros con `gzip` y `bzip2`
* Ser capaz de procesar ficheros con `zip` y `unzip`

### Archivado y compresión

El proceso de almacenar varios ficheros en uno solo, se denomina «archivado». La típica aplicación de archivado es el almacenamiento de un árbol de directorios en una cinta magnética.
La «compresión» es la transformación de los datos a una formato que ahorre espacio respecto del original. Por supuesto, solo nos interesa la compresión «sin pérdida», a través de la cual es posible volver a construír el fichero original.

Como un ejemplo simple, podemos transformar la siguiente cadena de texto:

`ABBBBAACCCCCCAAAABAAAAAC`

a un formato más compacto:

`A*4BAA*5C*4AB*5AC`

Donde, «\*4B» equivale a una secuencia de cuatro caracteres «B». Este método es conocido como «run-length encoding», y aún se puede encontrar en algunas máquinas de fax. Los programas de compresión que veremos utilizan métodos más sofisticados.

En sistemas Windows se suelen utilizar programas que combinan el archivado y la compresión (WinZIP, PKZIP...), sin embargo, en Linux y UNIX estes procesos se gestionan por diferentes programas. Una estrategia popular es archivar un conjunto de ficheros con el comando `tar`, y luego comprimir el resultado con `gzip`, por ejemplo. PKZIP, y programas similares, comprimen cada fichero por separado y luego los archivan en un único fichero.
La ventaja de esta estrategia en comparación con la utilizada por PKZIP, es que la compresión puede realizarse a través del contenido de todos los ficheros, obteniendo alta tasas de compresión. Sin embargo, cuenta con una importante desventaja: si el fichero comprimido sufre algún daño, el archivo completo se vuelve inaccesible.

> Por supuesto, que nada impide que en sistemas Linux, se comprima cada fichero por separado, y luego se archiven todos juntos.

### Archivando ficheros con `tar`

El nombre «tar» proviene de «tape archive». El programa escribe los ficheros individuales al archivo uno tras otro, de forma secuencial, y añade información adicional (fecha, permisos de acceso, propietario, ...). Aparte de otros usos, los ficheros «tar» son el formato estándar para publicar el código fuente de Linux y de otros paquetes de programas libres.
La versión GNU de tar incluye características que no están presentes en otras implementaciones de Unix. Por ejemplo, «tar» soporta la creación de varios volúmenes, para poder ser copiados en diferentes medios (disquetes,...).

>El comando `split` permite la división de ficheros de gran tamaño en piezas más pequeñas, que pueden ser copiadas a disquetes, o enviados por correo electrónico. Posteriormente, pueden ser reconstruidos con el comando `cat`.

Los archivos tipo `tar` pueden contener ficheros y directorios (con su estructura jerárquica). Normalmente, los ficheros `tar` no suelen estar comprimidos, pero pueden comprimirse a través de programas externos (`gzip` o `bzip2`). Esta última opción no suele ser una buena idea para realizar copias de seguridad, ya que los errores en la integridad del fichero pueden impedir el acceso a los datos originales.
Las extensiones más frecuentes suelen ser `.tar`, `.tar.gz` o `tar.bz2`. La extensión `tgz` suele utilizarse para guardar compatibilidad con sistemas DOS.

Las opciones más importantes de `tar` son:

* `-c` («create») - crea un nuevo archivo.
* `-f` «file» - crea el nuevo archivo en «file», que puede ser un fichero plano o un dispositivto tipo fichero.
* `-M` - para gestionar archivos de varios volúmenes.
* `-r` - añade ficheros al archivo.
* `-t` - imprime el contenido del archivo.
* `-u` - sustituye los ficheros que tienen una versión más reciente que la incluída en el archivo. Si el fichero no está en el archivo, se añade.
* `-v` - modo «verbose»
* `-x` - extrae los ficheros y directorios del archivo.
* `-z` - comprime o descomprime el archivo usando `gzip`.
* `-j` - comprime o descomprime el archivo usando `bzip2`.

El uso de las opciones del comando `tar` son inusuales, ya que permite el agrupamiento de varias de ellas, incluso aquellas que precisan un parámetro adicional (`-f`).

>Se recomienda el uso del guión «-» antes de las opciones.

El siguiente comando archiva todos los ficheros del directorio actual cuyo nombre comienza por `data` en el fichero `data.tar` situado en el directorio personal del usuario:

`$ tar -cvf ~/data.tar data*`

La opción `-c` hace que sea creado el archivo «`-f ~/data.tar`». La opción `-v` solo imprime por pantalla los nombres de los ficheros incluídos en el archivo (Si alguno de los ficheros es un directorio, entonces se añade el contenido completo del mismo).

`tar` permite archivar directorios completos. Se recomienda indicar el propio nombre del directorio, para que sea creado un subdirectorio en el archivo, que luego será creado de nuevo al desempaquetar el archivo. Por ejemplo:

`# tar -cvf /tmp/home.tar /home`

El administrador del sistema, `root`, almacena un archivo del directorio `/home` con el nombre `home.tar` dentro del directorio `/tmp`.

>A pesar de que se indiquen las rutas completas, `tar` almacena las rutas relativas. Es decir, elimina «/» de cada nombre de fichero o directorio. Esto evita posible problemas a la hora de desempaquetar el archivo en otros ordenadores.

Para poder visualizar el contenido de un archivo, usaremos la opción `-t`:  
`$ tar -tf data.tar`  
La opción `-v` hace que `tar` muestre información adicional:  
`$ tar -tvf data.tar`  
Para desempaquetar los datos debemos hacer uso de la opción `-x`:  
`$ tar -xf data.tar`  
Y si queremos visualizar el contenido extraído, debemos incluír la opción `-v`:  
`$ tar -xvf data.tar`  
A la hora de extraer el contenido, podemos indicar el nombre de un fichero o directorio. En cuyo caso, solo se actuará sobre ese elemento del archivo. Pero hay que tener cuidado con la sintaxis:

```
$ tar -cf data.tar ./data
$ tar -tvf data.tar
drwxr-xr-x joe/joe 0 2009-01-27 12:04 ./data/
-rw-r--r-- joe/joe 7 2009-01-27 12:04 ./data/data2
>>>

$ mkdir data-new
$ cd data-new
$ tar -xvf ../data.tar data/data2
tar: data/data2: Not found in archive
tar: Error exit delayed from previous errors
```

## Copiando con `cpio`

El significado de esta utlidad es *copy in and out*. Lo que hace es reunir las copias de los ficheros y guardarlos en un archivo. Las opciones más frecuentes son las siguientes:

Corta | Larga | Descripción
--- | --- | ---
-I | | Indica el archivo a usar
-i | --extract | Copia o muestra, dependiendo de otras opciones, los ficheros de un archivo
| --no-absolute-filenames | Solo se pueden usar rutas relativas (por defecto, son absolutas)
-o | --create | Crea un archivo copiando los ficheros dentro del mismo
-t | --list | Muestra la lista de ficheros incluídos en el archivo
-v | --verbose | Muestra los nombres de los ficheros al mismo tiempo que son procesados

Para usar el comando `cpio`, debemos enviarle a través de una *tubería* los nombres de los ficheros.

```
$ ls report4?.txt
report42.txt  report43.txt  report44.txt  report45.txt  report46.txt
$ ls report4?.txt | cpio -ov > report.cpio
report42.txt
report43.txt
report44.txt
report45.txt
report46.txt
1 bloque
$ 
```

> Para realizar una copia de seguridad de los ficheros de un usuario del sistema, hacemos uso del comando `find / -user <username>`, y luego enviamos esa lista de ficheros al comando `cpio`. De esta forma tan ágil conseguimos realizar una copia de todos sus ficheros.

Para visualizar los nombres de los ficheros que se encuentran en el archivo, debemos hacer uso de la opción `-itv`, y la `-I` para indicar el nombre del archivo.

```
$ cpio -itvI report.cpio
-rw-r--r--   1 12006    513             0 Dec  4 08:42 report42.txt
-rw-r--r--   1 12006    513             0 Dec  4 08:42 report43.txt
-rw-r--r--   1 12006    513             0 Dec  4 08:42 report44.txt
-rw-r--r--   1 12006    513             0 Dec  4 08:42 report45.txt
-rw-r--r--   1 12006    513             0 Dec  4 08:42 report46.txt
1 bloque
```

Y para restaurar los ficheros, usaremos las opciones `-ivI`. Sin embargo, como `cpio` mantiene las rutas absolutas, se debe tener en cuenta a la hora de extraerlos a una ruta diferente. Para ello, usaremos la opción `--no-absolute-filenames`.

```
$ ls -dF reports/
reports//
$ mv report.cpio reports/
$ cd reports/
$ pwd
/tmp/reports
$ cpio -iv --no-absolute-filenames -I report.cpio 
report42.txt
report43.txt
report44.txt
report45.txt
report46.txt
1 bloque
$ ls report4?.txt
report42.txt  report43.txt  report44.txt  report45.txt  report46.txt
```

## Duplicando con dd

La utilidad `dd` nos permite realizar una copia de un disco completo, incluyendo el MBR en aquellas distribuciones Linux que todavía lo usan. Se utiliza para realizar copias a bajo nivel de un disco completo o una partición, y es frecuente su uso en tareas de análisis forense.

La sintaxis básica de este comando es la siguiente:

	dd if=INPUT_DEVICE of=OUTPUT_DEVICE [OPERANDS]

En este caso, los dispositivos de entrada y salida pueden ser discos completos o particiones. Es muy importante no confundir este orden, ya que podemos acabar sobreescribiendo el disco equivocado.

Algunos de los *operandos* que podemos usar con `dd` son los siguientes:

Operando | Descripción
--- | ---
bs=BYTES | Ajusta el tamaño máximo de bloque que se leen y escriben en cada operación. Por defecto, son 512 bytes
count=N | Indica el número de bloques de entrada a copiar
status=LEVEL | Ajusta la cantidad de información a enviar a STDERR

Los niveles que se pueden indicar en el último operando son los siguientes:

* `none` solo muestra mensajes de error
* `noxfer` no muestra estadísticas finales de transferencia
* `progress` muestra estadísticas periódicas de transferencia

Uno de los usos de esta herramienta es el borrado (con ceros) de un disco, por ejemplo:

	# dd if=/dev/zero of=/dev/sdc status=progress

Debemos repetir esta operación un mínimo de 10 veces. Otra opción es escribir datos aleatorios haciendo uso de los ficheros `/dev/random` o `/dev/urandom`.

### Comprimiendo ficheros con `gzip`

El programa más utilizado para comprimir un ficheros regulares es `gzip`. Su modo de ejecución es procesar y reemplazar el fichero, añadiendo el sufijo `gz` al nombre. Si lo que queremos es comprimir varios ficheros, debemos combinar el uso de `gzip` con `tar`.
Las opciones más importantes son:

* `-c` envía el fichero comprimido a la salida estándar, en lugar de sustituír al original, el cual permanece intacto.
* `-d` descomprime el fichero (alternativa: `gunzip`)
* `-l` - visualiza información relevante sobre el fichero comprimido.
* `-r` («recursivo») - comprime fichero de subdirectorios
* `-S` («suffix») - utiliza el sufijo indicado en lugar de `.gz`.
* `-v` - muestra el nombre y el factor de compresión.
* `-1... -9` - indica el factor de compresión: `-1 (--fast)` es el más rápido, pero con menor tasa de compresión, mientras que `-9 (--best)` ofrece mejor compresión pero más lento; el valor por defecto es `-6`.

El siguiente comando comprime el fichero `letter.tex`, guarda el resultado en `letter.tex.gz`, y elimina el original:  

	$ gzip letter.tex

El fichero puede ser descomprimido con la siguiente instrucción:  

	$ gzip -d letter.tex.gz

o  

	$ gunzip letter.tex.gz

En el siguiente comando, el fichero es guardado como `letter.text.t` en lugar de `letter.tex.gz` (-S .t), y se muestra el ratio de compresión alcanzado en el fichero resultante:  

	$ gzip -vS .t letter.tex

La opción `-S` también se debe indicar a la hora de descomprimir, debido a que `gzip -d` espera un fichero con la extensión `.gz`:  

	$ gzip -dS .t letter.tex.t

Si queremos comprimir todos los ficheros con extensión `.tex`, y luego ser comprimidos, el comando es:  

	$ tar -cvzf tex-all.tar.gz *.tex

Recuerda que `tar` no elimina los ficheros originales. Podemos desempaquetar el fichero con:  

	$ tar -xvzf tex-all.tar.gz

## Duplicando con dd

La utilidad `dd` nos permite realizar una copia de un disco completo, incluyendo el MBR en aquellas distribuciones Linux que todavía lo usan. Se utiliza para realizar copias a bajo nivel de un disco completo o una partición, y es frecuente su uso en tareas de análisis forense.

### Comprimiendo ficheros con `gzip`

El programa más utilizado para comprimir un ficheros regulares es `gzip`. Su modo de ejecución es procesar y reemplazar el fichero, añadiendo el sufijo `gz` al nombre. Si lo que queremos es comprimir varios ficheros, debemos combinar el uso de `gzip` con `tar`.
Las opciones más importantes son:

* `-c` envía el fichero comprimido a la salida estándar, en lugar de sustituír al original, el cual permanece intacto.
* `-d` descomprime el fichero (alternativa: `gunzip`)
* `-l` - visualiza información relevante sobre el fichero comprimido.
* `-r` («recursivo») - comprime fichero de subdirectorios
* `-S` («suffix») - utiliza el sufijo indicado en lugar de `.gz`.
* `-v` - muestra el nombre y el factor de compresión.
* `-1... -9` - indica el factor de compresión: `-1 (--fast)` es el más rápido, pero con menor tasa de compresión, mientras que `-9 (--best)` ofrece mejor compresión pero más lento; el valor por defecto es `-6`.

El siguiente comando comprime el fichero `letter.tex`, guarda el resultado en `letter.tex.gz`, y elimina el original:  

	$ gzip letter.tex

El fichero puede ser descomprimido con la siguiente instrucción:  

	$ gzip -d letter.tex.gz

o  

	$ gunzip letter.tex.gz

En el siguiente comando, el fichero es guardado como `letter.text.t` en lugar de `letter.tex.gz` (-S .t), y se muestra el ratio de compresión alcanzado en el fichero resultante:  

	$ gzip -vS .t letter.tex

La opción `-S` también se debe indicar a la hora de descomprimir, debido a que `gzip -d` espera un fichero con la extensión `.gz`:  

	$ gzip -dS .t letter.tex.t

Si queremos empaquetar todos los ficheros con extensión `.tex`, y luego ser comprimidos, el comando a utilizar es el siguiente:  

	$ tar -cvzf tex-all.tar.gz *.tex

Recuerda que `tar` no elimina los ficheros originales. Podemos desempaquetar el fichero con:  

	$ tar -xvzf tex-all.tar.gz

### Comprimiendo ficheros con `bzip2`

`bzip2` es un programa de compresión que alcanza unos ratios de compresión mayores que `gzip`, pero emplea para ello más tiempo y memoria (para descomprimir, apenas hay diferencia).

>`bzip2` emplea una transformación «Burrows-Wheeler» para codificar cadenas de caracteres en secuencias de caracteres individuales. De este proceso se consigue un resultado intermedio, que será más sencillo de comprimir. Este paso de compresión se realiza con el esquema Huffman.

>¿Que ha ocurrido con `bzip`? `bzip` usaba una codificación aritmética en lugar de Huffman. Pero debido a problemas con la patente, el autor ha decidido abandonar esa codificación.

Al igual que `gzip`, `bzip2` acepta uno, o varios, nombres de ficheros como parámetros de la compresión. Estos ficheros son reemplazados por su versión comprimida, cuyos nombres acaban en `.bz2`.
Las opciónes `-c` y `-d` tienen el mismo efecto que en `gzip`. Sin embargo, las opciones del nivel de compresión `-1` a `-9` son diferentes: solamente indican el tamaño del bloque a usar durante la compresión. El valor por defecto es `-9`, mientras que `-1` no supone ninguna diferencia en cuanto a la velocidad.

>`-9` utiliza un bloque de 900KiB. Esto supone un uso de memoria de unos 3,7 MiB para descomprimir (7,6 MiB para comprimir), lo que no debería suponer un problema para un equipo moderno. Un incremento en el tamaño del bloque no supone una ventaja significante. Tan solo hay que tener en cuenta que un cambio en el tamaño del bloque, va suponer un incremento en la memoria necesaria para realizar la tarea de descompresión. Algo a tener en cuenta en hardware de pequeño tamaño.

`bunzip2` es el nombre del programa para descomprimir, aunque en realidad es otro nombre para el programa `bizp2`. También podemos usar `bzip2 -d` en su lugar.

### Comprimiendo con `xz`

`xz` fue desarrollado en el año 2009 y pronto se hizo muy popular entre los administradores Linux. Proporciona un ratio de compresión por defecto mayor que `bzip2` y `gzip`, gracias al uso del algoritmo de compresión LZMA. La utilidad `xz` reemplazó a `bzip2` en el año 2013 para comprimir el kernel de Linux.

Para comprimir un fichero, debemos escribir `xz` y a continuación el nombre del fichero. El fichero original será reemplazado por la versión comprimida, y con la extensión `.xz`. Para deshacer la operación, escribimos `unxz` seguido del nombre del fichero comprimido.

### Archivando y comprimiendo ficheros usando `zip` y `unzip`

Cuando deseamos intercambiar información con sistemas Windows, nos puede interesar el uso del formato ZIP (aunque hoy en dia la mayoría de sistemas Windows soportan el formato `tar.gz`). En los sistemas Linux, hay dos programas diferentes: `zip` para crear archivos, y `unzip` para desempaquetar archivos.

>Dependiendo de la distribución puede ser necesario instalar los programas por separado. 

El programa `zip` combina el archivado y la compresión en un solo paso. En el caso más simple, procesa los ficheros que recibe a través de la línea de comandos:

```
$ zip test.zip file1 file2
  adding: file1 (deflated 66%)
  adding: file2 (deflated 62%)
$ _
```

Podemos usar la opción `-r` para que `zip` pueda descender recursivamente a través de un subdirectorio:

```
$ zip -r test.zip ziptest
  adding: ziptest/ (stored 0%)
  adding: ziptest/testfile (deflated 62%)
  adding: ziptest/file2 (deflated 62%)
  adding: ziptest/file1 (deflated 66%)
```

Con la opción `-@`, `zip` lee los nombres de los ficheros a archivar desde su entrada estándar:

```
$ find ziptest | zip -@ test
  adding: ziptest/ (stored 0%)
  adding: ziptest/testfile (deflated 62%)
  adding: ziptest/file2 (deflated 62%)
  adding: ziptest/file1 (deflated 66%)
```

>`zip` utiliza dos métodos para añadir ficheros a un archivo. `stored` significa que el fichero se guarda sin compresión, mientras que `deflated` indica el uso de compresión («`deflated 62%`», por ejemplo, indica que el fichero ocupa tan solo el 38% de su tamaño original).`zip` elige la opción óptima, a no ser que se deshabilite completamente con la opción `-0`.

>Si ejecutamos `zip` con un archivo ZIP existente como primer parámetro sin indicar nada más, los ficheros a archivar se _añadirán_ en la parte superior del contenido existente (los ficheros existentes con el mismo nombre se sobrescribirán).

`zip` soporta diferentes opciones que podemos consultar con `zip -h` (o `-h2` para obtener información más detallada).

Para desempaquetar un archivo ZIP haremos uso de `unzip`. Antes de ejecutar esta acción, podemos examinar el contenido del archivo con la opción `-v`.

Ejecutando `unzip` con el nombre del fichero como único parámetro es suficiente para desempaquetar el archivo:

```
$ mv ziptest ziptest.original
$ unzip testfile
Archive:  test.zip
   creating: ziptest/
  inflating: ziptest/testfile
  inflating: ziptest/file2
  inflating: ziptest/file1
```

Usaremos la opción `-d` para desempaquetar el archivo en un directorio diferente al actual. El directorio será creado si es necesario:

```
$ unzip -d dir testfile
Archive:  test.zip
   creating: dir/ziptest/
  inflating: dir/ziptest/testfile
  inflating: dir/ziptest/file2
  inflating: dir/ziptest/file1
```

Si escribimos el nombre de un fichero, solo se extraerá dicho fichero:

```
$ rm -rf ziptest
$ unzip test ziptest/file1
Archive:  test.zip
  inflating: ziptest/file1
```

Por otro lado, podemos utilizar la opción `-x` para seleccionar los ficheros a excluír del desempaquetado:

```
$ rm -rf ziptest
$ unzip test -x ziptest/file1
```

Si necesitamos ver el contenido de un fichero comprimido, lo podemos hacer sin tener que descomprimirlo. Existen tres variantes del comando `cat`: `bzcat`, `xzcat` y `zcat`. Lo que hacen es ejecutar una descompresión temporal del fichero y enviar el contenido a la salida estándar STDOUT.

```
$ cat > alfabeto.txt
Alpha
Tango
Bravo
Echo
Foxtrot
$ xz alfabeto.txt
$ ls
alfabeto.txt.xz
$ xzcat alfabeto.txt.xz
Alpha
Tango
Bravo
Echo
Foxtrot
```

Hay que recordar que la utilidad `xzcat` se usa para visualizar ficheros comprimidos con `xz`. No se puede utilizar con ficheros comprimidos con `bzip2`.

> Si nos encontramos con un fichero del que desconocemos el tipo de compresión utilizado, debemos hacer uso del comando `file` para comprobarlo.

### Comandos vistos en el tema

Comando | Descripción
--- | ---
bunzip2 | Programa de descompresión para ficheros `.bz2`.
bzip2 | Programa de compresión de ficheros
gzip | Utilidad de compresión de ficheros
split | Divide un fichero en trozos
tar | Gestor de archivos
unzip | Software para descomprimir archivos ZIP
zip | Software para archivar y comprimir similar a PKZIP
