# Tema 2 - Usando Linux

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Loggin In Out](#loggin-in-out)
    - [Inicio de sesión en entorno gráfico](#inicio-de-sesión-en-entorno-gráfico)
    - [Fin de sesión en entorno gráfico](#fin-de-sesión-en-entorno-gráfico)
    - [Inicio de sesión en una consola de texto](#inicio-de-sesión-en-una-consola-de-texto)
    - [Fin de sesión en una consola de texto](#fin-de-sesión-en-una-consola-de-texto)
- [Encendido y apagado](#encendido-y-apagado)
- [El administrador del sistema](#el-administrador-del-sistema)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)
- [Resumen](#resumen)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Acceder al sistema 
* Comprender la diferencia entre las cuentas de usuario sin privilegios y la de administrador del sistema

## Loggin In Out

Linux distingue entre diferentes usuarios, por lo tanto, debemos identificarnos antes de poder usar el sistema. Debemos identificarnos (* hacer login*), y según la información proporcionada, el sistema decide qué podemos hacer (y que no!). Necesitamos una cuenta de usuario. La contraseña sirve para verificar que sólo nosotros podemos usar esa cuenta de usuario. La cual, se debe mantener en secreto.

> Algunas distribuciones modernas intentan ayudarnos e incorporan la opción de *inicio automático*. Si usamos esta opción, el equipo no solicita identificación, e inicia sesión de forma automática. Debemos evitar el uso de esta opción en equipos a los que puedan acceder terceras personas.

### Inicio de sesión en entorno gráfico

Es común que las estaciones Linux dispongan de un entorno gráfico. Después de identificarnos, se inicia la sesión gráfica.
<p align="center"><img width="400" src="img/T02_01.png"></p>

### Fin de sesión en entorno gráfico

Una vez finalizado el trabajo, debemos salir del sistema. El modo de salir depende del entorno gráfico usado, pero suele existir un menú para realizar dicha tarea.
<p align="center"><img width="400" src="img/T02_02.png"></p>

### Inicio de sesión en una consola de texto

Los servidores suelen soportar únicamente una consola de texto. En el prompt de login debemos introducir nuestros datos de usuario.
<p align="center"><img width="400" src="img/T02_03.png"></p>

### Fin de sesión en una consola de texto

Usaremos el comando `logout`. Una vez que hayamos salido del sistema, se volverá a mostrar el prompt de login.
<p align="center"><img width="400" src="img/T02_04.png"></p>

## Encendido y apagado

Un equipo Linux lo podremos arrancar si tenemos acceso al botón de encendido. Por otro lado, nunca debemos apagarlo haciendo uso de ese mismo botón. Los equipos *importantes* sólo pueden ser apagados por el administrador. Las estaciones de trabajo pueden apagarse a través del entorno gráfico.

## El administrador del sistema

Como usuario *normal*, los privilegios sobre el sistema son limitados. Por ejemplo, no puedes modificar los ficheros que no te pertenecen, o incluso leer otros, como el que contiene las contraseñas cifradas. Pero existe una cuenta para administrar el sistema que no está sujeta a esas restricciones. Es la cuenta del usuario *root*.
El usuario *root*, puede leer y escribir todos los ficheros, y realizar tareas que estaban prohibidas para un usuario normal. Tener permisos de administrador, es al mismo tiempo un privilegio y una responsabilidad.
<p align="center"><img width="300" src="img/T02_05.gif"></p>

Debemos evitar hacer login con el usuario *root* a través del entorno gráfico. Esto supone un riesgo para la seguridad del sistema. Podemos hacer uso del comando `/bin/su -` para asumir la identidad de *root*. De esta forma se inicia una nueva shell idéntica a si iniciaramos sesión como *root*. Para cerrar esta shell usaremos el comando `exit`.

> Debemos hacer uso del comando `su` a través de su ruta completa `/bin/su -`. Un usuario podría engañarnos haciendo uso del comando `su` en su equipo, donde tendríamos que introducir nuestra contraseña. Este usuario podría haber modificado dicho comando, el cual podría escribir la contraseña a un fichero, escribir un mensaje de *contraseña incorrecta* y finalizar. Cuando intentaramos de nuevo utilizar el comando `su`, el usuario ya podría haber cambiado nuestra contraseña.

Nos daremos cuenta de que poseemos privilegios de administrador observando el prompt, ya que para el usuario *root* suele estar indicado con el símbolo `#`. Mientras que para los usuarios normales suele utilizarse `$` ó `>`.

> En Ubuntu no podemos hacer login como *root*. En su lugar, el usuario creado durante la instalación podrá ejecutar comandos con privilegios de administrador, usando el comando `sudo`:

```
	$ sudo chown tom file.txt
```

## Comandos vistos en el tema

| Comando | Descripción |
| --- | --- |
| exit | Sale de la shell |
| logout | Finaliza una sesión de shell |
| su | Inicia una shell con un usuario diferente |
| sudo | Ejecuta comando con los permisos de otro usuario diferente |

## Resumen

* Antes de usar un sistema GNU/Linux, debemos iniciar sesión introduciendo el nombre de usuario y la contraseña. Al finalizar, debemos salir del sistema (`logout`).
* El usuario *root* posee todos los privilegios del sistema. No debemos iniciar sesión en un entorno GUI con la cuenta de usuario *root*.
