# Tema 15 - El sistema de ficheros

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Términos](#términos)
- [Tipos de ficheros](#tipos-de-ficheros)
- [El árbol de directorios](#el-árbol-de-directorios)
- [Resumen](#resumen)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos

* Comprender los terminos «fichero» y «sistema de ficheros».
* Reconocer los diferentes tipos de ficheros.
* Conocer los caminos en el árbol de directorios de Linux.
* Saber cómo los sistemas de ficheros externos son integrados dentro del árbol de directorios.

## Términos

De forma general, un fichero es una colección de datos. No hay restricción en cuanto al tipo de dato almacenado en el fichero; puede ser un texto de pocas letras o un archivo conteniendo varios megabytes de los trabajos de un usuario.
Para averiguar el tipo de dato contenido en un fichero podemos usar el comando `file`:

	$ file /bin/ls /usr/bin/groups /etc/passwd

> El comando `file` reconoce el tipo de un fichero basándose en las reglas almacendas en el directorio `/usr/share/file`. El fichero `magic` contiene una versión en texto claro de las reglas. El usuario puede definir sus propias reglas colocándolas en el fichero `/etc/magic`.

Un sistema de ficheros determina la forma de organizar y gestionar los datos en un medio de almacenamiento. Un disco duro almacena bytes que el sistema de ficheros es capaz de recuperar de algún modo. Los detalles de la forma de trabajar de un sistema de ficheros pueden variar de unos a otros, pero al usuario se le presenta una jerarquía en forma de árbol de ficheros y directorios.

## Tipos de ficheros

Los sistemas Linux parten de la premisa de que «todo es un fichero». Puede resultar algo confuso, pero es un concepto muy útil. En principio, se distinguen seis tipos de ficheros:
* **Ficheros simples**. Incluye textos, graficos, sonidos, etc, incluso los programas ejecutables. Se generan usando herramientas como los editores, redirecciones de la shell, etc.
* **Directorios**. Su función es mantener la estructura de almacenamiento. Es una tabla de nombres asociados con números de inodo. Se crean con el comando `mkdir`.
* **Enlaces simbólicos**. Contienen una ruta indicando la ruta a un fichero diferente. Se crean con el comando `ln -s`.
* **Ficheros de dispositivo**. Estos ficheros sirven como interfaces a dispositivos, como pueden ser los discos. Por ejemplo, el fichero `/dev/fd0` representa la primera unidad de disquete. Cada acceso de lectura o escritura a este fichero es redireccionado al dispositivo. Usamos el comando `mknod` para crearlos.
* **FIFO's**. (tuberías con nombre) Al igual que las «pipes» de la shell, permiten la comunicación directa entre procesos. Un proceso abre el FIFO para escritura y el otro para lectura. A diferencia de las tuberías de la shell, los FIFO's tienen nombres de fichero y pueden ser abiertos como ficheros por cualquier programa. Se usa el comando `mkfifo`.
* **Sockets de dominio UNIX**. También son un método de comunicación entre procesos. Es similar a una conexión de red TCP/IP pero en el mismo equipo. Soportan comunicación bidireccional entre los procesos. Son usados por el sistema gráfico X11, en caso de que el servidor X y los clientes se ejecuten en el mismo equipo. No existe un comando especial para crearlos.

## El árbol de directorios

Para mantener cierta compatibilidad, hay ciertas normas a seguir en cuanto a la estructura de directorios y los ficheros que comprenden un sistema Linux, el **FHS** (Filesystem Hierarchy Standard). La mayoría de las distribuciones siguen este estándar (con alguna variación). El FHS describe todos los directorios presentes en el directorio raíz y un segundo nivel para `/usr`. Con el siguiente comando mostramos los subdirectorios presentes en el directorio raíz:

	$ ls -la /

* **El kernel del sistema operativo- /boot**. Contiene el actual sistema operativo: vmlinuz es el kernel. En /boot también existen otros ficheros necesarios para el gestor de arranque (LILO o GRUB).
* **Utilidades generales- /bin**. Contiene los ficheros ejecutables más importantes (en la mayoría, programas del sistema) que son necesarios para el arranque del sistema. Incluye, por ejemplo, `mount` y `mkdir`. Muchos de esos ficheros son tan importantes que no sólo son necesarios durante el arranque del sistema, sino cuando el sistema está ejecutándose -como `ls` y `grep`. También contiene los programas necesarios para recuperar un sistema dañado. Los programas adicionales que no se requieren durante el arranque o para reparar el sistema, se localizan en `/usr/bin`.
* **Programas de sistema especiales - /sbin**. También contiene programas necesarios para el arranque del sistema. Sin embargo, la mayoría de ellos son herramientas de configuración que sólo puede utilizar root. También existe un directorio `/usr/sbin` para otros programas del sistema.
* **Librerías del sistema - /lib**. Contiene las librerías (ficheros o enlaces simbólicos) compartidas que usan los programas de `/bin` y `/sbin`. Bajo el directorio `/lib/modules` se guardan los módulos del kernel, que es código del kernel que puede no utilizarse -drivers de dispositivos, sistemas de ficheros, etc. Pueden ser cargados cuando se les necesite, y en la mayoría de los casos eliminados del kernel después de su uso.
* **Ficheros de dispositivo -/dev**. Contiene una gran variedad de entradas para ficheros de dispositivo. Estos ficheros no poseen contenido como otros ficheros, sino que se refieren a un driver en el kernel a través de «números de dispositivo».

Linux diferencia entre dispositivos de carácter y de bloque. Un dispositivo de carácter, es por ejemplo, un terminal, un ratón o un módem -un dispositivo que procesa caracteres simples. Un dispositivo de bloque manejas los datos en bloques -discos duros o disquetes, por ejemplo- donde los bytes se leen en bloques de 512 (o más). En la salida de `ls -l` se identifican con una letra «c» o «b».

En lugar del tamaño del fichero, se muestran dos números. El primero de ellos se refiere al «número de dispositivo principal», que indica el tipo de dispositivo (SCSI → 8), y el segundo es el «número de dispositivo secundario», que se utiliza para diferenciar entre dispositivos similares, o las diferentes particiones de un disco.

http://www.lanana.org/docs/device-list/

También hay varios «pseudo-dispositivos», por ejemplo, el `/dev/null` que es como un agujero negro para las salidas de los programas que no se necesitan, pero que es necesario enviar a algún lugar. Un comando como:

	$ programa >/dev/null

provoca que toda salida, en lugar de enviarse hacia la pantalla, sea descartada.

Los «dispositivos» `/dev/random` y `/dev/urandom` devuelven una serie de bytes aleatorios. Y pueden usarse para generar claves para algoritmos criptográficos. El fichero `/dev/zero` devuelve un número ilimitado de bytes nulos, que pueden ser usados, por ejemplo, para sobreescribir un fichero con el comando `dd`.
* **Ficheros de configuración - /etc**. Este directorio es muy importante, ya que contiene los ficheros de configuración para la mayoría de programas. Los ficheros `/etc/inittab` y `/etc/init.d/*`, por ejemplo, contienen la mayoría de los datos necesarios para iniciar los servicios del sistema. En la mayoría de ellos, sólo root tiene permisos de escritura, pero todo el mundo puede leerlos.
  * **/etc/fstab** Contiene todos los sistemas de ficheros «montables», así como sus características (tipo, método de acceso, punto de montaje...)
  * **/etc/hosts** Este fichero pertenece a la configuración de la red TCP/IP. Relaciona los nombres de «hosts» a sus respectivas direcciones IP. En redes pequeñas puede sustituír al servidor de nombres.
  * **/etc/inittab** Guarda la configuración para el programa init.
  * **/etc/init.d** Este directorio contiene los scripts init para diferentes servicios del sistema. Se usan para arrancar o parar los servicios durante el inicio o parada del sistema.
  * **/etc/issue** Contiene el mensaje de bienvenida que se muestra antes de solicitar el inicio de sesión al usuario. Suele guardar el nombre de la distribución.
  * **/etc/motd** «Mensaje del día» que se muestra al usuario cuando finaliza el inicio de sesión. Puede utilizarse para avisar a los usuarios de hechos relevantes.
  * **/etc/mtab** Contiene una lista de los sistemas de ficheros montados, incluyendo sus puntos de montaje. A diferencia de `/etc/fstab` solo contiene los sistemas de ficheros que están montados en ese momento, mientras que fstab contiene la configuración de los sistemas que pueden llegar a ser montados. Este fichero no sigue la filosofía del resto de ficheros que se guardan en `/etc`, al guardar información dinámica, al contrario del resto, que tienen un carácter estático.
  * **/etc/passwd** Guarda una lista de todos los usuarios conocidos por el sistema, junto a otros datos relativos a los mismos.
* **Accesorios - /opt**. Este directorio será usado por los paquetes de instalación de software de terceros, y así poder evitar conflictos con los ficheros de la distribución. Este software se instalará bajo un subdirectorio, y debe estar vacío justo después de la instalación de la distribución.
* **Ficheros invariables - /usr**. En este directorio existen varios subdirectorios que contienen programas y ficheros de datos que no son esenciales para el arranque ni para reparar el sistema. Por ejemplo:
  * **/usr/bin**. Programas del sistema no esenciales para el arranque, u otra tarea importante.
  * **/usr/sbin**. Más programas de sistema para root
  * **/usr/lib**. Más librerías
  * **/usr/local**. Directorio para ficheros instalados por el administrador de sistema local, de forma similar al directorio `/opt`. Puede estar vacío en alguna distribución.
  * **/usr/share**. Datos independientes de la arquitectura.
  * **/usr/share/doc**.	Documentación (HOWTO's)
  * **/usr/share/info**. Páginas info
  * **/usr/share/man**.	Páginas man (en subdirectorios)
  * **/usr/src**. Código fuente para el kernel y otros programas.

> El nombre `/usr` suele confundirse con «Unix system resources». Hace tiempo, este directorio, se montaba en un disco grande y más lento. De tal forma que los programas más usados residían en otro disco de menor tamaño, pero más rápido. Hoy esto puede utilizarse de otro modo: colocar `/usr` en su propia partición y montarla como sólo lectura. O incluso importar `/usr` desde un servidor remoto.

* **Una ventana al kernel - /proc**. Uno de los directorios más importantes e interesantes. En realidad es un «pseudosistema» de ficheros, al no ocupar espacio en disco. Los ficheros y subdirectorios son creados por el kernel. Algunos ficheros importantes son:
  * **/proc/cpuinfo**. Contiene información sobre las CPU's.
  * **/proc/devices**. Lista completa de dispositivos soportados por el kernel, incluyendo el número de dispositivo principal.
  * **/proc/dma**. Una lista de los canales DMA en uso.
  * **/proc/interrupts**. Todas las interrupciones hardware en uso.
  * **/proc/meminfo**. Uso de memoria y espacio swap. Lo utiliza el programa `free`.
  * **/proc/version**. Contiene el número de versión y fecha de compilación del kernel actual.
  * **/proc/loadvg**. Tres números con la carga de trabajo de la CPU durante los últimos 1, 5 y 15 minutos. Visibles con el comando `uptime`.

> Antes de inventarse `/proc`, los programas que mostraban información del estado de los procesos, por ejemplo `ps`, debían acceder a la información que poseía el kernel, y por lo tanto era necesario un alto conocimiento de las estructuras de datos internas al núcleo. Estas estructuras podían modificarse con cierta frecuencia, lo que obligaba a modificar también los programas que accedían a ellas. El sistema de ficheros `/proc` sirve para mantener una capa intermedia entre el kernel y los programas. 

* **Control hardware - /sys**. El kernel Linux utiliza este directorio desde su versión 2.6. Al igual que `/proc`, es creado bajo demanda por el propio kernel, y consiste en una «vista» del hardware disponible.
* **Ficheros variables dinámicamente - /var**. Contiene ficheros que se modifican de forma dinámica. Los programas, durante su ejecución crean datos, por ejemplo, `man` debe descomprimir los ficheros fuente, mientras que las páginas formateadas deben estar disponibles en caso de que se vuelvan solicitar. De forma similar, cuando se imprime un documento, los datos «formateados» deben ser guardados para el envío a la impresora. Los ficheros de «log» se almacen en `/var/log`, y el correo electrónico se guarda en `/var/mail`.
* **Ficheros temporales - /tmp**. Muchas utilidades, como los editores o el comando `sort`, requieren de espacio para ficheros temporales. Algunas distribuciones realizan una limpieza de este directorio durante el arranque del sistema.
* **Ficheros del servidor - /srv**. Aquí se pueden encontrar diferentes ficheros de varios programas servidor. Es un directorio relativamente nuevo, y es posible que no exista en algunas distribuciones. Al no existir una ubicación «estándar» para los documentos de los servidores web, FTP, etc., el FHS trata de poner de acuerdo la ubicación de dichos ficheros, que en otro caso (un sistema sin `/srv`), podrían ubicarse en `/usr/local` o `/var`.
* **Acceso al CDROM o disquetes - /media**. Este directorio se suele generar de forma automática; y contiene directorios vacíos, como `/media/cdrom` y `/media/floppy`, que pueden servir como puntos de montaje para CDROM's o disquetes.
* **Acceso a otros medios de almacenamiento - /mnt**. Este directorio sirve para el montaje durante un breve periodo de tiempo de otros medios de almacenamiento. Algunas distribuciones, como Red Hat, utilizan este directorio para el montaje del CDROM o disquete, en lugar de `/media`.
* **Directorios personales de los usuarios - /home**. Contiene los directorios home de los usuarios, excepto `root`.

> Si tenemos que gestionar varios cientos de usuarios, es recomendable, para proteger la privacidad y la eficiencia, no disponer de todos los directorios bajo `/home`, sino realizar subdirectorios, utilizando los grupos como criterio de agrupamiento.<br/>
	`/home/develop/jim`<br/>
	`/home/support/tom`

* **Directorio personal del Administrador -/root**. Se trata de un directorio de usuario similar al resto de usuarios, con la diferencia de encontrarse situado bajo la raíz del sistema de ficheros «/». Como el directorio `/home` se suele situar en un disco (partición) diferente, se trata de evitar un fallo en el acceso al directorio del administrador.
* **Propiedad perdida -lost+found**. (pertenece a `ext`, no a FHS). Este directorio contiene aquellos ficheros en buen estado, pero que no pertenecen a ningún directorio. Los comprobadores de consistencia del sistema de ficheros crean enlaces a dichos ficheros en el directorio `lost+found`, dentro del propio sistema de ficheros.

## Resumen

* Los ficheros son colecciones de datos almacenados bajo un nombre. Linux también usa este concepto para los dispositivos y otros objetos.
* El método para organizar los datos y la información administrativa sobre un disco, se denomina sistema de ficheros. El mismo término se utiliza para designar la jerarquía de árbol de los ficheros y directorios en el sistema.
* Los sistemas de ficheros en Linux, contienen ficheros simples, directorios, enlaces simbólicos, ficheros de dispositivo, FIFO's y sockets de dominio UNIX.
* El FHS describe el significado de los directorios más importantes en un sistema Linux, y es seguido por la mayoría de las distribuciones.

## Comandos vistos en el tema

Comando | Descripción
-- | --
file | Muestra el tipo del contenido del fichero
free | Muestra el espacio usado por la memoria principal y por el espacio de intercambio
mkfifo | Crea FIFO's (tuberías con nombre)
mknod | Crea ficheros de dispositivo
