# Tema 13 - Almacenamiento

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Conceptos básicos](#conceptos-básicos)
- [Tipos de dispositivos](#tipos-de-dispositivos)
- [Particiones](#particiones)
- [Detección automática de las unidades](#detección-automática-de-las-unidades)
- [Almacenamientos alternativos](#almacenamientos-alternativos)
    - [Multipath](#multipath)
    - [Logical Volume Manager (LVM)](#logical-volume-manager-lvm)
    - [Tecnología RAID](#tecnología-raid)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Conceptos básicos

El método más común de almacenar información de forma permanente en un ordenador es guardarla en un disco duro (HDD). Las unidades de disco duro son dispositivos que almacenan los datos en discos magnéticos a través de cabezas que ejecutan las operaciones de lectura y escritura.

En la actualidad, es de uso mayoritario otro tipo de discos, los *solid-state drive* (SSD). Estos dispositivos hacen uso de circuítos integrados para el almacenado de los datos. Por lo tanto, al carecer de piezas móviles obtienen mayores velocidades de transferencia y menor desgaste durante su vida útil. La gestión que realiza Linux de ambos tipos de discos es la misma, ya que tan solo cambia el método de conexión.

## Tipos de dispositivos

Mientras que los HDD's y los SSD's solo se diferencian en la forma de guardar los datos internamente, usan el mismo tipo de conexión al sistema. A continuación se muestran los diferentes métodos que se usan en Linux para conectar dispositivos de almacenamiento.

* *Parallel Advanced Technology Attachment* (PATA) conecta los dispositivos a través de un interfaz paralelo. Soporta dos dispositivos por adaptador.
* *Serial Advanced Technology* (SATA) conecta dispositivos a través de un interfaz serie, pero que es mucho más rápido que PATA. Soporta hasta cuatro dispositivos por adaptador.
* *Small Computer System Interface* (SCSI) conceta los dispositivos a través de un cable paralelo, pero con la velocidad de un SATA. Soporta hasta 8 dispositivos por adaptador.

Cuando conectamos una unidad de disco a Linux, el kernel le asigna un fichero en el directorio `/dev`. A este fichero se le conoce como *raw device*, y proporciona un acceso directo a la unidad desde Linux. Cualquier dato enviado a este fichero, será escrito en la unidad de disco, y si se lee del fichero, los datos se reciben desde el disco.

Para los dispositivos PATA, este fichero tiene el formato `/dev/hdx`, donde la `x` representa cada uno de los discos conectados, comenzando en la `a`. En el caso de SATA y SCSI, Linux usa el formato `/dev/sdx`.

## Particiones

La mayoría de los sistemas operativos nos permiten dividir el disco en diferentes secciones, llamadas *particiones*. El particionado de una unidad de disco nos puede ayudar a organizar la información almacenada en la misma.

Para poder gestionar las particiones, precisamos de algún método para su identificación, así como delimitar su espacio asignado. Los sistemas que usaban la antigua BIOS, empleaban el *Master Boot Record* para la gestión de dichas particiones. Soportaba hasta cuatro particiones primarias en un único disco.

Los sistemas actuales que usan UEFI, hacen uso del método *GUID Partition Table (GPT)* para la gestión de las particiones. Admiten hasta un máximo de 128 particiones en un disco.

Linux crea un fichero en el directorio `/dev` para cada partición del disco. Le añade un número después de la letra asignada al disco, por ejemplo, la primera partición del primer disco SATA, será vinculada a `/dev/sda1`.

## Detección automática de las unidades

Linux detecta las unidades y las particiones en el momento de arranque del sistema, asignando los ficheros correspondientes. Sin embargo, esta situación cambia cuando hacemos uso de unidades extraíbles, que deben ser reconocidas en tiempo de ejecución.

La mayoría de los sistemas Linux usan `udev`, el cual se ejecuta en segundo plano y detecta el nuevo hardware que se conecta al sistema. Una característica de `udev` es que hace uso de *ficheros persistentes* para las unidades de almacenamiento. Cuando se conecta una unidad extraíble, el nombre asignado en `/dev` puede cambiar, lo que resultará poco útil para ciertas aplicaciones. Para resolver esta situación, `udev` emplea el directorio `/dev/disk` para guardar los ficheros de dispositivo que se basan en atributos únicos de la unidad. `udev` crea cuatro directorios separados para almacenar los enlaces:

* `/dev/disk/by-id`: Se guardan según los datos del fabricante, modelo y número de serie.
* `/dev/disk/by-label`: Se guardarn según la etiqueta asignada al dispositivo.
* `/dev/disk/by-path`: Se guardan según el puerto físico al que está conectado.
* `/dev/disk/by-uuid`: Se guardan según su UUID (identificado universal).

Con el uso de los anteriores enlaces, podemos hacer referencia a un dispositivo de forma independiente a donde, o cuando se encuentre conectado al sistema Linux.

## Almacenamientos alternativos

El sistema de particionado visto en la sección anterior presenta importantes limitaciones. Por ejemplo, una vez creadas y formateadas las particiones, resulta muy complejo hacer cambios en el tamaño. Con el objetivo de conseguir un sistema de almacenamiento más dinámico y seguro antes posibles fallos, Linux incorpora algunas técnicas que nos ayudan a superar estas y otras limitaciones.

### Multipath

El kernel Linux incorpora soporte para *Device Mapper (DM) multipathing*, que nos permite configurar diferentes rutas entre el sistema Linux y los dispositivos de almacenamiento en red. *Multipathing* añade las diferentes rutas que van incrementar el rendimiento cuando están todas activas, y presenta tolerancia a fallos cuando alguna de ellas se vuelve inactiva.

Las herramientas para la gestión del *multipathin* incluyen:

* *dm-multipath*: El módulo del kernel que proporciona soporte para *multipath*.
* *multipath*: Un comando para consultar los dispositivos *multipath*.
* *multipathd*: Un proceso en segundo plano para monitorizar las rutas y poder activarlas/desactivarlas.
* *kpartx*: Una herramienta de la línea de comandos para crear nuevas entradas asociadas a los dispositivos *multipath*.

El *DM multipathin* hace uso del directorio `/dev/mapper`, donde se crean un fichero `mpathN` para cada dispositivo de almacenamiento *multipath* que se añade al sistema. Este fichero se comporta como un fichero de dispositivo a través del cual se pueden crear particiones y sistemas de ficheros.

### Logical Volume Manager (LVM)

El *Logical Volume Manager* también usa `/dev/mapper` para crear las unidades virtuales. Podemos agregar varias particiones físicas a nuestros volúmenes virtuales, que serán vistas como una única partición desde el sistema.

El gran beneficio que presenta LVM es que podemos añadir o quitar particiones físicas a un volumen lógico, aumentando o disminuyendo el espacio de dicho volumen lógico.

Cuando deseamos usar una partición dentro de nuestro esquema LVM, debemos *marcarla* con el tipo apropiado (Linux LVM) a través de las herramientas `fdisk` o `gdisk`. A continuación, podemos hacer uso de diferentes herramientas para la gestión de los volúmenes lógicos:

* `pvcreate`: Crea un volumen físico.
* `vgcreate`: Agrupa los volúmenes físico en un grupo.
* `lvcreate`: Crea un volumen lógico a partir de un grupo.

Los volúmenes lógicos tendrán una entrada en el directorio `/dev/mapper`, el cual se comporta como un disco que se puede formatear y usar como una partición normal.

### Tecnología RAID

*Redundant Array of Inexpensive Disks (RAID)* es una tecnología que ha cambiando el mundo de los centros de datos. RAID nos permite aumentar el rendimiento y la seguridad. Existen diferentes esquemas de uso:

* *RAID 0*: Distribuye los datos a lo largo de varios discos, aumentando la velocidad de acceso.
* *RAID 1*: Replica los datos en varios discos. Aumenta la tolerancia a fallos.
* *RAID 10*: Combina los dos esquemas anteriores.
* *RAID 4*: Distribuye los datos haciendo uso de *paridad*. Consiste en guardar un *bit de paridad* en un disco asignado a tal efecto. Es posible recuperar la información ante el fallo de uno de los discos principales.
* *RAID 5*: Distribuye los datos, y los *bit de paridad* entre los diferentes discos. Elimina la dependencia del disco de *paridad* del esquema anterior.
* *RAID 6*: Aumenta la paridad al doble, por lo que resultará tolerante al fallo de dos de los discos del esquema.

Los sistemas que implementan RAID a través de hardware suelen ser costosos y no serán de uso frecuente en entornos personales y/o domésticos. Linux realiza una implementación de RAID a nivel de software, haciendo uso de la herramienta `mdadm`. El dispositivo RAID aparecerá como un dispositivo único en el directorio `/dev/mapper`, que luego podremos particionar y crear los sistemas de ficheros necesarios.


