# Tema 1 - Introducción a GNU/Linux

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [¿Por qué debería conocer Linux?](#%C2%BFpor-qué-debería-conocer-linux)
    - [Una breve historia de GNU/Linux](#una-breve-historia-de-gnulinux)
    - [Efectos del modelo de licencias abiertas](#efectos-del-modelo-de-licencias-abiertas)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Conocer el nacimiento del sistema operativo, y las personas responsables.
* Entender los efectos de las licencias libres sobre el código fuente.

## ¿Por qué debería conocer Linux?

Es un sistema operativo en rápido crecimiento, barato y flexible. Es el protagonista en la mayoría de los servidores de Internet, y una plataforma cada vez más viable en las estaciones de trabajo. Conocer Linux te ayudará a mejorar tu situación en el mercado, proporcionando una ventaja a la hora de buscar trabajo o un ascenso en tu organización.

El [Linux Professional Institute](https://www.lpi.org) (LPI) ha creado la certificación [LPIC-1](https://www.lpi.org/our-certifications/lpic-1-overview) orientada para aquellas personas que desean desarrollar su carrera en el mundo Linux.

La finalidad de este curso es ayudarle a aprender Linux, y conseguir la certificación.

### Una breve historia de GNU/Linux

* 1984 - Richard Stallman inicia el proyecto GNU
  * GNU's not UNIX
  * http://www.gnu.org
* Propósito: un UNIX libre
  * «Libre como en la palabra Libertad, no barra libre»
* Primer paso: reimplementar las utilidades de UNIX
  * Compilador de C, librerías de C
  * editor de texto (emacs)
  * bash
* Para financiar el proyecto GNU, se crea la Free Software Foundation
  * http://www.fsf.org
* 1991 - Linus Torvalds escribe la primera versión del kernel Linux
  * Incialmente, un proyecto de investigación sobre el modo protegido del 386
  * Linus'UNIX -> Linux
  * Junto con GNU y otras herramientas forma un completo sistema UNIX
* 1192 - Aparecen las primeras distribuciones
  * Kernel Linux
  * GNU y otras herramientas
  * Procedimientos de instalación
* El resto es historia...
* La mayor parte del software se distribuye bajo la licencia [GPL de GNU](https://www.gnu.org/licenses/licenses.es.html)
* A Linux se le denomina «copyleft» (en lugar de «copyright»)
  * Puedes copiar el software
  * Puedes obtener el software
  * Puedes modificar el código fuente y recompilarlo
  * Puedes distribuir el código modificado y sus binarios
  * Puedes cobrar por todo ello
* Pero no puedes modificar la licencia
  * Por lo que tus clientes/usuarios tendrán los mismos derechos que tú
  * Por lo tanto, no podrás ganar dinero sólo con la venta del software.
* Se utilizan otras licencias (BSD, Mozilla, Apache, etc)

### Efectos del modelo de licencias abiertas

* Todo el mundo puede acceder al código
  * Los voluntarios pueden desarrollar software a través de internet.
  * Linus Torvalds coordina el desarrollo del kernel.
* Código mejor revisado
  * Seguridad
  * Rendimiento
* La licencia no puede cambiar
