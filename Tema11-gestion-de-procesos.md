# Tema 11 - Gestión de procesos

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [¿Qué es un proceso?](#%C2%BFqué-es-un-proceso)
- [Estados de un proceso](#estados-de-un-proceso)
- [Información de un proceso](#información-de-un-proceso)
- [Examinar procesos con top](#examinar-procesos-con-top)
- [Gestión de varias pantallas](#gestión-de-varias-pantallas)
    - [Multiplexing con screen](#multiplexing-con-screen)
    - [Multiplexing con tmux](#multiplexing-con-tmux)
- [Controlando los procesos con kill y killall](#controlando-los-procesos-con-kill-y-killall)
    - [Envío de señales con el comando pkill](#envío-de-se%C3%B1ales-con-el-comando-pkill)
- [Prioridades de los procesos con nice y renice](#prioridades-de-los-procesos-con-nice-y-renice)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

<!-- START DOCTOC -->
<!-- END DOCTOC -->

## Objetivos

* Entender el concepto de proceso en Linux
* Utilizar los comandos más importantes para obtener información de los procesos.
* Enviar señales a los procesos.
* Modificar la prioridad de los procesos.

## ¿Qué es un proceso?

Un proceso es un «programa en ejecución». Los procesos tienen código que se ejecuta, y datos sobre los que trabaja, pero también algunos atributos:
* El número de proceso (**PID**, «process identity»), que sirve para identificar el proceso, y que sólo puede asignarse a un proceso al mismo tiempo.
* Todos los procesos conocen el número de proceso padre, o **PPID**. Cada proceso puede *engendrar* a otros, que incorporan una referencia a su creador. El único proceso que no tiene *padre*, es el pseudo proceso con PID 0, que es generado durante el arranque del sistema, y que es responsable de crear el proceso «init» con PID igual a 1, que a su vez, se convierte en el antepasado de todos los procesos en el sistema.
* Cada proceso es asignado a un usuario y un conjunto de grupos. Es importante para poder determinar los permisos de acceso que posee a ficheros, dispositivos, etc. Por otro lado, el usuario del proceso puede actuar sobre el mismo, parándolo, finalizándolo, etc. El propietario y el grupo son pasados a los procesos hijo.
* El sistema divide el tiempo de la CPU en pequeños slots («time slices»), que pueden ser del orden de fracciones de segundo. El proceso actual es ejecutado durante uno de esos slots de tiempo, y el sistema debe decidir qué proceso ejecutará en el siguiente intervalo. Esta decisión es responsabilidad del planificador, basándose en la prioridad de los procesos.
* Un proceso tiene otros atributos, como el directorio actual, entorno del proceso,... que también serán enviados a los procesos hijo.
* Podemos consultar el sistema de ficheros `/proc` para encontrar información al respecto. Hay varios directorios que utilizan números como nombres; cada uno de esos directorios corresponde a un proceso y su nombre es el PID del proceso.

En el directorio del proceso, hay varios ficheros que contienen información del proceso. Los detalles se encuentran en la página `man proc(5)`.

> El control de trabajos (*jobs*) disponible en muchas shells es también un método de gestión de procesos. Un trabajo es un proceso cuyo padre es la shell. Desde la propia shell podemos controlar los trabajos a través de los comandos, `fg`, `bg` y `jobs`, así como con las combinaciones CTRL-Z y CTRL-C.

## Estados de un proceso

Otra importante propiedad de un proceso es su estado de proceso. 
* **runnable** - Un proceso que está en memoria, espera a que sea ejecutado por la CPU.
* **operating** - Cuando el proceso está siendo ejecutado por la CPU, y después de agotar su slot de tiempo, vuelve al estado *runnable*.
* **sleeping** - Es frecuente que un proceso tenga que esperar por un dispositivo, y no se le puede asignar tiempo de CPU. Durante este tiempo se mueve a la memoria virtual.
* **stopped** - Procesos parados, por ejemplo, por la combinación CTRL-Z enviada desde la shell.

Una vez que el proceso finaliza, devuelve un código de retorno, que puede ser usado para conocer si se ha ejecutado con éxito o no.

De vez en cuando, los procesos aparecen marcados como zombies usando el estado «Z». Un proceso se convierte en zombie cuando finaliza pero no tiene a quién devolver su código de retorno. Si el proceso *zombie* no desaparece de la tabla de procesos, es porque su padre todavía está esperando el código de retorno. Este proceso *zombie* no puede eliminarse de la tabla de procesos. No hay problema, ya que tampoco está haciendo uso de memoria ni CPU, tan sólo tiene ocupada una entrada de la tabla de procesos.

> Los procesos *zombie* desaparecen cuando su padre también desaparece, ya que que los procesos huérfanos son adoptados por el proceso *init*. El proceso *init* está la mayoría del tiempo esperando los códigos de retorno de los procesos que finalizan, por lo que puede recoger ese código de retorno.

## Información de un proceso

Normalmente, no podremos acceder a la información de procesos almacenda en el directorio `/proc`, pero disponemos de comandos para consultarla.	El comando `ps` («process status») está disponible en todo sistema tipo Unix. Sin argumentos, muestra los procesos de la terminal actual. En la lista, se muestran el PID, el terminal TTY, el estado de proceso STAT, el tiempo usado de CPU y el comando que se ha ejecutado.

La sintaxis de `ps` es un poco confusa, ya que admite el formato de opciones propio de GNU, de BSD y de Unix-98. Algunas de estas opciones son:

Opción | Descripción
--- | ---
**a** («all») | Muestra todos los procesos que están asociados a una terminal
**-A**, **-e** | Muestra todos los procesos del sistema
**--forest** | Muestra la jerarquía de procesos
**l** («long») | Muestra información extra, como la prioridad
**r** («running») | Solo se muestran los procesos «runnable»
**T** («terminal») | Muestra todos los procesos de la terminal actual
**-u**, **--user** <list> | Solo muestra los procesos cuyo usuario «efectivo» se encuentra en la lista <list>
**-U**, **--User** <list> («user») | Solo muestra los procesos cuyo usuario «real» se encuentra en la lista <list>
**x** | También se muestran los procesos sin terminal.

> El usuario «real» de un proceso es aquel con el que se ha iniciado sesión en el sistema. Y el usuario «efectivo» es el que se usa de forma temporal, a través de permisos SUID o GUID, por ejemplo. Por lo tanto, tendremos la precaución de usar las dos opciones para tener una vista completa de los procesos del usuario en cuestión.

La salida por defecto del comando `ps` se muestra a continuación:

```
$ ps
  PID TTY          TIME CMD
  813 pts/0    00:00:00 bash
  816 pts/0    00:00:00 ps
```

En la salida anterior, tan solo se muestran los procesos que se encuentran en ejecución en la *shell* del usuario.

Tal y como se comentó anteriormente, la gran cantidad de diferentes opciones y formato de las mismas dificulta el uso frecuente de `ps`, polo que se aconseja la consulta del manual en cuanto tengamos alguna duda. La mayoría de administradores de sistema cuenta con un pequeño conjunto de opciones de uso frecuente que le facilitan su trabajo. Por ejemplo, si precisamos consultar todos los procesos en ejecución en nuestro sistema, usaremos la combinación `-ef` del estilo Unix.

	$ ps -ef
	UID        PID  PPID  C STIME TTY          TIME CMD
	root         1     0  0 10:23 ?        00:00:00 /sbin/init
	[...]
	vagrant    813   812  0 10:23 pts/0    00:00:00 -bash
	vagrant    817   813  0 10:25 pts/0    00:00:00 ps -ef

Este salida nos muestra información relevante de cada uno de los procesos del sistema:

* **UID** - El usuario responsable del proceso
* **PID** - El ID del proceso
* **PPID** - El ID del proceso padre (si se ha iniciado desde otro proceso)
* **C** - El uso de procesador realizado durante su tiempo activo
* **STIME** - La hora del sistema cuando el proceso se inició
* **TTY** - El terminal desde el cual se inició el proceso
* **TIME** - El tiempo de CPU necesario para la ejecución del proceso
* **CMD** - El nombre del programa que ha iniciado el proceso

> Algunos nombres de comando mostrados en esta salida aparecen entre corchetes. Eso indica que dichos procesos no residen en la memoria del sistema, sino que se han enviado al espacio de memoria virtual del disco (*swapping*).

Si le pasamos al comando `ps` un PID, sólo muestra cierta información de dicho proceso:

	$ ps 1

Con la opción `-C`, `ps` muestra información sobre el proceso (o procesos), basados en un comando particular:

	$ ps -C bash

Para visualizar todos los procesos de un usuario:

```
	$ ps -u vagrant -U vagrant
	  PID TTY          TIME CMD
	  550 ?        00:00:00 systemd
	  551 ?        00:00:00 (sd-pam)
	  812 ?        00:00:00 sshd
	  813 pts/0    00:00:00 bash
	  819 pts/0    00:00:00 ps
```

## Examinar procesos con top

El comando `ps` es una gran herramienta para conseguir una foto fija del estado de los procesos del sistema, pero en ocasiones es necesario obtener información adicional. Por ejemplo, si deseamos obtener información sobre la actividad en cuanto al traslado de procesos hacia la memoria virtual.

Para solucionar el anterior escenario, además de otras funcionalidades, podremos hacer uso de la salida en tiempo real del comando `top`.

![image](/img/T11_01.png)

La primera parte de la salida muestra información general del sistema, como el tiempo de «vida» del sistema (uptime), el número de usuarios conectados y la carga media del ssitema. La carga media del sistema se representa con 3 valores; 1 minuto, 5 minutos y 15 minutos. Valore altos representan mayor carga para el sistema.

> Para comprobar de forma rápida la carga de nuestro sistema podemos utilizar el comando `uptime`.

En la segunda línea observamos información al respecto de los procesos; cuantos están en ejecución, cuantos en estado «sleeping», o incluso si tenemos algún proceso «zombie» en nuestro sistema.

En la siguiente línea se muestra información relativa al uso de la/s CPU/s. Y, a continuación aparecen dos líneas con información relativa al uso de la memoria. En la primera hay información relativa a la memoria física del sistema, y en la segunda información sobre la memoria de intercambio.

> Para una consulta rápida del estado de la memoria de nuestro sistema, ejecutaremos el comando `free -h`

Y por último, en la salida del comando `top` se muestra una lista detallada de los procesos en ejecución, con información que nos puede resultar familiar si hemos usado antes el comando `ps`.

* **PID** - El ID del proceso
* **USER** - El nombre del propietario del proceso
* **PR** - La prioridad del proceso
* **NI** - El valor «nice» del proceso
* **VIRT** - La cantidad total de memoria virtual usada por el proceso
* **RES** - La cantidad total de memoria física usada por el proceso
* **SHR** - La cantidad de memoria que el proceso comparte con otros procesos
* **S** - El estado del proceso
* **%CPU** - El porcentaje de tiempo de CPU que está usando el proceso
* **%MEM** - El porcentaje de memoria usada por el proceso
* **TIME+** - El tiempo total de CPU usado por el proceso desde su inicio
* **COMMAND** - El nombre del proceso en la línea de comandos

Esta herramienta dispone de multitud de variantes para modificar la salida, como su ordenación, inclusión de nuevas columnas, etc. Pulsando la tecla `h` durante su ejecución accedemos a la ayuda que nos permite conocer con detalle cada una de las opciones disponibles.

> En este punto, debemos conocer la existencia de la utilidad `watch`, a la que podemos pasarle como parámetro un comando que será ejecutado en intervalos de 2 segundos. Por ejemplo, `watch uptime` nos mostrara la salida del comando `uptime` cada 2 segundos.

## Gestión de varias pantallas

Cuando trabajamos a través de una interfaz web resulta sencillo disponer de varias ventanas con diferentes sesiones en nuestra shell favorita. Sin embargo, si solo disponemos de una conexión a través de una terminal virtual, o sesión SSH, debemos recurrir a otras utilidades. En este caso, haremos uso de un *terminal multiplexer*. Entre las soluciones más populares se encuentran `screen` y `tmux`.

### Multiplexing con screen

Aunque está disponible para la mayoría de las distribuciones, puede ocurrir que no se encuentre instalada por defecto, por lo que tendremos que realizar su instalación a través de la herramienta de gestión de paquetes de nuestra distribución. Después de esto, podremos iniciar su ejecución escribiendo `screen` en la línea de comandos.

Nos muestra la pantalla de bienvenida, en la que debemos pulsar la tecla INTRO. A continuación nos aparece de nuevo el prompt de nuestra shell, lo que dificulta saber si nos encontramos en una sesión de *screen*. Para consultar nuestra localización, haremos uso de los comandos `screen -ls`, o bien `w`.

	$ screen -ls
	There is a screen on:
        	1051.pts-0.buster       (11/15/2020 12:50:08 PM)        (Attached)
	1 Socket in /run/screen/S-vagrant.

	$ w
	 12:51:57 up  2:28,  2 users,  load average: 0.00, 0.00, 0.00
	USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
	vagrant  pts/0    10.0.2.2         11:18    0.00s  0.02s  0.00s screen
	vagrant  pts/1    :pts/0:S.0       12:50    0.00s  0.01s  0.00s w

El objetivo de `screen` es que podamos ejecutar un comando dentro de esa ventana, abandonar esa ventana (detach), y volver más tarde, sin que el comando se vea afectado. Para ello debemos escribir el atajo CTRL+A, y luego pulsar la tecla D. En ese momento, el comando `screen -ls` nos mostrará la sesión que hemos abandonado.

```
[detached from 1095.pts-0.buster]
$ screen -ls
There is a screen on:
	1095.pts-0.buster	(11/15/2020 01:00:48 PM)	(Detached)
1 Socket in /run/screen/S-vagrant.
```

Para regresar a la ventana anterior, debemos hacer uso de la opción `-r`. En nuestro ejemplo:

	$ screen -r 1095	# Donde 1095 es el ID de la ventana

Podemos dividir una ventana de *screen* en varias ventanas, que se conocen como *focos*. Para conseguir este efecto, hacemos uso de la combinación CTRL+A, y luego una tecla (o combinación) para ejecutar una acción determinada, que se detallan en la siguiente tabla:

Tecla/Combinación | Descripción
--- | ---
\ | Finaliza todos los procesos de la ventana y finaliza *screen*
&#124; | Divide en dos focos verticales la ventana actual
Tab | Se mueve al siguiente *foco*
D | Abandona la actual ventana
K | Finaliza la actual ventana
N | Se mueve a la siguiente ventana
P | Se mueve a la anterior ventana
Mayús+S | Divide en dos focos horizontales la ventana actual

En la siguiente práctica vamos a crear una ventana con tres focos, permitiendo la monitorización del sistema en dos de ellas, y dejando la tercera para ejecutar comandos:

1. Ejecutamos el comando `screen` para crear la ventana. Pulsamos INTRO en el mensaje de bienvenida.
2. Ejecutamos el primero de los comandos de monitorización; `top`.
3. Dividimos la ventana en dos *focos*, primero con la combinación CTRL+A, y a continuación Mayús+S.
4. Para movernos al siguiente foco, pulsamos CTRL+A y luego TAB. Comprobamos que aún no tenemos un *prompt* en ese foco.
5. Pulsamos CTRL+A y luego C para crear una nueva ventana dentro del foco. Nos aparecerá el *prompt*.
6. Ejecutamos un segundo comando de monitorización (p.ej. `sudo tail -f /var/log/syslog`).
7. Dividimos la nueva ventana en horizontal haciendo uso de CTRL+A y luego | (barra vertical).
8. Nos movemos al siguiente foco al igual que en el paso 4 (CTRL+A > TAB).
9. Pulsamos CTRL+A y C para tener un *prompt* en ese foco.
10. Podemos ejecutar un tercer comando en ese nuevo foco para ver los resultados en las de monitorización.
11. Después de finalizar nuestro trabajo, pulsamos CTRL+A y luego \ (backslash). Aparece la confirmación de que deseamos salir y finalizar todas nuestras ventanas, introducimos Y para aceptar.

![image](/img/T11_02.png)

### Multiplexing con tmux

La utilidad *tmux* es una recién llegada, y fue liberada 20 años después de la primera versión de *screen*. Presenta funcionalidades similares a `screen` con algunas mejoras. Al igual que `screen`, no suele estar disponible por defecto en la mayoría de distribuciones, por lo que será necesario instalarla.

Después de instalar, creamos nuestra primera ventana con el comando `tmux new`.

![image](/img/T11_03.png)

`tmux` también incorpora una combinación de teclas para acceder a los comandos, en este caso debemos usar CTRL+B. Entonces, para abandonar una ventana pulsamos CTRL+B y luego D. De forma análoga, podemos usar `tmux ls` para mostrar el estado actual de las ventanas.

```
$ tmux new
[detached (from session 0)]
$ tmux ls
0: 1 windows (created Sun Nov 15 16:15:29 2020) [173x20]
```

Y con `attach-session` volvemos a entrar en la ventana deseada:

	$ tmux attach-session -t 0

Por supuesto, también es posible dividir el espacio de trabajo en diferentes paneles. En la siguiente tabla aparecen las teclas (o combinaciones) para aplicar las diferentes configuraciones de `tmux`.

Tecla/Combinación | Descripción
--- | ---
& | Finalizar la ventana actual
% | Dividir la ventana actual en dos paneles verticales
" | Dividir la ventana actual en dos paneles horizontales
D | Abandonar la ventana actual
L | Mover hacia la ventana anterior
N | Mover hacia la ventana siguiente
O | Mover al siguiente panel
CTRL+O | Rota los paneles hacia la ventana actual


## Controlando los procesos con kill y killall

El comando `kill` envía señales a los procesos seleccionados. La señal deseada se especifica bien con un número o con un nombre; también debemos indicar el número del proceso. Algunos ejemplos:

	$ kill -15 4711
	$ kill -TERM 4711
	$ kill -SIGTERM 4711
	$ kill -s TERM 4711
	$ kill -s SIGTERM 4711
	$ kill -s 15 4711

Las señales más importantes, junto con su número y significado:

* **SIGHUP** (1, «hang up») provoca que la shell finalice todos sus procesos hijo que usan la misma terminal que ella. Para procesos en segundo plano sin terminal, se suele usar para volver a leer sus ficheros de configuración.
* **QUIT** (3, «quit») Detiene la ejecución.
* **SIGINT** (2, «interrupt») Interrumpe el proceso, equivale a CTRL-C.
* **SIGKILL** (9, «kill») Finaliza el proceso sin poder ignorarse.
* **SIGTERM** (15, «terminate») Valor por defecto para `kill` y `killall`; finaliza el proceso.
* **SIGCONT** (18, «continue») Permite que un proceso parado con SIGSTOP pueda continuar.
* **SIGSTOP** (19, «stop») Para un proceso temporalmente.
* **SIGSTP** (20, «terminal stop») Equivale a la combinación CTRL-Z.

> Los números de las señales pueden variar entre diferentes versiones de UNIX, e incluso en Linux. Las señales 1, 9 y 15 se pueden usar sin temor, el resto, mejor usar el nombre.

A menos que se indique otra señal, se enviará la SIGTERM, que finaliza el proceso. Los programas pueden ser escritos de tal forma que capturen estas señales, o bien ignorarlas. Las señales SIGKILL y SIGSTOP no son manejadas por el proceso, sino por el kernel, y por lo tanto no pueden ser capturadas o ignoradas. La señal SIGKILL finaliza el proceso sin darle otra opción al proceso, y la SIGSTOP para el proceso y no se le asigna tiempo de CPU. Existen procesos que se ejecutan en segundo plano, que utilizan la señal SIGHUP para volver a leer sus ficheros de configuración, sin tener que reinciarse.

Por supuesto, sólo podemos aplicar `kill` a aquellos procesos que nos pertenezcan. Hay ocasiones en las que un proceso no reacciona ni siquiera ante una señal SIGKILL, ¿cuál?.	

Una alternativa al comando `kill`, es `killall`. La diferencia radica en que los procesos se designan por el nombre, en lugar del número. Y por lo tanto, se le envía la señal a todos los procesos con ese nombre. También se le envía la señal SIGTERM por defecto.
Las opciones más importantes de killall, son:

* **-i** pide confirmación antes de enviar la señal al proceso.
* **-l** muestra una lista de las señales disponibles.

### Envío de señales con el comando pkill

El comando `pkill` permite el envío de señales a los procesos haciendo uso de criterios diferentes a los utilizados por los comandos anteriores, como son el PID o el nombre del comando ejecutado. Se puede filtrar por nombre de usuario, ID de usuario, terminal asociado, etc. Además, se permite el uso de comodines.

Incluso, se puede combinar con `pgrep` para comprobar el funcionamiento del criterio de selección antes de enviar las señales con el comando `pkill`.

En el siguiente ejemplo, se usa la opción `-t` de `pgrep` para obtener todos los PID's asociados a la *tty3*.

	$ pgrep -t tty3
	1716
	1804
	1829
	1832
	$
	$ ps 1829
	  PID TTY	STAT   TIME COMMAND
	 1829 tty3	R+     0:29 stress-ng --class cpu -a 10 ...
	$
	$ sudo pkill -t tty3
	$
	$ pgrep -t tty3
	1846

## Prioridades de los procesos con nice y renice

Como sabemos, la CPU debe ser compartida entre varios procesos. Esto es trabajo del planificador de tareas. Y normalmente, hay más de un proceso en ejecución, y el planificador debe asignar el tiempo de CPU según ciertas reglas. Un factor decisivo es la prioridad del proceso.

Como usuarios (o administradores) no podemos configurar las prioridades directamente. Lo que podemos hacer es pedirle al kernel que favorezca o penalice a ciertos procesos. El valor «nice» indica el grado de favoritismo hacia un proceso, y también es enviado a los procesos hijo.
Con el comando `nice` se indica un nuevo valor nice para un proceso.

	nice [-<valor>] <comando> <parámetro> ...

Los posibles valores para `nice` van desde *-20* hasta *+19*. Un valor negativo incrementa la prioridad, y un valor positivo decrementa dicha prioridad. Si no se indica ningún valor, se usa *+10*. Sólo *root* puede iniciar un proceso con valor `nice` negativo. 

> Es importante diferenciar la prioridad por defecto cuando usamos el comando `nice`, que es *+10*, de aquella que aplica el sistema por regla general, que es cero;*0*.

La prioridad de un proceso en ejecución puede ser modificada a través del comando `renice`.

De nuevo, sólo el administrador puede asignar valores `nice` arbitrarios. Los usuarios normales sólo pueden incrementar los valores `nice` de sus propios procesos, a través de `renice`.
	
	$ nice -10 sleep 10 &
	$ ps -l
	# nice --15 sleep 10 &
	# ps -l
	# nice -15 sleep 20 &
	# renice 0 'PID'

## Comandos vistos en el tema

Comando | Descripción
-- | --
kill | Envía una seña a un proceso
killall | Envía una señal a todos los procesos con un cierto nombre.
pkill | Envia señales a los procesos en base a diferentes criterios.
nice | Ejecuta un programa con un valor nice diferente.
nohup |	Inicia un programa que es inmune a la señal SIGHUP.
ps | Muestra información del estado de los procesos.
screen | gestor de ventanas con emulación de terminal
tmux | terminal multiplexer
renice | Cambia el valor nice de un proceso en ejecución.
top | Herramienta orientada a pantalla para monitorizar	procesos.
