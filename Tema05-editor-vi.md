# Tema 5 - Editor vi

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Editores](#editores)
- [El estándar *vi*](#el-estándar-vi)
    - [Funciones básicas](#funciones-básicas)
    - [Modos *vi*](#modos-vi)
    - [Comandos *vi* para modo inserción](#comandos-vi-para-modo-inserción)
- [Desplazándose a través del texto](#desplazándose-a-través-del-texto)
- [Borrando caracteres](#borrando-caracteres)
- [Reemplazando caracteres](#reemplazando-caracteres)
- [Comandos extendidos](#comandos-extendidos)
- [Comandos de línea de comandos](#comandos-de-línea-de-comandos)
- [Otros editores](#otros-editores)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)
- [Resumen](#resumen)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Familiarizarse con el editor *vi*
* Ser capaces de crear y modificar ficheros de texto

## Editores

La mayoría de los sistemas operativos ofrecen herramientas para crear y modificar documentos de texto. Tales programas son los denominados *editores*. Generalmente, los editores ofrecen funciones que van mas allá de la simple introducción de texto. Los buenos editores, permiten a los usuarios eliminar, copiar o insertar palabras o líneas enteras. Para ficheros grandes, resulta útil buscar secuencias de caracteres. En contraste con los editores más extendidos (LibreOffice, MSOffice, Lotus, etc.), los editores de texto trabajan sin formato, es decir, en texto plano.

## El estándar *vi*

El único editor de texto que probablemente sea parte de todo sistema GNU/Linux, es el *vi* (de visual). Esto no significa que dispongamos del *vi* original de BSD, sino algún clon, como *vim* (vi improved), o *elvis*. *vi*, fue el primer editor de texto de pantalla, a diferencia de los existentes en el momento, que sólo trabajaban con una sola línea. Todavía se soporta un editor de línea, llamado *ex*.
	
> «Todo aquel administrador que no use *vi* no es un verdadero usuario de GNU/Linux»

### Funciones básicas

**Buffer**. *vi* trabaja en términos de buffers. Si ejecutamos *vi* con un nombre de fichero como argumento, el contenido de ese fichero será leido en un buffer. Si el fichero no existe, se crea un buffer vacío. Todos los cambios realizados a través del editor sólo se aplican dentro del buffer. Para fijar permanentemente esas modificaciones hay que volcar el contenido del buffer a un fichero. Si queremos descartar los cambios, simplemente salimos del editor sin guardar el contenido del buffer. Además del nombre de un fichero, le podemos pasar otras opciones al editor *vi*.

### Modos *vi*

Como se mencionaba antes, una de las características de *vi* es su inusual forma de uso. *vi* soporta tres modos de trabajo diferentes:

* **Modo comando**: Todas las entradas de teclado corresponden a comandos que no se mostrarán en la pantalla, y la mayoría no precisan de *Intro* para finalizarse. Después de iniciar *vi*, entramos en este modo. Debemos ser cautelosos, ya que cualquier tecla puede ejecutar un comando.
* **Modo insertar**: Toda entrada de teclado es considerada un texto y se muestra en la pantalla. En este modo, *vi* se comporta como un editor *moderno*, aunque sin opción de navegar o corregir texto.
* **Modo linea de comando**: Se usa para introducir comandos más extensos. Suele iniciarse con «:» y finalizados con *Intro*.

En el modo de inserción, casi todos los comandos de navegación y corrección están deshabilitados, lo que lleva a un frecuente intercambio entre los modos inserción y comando. Y puede resultarnos complicado determinar el modo en el que nos encontramos, y esto no ayuda para nada a los usuarios recién llegados.

> Debemos ser conscientes de que vi fue desarrollado cuando los teclados sólo soportaban el bloque de letras de los actuales.

Después de iniciar *vi* sin ningún nombre de fichero, entramos en el modo comando. A diferencia de otros editores, la entrada directa de texto no es posible. Tenemos un cursor en la parte superior, y varias líneas con una tilde al principio.

La última línea (estado), muestra el modo actual (quizás), el nombre del fichero que se está editando (si está disponible) y la posición actual del cursor.

> Si nuestra versión de *vi* no muestra la información de estado, podemos activar esa opción con `ESC:set showmodeINTRO`

Sólo después de un comando como «a» (append), «i» (insert), o «o» (open) entraremos en el modo de inserción. La línea de estado mostrará algo como `-- INSERT --`. Los comandos admitidos se muestran en la siguiente tabla. Hay que tener en cuenta que se diferencia entre mayúsculas y minúsculas. Para salir del modo comando, y volver al modo inserción, pulsamos **ESC**. En modo comando, pulsando **ZZ**, se escribe el buffer y salimos de *vi*.

### Comandos *vi* para modo inserción

| Comando | Resultado |
| --- | --- |
| a | Añade nuevo texto después del cursor |
| A | Añade nuevo texto al final de la línea |
| i | Inserta nuevo texto en la posición actual del cursor |
| I | Inserta nuevo texto al principio de la línea |
| o | Inserta una nueva línea por debajo de la actual |
| O | Inserta una nueva línea por encima de la actual |

Si lo que deseamos es salir del editor sin guardar los cambios, usaremos el comando `:q!`.

Cuando introducimos el comando «:», *vi* se cambia al modo línea de comandos. Podemos apreciar el cambio en la línea de estado, donde aparece el carácter «:». En ese momento, podemos escribir el comando y finalizar con INTRO; *vi* ejecuta el comando y nos devuelve al modo comando. El comando `:w` guarda el buffer. Los comandos `:x` y `:wq` guardan el contenido del buffer y salen del editor; ambos comandos son idénticos al visto antes: `ZZ`.

## Desplazándose a través del texto

En modo inserción, los caracteres introducidos serán colocados en la línea actual. La tecla **INTRO** introduce una nueva línea. Sólo puedo corregir texto con la tecla **RETROCESO**. La navegación sólo es posible en modo comando. Es posible corregir texto en modo comando.

## Borrando caracteres

El comando `d` es usado para borrar caracteres; siempre va seguido de otro caracter que indica exactamente qué borrar. Para facilitar la edición, se puede colocar un prefijo que indica cuántas veces repetir dicho comando. Por ejemplo, el comando `3x` borrará los 3 caracteres siguientes.

Si hemos sido muy impacientes y hemos eliminado más texto del que debíamos, podemos deshacer el último cambio, usando el comando `u` (undo).

## Reemplazando caracteres

El comando `c` sirve para sobreescribir una selección del texto. `c` es un comando combinado, al igual que `d`, precisando de información adicional para saber qué sobreescribir. *vi* eliminará esa parte de texto antes de cambiar a modo de inserción automáticamente.

## Comandos extendidos

*Cortar, copiar y pegar texto*. *vi* ofrece la combinación de comandos para realizar estas tareas, que toman las especificaciones similares a las del comando `c`. El comando `y` copia al buffer interno sin alterar el texto original, mientras que `d` lo *mueve* al buffer interno. Y el comando para pegar es `p`, para insertar el contenido del buffer después del cursor, y `P` para insertar en la posición actual del cursor.

Una peculiaridad de *vi*, es que no sólo existe un buffer, sino 26. De esta forma es posible pegar diferentes textos a diferentes lugares del fichero. Los bufferes internos son referenciados desde la letra «a» a la «z», usando una combinación de dobles comillas y nombres de buffer. La secuencia `“c4w`, copia las siguientes cuatro palabras al buffer interno *c*; la secuencia `“gp` inserta el contenido del buffer *g* en la posición actual del cursor.

*Búsqueda de texto con expresiones regulares*. Para iniciar una búsqueda introducimos un slash «/» en modo comando. Aparece en el fondo de la ventana seguido de un cursor. Introducimos el patrón de búsqueda y comenzamos la búsqueda al pulsar **INTRO**. *vi* comienza la búsqueda desde la posición del cursor hasta el final del documento. Para buscar hacia el principio, la búsqueda debe comenzar con «?» en lugar de «/». Una vez localizada la cadena a buscar, para la búsqueda y se coloca en el primer carácter del patrón localizado. Podemos repetir la misma búsqueda hacia el final usando «n» (next) o hacia el principio usando «N».

*Búsqueda y reemplazo*. *vi* también soporta el reemplazo de caracteres. Se puede usar el siguiente comando *ex*:

	:[<start line>,<end line>]s/<regexp>/<replacement>[/g]

`<start line>` y `<end line>` delimitan el rango de líneas en las que buscar. Sin esto, sólo se trabaja con la línea actual. En lugar de números de línea, podemos usar «.» para referenciar a la línea actual y «$» para la línea final (sin confundir el significado de estos caracteres en las expresiones regulares).

	:5,$s/red/blue/

reemplaza la **primera** ocurrencia de *red* por *blue*, donde las primeras cuatro líneas no se contemplan.

	:5,$s/red/blue/g

reemplaza toda ocurrencia de *red* por *blue*. Incluso si *red* pertenece a una palabra (Freddy Mercury -> Fbluedy Mercury).

## Comandos de línea de comandos

Hemos visto algunos comandos de línea de comandos (o modo «ex»), pero existen muchos más, a los que siempre accedemos desde el modo comando anteponiendo el carácter «:»

## Otros editores

La elección de un editor de texto, es siempre una decisión personal de cada usuario. Tenemos a nuestra disposición los editores *kate* y *gedit*, de KDE y GNOME respectivamente, que son fáciles de usar y aprender, ya que cuentan con una interfaz gráfica que puede satisfacer los requisitos de un usuario normal. También podemos navegar por la lista de paquetes disponibles para nuestra distribución y probar alguno nuevo.

## Comandos vistos en el tema

| Comando | Descripción |
| --- | --- |
| ed | Editor de texto primitivo orientado a línea 
| elvis | Clon popular del editor *vi* |
| ex | Editor de texto orientado a línea |
| vi | Editor de texto orientando a pantalla |
| vim | Clon popular del editor *vi* |


## Resumen

* Los editores de texto son importantes para modificar el contenido de los ficheros. Ofrecen características especiales para facilitarnos el trabajo.
* *vi* es un editor de texto tradicional, muy extendido y potente, con una interfaz de uso muy propia.
