# Tema 22 - Virtualizando Linux

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Máquinas virtuales](#máquinas-virtuales)
    - [Gestión de MVs](#gestión-de-mvs)
    - [Creando una máquina virtual](#creando-una-máquina-virtual)
- [Extensiones Linux](#extensiones-linux)
- [Contenedores](#contenedores)
- [Infraestructura como servicio](#infraestructura-como-servicio)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

Cuando hablamos del concepto *virtualización* nos referimos a algo que no existe físicamente, sino que es una simulación. En el mundo de la informática, esta virtualización la obtenemos gracias a un software específico. A lo largo de este capítulo, repasaremos diferentes tipos de virtualización, la terminología relacionada y algunas herramientas de gestión.

## Máquinas virtuales

Las *máquinas virtuales* (MVs) son sistemas informáticos que actúan como máquinas físicas ante sus usuarios. El proceso de creación de dichas máquinas virtuales, es lo que se conoce como *virtualización*.

### Gestión de MVs

La principal herramienta para la creación y gestión de las MVs es el *hypervisor*, que también es conocido como *monitor de máquina virtual* o *gestor de máquina virtual* (en inglés VMM). Se dividen en dos clases: Tipo 1 y Tipo 2.

Comenzaremos con el Tipo 2, al ser el más fácil de comprender. Este hypervisor es una aplicación que se sitúa entre la máquina virtual (*guest*) y el sistema físico (*host*) en el cual se ejecuta el hypervisor.

![image](/img/T43_01.png)

El hypervisor de Tipo 2 es una aplicación software que interactúa con el sistema operativo del equipo anfitrión. Las máquinas virtuales poseen sus propios sistemas operativos capaces de ejecutar sus propias aplicaciones. Por supuesto, el sistema operativo del anfitrión (host) puede ser diferente al de las máquinas virtuales. Entre los hypervisores de Tipo 2 más conocidos están Oracle VirtualBox y VMware Workstation Player.

El hypervisor de Tipo 1 elimina la necesidad de contar con un SO en el equipo anfitrión, al poder ejecutarse directamete sobre el hardware del sistema. También es conocido como hypervisor *bare-metal*.

![image](/img/T43_02.png)

En este caso, también tenemos diferentes opciones a nuestra disposición. Entre ellas, destacamos KVM, Xen y Hyper-V. Una característica importante de KVM e Hyper-V es que se pueden iniciar durante la ejecución del sistema operativo anfitrión. En este caso, los hypervisores toman el relevo del SO del anfitrión y así nos encotramos ante una situación en la que el hypervisor actúa como un VMM de Tipo 1.

### Creando una máquina virtual

Hay diferentes métodos para la creación de una máquina virtual, pero la más común es que se haga desde cero; indicamos las características de la MV en el propio software del hypervisor y accedemos a un fichero ISO para proceder a la instalación del sistema operativo invitado.

Entre las diferentes opciones que tendremos en nuestro entorno particular, veremos a continuación algunos métodos para el despliegue de nuestras máquinas virtuales:

* **Clonado** Un*clon* es una copia exacta de otra máquina virtual. Internamente, este proceso consistirá en la copia de los ficheros que forman parte de la máquina original e una localización diferente de nuestro sistema de ficheros.
De todas formas, debemos tener en cuenta ciertos aspectos que pueden afectar al correcto funcionamiento de la nueva máquina. Por ejemplo, las direcciones MAC y/o IP de las tarjetas de red deben crearse de nuevo, para evitar conflictos de red entre ambas máquinas.

* **Open Virtualization Format** Este estándar permite la exportación de los ficheros de la MV al formato OVF para que pueda ser usado por otros hypervisores diferentes. Mientras que este estándar crea diferentes ficheros, algunos hypervisores tan solo reconocen el fichero comprimido conocido como Open Virtualization Archive (OVA).

* **Plantilla** Podemos ver a esta plantilla como una copia maestra, que nos servirá de base para la creación de nuevas máquinas. Esta plantilla será creada desde las herramientas disponibles por nuestro hypervisor, y contendrá el sistema operativo invitado, las aplicaciones y los ficheros de configuración necesarios. Al igual que en el clonado, una vez creada la nueva máquina se deben revisar las configuraciones que puedan ocasionar conflictos con otras máquinas de nuestro entorno.

Por último, algunas compañías ofrecen herramientas capaces de crear una máquina virtual a partir de nuestro sistema operativo anfitrión. Es lo que se conoce como *physicl-to-virtual* (P2V).

## Extensiones Linux

Como tarea previa a la creación de máquinas virtuales en nuestro equipo anfitrión, debemos comprobar que tiene soporte para la virtualización a través de *extensiones* y módulos.

Una extensión hardware capacita al hypervisor para acceder directamente a la CPU sin pasar antes por el SO anfitrión. Esta opción suele estar disponible para su activación en la configuración de la BIOS de nuestro sistema.

Para comprobar que la CPU dispone de dichas extensiones, debemos revisar la información guardada en el fichero `/proc/cpuinfo`.

	$ grep ^flags /proc/cpuinfo
	flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault invpcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid ept_ad fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp md_clear flush_l1d

Para procesores Intel, debemos buscar `vmx`, y para AMD; `svm`.

> Si vemos la etiqueta `hypervisor`, significa que nuestro sistema operativo no se está ejecutando en una máquina física. Podemos comprobar el hypervisor que estamos utilizando a través de la utilidad `virt-what` (si está disponible).

Para hacer uso de dichas extensiones, es necesario que los módulos apropiados sean cargados en Linux. Un ejemplo de comprobación del soporte para el hypervisor KVM es el siguiente:

	$ lsmod | grep -i kvm

## Contenedores

A diferencia de las máquinas virtuales, que nos proporcionan un sistema operativo completo, los contenedores están diseñados para ofrecer soporte a una aplicación. Un contenedor es gestionado por un *motor* (engine).

![image](/img/T43_03.png)

El sistema operativo de la máquina anfitrión es compartido entre los diferentes contenedores, pero cada uno de ellos dispondrá de sus propias librerías y binarios para dar soporte a la aplicación concreta.

Dependiendo de su propósito final, hay dos grandes tipos:

* **Contenedores de aplicación** Se usan para almacenar una única aplicación, o un servicio como un servidor web. Son de gran utilidad en entornos de desarrollo, ya que permiten el trabajo en una máquina diferente a la que albergará la versión final en producción, una vez revisados los cambios realizados.

* **Contenedores de sistemas operativos** Los administradores de sistemas también hacen uso del concepto de contenedores, y para ello crean sus propios contenedores con un sistema operativo funcional y separado del SO anfitrión. *Docker* es un motor de contenedores muy popular para este propósito.

## Infraestructura como servicio

Para evitar los costes generados por un centro de datos propio, ciertas compañías hacen uso del proveedores de *Infrastructure as a Service* (IaaS). Digamos que este proveedor nos ofrece no solo el hypervisor, sino toda la capa física que hay por debajo, y todo ello con acceso desde Internet.

Entre los proveedores más importantes se encuentran Amazon Web Services (AWS), Google Cloud Platform, Digital Ocean o Microsoft's Azure. A la hora de seleccionar un proveedor (CSP) debemos conocer los siguientes conceptos:

* **Computing instance** También conocida como *cloud instance*, es una máquina virtual (o contenedor) en ejecución en una plataforma *cloud*. Cuando se arranca esta instancia, es lo que se conoce como *provisioning*.
* **Cloud Type** Puede ser públicas, privadas o mixtas. Las públicas son accesibles desde Internet o a través de API's, a través de cortafuegos virtuales, gateways y enrutadores. Las privadas solo son accesibles a usuarios autorizados. Y las híbridas están formadas por recursos en la nube y por otros en las instalaciones de la compañía cliente.
* **Elasticidad** Permite a una instancia aumentar o disminuir los recursos bajo demanda. El escalado puede ser horizontal, mediante la creación de nuevas instancias para soportar la creciente demanada, o vertical, que consiste en dotar de mayores recursos a dicha instancia.
* **Balanceo de carga** Al disponer de varias instancias podemos distribuír las peticiones entre ellas.
* **Portal o consola de gestión** Los proveedores nos ofrecen un *portal* accesible vía web para poder gestionar nuestras instancias.
* **Block and Object Storage** Los *block storage* son conocidos por los administradores de sistemas, y se suelen configurar en RAID. Los *object storage* son diferentes en cuanto se permite el acceso desde el portal de administración.
* **Remote Intansce Access** Para algunas tareas de administración resulta más útil el acceso a las instancias a través de una conexión SSH en lugar de hacerlo a través del portal de gestión. La autenticación se debe realizar a través de *llaves ssh*, en lugar de credenciales usuario/contraseña.

> Cloud-init es un producto de Canonical, que describe con mayor detalle en su sitio web [cloud-init.io](http://cloud-init.io). Este servicio está disponible para diferentes CSPs, como AWS, Azure o Digital Ocean. Podemos comprobar su funcionamiento a través de la instalación del paquete `cloud-init`.
