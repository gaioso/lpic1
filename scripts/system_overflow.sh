#!/bin/bash
#
# Visualiza un mensaje de alerta si alguno de los sistemas de ficheros está al límite
# de su capacidad.

if df | egrep '(9[0-9]%|100%)' > /dev/null 2>&1	
			
			# La salida del comando df se envia a egrep para que se
			# aquellas lineas contengan la cadena 9[0-9]% o
			# 100%. Redireccionando la salida estándar hacia NULL, así
			# como la salida de error estándar.
then
	echo "Un sistema de ficheros esta al limite de su capacidad"
fi
