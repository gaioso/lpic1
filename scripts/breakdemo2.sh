#!/bin/bash
# Finaliza la ejecución de los bucles (de dentro hacia fuera), indicados en el primer
# parámetro. Es decir, si le pasamos como parámetro "1", cuando llegue al tercer bucle
# for (k), interrumpe su ejecución. Si le pasamos "2", interrumpe la ejecución del
# tercer bucle (k) y del segundo (j). Por lo tanto, si le pasamos "3", no se ejecutará
# ningún bucle.

for i in a b c
do
	for j in p q r
	do
		for k in x y z
		do
			break $1
		done
	echo $j
	done
echo $i
done
