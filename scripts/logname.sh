#!/bin/bash
# Se le pasa como parámetro un nombre de usuario.


if test "$1" = "$LOGNAME"		# Realizamos la comprobación de que el primer
					# argumento ($1) sea igual al nombre de login
					# del usuario que ejecuta el script.
then
	echo "Ese es su nombre !"	# Si los nombres coinciden.
else
	echo "Ese NO es su nombre !"	# Si los nombres NO coinciden.
fi

echo "Fin del programa"
