#!/bin/bash
# Visualiza todos los múltiplos de tres hasta un número, que debe ser pasado como
# único argumento.
#
# Uso:	multiplotres <numero máximo>
#

i=3
while test $i -lt $1		# Mientras la variable $i sea menor que el número
				# máximo, ejecutar el bucle.
do
	echo $i		
	i=$((i+3))		# Aumentamos el valor de la variable $i en 3 unidades.
done
