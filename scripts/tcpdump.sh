#!/bin/sh

SERVICE=/usr/sbin/tcpdump
SERVICEOPTS="-w"
DUMPFILE="/tmp/tcpdump.`date +%Y-%m-%d_%H:%M`"
INTERFACE=eth1
PIDFILE=/var/run/tcpdump.pid

case $1 in

start)
	echo "Iniciando $SERVICE"
	nohup "$SERVICE" "$SERVICEOPTS" "$DUMPFILE" > /dev/null 2>&1 &
	echo "$!" > "$PIDFILE"
	;;
stop)
	echo "Parando $SERVICE"
	kill -15 `cat "$PIDFILE"`
	rm -f "$PIDFILE"
	;;
status)
	if [ -f $PIDFILE ]
	then
		if ps `cat $PIDFILE` > /dev/null 2>&1
			then echo "Servicio $SERVICE ejecutandose"
		fi
	else
		echo "Servicio $SERVICE NO ejecutandose"
	fi
	;;
clean)
	echo "Que ficheros dump desea eliminar?" 
	rm -i /tmp/tcpdump.`date +%Y`**
	;;
*)
	echo "Error: Por favor use una opcion (start|stop|status|clean)"
	;;
esac
