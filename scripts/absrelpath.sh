#!/bin/sh
# Este script devuelve una cadena de texto indicando si la ruta pasada como
# parámetro es Absoluta o Relativa.

# Uso:  absrelpath <ruta>
#

if test "${1:0:1}" = "/"	# ${1:0:1} - Extrae de la variable $1, la subcadena
				# que comienza en el caracter 0 y tiene longitud 1,
				# por lo tanto el primer caracter del primer
				# argumento, y lo compara con el caracter "/".

then
	echo "Ruta absoluta"	# Si coninciden muestra por pantalla "Ruta
				# absoluta" ya que su primer caracter es igual a
				# "/"
else
	echo "Ruta relativa"	# Si no coinciden muestra por pantalla "Ruta
				# relativa", ya que el primer caracter del
				# primer argumento es distinto de "/"
fi


# *********** #
# Alternativa #
# *********** #


[ "${1:0:1}" = "/" ] && echo "Ruta absoluta" || echo "Ruta relativa"

				# Realiza el 'test' igual que en la
				# alternativa anterior, pero utilizando la
				# notación []. Si el resultado de la ejecución
				# de ese comando es 0 entonces ejecutamos el
				# 'echo "Ruta absoluta"', y si el resultado de
				# la ejecución de ese comando es 1 entonces
				# ejecutamos el 'echo "Ruta relativa"'.
