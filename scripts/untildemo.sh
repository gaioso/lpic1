#!/bin/bash
#Nombre: untildemo. Se ejecuta el bucle hasta alcanzar la variable i el valor 6.

i=1
until test $i -gt 5		# Se ejecuta el bucle hasta que se cumpla la
				# condición, es decir, hasta que la variable $i sea
				# mayor que cinco (6).
do
	echo $i
	i=$((i+1))		# Se incrementa el valor de la variable $i en 1 unidad.
done
