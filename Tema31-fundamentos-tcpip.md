# Tema 31 - Fundamentos de TCP/IP

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Introducción](#introducción)
- [Protocolos](#protocolos)
- [TCP/IP](#tcpip)
- [TCP y UDP](#tcp-y-udp)
- [Capa de aplicación](#capa-de-aplicación)
- [Direccionamiento, enrutado y subredes](#direccionamiento-enrutado-y-subredes)
- [Protocolo IPv6](#protocolo-ipv6)
- [Nombres de host - hostname](#nombres-de-host-hostname)
- [DHCP - Dynamic Host Configuration Protocol](#dhcp-dynamic-host-configuration-protocol)
- [Resumen](#resumen)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Conocer la estructura básica de la familia de protocolos TCP/IP.
* Conocer los fundamentos del direccionamiento IP.
* Comprender los conceptos de subredes y enrutado.
* Diferencias más importantes entre TCP, UDP y ICMP.
* Conocer los servicios más importantes TCP y UDP.

## Introducción

Cuando queremos comunicar dos ordenadores debemos transformar las señales digitales internas a señales analógicas, para su envío a través de diferentes medios. Existen diferentes tecnologías para diferentes necesidades, desde conexiones inalámbricas de corto alcance, pasando por tecnologías Ethernet para redes locales, hasta conexiones de fibra en redes ATM.

En la actualidad no se concibe un sistema Linux sin la capacidad de conexión a una red, bien sea para compartir recursos como ficheros o impresoras, o bien conectarse a Internet para descargar actualizaciones.

## Protocolos

Un protocolo define el esquema y las reglas para que dos nodos de una red puedan comunicarse con un tercero. Podemos hablar de las normas para compartir un medio eléctrico, o una conexión a una base de datos. Podemos dividirlos en tres clases:

* **Protocolos de transmisión**: (métodos de acceso) controla la transmisión de los datos al nivel de las tarjetas de red y las conexiones físicas. Depende mucho del medio utilizado, por ejemplo, no es lo mismo una comunicación a través de un cable modem nulo, que a través de un enlace radio de una wifi.
* **Protocolos de comunicación**: organiza la comunicación entre ordenadores situados en diferentes redes sin importar el medio utilizado a un nivel inferior. Podemos hablar de los protocolos IP, TCP, UDP, etc.
* **Protocolos de aplicación**: usados en servicios actuales, como pueden ser el correo electrónico, transferencia de ficheros, etc. Si el protocolo de comunicaciones sirve para enviar bytes a través de redes mundiales, los protocolos de aplicación sirven para darles sentido a eses bytes. Aquí se incluyen SMTP, FTP, SSH, etc.

El hecho de que el protocolo de comunicaciones oculte el protocolo de transmisión, y que el protocolo de aplicación oculte los detalles del protocolo de comunicaciones, nos permite construir un modelo en capas.

Cada capa del emisor recibe los datos de la capa superior y los envía hacia la capa inferior, y en el receptor cada capa recibe los datos de la capa inferior y los envía a la superior. Solemos decir que dos nodos se comunican a través de HTTP, pero por debajo se hace uso de TCP, IP y otra serie de protocolos necesarios para enviar los datos a través de diferentes redes.

<p align="center"><img width="400" src="img/T21_01.png"></p>

Técnicamente, cada capa que recibe los datos de su capa anterior, trata eses datos como una unidad y se le añade una cabecera correspondiente con ese nivel, que le será enviada a la capa siguiente como un conjunto de datos, sin saber que existen datos de cabecera, y en el extremo receptor, cada una de las capas va quitando las respectivas cabeceras y enviando los datos a su capa siguiente. El estándar OSI define 7 niveles o capas, pero en la práctica es difícil hacer uso de tantas capas y se reduce el número total. Las **capas 1 y 2**, describen el modo de envío de los datos a través del cable. La **capa 3** define las funciones necesarias para el enrutado, y direccionamiento. La **capa 4** distingue entre servicios orientados a conexión y no orientados a conexión. Las **capas 5, 6 y 7** describen la representación de los datos que se usan en la red y son enviados a los protocolos de aplicación.

## TCP/IP

No es un solo protocolo, sino un conjunto de protocolos, es decir una familia de protocolos. Reduce el número de niveles OSI a cuatro, siguiendo el mismo esquema:

1. Capa de acceso al medio: Ethernet, PPP (no son protocolos estrictamente TCP/IP)
2. Capa internet:  IP, ICMP, ARP.
3. Capa de transporte: TCP, UDP...
4. Capa de aplicación: HTTP, DNS, FTP, SSH, ...

Vamos a ver en detalle alguno de estos protocolos:

**IP** conecta dos nodos. Como un protocolo OSI de capa 3 es responsable de encontrar el camino que une dos nodos en Internet. Teniendo en cuenta que se pueden tener que cubrir largas distancias y usar diferentes tipos de red. Uno de los objetivos de este protocolo es proporcionar un medio de direccionamiento para cado uno de los dispositivos conectados a la red, y que sea de forma única. Es un protocolo no orientado a conexión, a diferencia por ejemplo de las redes de telefonía que establecen canales de comunicación «fijos» para una comunicación establecida entre dos nodos, aquí los datos se dividen en trozos, denominados «datagramas», que pueden ser direccionados y enviados de forma diferente a través de diferentes caminos. IP no garantiza la llegada de los datagramas ni el orden en el que lleguen. Debe ser una responsabilidad de las capas superiores.

<p align="center"><img width="400" src="img/T21_02.png"></p>

Es una ventaja porque durante una transmisión se pueden buscar alternativas de rutas ante fallos en algunos caminos usados anteriormente. IP tiene en cuenta la fragmentación, ya que un datagrama puede tener una longitud máxima de 65535 bytes, pero los protocolos de transmisión no suelen admitir eses tamaños (Ethernet tiene un tamaño máximo de trama de 1500 bytes), por lo que este protocolo considera que el datagrama se ha recibido si se han recibido todas sus tramas troceadas por los protocolos inferiores.

**TTL**. (*time to live*) Define el tiempo máximo de vida de un datagrama. Es ajustado por el emisor y se disminuye en cada nodo por el que pasa dicho datagrama. Si el TTL llega a cero, se deshecha dicho datagrama, y se le notifica al emisor. Esto evita que paquetes que no pueden ser entregados circulen por la red de forma infinita. Un valor usual es 64, que es suficiente teniendo en cuenta la extensión de Internet.

**TOS**. (*type of service*) Especifica la calidad de servicio para el datagrama, ya que algunas redes usan esto como método para asignar prioridades a los datagramas. Por ejemplo, en momentos de sobrecarga sólo admiten datagramas con máxima prioridad.

**ICMP** Se utiliza para la gestión de la red y para reportar problemas de red. El programa `ping`, usa dos mensajes ICMP (`echo request` y `echo reply`). El paquete ICMP es encapsulado como datos dentro del datagrama IP.

**IP y protocolos de transmisión**. El protocolo IP debe conocer el medio a través del cual debe enviar los datagramas. Por ejemplo, en Ethernet cada interfaz de red tiene una dirección única, la dirección MAC de 48 bits, y por tanto la transmisión de tramas a través de esta tipología de red nos obliga a conocer la MAC de destino, por lo que debemos preguntar a toda la red, por un nodo que posee la IP `a.b.c.d`, y ese nodo nos debe devolver su dirección MAC. Es lo que se denomina Protocolo de Resolución de Dirección (ARP). Una vez que recibamos la dirección MAC de otro nodo, la almacenamos en una caché para usos posteriores. Podemos acceder a esta caché a través del comando `arp`, no solo para lecturas, sino también para realizar escrituras de nuevas entradas. Los datagramas hacia direcciones IP que no pertenecen a nodos del mismo segmento de red deben ser enrutados.

## TCP y UDP

**TCP**. Es un protocolo fiable y orientado a conexión. A diferencia de IP, TCP soporta operaciones para abrir y cerrar conexiones «virtuales» entre los nodos fuente y destino. TCP consigue la fiabilidad a través de mensajes de reconocimiento (ACK) por parte del destino de cada segmento recibido. Cada segmento es numerado e indicado por cada uno de los nodos. Si no se recibe el ACK de un segmento después de un tiempo, se realiza otro intento de envío. Para evitar perdida de rendimiento, hay un número máximo de segmentos pendientes de aceptación, por lo que TCP se considera más lento que IP. Cada segmento TCP contiene una cabecera de por lo menos 20 bytes. Se soportan varias conexiones TCP simultáneas haciendo uso del concepto de números de puerto. La combinación de dirección IP y número de puerto se denomina «socket».

<p align="center"><img width="400" src="img/T21_03.png"></p>

**UDP**. A diferencia de TCP, no es fiable y no está orientado a conexión. No es mucho más que IP pero con puertos, ambos protocolos usan 65535 puertos, incluso de forma simultánea. UDP no utiliza segmentos de aceptación, por lo que es más rápido, y el precio que se paga es la posibilidad de pérdida de datos. UDP se usa cuando hay que transmitir pocos datos, por lo que el coste de la inicialización de una conexión TCP puede ser muy alta en comparación (DNS), o cuando los retrasos o pérdidas no se pueden recuperar. En una conexión de voz o de video, los paquetes perdidos se transforman en ruido, pero con una conexión TCP que puede incluir más tiempo en la recuperación de eses segmentos perdidos, los efectos pueden ser peores.

**Puertos** Tanto TCP como UDP soportan el uso de puertos, que permite al sistema mantener más de una conexión abierta al mismo tiempo. Hay 65536 puertos disponibles, el puerto 0 se usa para indicar a la pila TCP/IP que use otro puerto libre. Hay varios puertos que están asignados a servicios particulares. Distinguimos entre los «bien conocidos» (0 – 1023) y los «registrados» (1024 – 49.151). El resto de puertos son denominados puertos dinámicos o privados, y los suele utilizar el cliente para comunicarse con el servidor. En Linux, la tabla de asignaciones está disponible en `/etc/services`. Los puertos 0-1023 sólo los puede abrir el usuario *root*.

## Capa de aplicación

En esta capa es donde transcurre la acción. Es aquí donde las aplicaciones son capaces de procesar los datos que han sido enviados a través de la red y conseguir un resultado final con los mismos. La mayoría de estas aplicaciones operan bajo un modelo *cliente/servidor*, donde uno de los equipos ofrece servicios a múltiples clientes (como puede hacer un servidor web). Este sevidor se mantiene a la escucha de peticiones en un puerto de una capa específica de transporte, y por su parte, el cliente debe ser capaz del envío de las peticiones.

Para simplificar esta tarea de comunicación, tanto TCP como UDP usan el concepto de puertos *well-known* para su uso con las aplicaciones más frecuentes. Estos números de puerto se encuentran reservados, por lo que los clientes los usarán a la hora de buscar ciertas aplicaciones en la red.

Puerto | Protocolo | Aplicación
--- | --- | ---
20 | TCP | File Transfer Protocol (FTP) - datos
21 | TCP | File Transfer Protocol (FTP) - mensajes de control
22 | TCP | Secure Shell (SSH)
23 | TCP | Telnet interactive protocol
25 | TCP | Simple Mail Transfer Protocol (SMTP)
53 | TCP/UDP | Domain Name System (DNS)
80 | TCP | Hypertext Transfer Protocol (HTTP)
110 | TCP | Post Office Protocol version 3 (POP3)
123 | UDP | Network Time Protocol (NTP)
139 | TCP | NetBIOS Session Service
143 | TCP | Internet Message Access Protocol (IMAP)
161 | UDP | Simple Network Management Protocol (SNMP)
162 | UDP | Simple Network Management Protocol trap
389 | TCP | Lightweight Directory Access Protocol (LDAP)
443 | TCP | Hypertext Transfer Protocol (HTTPS) over TLS/SSL
465 | TCP | Authenticated SMTP (SMTPS)
514 | TCP/UDP | Remote Shell (TCP) o Syslog (UDP)
636 | TCP | Lightweight Directory Access Protocol over TLS/SSL (LDAPS)
993 | TCP | Lightweight Message Access Protocol over TLS/SSL (LDAPS)
995 | TCP | Post Office Protocol 3 over TLS/SSL (POP3S)

En los sistemas Linux se mantiene una lista de los nombres de puertos en el fichero `/etc/services`, de tal forma que las aplicaciones pueden hacer uso del nombre en lugar del número de puerto. Esta capacidad resultará de gran utilidad a la hora de resolver problemas en nuestra red.

## Direccionamiento, enrutado y subredes

Cada interfaz de red en una red TCP/IP debe poseer por lo menos una dirección IP. Un sistema puede contener varias interfaces de red, y por lo tanto varias direcciones IP.

	$ /sbin/ip addr show

Las direcciones IP tienen una longitud de 32 bits, y se escriben separadas por punto. Cada dirección IP es asignada para que sea única globalmente y corresponde a un nodo en una red particular. Por lo tanto, las direccines IP pueden ser divididas en dos partes: una red y un nodo.  Esta división es variable y se puede adaptar a las necesidades de cada red en cuestión. La división está indicada por la máscara de red, que contiene un 1 binario para cad bit de la dirección IP que pertenece a la red, y un 0 binario para cada bit que pertenece al host.

Por ejemplo, supongamos una red con 28 nodos. La siguiente potencia de 2 es 32 (2 elevado a 5). Es decir, necesitamos 5 bits para numerar los nodos. Nos quedan 27 bits para identificar la red. La máscara de red es 255.255.255.224 (es decir, como tenemos tres 1's binarios, esto equivale a sumar 128+64+32 = 224). La primera y la última dirección IP de la red están reservadas. La primera es la dirección de la red (la parte de host todo a ceros), y la última dirección de la red (la parte de host todo a unos) es la dirección de difusión. En nuestro ejemplo, 203.177.8.0 y 203.177.8.31. Los números 1 a 30 son para los nodos. La dirección 255.255.255.255 no es una dirección de difusión para todo Internet, sino para el segmento de red local, y se usa cuando no se puede hacer de otra forma, por ejemplo cuando necesitamos conseguir una dirección IP a través de un servidor DHCP.

**Enrutado**. Se usa para enviar datagramas IP que no pueden ser enviados directamente dentro de la red local al destino correcto. Entonces debemos consultar una tabla que contiene por lo menos una «pasarela por defecto», denominado *router*. Un enrutado envía tráfico entre redes diferentes.

**Clases de redes IP**. El conjunto de direcciones IP han sido divididas en clases: A, B y C. Se diferencian en sus máscaras de red. Una dirección clase A, tiene una parte de red de 8 bits, una dirección de clase B, 16 bits, y una dirección de clase C, 24 bits. Debido a la escasez de direcciones IP, este modelo fue abandonado en los años 90.

**Subredes**. Cuando una red es muy grande, los administradores de red suelen dividir esas redes en subredes más pequeñas. Esto se consigue añadiendo otra parte fija a la parte de red de una dirección IP. En el ejemplo anterior, en lugar de usar una red con 32 direcciones, podríamos tener dos redes más pequeñas con 16 direcciones (con 14 nodos). Podríamos prolongar la máscara de red con 1 bit.

**Direcciones IP privadas**. Se asignan a aquellos nodos que no están conectados directamente a Internet. Las podemos usar libremente en una red local, que será conectada a Internet usando un router con NAT. Este router tiene una interfaz de red con IP pública, y reescribe los datagramas con su dirección IP pública para enviarlos a través de Internet, y luego le devuelve la respuesta a su respectiva direccion privada de la red local.

Los rangos reservados para uso en redes privadas son los siguientes:

* 10.0.0.0    - 10.255.255.255
* 172.16.0.0  - 172.31.255.255
* 192.168.0.0 - 192.168.255.255

En resumen, para que un equipo pueda comunicarse dentro de una red IP, debe poseer tres valores:

* Su propia dirección en la red
* La máscara de dicha red para usar en la capa física
* La dirección de un enrutador local para enviar paquetes a redes remotas

Por ejemplo:

* Host: 192.168.20.5
* Máscara: 255.255.255.0
* Router: 192.168.20.1

## Protocolo IPv6

El protocolo IP en su versión 6 (IPv6) usa un esquema de direcciones de 128 bits en lugar de 32, lo que permite una capacidad mucho mayor de identificación única de dispositivos conectados a Internet.

Este protocolo usa números en base 16 (hexadecimal) para la representación de las direcciones, que se divide en ocho grupos de cuatro dígitos hexadecimales separados por «dos puntos» (:), como por ejemplo:

	fed1:0000:0000:08d3:1319:8a2e:0370:7334

Si alguno de los grupos está formado por cuatro ceros (0000), se puede omitir de la siguiente forma:

	fed1::08d3:1319:8a2e:0370:7334

El protocolo IPv6 también soporta direcciones de tipo local. Por ejemplo, cada dispositivo dispondrá de una «dirección local» que hace uso de la dirección de red por defecto `fe80::` seguido de la parte de *host* que será la MAC de la interfaz de red asociada. De esta forma es posible que cualquier dispositivo con capacidad IPv6 se pueda comunicar con otros dispositivos IPv6 sin disponer de una configuración adicional.

El direccionamiento global en IPv6 se comporta de forma similar a la versión original de IP; cada red tendrá una única dirección de red, y cada *host* debe poseer una dirección única dentro de la misma.

## Nombres de host - hostname

Para no tener que memorizar las direcciones IP de los dispositivos presentes en una red, se ha creado *Domain Name System* (DNS) que es capaz de asignar nombres a dichos dispositivos.

En DNS, a cada dirección de red se le asigna un *nombre de dominio* (p.ej. `lpic.lan`) que identifica a la propia red, y luego a cada *host* se le añade un *nombre de host* único.

Los servidores DNS realizan un *mapeo* entre los nombres de dominio (y host) con sus respectivas direcciones IP, haciendo posible de este modo la comunicación con los mismos. Estos servidores operan en modo jerárquico, de tal forma que el servidor de nuestra red local será capaz de intercambiar información con los de un nivel superior.

## DHCP - Dynamic Host Configuration Protocol

El mantenimiento manual de la direcciones IP dentro de una red de gran tamaño, acabará siendo un trabajo arduo y no exento de conflictos y colisiones. Para evitar este trabajo se ha desarrollado el protocolo DHCP, donde los clientes solicitan al servidor DHCP una dirección IP temporal (así como el resto de datos necesarios para poder trabajar correctamente en la red).

Por supuesto, lo que supone una gran ventaja para las estaciones de trabajo, nunca se utilizará en servidores de nuestra red, a los que debemos asignar direcciones estáticas.

## Resumen

* Hoy en día, TCP/IP es la familia de protocolos más popular para la transmisión de datos a través de una red.
* ICMP es usado para gestión de red, y reportes de problemas.
* TCP ofrece una conexión orientada a conexión.
* Al igual que IP, UDP no está orientado a conexión, pero más simple y rápido que TCP.
* TCP y UDP usan números de puertos para distinguir entre diferentes conexiones en el mismo ordenador.
* Las direcciones IP identifican los nodos a nivel mundial. Tienen una longitud de 32 bits, y están formadas por una parte de red y una de host.
