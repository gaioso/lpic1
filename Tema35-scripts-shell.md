# Tema 35 - Scripts de Shell

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Introducción](#introducción)
- [Ejecutar scripts de shell](#ejecutar-scripts-de-shell)
- [Estructura de un script](#estructura-de-un-script)
- [Diseñando scripts](#diseñando-scripts)
- [Tipos de errores](#tipos-de-errores)
- [Diagnóstico de errores](#diagnóstico-de-errores)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos

* Conocer el propósito y la sintaxis básica de los scripts
* Ser capaz de ejecutar scripts
* Comprender las líneas #!

## Introducción

La ventaja indiscutible de los scripts de shell es: Si sabes usar la shell, ¡sabes programar!. Los scripts de shell son útiles para automatizar tareas que antes hacíamos «a mano». Como desventaja; no existen estructuras de datos complejas. Tenemos la opción de utilizar algún lenguaje de scripting como Tcl, Perl, o Python.

## Ejecutar scripts de shell

Un script puede iniciarse de varias formas. Pasando como parámetro el nombre del script a una shell.

	$ cat script.sh
	echo Hola Mundo
	$ bash script.sh
	Hola Mundo

Esta opción no es recomendable cuando los usuarios no tienen porque saber qué es un script, y mucho menos que debe ser ejecutada con *bash*. Sería más interesante poder iniciarlo igual que se hace con el resto de comandos. Para ello, debemos hacer ejecutable nuestro script, con el comando `chmod`:

	$ chmod u+x script.sh

ahora, podemos ejecutar el script directamente:

	$ ./script.sh

Si queremos evitar el uso de «./», debemos asegurarnos de que la shell es capaz de localizar el script. La mejor opción es crear un directorio, por ejemplo `$HOME/bin`, donde guardar nuestros scripts, y añadir este directorio a la variable *PATH*.	El tercer método de ejecución de un script, es ejecutarlo en la shell actual en lugar de iniciar un proceso hijo. Lo podemos hacer usando el comando `source`, o su abreviatura «.»:

	$ source script.sh
	$ . script.sh

Un script iniciado de este modo, es capaz de acceder a todo el entorno de la shell actual.

Cuando guardamos nuestros scripts con un determinado nombre, es recomendable seguir la costumbre de añadir el sufijo `.sh` o `.bash`, y ayudarnos así a diferenciar de forma clara el contenido del fichero.

> Podríamos restringir el uso de la extensión `.sh` a los scripts que utilicen una shell Bourne, y la extensión `.bash` para los scripts que necesiten a bash.

## Estructura de un script

Los scripts son tan sólo secuencias de comandos de la shell que se han almacenado en un fichero. Para separar los comandos, podemos usar el salto de línea, a diferencia del uso que le damos en el modo «real». Es recomendable separar los comandos en diferentes líneas. Para mejorar la legibilidad del script, podemos insertar líneas en blanco de forma intencionada, así como insertar comentarios precedidos por el carácter «#». Los scripts de una gran longitud deben contener un bloque inicial con los datos que resumen su función, propietario, cómo ejecutarlo, etc. Los ficheros de texto marcados como ejecutables, son considerados scripts para la shell `/bin/sh`, que suele ser un sinónimo de bash, pero para asegurarnos de que se ejecutará con bash, debemos introducir la siguiente línea al principio del script:

	#!/bin/bash

## Diseñando scripts

Cualquier persona con experiencia en programación, sabe que rara vez los programas ejecutan la tarea en el primer intento. La depuración de un programa se incrementa al mismo tiempo que crece su tamaño. Por lo tanto, se recomienda un cuidado diseño, realizado paso a paso, así como un conocimiento de los errores más frecuentes. En la mayoría de las ocasiones, necesitaremos escribir un pequeño plan para llevar a cabo alguna tarea concreta. En este caso, el propósito del script lo tenemos claro, y conocemos de sobra los comandos a ejecutar, y en qué orden.	Siguiendo algunas normas básicas, escribimos todos los comandos, y los conectamos de algún modo. Por ejemplo, introduciendo sentencias condicionales o bucles, si así hacemos el script más legible, más tolerante a fallos, o más universal. Además, podemos ubicar los nombres de los ficheros más usados en variables, y sólo usar éstas.
Los parámetros de la línea de comandos pueden ser usados para modificar el comportamiento del script, y por supuesto, comprobaremos eses parámetros, mostrando mensajes de error o advertencia si algo falla. También podemos usar este método para programas mayores, iniciando el desarrollo con el método más obvio y desarrollar el script basándose en él.	La gran desventaja de todo esto, es que es fácil cometer errores conceptuales, y los posibles cambios podrían hacer que el trabajo realizado no sirviera de nada. Por lo tanto, en cualquier caso, se recomienda comenzar con el diseño de las posibles etapas de nuestro programa. Es suficiente crear una lista con los pasos, escrita en «lenguaje coloquial», sin preocuparse en este momento de los comandos que utilizaremos.
Otra ventaja de este modelo, es que podemos decidir qué comandos son los más adecuados para cada etapa.	Un buen diseño, nos ayudará en el futuro a realizar cambios, cuando ya no recordemos el porqué del uso de ciertos comandos. Asimismo, es una buena costumbre introducir este plan dentro del propio script, en lugar de comentar línea a línea.

## Tipos de errores

Fundamentalmente, podemos distinguir entre dos tipos de errores:
1. **Errores conceptuales**. Afectan a la estructura lógica del programa. Suelen ser difíciles de reconocer y de solucionar, y deben ser evitados mediante el diseño de un buen plan.
2. **Errores de sintaxis**. Estos errores ocurren muy a menudo, y tienen relación con cualquier error tipográfico en el programa: un carácter olvidado, y nada funcionará!. Algunos los podremos evitar si vamos desde lo simple hacia lo complejo. Por ejemplo, en una expresión con paréntesis, primero introducimos ambos paréntesis, y luego su contenido. La misma filosofía se puede aplicar a las estructuras de control: nunca escribir un «if» sin su correspondiente «fi».

## Diagnóstico de errores

Al trabajar con un intérprete, la mayoría de errores aparecerán cuando ejecutemos el programa. Por lo que debemos ser cuidadosos a la hora de comprobar nuestros scripts durante la etapa de desarrollo. De todas formas, es recomendable utilizar un «entorno seguro» para realizar pruebas, sobre todo si trabajamos con ficheros existentes. Muchos scripts hacen uso de comandos externos. En estos casos, los mensajes de error internos a los comandos, nos pueden ayudar a detectar errores, sobre todo aquellos que afectan a la sintaxis. Para un mejor análisis de estos errores, podemos hacer que bash sea más «habladora». Con la opción `set -x`, podemos ver los pasos que realiza la shell para hacer su trabajo. La desventaja de `set -x` es que el comando es ejecutado. En el caso de las sustituciones, es mejor mostrar los comandos en lugar de ejecutarlos. La opción «-n» hace justamente esto. Por supuesto, podemos equivocarnos a la hora de escribir un comando, o en el nombre de una variable. Es aquí donde vemos una clara desventaja respecto a un lenguaje de programación.
Otra opción útil es «-v», que ejecuta el comando, y además la shell también muestra el comando que ha ejecutado. Esto, en el caso de un script, aparte de obtener el resultado, podemos ver su contenido. En la programación de scripts, colocaremos la opción deseada en la primera línea del script:

	#!/bin/bash -x
	....
