# Tema 9 - Gestión de paquetes en Debian

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Herramientas `dpkg`](#herramientas-dpkg)
- [Paquetes Debian](#paquetes-debian)
- [El comando dpkg](#el-comando-dpkg)
- [Instalación de paquetes](#instalación-de-paquetes)
    - [Problemas durante la instalación](#problemas-durante-la-instalación)
- [Eliminado de paquetes](#eliminado-de-paquetes)
- [Paquetes Debian y código fuente](#paquetes-debian-y-código-fuente)
- [Información de paquetes](#información-de-paquetes)
- [Gestión de paquetes Debian. APT.](#gestión-de-paquetes-debian-apt)
- [Búsqueda de paquetes](#búsqueda-de-paquetes)
- [La infraestructura `debconf`](#la-infraestructura-debconf)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos

* Conocer las herramientas básicas de paquetes Debian.
* Ser capaces de usar `dpkg` para la gestión de paquetes.
* Ser capaces de usar `apt-get`, `apt-cache` y `aptitude`.
* Saber convertir paquetes RPM a paquetes Debian.

## Herramientas `dpkg`

Funciones del comando `dpkg`:

* Instalar paquetes de software
* Gestionar dependencias
* Catalogar el software instalado
* Controlar las actualizaciones de los paquetes
* Desinstalar paquetes

`aptitude` es un «front-end» de `dpkg`.
`debconf` configura los paquetes bajo una instalación.

## Paquetes Debian

Como seguramente ya sabes, el sistema de gestión de paquetes Debian es empleado en las distribuciones basadas en Debian, tales como Ubuntu o Linux Mint. Gracias a este sistema, es posible instalar, modificar, actualizar o eliminar los paquetes software de nuestro sistema.

El software en Debian se divide en paquetes, que tienen nombres indicando el software que contienen dentro.

	figlet_2.2.5-3_amd64.deb

El programa `figlet`, en su versión 2.2.5, en particular la liberación 3 de este paquete en la distribución (la versión 2.2.6 comenzará de nuevo en 1). El *amd64* indica que el paquete contiene partes específicas de la arquitectura *amd64*. Los paquetes que sólo contienen documentación o que son scripts independientes de la arquitectura usan *all* en su lugar.

> Un paquete Debian es un archivo creado con el programa `ar`, y contiene tres componentes:<br/>
`$ ar t figlet_2.2.5-3_amd64.deb`<br/>
  `debian-binary`<br/>
  `control.tar.gz`<br/>
  `data.tar.gz`<br/>
> El fichero `debian-binary` contiene el número de versión del paquete. En `control.tar.gz` están los scripts específicos para Debian, y ficheros de configuración. Y en `data.tar.gz` están los ficheros del paquete. Durante la instalación, el fichero `control.tar.gz` se desempaqueta, por lo que se ejecutan los scripts `preinst`. Luego se desempaqueta el fichero `data.tar.gz`, y luego los scripts de `postinst`.<br/>
`$ ar x figlet_2.2.5-3_amd64.deb`<br/>
  `$ tar tf control.tar.gz`<br/>
  `$ tar tf data.tar.gz`<br/>
  `$ cat debian-binary`<br/>

> Si queremos obtener el fichero del paquete Debian debemos usar, por ejemplo, el comando `apt-get download vim` para descargar el paquete perteneciente a `vim` al directorio actual.

## El comando dpkg

La herramienta central para la gestión de los ficheros `.deb` es el programa `dpkg`, que trabaja en la línea de comandos e incorpora opciones para instalar, actualizar y eliminar paquetes `.deb` en nuestro sistema Linux.

Las acciones más comunes de este comando se resumen en la siguiente tabla:

Formato corto | Formato largo | Descripción
--- | --- | ---
-c | --contents | Muestra el contenido de un fichero
-C | --audit | Busca paquetes instalados con errores y sugiere como solucionarlo
N/D | --configure | Configura de nuevo un paquete instalado
N/D | --get-selections | Muestra los paquetes instalados
-i | --install | Instala el paquete; en caso de existir, lo actualiza
-I | --info | Muestra información sobre un fichero de paquete no instalado
-l | --list | Muestra todos los paquetes instalados que coinciden con un patrón
-p | --print-avail | Muestra información sobre un paquete instalado
-P | --purge | Elimina un paquete, incluyendo los ficheros de configuración
-r | --remove | Elimina un paquete, dejando los ficheros de configuración
-s | --status | Muestra el estado de un paquete determinado
-S | --search | Indica el paquete al que pertenece el fichero

## Instalación de paquetes

Una vez obtenido el fichero del paquete en cuestión, es posible obtener información detallada del mismo, como su número de versión y dependencias.

	$ apt-get download figlet
	Get:1 http://deb.debian.org/debian buster/main amd64 figlet amd64 2.2.5-3 [136 kB]
	Fetched 136 kB in 0s (327 kB/s)
	$
	$ dpkg -I figlet_2.2.5-3_amd64.deb

Y para obtener la lista de los ficheros que contiene el fichero `.deb`, cambiamos la opción `-I` por `--contents`.

	$ dpkg --contents figlet_2.2.5-3_amd64.deb
	[...]
	-rwxr-xr-x root/root     47168 2017-06-22 21:31 ./usr/bin/figlet-figlet
	[...]

Una vez que tenemos la certeza de que estamos ante el fichero correcto, procedemos con la instalación:

	# dpkg --install figlet_2.2.5-3_amd64.deb

donde `--install` puede ser abreviado con `-i`. Con las opciones `--unpack` y `--configure`, se pueden ejecutar de forma separada.

> Las opciones para `dpkg` se pueden pasar a través de la línea de comandos, o bien situarlas en `/etc/dpkg/dpkg.cfg`. En este fichero, se omiten los guiones al principio de los nombres de las opciones.

Cuando hacemos una instalación, y existe una versión anterior, es desinstalada antes de configurar la nueva. Si hay un error durante la instalación, la versión anterior puede ser restaurada.

Si intentamos realizar la instalación de un paquete que no tenga todas las dependencias resueltas, este proceso no es capaz de finalizar con éxito.

```
$ apt-get download zsh
[..]
$ sudo dpkg -i zsh_5.7.1-1_amd64.deb
[...]
dpkg: dependency problems prevent configuration of zsh:
 zsh depends on zsh-common (= 5.7.1-1); however:
  Package zsh-common is not installed.

dpkg: error processing package zsh (--install):
 dependency problems - leaving unconfigured
Processing triggers for man-db (2.8.5-2) ...
Errors were encountered while processing:
 zsh
```

Tal y como se pudo comprobar, la instalación del paquete `zsh` no ha sido posible, y con el comando `dpkg -s zsh` comprobamos el estado en el que se encuentra dicho paquete en nuestro sistema.

	$ dpkg -s zsh
	[...]
	Status: install ok unpacked
	[...]

### Problemas durante la instalación

1. Son necesarios uno o más paquetes (`--force-depends`). El paquete necesita uno o más paquetes que no han sido instalados, o que no han sido incluidos en la misma instalación.
2. Existe una versión anterior (`hold`). Hay una versión anterior instalada y marcada como «hold» (usando `aptitude`). Esto impide la instalación de nuevas versiones.
3. El fichero desempaquetado ya existe en otro paquete diferente (`--force-overwrite`). El paquete intenta desempaquetar un fichero que ya existe y pertenece a un paquete diferente. A menos que el paquete a instalar esté marcado como reemplazo del paquete existente, o usemos la opción `--force-overwrite`.
4. Algunos paquetes entran en conflicto con otros. Por ejemplo, sólo podemos tener instalado un paquete para el transporte del correo. Si queremos instalar `Postfix`, debemos desinstalar `Exim`.

## Eliminado de paquetes

Un paquete se desinstala usando:

	# dpkg --remove figlet

Los ficheros de configuración (aquellos listados en el fichero `conffiles`, en `control.tar.gz`), se mantienen para una próxima reinstalación.

	# dpkg --purge figlet

elimina el paquete incluyendo sus ficheros de configuración.

> Examinar el fichero `/var/lib/dpkg/info/<nombre_paquete>.conffiles`

Posibles problemas en la desinstalación:

* El paquete sea requerido por uno o más paquetes que todavía están instalados.
* El paquete tiene la marca de «essential». Por ejemplo, no podemos eliminar la shell.

## Paquetes Debian y código fuente

Cuando trabajamos con código fuente, un principio básico de Debian es distinguir claramente entre el código original y cualquier cambio específico de Debian. Todos los cambios son colocados en un fichero que se crea usando el comando `diff`, y que contiene las diferencias entre la versión original y la versión Debian. Para cada versión del paquete, también existe un «fichero de control de la fuente» (con sufijo `.dsc`) que contiene los «checksums» del fichero original y el fichero de cambios, que será firmado digitalmente por el responsable de Debian a cargo del paquete:

	figlet_2.2.5-3.dsc
	figlet_2.2.5.orig.tar.gz
	figlet_2.2.5-3.debian.tar.gz

El comando ´dpkg-source´ se usa para reconstruir el código fuente de un paquete desde los archivos originales y los cambios de Debian, de tal forma que podemos recompilar nuestra propia versión del paquete Debian. Para realizar esto, debemos ejecutar este comando con el fichero de control de la fuente como argumento:

	$ dpkg-source -x figlet_2.2.5-3.dsc

el fichero original y el fichero `diff.gz` debe estar en el mismo directorio que el fichero de control de la fuente.

## Información de paquetes

Podemos obtener una lista de los paquetes instalados usando:

	$ dpkg --list

esta lista la podemos filtrar usando los patrones de búsqueda de la shell:

	$ dpkg -l libc*

Los paquetes con version *<ninguna>*, son parte de la distribución pero todavía no están instalados en el sistema (estado `un`), o han sido eliminados (estado `pn`). Podemos encontrar información acerca de un paquete concreto con la opción `--status`:

	$ dpkg --status figlet

Tras el nombre del paquete, la salida muestra información acerca del estado en el que se encuentra el paquete, su prioridad, el nombre de la persona a cargo del paquete en el proyecto Debian (*Maintainer*), y un área importante de información como son las dependencias, recomendaciones o conflictos con otros paquetes. Si el paquete no está instalado localmente, sólo se muestra un mensaje de error.

	# dpkg -s apache

La opción `--listfiles` (-L), muestra una lista de los ficheros dentro del paquete:

	$ dpkg --listfiles figlet
	$ dpkg -L coreutils

Por último, con la opción `--search` (-S) podemos encontrar el paquete al que pertenece un determinado fichero. Están permitidos los patrones de búsqueda:

	$ dpkg -S bin/m*fs
	$ dpkg -S /usr/bin/basename

## Gestión de paquetes Debian. APT.

`dpkg` es una potente herramienta, pero tiene limitaciones, por ejemplo, que no sea capaz de resolver las dependencias entre los paquetes. Es válida para realizar instalaciones de paquetes que están disponibles de forma local, pero no permite el acceso a servidores web o FTP. La herramienta `apt-get` representa una versión inteligente de `dpkg`. No ofrece una interfaz interactiva para la selección de paquetes, pero hoy en día es la más utilizada en la línea de comandos.

> Al igual que ocurre con `dnf` el sistemas RPM, en la gestión de paquetes Debian también existe una herramienta cada vez más popular, que es `apt`. Incorpora ciertas mejoras en la visualización de la interfaz y comandos más simples.

**Propiedades de apt-get**

1. Gestión de múltiples fuentes.
2. Actualización de la distribución (`dist-upgrade`)
3. Herramientas auxiliares (`apt-proxy`, `apt-zip`, `apt-listbugs`...)

En la siguiente tabla se pueden consultar los comandos de `apt-get`

Acción | Descripción
--- | ---
autoclean | Elimina la información de los paquetes que ya no están en el repositorio
check | Comprueba la base de datos del gestor de paquetes en busca de inconsistencias
clean | Limpia la base de datos y los ficheros temporales descargados
dist-upgrade | Actualiza todos los paquetes, además de modificar las dependencias con nuevas versiones
dselect-upgrade | Finaliza operaciones incompletas en los paquetes
install | Instala o actualiza un paquete y actualiza la base de datos
remove | Elimina el paquete
source | Obtiene el paquete con el código fuente del paquete indicado
update | Actualiza la información de los paquetes disponible en los repositorios
upgrade | Actualiza todos los paquetes instalados a sus nuevas versiones

Los repositorios de paquetes para `apt-get` están listados en el fichero `/etc/apt/sources.list`:

	deb http://ftp.de.debian.org/debian/ stable main
	deb-src http://ftp.de.debian.org/debian/ stable main

La ejecución estándar del comando `apt-get` comienza con la actualización de la base de datos de paquetes disponibles:

	# apt-get update

Esto consulta todas las fuentes de paquetes e integra los resultados en una lista común. Podemos instalar paquetes usando:

	# apt-get install figlet

De esta forma instalaremos también los paquetes listados en *Depends*, así como cualquier paquete del que dependan éstos a su vez.

El comando `apt-get upgrade` instala las nuevas versiones disponibles de todos los paquetes instalados en el sistema. Aquellos paquetes que no dispongan de una nueva versión, permanecen intactos. El comando `apt-get dist-upgrade` ejecuta un esquema «inteligente» de resolución de conflictos que intenta resolver las dependencias de forma prudente eliminando e instalando paquetes, dando prioridad a los más importantes. Podemos obtener el código fuente de un paquete con el comando:

	# apt-get source figlet

## Búsqueda de paquetes

Otra utilidad de `apt-get` es el programa `apt-cache`, que busca en los repositorios de `apt-get`:

	$ apt-cache search figlet
	$ apt-cache show figlet

La salida de `apt-cache show` corresponde a la de `dpkg --status`, excepto que en este caso funciona con todos los paquetes presentes en alguna de las fuentes, aunque no esté instalado localmente. Hay otros usos interesantes de `apt-cache`:

	$ apt-cache depends figlet
	$ apt-cache rdepends libxpm4
	$ apt-cache stats

## La infraestructura `debconf`

En ocasiones, es necesario responder a algunas cuestiones durante la instalación de paquetes. Por ejemplo, si estamos instalando un servidor de correo, es importante conocer datos acerca de la configuración de red del equipo en el cual se está instalando el servidor. El mecanismo *debconf* está diseñado para recoger esta información y almacenarla para un uso futuro. Por lo tanto, si necesitamos reconfigurar la información almacenada por *debconf*, debemos utilizar:

	# dpkg-reconfigure my-package

de esta forma se repiten las cuestiones realizadas durante la instalación.

	# dpkg-reconfigure locales

En el siguiente ejemplo vamos a comprobar como realizar cambios en la configuración del paquete `cups` después de ser instalado:

	$ sudo apt install cups
	[...]
	$ sudo dpkg-reconfigure cups

![image](/img/T17_01.png)
	
Una vez realizados los nuevos cambios, la utilidad `debconf-show` permite consultar la configuración del paquete. Este proceso resulta útil para poder guardar los parámetros iniciales del paquete antes de realizar los cambios, y así poder gestionar un historial de los mismos.

> Una técnica que se comienza a usar a través de Flatpak es la capacidad de definir como se debe ejecutar una aplicación en concreto. Es decir, haciendo uso de técnicas de virtualización, se combina la gestión de paquetes, desarrollo de software y empaquetado (sandboxing) de aplicaciones. Todas las dependencias necesarias se encuentran dentro del «sandbox», evitando la comunicación con el resto del sistema. Esto último representa una ventaja desde el punto de la seguridad, evitando efectos nocivos en el sistema cuando la aplicación presente problemas.

```
$ sudo debconf-show cups
* cupsys/raw-print: true
* cupsys/backend: lpd, socket, usb, snmp, dnssd
```
	
## Comandos vistos en el tema

Comando | Descripción
-- | --
apt-get | Potente herramienta de línea de comandos para la gestión de paquetes Debian GNU/Linux
dpkg | Herramienta de gestión de paquetes Debian GNU/Linux
dpkg-reconfigure | Reconfigura un paquete ya instalado.
