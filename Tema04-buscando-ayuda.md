# Tema 4- Buscando ayuda

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Autoayuda](#autoayuda)
- [El comando help y la opción help](#el-comando-help-y-la-opción-help)
- [El manual en línea](#el-manual-en-línea)
    - [Capítulos](#capítulos)
    - [Mostrando las páginas *man*](#mostrando-las-páginas-man)
- [Páginas *info*](#páginas-info)
- [HOWTO's](#howtos)
- [Otros recursos de información](#otros-recursos-de-información)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)
- [Resumen](#resumen)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Manejar páginas *info* y manuales
* Encontrar y usar HOWTO's.
* Familiarizarse con otras fuentes de información importantes.

## Autoayuda

GNU/Linux es un sistema intrincado y potente, y por lo tanto, complejo. La documentaicón es una herramienta importante para controlar esa complejidad, y la mayoría de aspectos que se gestionarán desde el sistema están extensamente documentados.

> El término *Ayuda* en GNU/Linux suele significar *autoyuda*. No está bien visto realizar preguntas obvias en la comunidad de usuarios, y que están resueltas en el primer párrafo de los manuales.


## El comando help y la opción help

En *bash*, los comandos internos son explicados con mayor detalle por el comando `help`, pasándole el nombre del comando como argumento:

	$ help exit
	---

Algunos comandos externos soportan la opción `--help`, mostrando su sintáxis y parámetros.

> No todos los comandos soportan la opción `--help`; esta opción se le puede denominar `-h` o `?`, o incluso puede mostrar ayuda si introducimos alguna opción no válida. No existe una norma universal.

> Si ejecutamos el comando `help` sin argumentos, podemos visualizar la lista completa de comandos internos de nuestro sistema.

## El manual en línea

Casi todos los programas incorporan un *manual page*, así como algunos ficheros de configuración, llamadas al sistema, etc. Suelen instalarse con los programas, y se accede a ellos con el comando `man`. Por ejemplo: `man bash`.
Sin embargo, existen algunas desventajas. La mayoría sólo están disponibles en inglés, y las traducciones pueden estar incompletas o no ser del todo exactas. El acceso a dicha ayuda puede resultar comleja para los recién llegados, y la estructura puede resultar algo oscura en documentos muy largos.
Por otro lado, debemos tener en cuenta que siempre estará disponible para su consulta desde la línea de comandos.
La estructura de las páginas `man`, sigue el esquema que se muestra en la siguiente tabla (aunque no de forma estricta):

| Sección | Contenido |
| --- | --- |
| NAME | Nombre de comando y breve descripción.|
| SYNOPSIS | Sintaxis del comando |
| DESCRIPTION | Descripción extendida de los efectos del comando |
| OPTIONS | Opciones disponibles |
| ARGUMENTS | Argumentos disponibles |
| FILES | Ficheros auxiliares |
| EXAMPLES | Ejemplos de línea de comandos |
| SEE ALSO | Referencias cruzadas |
| DIAGNOSTICS | Mensajes de error y de alerta |
| COPYRIGHT | Autores del comando |
| BUGS | Limitaciones conocidas del comando |

Las páginas *man* están escritas en un formato especial que puede ser procesado y mostrado en pantalla, o enviado a una impresora por un programa llamado `groff`. El código fuente para estas páginas se encuentra almacenado en el directorio `/usr/share/man`, en subdirectorios con el nombre `manX`, donde X es un número que corresponde con los capítulos de la siguiente tabla.

| No. | Tema |
| --- | --- |
| 1 | Comandos de usuario |
| 2 | Llamadas al sistema |
| 3 | Librerías de funciones C |
| 4 | Drivers y ficheros de dispositivos |
| 5 | Ficheros de configuración y formatos de ficheros |
| 6 | Juegos |
| 7 | Varios |
| 8 | Comandos de administrador |
| 9 | Funciones del kernel |
| n | Comandos nuevos |

> Podemos incorporar nuevos directorios con páginas *man* configurando la variable de entorno **MANPATH**, la cual contiene los directorios en los cuales buscará `man`, por orden. El comando `manpath` muestra ayuda para configurar **MANPATH**.

### Capítulos

Cada página del manual pertenece a un capítulo, mostrados en la tabla anterior. Donde los capítulos 1, 5 y 8 son los más importantes. Podemos indicar al comando `man` que limite la búsqueda a ese capítulo:

	$ man 1 crontab
	$ man 5 crontab

Cuando nos referimos a páginas *man*, es habitual añadir el número de capítulo en paréntesis, por ejemplo *crontab(1)*, *crontab(5)*. Con la opción `-a`, el comando `man` muestra todas las páginas que coincidan con el nombre pasado; y sin esta opción, sólo muestra la primera página encontrada (generalmente del capítulo 1).

### Mostrando las páginas *man*

El programa usado para mostrar las páginas *man* en un terminal de texto suele ser `less`, que se verá más adelante. Podemos desplazarnos por el contenido con las flechas del cursor, ↑ y ↓. Para buscar un término, pulsamos el carácter `/`. Y para quitar el cuadro de búsqueda, pulsamos la tecla `q`.

Antes de dar vueltas sin rumbo a través de varias páginas *man*, es útil buscar información general acerca de un término con el comando `apropos`. Realiza la misma función que `man -k`; ambos buscan en la sección **NAME** de todas las páginas *man* el término dado en la línea de comandos. 
Un comando relacionado es `whatis`. También busca en todas las páginas *man*, pero por un nombre de comando (fichero, ...), en lugar de una palabra clave - es decir, la parte izquierda de la sección **NAME**, justo antes del guión. Es equivalente a `man -f`.

## Páginas *info*

Para algunos comandos también existen las *páginas info*, en lugar de (o a mayores) las páginas *man*. Están basadas en los principios del hipertexto. Se pueden mostrar usando el siguiente comando:

	info <comando>

Una de las ventajas de las páginas *info* es que están escritas en un formato que puede ser procesado por otras aplicaciones.

## HOWTO's

No todos nuestros problemas se resuelven a través del uso de un sólo comando. Estamos hablando de resolver un problema, en lugar de usar un comando determinado. Los HOWTOs han sido diseñados para ayudarnos a resolver estos problemas. Los HOWTOs son documentos más extensos, e intentan explicar los pasos a seguir para solucionar un problema. Por ejemplo, existe un **DSL HOWTO** con los detalles para conectar un sistema Linux a Internet usando una línea DSL. La mayoría de las distribuciones proporcionan HOWTOs como paquetes instalados en local. Por ejemplo, en Debian se sitúan en `/usr/share/doc/HOWTO`. Las versiones actualizadas podemos encontrarlas en la web [tldp](www.tldp.org).

## Otros recursos de información

Podemos localizar documentación adicional y ficheros de ejemplo para cada paquete instalado en los directorios `/usr/share/doc` y `/usr/share/doc/packages` (depende de la distribución). Las **GUI** también ofrecen menús de Ayuda. Algunos de los sitios más conocidos con ayuda para Linux:

http://www.tldp.org
http://www.linux.org
http://lwn.net
http://freshmeat.net

La gran ventaja del software libre es la gran cantidad de documentación existente. Hay una parte de la comunidad que colabora en la elaboración de estos materiales, junto a los desarrolladores. Pudiendo traducirlo a varios idiomas. Hay una tarea pendiente en cuanto a reconocer del mismo modo el trabajo de los responsables de dicha labor de documentación que el de los programadores.

## Comandos vistos en el tema

| Comando | Descripción |
| --- | --- |
| apropos | Muestra las páginas *man* cuya sección **NAME** contenga un término |
| groff | Programa de *tipado* sofisticado |
| help | Muestra ayuda en línea para comandos *bash* |
| info | Muestra páginas info de GNU |
| less | Muestra texto por páginas |
| man | Muestra las páginas *man* del sistema |
| manpath | Establece la ruta de búsqueda para las páginas *man* |
| whatis | Localiza paǵinas *man* que contengan un término dado en su descripción |

## Resumen

* `help <comando>` muestra ayuda para comandos internos de bash. Muchos comandos externos soportan la opción `--help`.
* La mayoría de los programas vienen con páginas *man*, para ser usadas con el comando `man`. El comando `apropos` busca en todas las páginas por palabra clave, y `whatis` busca en las páginas man.
* Para algunos programas, las páginas *info* son una alternativa a las páginas *man*.
* Los HOWTOs están orientados a resolver problemas concretos.
* Hay una gran cantidad de recursos disponibles en la web.
