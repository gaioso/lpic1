# Tema 37 - Scripts de Shell interactivos

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Introducción](#introducción)
- [El comando `read`](#el-comando-read)
- [Menús con `select`](#menús-con-select)
- [Interfaces «gráficas» con `dialog`](#interfaces-«gráficas»-con-dialog)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Dominar técnicas de control de scripts
* Conocer métodos para interactuar con el usuario
* Utilizar el paquete `dialog`

## Introducción

En temas anteriores, hemos visto como escribir scripts de shell que manejan nombres de ficheros, y otro tipo de información desde la línea de comandos. Pero esto se limita a aquellos usuarios más acostumbrados con el uso de la terminal, sin embargo, el resto de usuarios prefieren otro tipo de uso a través de preguntas, menús, o interfaces gráficas.

## El comando `read`

Ya hemos visto algún ejemplo de uso del comando `read`, donde lee líneas de su entrada estándar y las asigna a las variables de shell.
Es posible el uso de varias variables al mismo tiempo. La entrada se divide en 'palabras', de tal modo que la primera palabra se asigna a la primera variable, la segunda a la segunda, y así sucesivamente.

```
$ echo Hola mundo | read h m
$ echo $m $h
mundo Hola
```

Las variables 'sobrantes' permanecen vacías:

```
$ echo 1 2 | read a b c
$ echo $c

$
```

Pero, que es una 'palabra'?. La shell usa el contenido de la variable IFS (internal field separator) como una lista de separadores. El valor por defecto de IFS consiste en un espacio, tabulador y salto de línea. Pero es posible asignar un valor diferente:

```
IFS=":"
cat /etc/passwd | while read login pwd uid gid gecos dir shell
do
	echo $login: $gecos
done
```

evitando de este modo el uso de comandos como `cut`.
Por supuesto, también podemos «leer» desde el teclado:

```
$ read x
Hola
$ echo $x | tr a-z A-Z
HOLA
```

Con `bash`, incluso podemos añadir un texto, como se puede comprobar en el siguiente script para crear una nueva cuenta de usuario:

```
	#!/bin/bash
	# newuser

	read -p "Login name: " login
	read -p "Real name: " gecos

	useradd -c "$gecos" $login
```

Es importante encerrar entre comillas las variables del comando `read`, para evitar problemas con los espacios.

En ocasiones, el usuario puede introducir valores inválidos que hay que validar. En el ejemplo anterior, debemos comprobar que el nombre de usuario esté formado por letras minúsculas y números solamente:

```
read -p "Login name: " login

test=$(echo "$login" | tr -cd 'a-z0-9')
if [ -z "$login" -o "$login" != "$test" ]
then
	echo >&2 "Invalid login name $login"
	exit 1
fi
```

En la variable `test` guardamos el contenido de `login` después de aplicar el comando `tr`, eliminando el complemento de los caracteres válidos, que luego se comprueba para ver si es igual al valor original.
Con la opción `-z` del comando `test`, comprobamos que la longitud sea diferente a cero.

En operaciones críticas como la creación de un nuevo usuario, debemos realizar una última verificación antes de ejecutar el comando principal (`useradd`):

```
function confirm () {
	done=0
	until [ $done = 1 ]
	do
		read -p "Please confirm (y/n): " answer
		case $answer in
			[Yy]*) result=0; done=1 ;;
			[Nn]*) result=1; done=1 ;;
			*) echo "Please answer 'yes' oder 'no'" ;;
		esac
	done
	return $result
}
[...]
confirm && useradd ...
```

## Menús con `select`

`Bash` incorpora un comando para seleccionar opciones de una lista, `select`, con una sintaxis similar al comando `for`:

```
select <variable> [in <list>]
do
	<commands>
done
```

Se ejecuta el bucle una y otra vez hasta que se introduce un EOF en la entrada estándar. El valor de `<variable>` es seleccionado por el usuario de una `<lista>`. La shell añade un número al principio de cada valor, y el usuario selecciona un valor escribiendo uno de esos números.

```
$ select plato in Polbo Raxo Pementos
> do
> 	echo Marchando unha de $plato
> done
1) Polbo
2) Raxo
3) Pementos
#? 2
Marchando unha de Raxo
...
#? CTRL+D
$
```

> `select` también se parece a `if` en que si no se le indica una `<lista>`, utiliza los argumentos del script. El bucle puede ser cancelado con los comandos `break` y `continue`.

Actualización de `newuser`. Podemos mejorar nuestro script con el soporte para varios tipos de usuarios (profesores, estudiantes, personal administración, etc.) En este caso, el script debe ofrecer una lista de tipos de usuarios, para poder seleccionar uno de ellos. Además, cada tipo de usuario tendrá una serie de valores por defecto, como el grupo primario, el directorio `home`, etc.

En el directorio `/etc/newuser` guadaremos un fichero con los valores por defecto para cada tipo de usuario. Por ejemplo, el fichero `/etc/newuser/profesor` contendrá los valores para el tipo de usuario `profesor`, y el fichero `/etc/newuser/estudiante`, para el tipo de usuario `estudiante`.

```
	# /etc/newuser/profesor
	GROUP=profs
	EXTRAGROUP=office
	HOMEDIR=/home/$GROUP
	SKELDIR=/etc/skel-$GROUP
```

Entonces, una vez que sabemos el tipo de usuario, nuestro script ejecutará las siguientes instrucciones:

```
confirm || exit 0

. /etc/newuser/$type
useradd -c "$gecos" -g $GROUP -G $EXTRAGROUP -m -d $HOMEDIR/$login -k $SKELDIR $login
```

Para poder seleccionar el tipo de usuario no lo haremos con una lista cerrada de valores, sino con el contenido del directorio `/etc/newuser`:

```
echo "The following user types are available:"
PS3="User type: "
select type in $(cd /etc/newuser; ls) '[Cancel]'
do
	[ "$type" = "[Cancel]" ] && exit 0
	[ -n "$type" ] && break
done
```

## Interfaces «gráficas» con `dialog`

En lugar de trabajar con menús de texto y diálogos más parecidos a teletipos, podemos aproximarnos a una interfaz gráfica para mejorar nuestros scripts. El programa `dialog`, que en algunas distribuciones será necesario instalar a mayores del sistema base, nos ofrece alguna de las capacidades de las terminales modernas para mostrar menús a pantalla completa, listas de selección, campos de entrada de texto, así como otros controles, y con posibilidad de colores.

`dialog` incorpora una extensa lista de elementos, cuyos detalles exceden los objetivos de este tema y deben ser consultados para conocer los detalles de su funcionamiento.

Por ejemplo, podemos modificar el ejemplo anterior de selección de plato de menú con la opción `menu`:

```
$ dialog --clear --title "Selecciona el plato" \
	--menu "Que quieres comer?" 12 40 4 \
		"R" "Raxo" \
		"P" "Polbo" \
		"T" "Pementos"
```

La opción `clear`, limpia la pantalla antes de mostrar el menú, y `title` asigna el título para el menú. La opción `menu` indica que el formato del diálogo será un menú de selección, seguido por un texto 'prompt', y tres números: altura del menú en lineas, anchura del menú en caracteres, y el número de entradas mostradas al mismo tiempo.

Cuando utilizamos `dialog`, debemos tener en cuenta que el resultado se envía al canal de error estándar. Esto quiere decir, que si queremos obtener el valor devuelto por el programa, debemos redirigir su salida de error estándar, no la salida estándar. Esto se debe a que su salida estándar es utilizada para mostrar los diálogos, por lo que si redirigimos su salida estándar no se verá nada por pantalla.

Para solucionar esta situación debemos realizar la redirección de la salida de error estándar previamente a la salida estándar:

`resultado=$(dialog ... 2>&1 1>/dev/tty)`

## Comandos vistos en el tema

Comando | Descripción
--- | ---
read | Comando para leer desde teclado, fichero o «pipes»
dialog | Programa para mostrar controles similares a una GUI en una consola de caracteres 
