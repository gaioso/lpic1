# Tema 30 - Correo electrónico

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Fundamentos](#fundamentos)
- [Funcionalidad básica](#funcionalidad-básica)
- [Gestionando la cola de correo](#gestionando-la-cola-de-correo)
- [Entrega local, alias y reenvío](#entrega-local-alias-y-reenvío)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Conocer los programas servidores de correo más comunes en Linux
* Ser capaz de configurar *alias* y reenvío de correo
* Comandos más importantes para la gestión del correo 

## Fundamentos

Una de las mayores innovaciones del sistema operativo Unix fue hacer que el procesado del correo electrónico se realice en diferentes módulos. En lugar de tener un proceso único que gestione todas las tareas necesarias para enviar y recibir correo electrónico.

Los *Mail Transfer Agents* (MTA's), son los programas que envían o reciben los mensajes de correo electrónico. Mientras que los usuarios interactúan con los MUA's (*Mail User Agent*) -como Outlook, Thunderbird, Kmail, etc., para leer, crear, responder, o eliminar mensajes. Los MUA's se valen de los MTA's para transportar los mensajes a sus destinatarios. Los MTA's pueden estar localizados en los ISP's, o instalados localmente. Las tareas que llevan a cabo los MTA's incluyen la reescritura de las direcciones a un formato «útil», reintentar el envío de mensajes, notificar al remitente posibles fallos, etc. Entre otros, los MTA's en Internet se comunican usando el protocolo SMTP.
Y, por último, los MDA (Mail Delivery Agent) se encargan de entregar los mensajes en los buzones locales de los usuarios.

## Funcionalidad básica

Un MTA como *Sendmail* tiene dos tareas principales que realizar:
1. Escuchar en el puerto SMTP (puerto TCP 25), por conexiones de otros MTA's que quieren enviar mensajes a los buzones locales. Para poder recibir mensajes en el puerto TCP 25, el MTA debe ejecutarse como un *demonio* de forma permanente, o ser iniciado con un «super server» como *inetd* o *xinetd*. Este último caso sólo si hay muy poco correo que procesar.
2. Enviar mensajes que han sido entregados por MUA's locales y otros programas que han llamado directamente al MTA a través del puerto SMTP 25.

Un MTA común en UNIX y Linux es el programa *sendmail*. Otros MTA's son: Postfix, Exim y Qmail. Los desarrolladores de la certificación LPIC-1 han establecido que los candidatos deben conocer todos esos MTA's, pero a un nivel básico. Afortunadamente, tanto Postfix como Exim tratan de ser compatibles con Sendmail; sólo Qmail intenta ser diferente. En cuanto a sus arquitecturas, tanto Sendmail como Exim utilizan un único proceso. Sin embargo, Postfix y Qmail separan las funcionalidades del MTA en un conjunto de procesos, aumentando la seguridad y centrando los esfuerzos en una tarea concreta. Pero como desventaja deben establecer una comunicación eficaz entre las diferentes partes.

> No es necesario utilizar ambas funciones al mismo tiempo. Por ejemplo, si no esperas recibir mensajes desde Internet, porque nuestro equipo no está conectado al exterior directamente. En este caso no es necesario ejecutar el MTA escuchando en el puerto 25, es suficiente hacerlo en *localhost*.

> A la inversa, podemos permitir el envío local sin entregarlo; en redes locales es común designar un host como el servidor de correo, y en el resto de hosts, instalar un MTA mínimo que tan sólo envía todos sus mensajes al servidor de correo, que hace las entregas en local o hacia Internet.

Normalmente, Sendmail entrega los mensajes a los usuarios locales en sus buzones en `/var/mail`, donde los MUA's, o los servidores IMAP o POP pueden recogerlos. Cada buzón es un fichero cuyo nombre corresponde con el del usuario al que pertenece; los mensajes nuevos se añaden a dicho fichero. Para que este método funcione, todos los programas que usan los buzones deben ser capaces de *bloquear* el fichero.

> Por defecto, Postfix y Exim funcionan igual que Sendmail, y Qmail usa un formato llamado *Maildir* que se compone de una estructura de directorios donde cada mensaje se almacena en su propio fichero.

Sendmail intenta deshacerse de los mensajes en cuanto pueda; y si esto no es posible los almacena en una cola en el directorio `/var/spool/mail`. Los otros MTA's usan directorios similares para almacenar los mensajes no enviados.

> Puede ser una buena idea, no situar estos directorios -`/var/mail` y `/var/spool/mail`- en el directorio raíz, para evitar que mensajes de gran tamaño ocupen más espacio del recomendado.

## Gestionando la cola de correo

Si queremos comprobar el estado actual de la cola de correo, podemos usar el comando `mailq` en Sendmail.

> Lo mismo se aplica a Exim y Postfix, que incorporan programas compatibles con Sendmail; sólo Qmail tiene su propio software `qmail-qread` o `qmail-stat`

Sendmail puede procesar la cola a intervalos de tiempo. Podemos modificar el tiempo de que ha de transcurrir entre los intentos de envío con el comando:

	# sendmail -db -q30m

que inicia Sendmail como demonio e indica que revise su cola cada 30 minutos.

Podemos provocar que Sendmail procese de forma inmediata su cola con la opción `-q`:

	# sendmail -q

Tanto Postfix como Exim trabajan de forma similar, y para que Qmail procese su cola, necesitamos enviarle una señal **SIGALRM** al proceso `qmail-send`

## Entrega local, alias y reenvío

Como vimos, Sendmail (y compañeros) escriben los mensajes en sus buzones locales en `/var/mail`. Sin embargo, hay varios métodos para modificar esto:

* El fichero `/etc/aliases` permite configurar un método de envío diferente para ciertas direcciones locales. Una entrada como:<br>
	`root: hugo`<br>
envía los mensajes dirigidos a *root*, al usuario local *hugo*. También podemos enviar mensajes a varios destinos:<br>
	`hugo: \hugo, usuario@ejemplo.net`

El carácter *backslash* es necesario para evitar un bucle infinito.

Sendmail no lee directamente el fichero `/etc/aliases`, sino que lo hace desde una base de datos que guarda en formato binario. Para que los cambios realizados en `/etc/aliases` tengan efecto debemos ejecutar el comando `newaliases`. Los usuarios pueden realizar sus propios ajustes, a través de un fichero `.forward` que se encuentra en su directorio personal. Sendmail observa esa configuración cuando debe entregar un mensaje al usuario en cuestión. El ejemplo más común es:
	$ cat /home/debian/.forward
	\hugo, “|/usr/bin/vacation hugo”

> El programa *vacation* es un servicio de respuesta automática que responde a los mensajes entrantes usando el contenido de `~/.vacation.msg`. De esta forma podemos informar a nuestros amigos que no estaremos disponibles durante un tiempo.

> Postfix y Exim también manejan ficheros `.forward`. Qmail soporta algo equivalente bajo el nombre `.qmail-default`

## Comandos vistos en el tema

| Comando | Descripción |
| --- | --- |
| mailq | Muestra el estado de la cola de correo |
| newaliases | Actualiza la base de datos de alias |
| sendmail | Comando para administrar MTA's |
| vacation | Respuestas automáticas |
