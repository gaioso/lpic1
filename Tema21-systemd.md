# Tema 21 - Systemd

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
    - [Introducción](#introducción)
    - [Ficheros Unit](#ficheros-unit)
    - [Tipos de «Units»](#tipos-de-«units»)
    - [Dependencias](#dependencias)
    - [Targets](#targets)
    - [El comando `systemctl`](#el-comando-systemctl)
    - [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Comprender la infraestructura systemd
* Conocer la estructura de los ficheros «unit»
* Entender y ser capaz de configurar «targets»

### Introducción

Systemd, fue creado por Lennart Poettering y Kay Sievers, es otra alternativa a System-V init. Al igual que Upstart, intenta evitar las limitaciones de System-V, pero implementa varios conceptos para la activación y control de servicios mucho más rigurosos que Upstart.
Systemd gestiona las dependencias entre servicios de una forma muy diferente, por ejemplo, si un servicio necesita que `syslog` esté activado para poder enviar mensajes de log, Systemd crea el canal para que se puedan enviar dichos mensajes si todavía no está iniciado dicho servicio. Ese mensaje será enviado a `syslog` una vez iniciado.
Este esquema funciona incluso durante la ejecución del sistema. Si un servicio requerido no activo, Systemd lo inicia. También incluye al sistema de ficheros, de tal modo que si se intenta acceder a un fichero de un sistema de ficheros no presente, se suspende dicho acceso hasta que esté disponible dicho sistema de ficheros.

Systemd usa las «units» como una abstracción de los diferentes elementos a gestionar, como servicios, canales de comunicación, o dispositivos. Los «targets» sustituyen a los niveles de ejecución de System-V init, y se utilizan para agrupar las «units». Por ejemplo, existe un «target» `multiuser.target` que se corresponde con el tradicional nivel de ejecución 3. Algunos «targets» pueden depender de la disponibilidad de dispositivos, por ejemplo, `bluetooth.target` puede ser requerido cuando se conecta un adaptador Bluetooth, e iniciar el software correspondiente.

Systemd pretende guardar compatibilidad con los sistemas tradicionales. Por ejemplo, tiene soporte para los scripts tipo System-V si no existe un fichero de configuración nativo para un servicio determinado, e interpreta el contenido de `/etc/fstab` para montar los sistemas de ficheros.

Con el comando `systemctl` podemos interactuar con systemd para parar o iniciar servicios de forma explícita:

```
# systemctl status rsyslog.service
[...]
# systemctl stop rsyslog.service
[...]
# systemctl start rsyslog.service
```

Las ordenes anteriores son gestionadas a través de una cola de peticiones, y son tratadas como «transacciones», para poder evitar inconsistencias durante el cambio de estado de un servicio, pudiendo de esta forma ejecutar un «rollback» de dicha operación.

### Ficheros Unit

Una de las mayores ventajas de `systemd` es que se utiliza un formato unificado para los ficheros de configuración, no importando si se trata de un servicio, canal de comunicación o un sistema de ficheros. Contrastando con el modelo tradicional de System-V, al utilizar diferentes ficheros y formatos de configuración.
Es importante destacar que las sentencias de los ficheros de configuración son «declarativas», es decir, no contienen código ejecutable, tal y como ocurre en System-V. Por supuesto, el tamaño de los ficheros de `systemd` se reduce a una o dos decenas de lineas, que son fácilmente interpretables.

**Sintaxis**. Las sintaxis básica de los ficheros «unit» se explican en `systemd.unit(5)`. A continuación, se puede observar un ejemplo de fichero «unit» `console-getty.service` :

```
#  This file is part of systemd.
#
#  systemd is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.

[Unit]
Description=Console Getty
Documentation=man:agetty(8)
After=systemd-user-sessions.service plymouth-quit-wait.service
After=rc-local.service
Before=getty.target

[Service]
ExecStart=-/sbin/agetty --noclear --keep-baud console 115200,38400,9600 $TERM
Type=idle
Restart=always
RestartSec=0
UtmpIdentifier=cons
TTYPath=/dev/console
TTYReset=yes
TTYVHangup=yes
KillMode=process
IgnoreSIGPIPE=no
SendSIGHUP=yes

[Install]
WantedBy=getty.target
```

Una característica típica es la división en secciones que comienzan con un título entre corchetes. Todos los ficheros pueden incluír una sección `[Unit]` y otra `[Install]`. Las líneas con opciones tienen el formato «`<nombre>=<valor>`».

**Buscando configuraciones**. `Systemd` busca ficheros de configuración a lo largo de varios directorios que se enumeran durante la compilación. Los primeros de la lista sobreescriben a los últimos:

* /etc/systemd/system
* /run/systemd/system
* /lib/systemd/system

Consultar el manual para conocer los detalles de los ficheros de configuración del modo usuario (--user).

**Plantillas de ficheros**. En ocasiones, tenemos varios servicios que comparten ficheros de configuración muy parecidos. En este caso, no es necesario mantener varias copias. Por ejemplo, las líneas que hacen referencia a los `tty` en `/etc/inittab` sólo se diferencian en el número de terminal.
`Systemd` soporta ficheros con nombres del tipo `example@.service`. Por ejemplo, podemos tener un fichero `getty@.service`, y luego para configurar un terminal `/dev/tty2`, creamos el siguiente enlace simbólico `getty@tty2.service` que apunta al fichero anterior (plantilla). Cuando se activa la terminal `tty2` el sistema lee el correspondiente fichero `getty@service`, cambiando la variable `%I` por el texto que aparece entre `@` y el carácter `.`.

**Configuración básica**. La sección `[Unit]` contiene información general sobre la «unit», y la sección `[Install]` los detalles sobre su instalación, como pueden ser algunas dependencias.
Algunas de las opciones más importantes de la sección `[Unit]` son las siguientes:

* **Description** Un texto libre que describe la «unit».
* **Documentation** Una lista de URL's separadas por comas con documentación de la «unit».
* **OnFailure** Una lista de otras «units» que serán activadas en caso de que se pase a un estado `failed`.
* **SourcePath** La ruta a un fichero de configuración desde el que fue creado esta «unit». Utilizado por herramientas que crean estes ficheros desde ficheros externos.
* **ConditionPathExists** Comprueba la existencia de un fichero (o directorio) sin el cual no se puede iniciar la «unit».

### Tipos de «Units»

`Systemd` soporta una gran variedad de «units», que son fáciles de identificar por la extensión de sus ficheros. A continuación, una lista de los tipos más importantes:

* **.service** Un proceso del sistema que es ejecutado y controlado por `systemd`.
* **.socket** Un socket local o TCP/IP, es decir, un punto de comunicación que los clientes pueden utilizar para conectar con un servidor. Los sockets están asociados a servicios, que serán iniciados al detectar actividad en dicho socket.
* **.mount** Un punto de montaje, que puede ser un directorio donde montar un sistema de ficheros.
* **.automount** Un punto de montaje que será activado bajo demanda. Los detalles serán aportados por una «unit» de tipo «mount».
* **.swap** Un espacio de «swap» en el sistema.
* **.target** Un «target», o un punto de sincronización para otras «units» durante el arranque del sistema, o durante la transición a otro estado.
* **.path** Supervisa un fichero o directorio, e inicia una «unit» cuando se detecta un cambio en el mismo (acceso al fichero o un nuevo fichero en el directorio).
* **.timer** Inicia otra «unit» en un momento determinado, o bien a intervalos regulares. Puede suponer un sustituto para `cron` y `at`.

### Dependencias

Como hemos comentado, `systemd` es capaz de detectar las dependencias implícitas, pero en ciertas ocasiones es necesario establecer dependencias explícitas, por ejemplo:

* **Requires** Una lista de «units» que serán activadas junto con la actual (no se indica el orden). Si alguna de ellas es desactivada, o tiene algún fallo, también se detendrá la actual.
* **Wants** Un método menos restrictivo que el anterior. Se inicia la activación de las «units», pero el resultado de dicha activación no influye en el estado de la actual.
* **Conflicts** Lo contrario a `Requires`. Las «units» serán paradas cuando la «unit» actual se inicie.
* **Before (y After)** Esta lista determina el orden de ejecución.

### Targets

Los «Targets» en `systemd` son muy parecidos a los niveles de ejecución en System-V init, con la diferencia de que no existe un límite para el número de niveles que podemos configurar. Los ficheros de configuración no tienen opciones especiales, y se usan para agrupar otras «units» bajo dependencias. Para guardar cierta compatibilidad con System-V existen ciertos «targets» que se corresponden con los niveles de ejecución clásicos.

Podemos crear un «target» por defecto que se activará durante el inicio del sistema mediante la creación de un enlace simbólico desde `/etc/systemd/system/default.target` a la «unit» que deseemos:

```
# cd /etc/systemd/system
# ln -sf /lib/systemd/system/multi-user.target default.target
```

Otro método para realizar lo mismo con el comando `systemctl set-default`:

```
# systemctl get-default
...
# systemctl set-default graphical
...
# systemctl get-default
```

Para activar un determinado «target», al igual que se realizaba un cambio de nivel con el comando `telinit`, se puede conseguir del siguiente modo:

```
# systemctl isolate multi-user
```

Para detener el sistema, o cambiar al modo «rescate», existen varios métodos:

```
# systemctl rescue
# systemctl halt
# systemctl poweroff
# systemctl reboot
```

Estes comandos son los equivalentes a ejecutar `systemctl isolate`, pero mostrando un mensaje de advertencia a los usuarios con sesión iniciada. Debemos utilizar el comando `shutdown` para llevar a cabo estas operaciones. Para volver al estado normal, debemos ejecutar el comando `systemctl default`.

### El comando `systemctl`

El comando `systemctl` se usa para gestionar `systemd`. Ya hemos visto alguna de sus funciones, y ahora detallaremos alguna adicional.

**Comandos para «units»**

* **list-units** Muestra las «units» reconocidas por `systemd`. Podemos indicar un tipo (service, socket...), o bien un patrón de búsqueda `# systemctl list-units "*ssh"`
* **start** Inicia una o más «units» indicadas como parámetros.
* **stop** Detiene una o más «units» indicadas como parámetros.
* **reload** Recarga la configuración.
* **restart** Reinicia las «units» indicadas.
* **try-restart** Igual que la anterior, pero sin efecto para las «units» que no han sido iniciadas.
* **isolate** Se ejecuta la «unit» indicada, deteniendo todas las restantes.
* **status** Muestra el estado actual de las «units», junto con las entradas de log más recientes.
* **cat** Muestra el fichero de configuración.
* **help** Muestra la ayuda (manual) de la «unit». `$ systemctl help syslog`.

### Comandos vistos en el tema


| Comando | Descripción |
| --- | --- |
| systemctl | Utilidad principal de gestión de `systemd`  |
