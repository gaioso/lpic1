# Tema 32 - Configuración de red

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Ficheros de configuración](#ficheros-de-configuración)
- [Utilidades para la línea de comandos](#utilidades-para-la-línea-de-comandos)
- [Utilidades legacy](#utilidades-legacy)
- [El paquete iproute2](#el-paquete-iproute2)
- [Configuración automática de la red](#configuración-automática-de-la-red)
- [Agrupamiento de interfaces](#agrupamiento-de-interfaces)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)
- [Resumen](#resumen)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Conocer los mecanismos de configuración de red.
* Ser capaz de configurar las interfaces de red.
* Ser capaz de configurar rutas estáticas.

## Ficheros de configuración

Los sistemas Linux que hacen uso de *systemd* hacen uso del demonio *systemd-networkd* para poder detectar las interfaces de red y crear sus entradas en los ficheros de configuración. Por otro lado, cabe la posiblidad de modificar manualmente eses ficheros para ajustar la configuración de la red a nuestras necesidades.

Las distribuciones no hacen uso de un formato estándar para los ficheros de configuración, y en la siguiente tabla se muestra un resumen para las más importantes:

Distribución | Ubicación de la configuración de red
--- | ---
Debian (y derivados) | Fichero `/etc/network/interfaces`
Red Hat (y derivados) | Directorio `/etc/sysconfig/network-scripts`
OpenSUSE | Fichero `/etc/sysconfig/network`

Aunque los métodos sean diferentes entre las distribuciones, comparten características similares. Los diferentes valores que son necesarios en la configuración de la red se aplican como valores separados dentro del fichero de configuración.

A continuación se muestra un ejemplo de configuración estática en Debian:

```
auto eth0
iface eth0 inet static
  address 192.168.1.77
  netmask 255.255.255.0
  gateway 192.168.1.254
iface eth0 inet6 static
  address 2003:aef0::23d1::0a10:00a1
  netmask 64
  gateway 2003:aef0::23d1::0a10:0001
```

En los sistemas basados en Red Hat la configuración de la red se aplica desde dos ficheros de configuración. En el primero de ellos, se define las direcciones de red y máscara en un fichero cuyo nombre finaliza con el nombre de la interfaz, por ejemplo `ifcfg-enp0s3`.

```
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=dhcp
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=enp0s3
UUID=c8752366-3e1e-47e3-8162-c0435ec6d451
DEVICE=enp0s3
ONBOOT=yes
IPV6_PRIVACY=no
```

Y en el segundo (`network`) se define el nombre de host y la pasarela, tal y como aparece a continuación:

```
NETWORKING=yes
HOSTNAME=mysystem
GATEWAY=192.168.1.254
IPV6FORWARDING=yes
IPV6_AUTOCONF=no
IPV6_AUTOTUNNEL=noIPV6_DEFAULTGW=2003:aef0::23d1::0a10:0001
IPV6_DEFAULTDEV=eth0
```

Para poder usar nombres DNS es necesario configurar un servidor de nombres. En los sistemas basados en *systemd*, el servidor DNS se genera desde `systemd-resolved`. Para los viejos sistemas basados en SysVinit, la configuración se declara en el fichero `/etc/resolv.conf`:

```
domain mydomain.con
search mytest.com
nameserver 192.168.1.1
```

La entrada `domain` define el nombre de dominio asignado a la red. Por defecto, el sistema añade este nombre de dominio a cualquier nombre de host que se utilice. La entrada `search` declara los dominios adicionales que se usarán para buscar nombres de host. Y la entrada `nameserver` es donde se declara el servidor DNS asociado a nuestra red. Si fuese necesario añadir más servidores DNS, tan solo se añade una nueva entrada `nameserver`.

## Utilidades para la línea de comandos

Aunque es posible hacer uso de las utilidades disponibles en los diferentes gestores de escritorio, en esta unidad nos centraremos en las herramientas disponibles a través de la línea de comandos.

*Network Manager* nos ofrece dos tipos diferentes de utilidades:

* `nmtui` - Utiliza un menú basado en texto.
* `nmcli` - Se usa como un comando más de nuestra terminal.

El comando `nmcli` se puede usar sin opciones para visualizar los dispositivos de red configurados en nuestro sistema:

```
$ nmcli
eth0: connected to System eth0
        "Intel 82540EM"
        ethernet (e1000), 52:54:00:27:8B:50, hw, mtu 1500
        ip4 default
        inet4 10.0.2.15/24
        route4 0.0.0.0/0
        route4 10.0.2.0/24
        inet6 fe80::5054:ff:fe27:8b50/64
        route6 ff00::/8
        route6 fe80::/64
...
```

Haciendo uso de sus diferentes opciones se puede modificar la configuración de red:

	# nmcli con add type ethernet con-name eth1 ifname enp0s3 ip4 10.0.2.10/24 gw4 192.168.1.254

## Utilidades legacy

En el caso de que nuestro sistema no soporte las utilidades de *Network Manager*, todavía existen algunos comandos heredados, incluyedo las que integran el paquete `net-tools`. Algunas de las que podemos usar son las siguientes:

* `ethtool` - Muestra la configuración Ethernet para una interfaz
* `ifconfig` - Muesta o configura propiedades de red para una interfaz
* `iwconfig` - Configura el SSID y la clave para una interfaz inalámbrica
* `route` - Ajusta los valores de las rutas

El comando `ethtool` nos permite ajustar algunas propiedades más internas de una interfaz que podemos tener que ajustar para comunicarnos con otros elementos de la red, como un *switch*. Es posible modificar algunos parámetros como la velocidad, *duplex* y la capacidad de autonegociar ciertas características con el *switch*.

Con el comando `ifconfig` es posible la modificación de los parámetros de red, como su dirección IP o máscara. A día de hoy ya no está disponible en las distribuciones de mayor uso.

Antes de poder configurar los parámetros principales de nuestra interfaz inalámbrica, debemos indicar el SSID y su clave con el comando `iwconfig`:

	# iwconfig wlan0 essid "MyNetwork" key s:mypassword

La opción `essid` indica el nombre SSID del punto de acceso, y la opción `key` la clave para conectarse al mismo. Esta clave está precedida por el carácter `s:`, que nos permite especificar la clave en formato ASCII; en otro caso, debe indicarse en formato hexadecimal.

En caso de no conocer el nombre de la red wifi visible desde nuestro equipo, usaremos el comando `iwlist` para ver todas las señales que llegan hasta nosotros.

	$ iwlist wlan0 scan

Si es preciso activar la ruta por defecto en nuestro sistema, usaremos el comando `route`:

	# route add default gw 192.168.1.254

También es posible utilizar el comando `route` para consultar la tabla de enrutamiento del sistema.

## El paquete iproute2

La gran mayoría de las utilidades de la línea de comandos han sido reemplazadas por el nuevo paquete `iproute2`. Su principal comando el `ip`. Este comando es mucho más robusto que `ifconfig`, e incorpora diferentes opciones para visualizar y/o modificar la configuración de red. En la siguiente tabla se muestran las opciones del comando `ip`.

Opción | Descripción
--- | ---
`address` | Muestra o configura la dirección IP (v4 o v6) en el dispositivo
`addrlabel` | Configura las etiquetas
`l2tp` | Túnel Ethernet sobre IP
`link` | Especifica un dispositivo
`maddress` | Indica una dirección *multicast*
`monitor` | Vigila los mensajes *netlink*
`mroute` | Define una entrada en la caché de enrutado *multicast*
`mrule` | Define una regla en la base de datos de políticas de enrutado *multicast*
`neighbor` | Gestiona las entradas ARP o NDISC
`netns` | Gestiona los nombres de espacios
`ntable` | Gestiona la caché de los vecinos
`route` | Gestiona la tabla de enrutamiento
`rule` | Gestiona las entradas en la base de datos de políticas de enrutado
`tcpmetrics` | Gestiona las métricas TCP en la interfaz
`token` | Gestiona los identificadores de interfaz
`tunnel` | Túnel sobre IP
`tuntan` | Gestiona dispositivos TUN/TAP
`xfrm` | Gestiona las políticas IPSec para las conexiones seguras

Cada una de estas opciones posee parámetros para especificar la acción a realizar. Con el siguiente comando se muestra la configuración actual de la red:

```
$ ip address show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:8d:c0:4d brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe8d:c04d/64 scope link 
       valid_lft forever preferred_lft forever
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:e6:66:13 brd ff:ff:ff:ff:ff:ff
    inet 192.168.33.10/24 brd 192.168.33.255 scope global eth1
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fee6:6613/64 scope link 
       valid_lft forever preferred_lft forever
```

En la salida anterior se muestran tres interfaces de red en un sistema Linux:

* `lo`: La interfaz *loopback*
* `eth0`: Una interfaz de cable Ethernet
* `eth1`: Una segunda interfaz de cable

La interfaz *loopback* es una interfaz virtual que puede ser utilizada por cualquier aplicación local para comunicarse con otras igual que si lo hiciesen con tráfico de red.

Si en la salida anterior no se muestra una dirección de red asignada a la interfaz, se puede usar el comando `ip` para configurarla:

	# ip address add 192.168.1.77/24 dev eth1

Y finalmente, activamos la interfaz con la opción `link`:

	# ip link set eth1 up

> Para ajustes más precisos debemos usar el fichero `/etc/sysctl.conf`. En este fichero se definen algunos parámetros que utiliza el kernel para gestionar las operaciones de red. Es posible, por ejemplo, deshabilitar las respuestas a paquetes ICMP a través del parámetro `icmp_echo_ignore_broadcasts` (1), o desactivar el *forwarding* entre diferentes interfaces con el parámetro `ip_forward`.

## Configuración automática de la red

Si en nuestra red tenemos habilitado el servicio DHCP, debemos asegurarnos de la ejecución del cliente oportuno en nuestro sistema Linux. Este cliente se comunica con el servidor para obtener los valores de red necesarios para el correcto funcionamiento. En Linux tenemos disponibles, entre otros, los siguientes clientes:

* dhcpd
* dhclient
* pump

El programa *dhcpd* se están convirtiendo en uno de los más populares, pero también es frecuente el uso de los otros dos en algunas distribuciones Linux. Cuando se instalan estos clientes, se habilita su inicio durante el arranque del sistema.

## Agrupamiento de interfaces

La mayoría de los sistemas actuales tienen dos o más conexiones de Ethernet. Estas conexiones se pueden usar por separado o en paralelo mediante una técnica llamada *bonding*. Esta técnica es muy interesante porque permite equilibrar la transmisión de los datos (los datos se transmiten a través de dos interfaces), y además se implementa tolerancia a fallos (si un enlace falla, la transmisión está asegurada por el otro enlace).

En Linux podemos indicar a Linux como debe tratar este agrupamiento de tres formas diferentes:

* *Balanceo de carga*: El tráfico de red se comparte entre dos o más interfaces de red
* *Agrupamiento*: Dos o más interfaces de red se combinan para alcanzar mayor ancho de banda
* *Activo/Pasivo*: Mientras una de las interfaces está activa, la otra se usa como respaldo ante un posible fallo

La tabla siguiente muestra los diferentes modos soportados:

Modo | Nombre | Descripción
--- | --- | ---
0 | balance-rr | Proporciona balanceo de carga y tolerancia a fallos mediante un esquema *round-robin*
1 | active-backup | Proporciona tolerancia a fallos con una de las interfaces como activa y la otra como respaldo
2 | balance-xor | Proporciona balanceo de carga y tolerancia a fallos mediante el envío a través de la primera interfaz y la recepción por la segunda
3 | broadcast | Envía todos los paquetes a través de todas las interfaces
4 | 802.3ad | Combina las interfaces para crear una conexión con la suma de los anchos de banda
5 | balance-tlb | Proporciona balanceo de carga y tolerancia a fallos basado en la carga de los envíos en cada interfaz
6 | balance-alb | Proporciona balanceo de carga y tolerancia a fallos basado en la carga de la recepción en cada interfaz

Para inicializar esta funcionalidad, previamente debemos realizar la carga del módulo en el kernel:

	$ sudo modprobe bonding

Con el siguiente comando creamos una interfaz `bond0`:

	$ sudo ip link add bond0 type bond mode 4

A continuación, añadimos las interfaces de red al agrupamiento:

	$ sudo ip link set eth0 master bond0
	$ sudo ip link set eth1 master bond0

A partir de este momento, Linux tratará al dispositivo `bond0` como una interfaz independiente.

> Si disponemos de varias interfaces de red en nuestro sistema, podemos conectar diferentes redes como si fuera un *switch* entre dos o más redes. El comando `brctl` permite gestionar el *bridge*.

## Comandos vistos en el tema

Comando | Descripción
--- | ---
ifconfig | Configura interfaces de red
ifdown | Deshabilita una interfaz de red (Debian)
ifup | Habilita una interfaz de red
ip | Gestiona interfaces de red y enrutado
route | Gestiona las tablas estáticas del kernel de Linux

## Resumen

* El comando `ifconfig` se usa para la configuración de bajo nivel de los parámetros de las interfaces de red.
* Las rutas indican cómo se deben enviar los datagramas hacia sus destinos.
* El comando `route` se usa para configurar rutas.
* El comando `ip` es una mejora para los comandos `ifconfig` y `route`.
