# Tema 39 - Linux y la seguridad

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Introducción](#introducción)
- [Seguridad del sistema de ficheros](#seguridad-del-sistema-de-ficheros)
- [Usuarios y ficheros.](#usuarios-y-ficheros)
    - [Fortalecer contraseñas](#fortalecer-contraseñas)
- [Límites de recursos](#límites-de-recursos)
- [Privilegios de administrador con sudo](#privilegios-de-administrador-con-sudo)
- [Seguridad básica de la red](#seguridad-básica-de-la-red)
- [Explorar sockets con ss y systemd.socket](#explorar-sockets-con-ss-y-systemdsocket)
- [Auditoría de ficheros abiertos con lsof y fuser](#auditoría-de-ficheros-abiertos-con-lsof-y-fuser)
- [Deshabilitando servicios](#deshabilitando-servicios)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Buscar en el sistema de ficheros de entradas relacionadas con la seguridad (ficheros de dispositivo, bits SUID y SGID, etc.)
* Comprender y usar los límites de recursos.
* Configurar y usar `sudo`.

## Introducción

La seguridad ya no es algo que afecte tan sólo a las grandes instalaciones informáticas, debemos preocuparnos de la seguridad también de nuestros equipos personales, para evitar que caigan en manos de redes de *spam* y usen nuestra conexión a Internet para usos no autorizados. El uso de Linux puede ayudar mucho a conseguir este objetivo, pero debemos mantenernos en alerta, sin bajar nunca la guardia.

## Seguridad del sistema de ficheros

Las propiedades de un sistema de ficheros de Linux, se pueden resumir en dos principales:
1. Todo es un fichero (incluso los dispositivos)
2. Existe un sistema de permisos de acceso, simple y suficiente para la mayoría de los casos.

Esto tiene consecuencias muy útiles, pero algunas de ellas debemos examinarlas con atención:
* Los ficheros de dispositivos funcionan en cualquier parte del sistema, no sólo en `/dev`. Si un atacante consigue crear un fichero de dispositivo del tipo adecuado, estamos perdidos. No tiene porque tener el mismo nombre que el fichero de dispositivo *oficial*, sino que es suficiente con tener el tipo correcto.
* Los usuarios pueden convertir en ejecutables cualquiera de sus ficheros. No es un problema en la mayoría de casos, al contrario, es una ventaja respecto a otros sistemas, en los que esta circunstancia se controla a través de la extensión del fichero. Muchos problemas en Linux vienen dados porque un usuario consigue privilegios a través de la ejecución de un programa *normal*, y esta vía también está abierta a descargas que el usuario realice desde Internet.
* Los mecanismos **SUID** y **SGID** permiten a los usuarios normales ejecutar programas con los permisos de otro usuario (normalmente *root*).

Como administrador podemos reducir el riesgo mediante la restricción de ficheros de tipos *no usuales* a áreas del sistema de ficheros donde dichos ficheros son los que se esperan. Vamos a ver algunas pistas:

Los medios extraíbles son la forma más habitual de introducir ficheros inusuales en el sistema. Esto incluye tipos de ficheros que un usuario no está autorizado a crear usando su cuenta local. Por ejemplo, un usuario sin privilegios no puede ejecutar el siguiente comando:

	# mknod /home/hugo/vacation.jpg c 1 1

pero si este usuario es capaz de montar una memoria USB que contiene un sistema de ficheros *ext3* con este fichero, entonces podemos decir que *le ha tocado el gordo*. Linux evita esta situación impidiendo que los usuarios normales puedan montar dispositivos externos. La única forma segura de conseguir esto es añadir puntos de montaje específicos en el fichero `/etc/fstab` que contengan las opciones *user* o *users*.

Ambas opciones implican la opción `nodev`, que hace que el sistema ignore los ficheros de dispositivo del medio montado. Llegados a este punto, podríamos pensar que el problema está solucionado, pero podríamos tener una entrada en `fstab` como la siguiente:

	/dev/sda1 /media/usbstick auto noauto,user,dev 0 0

que no debería ser creada sin tener en cuenta sus implicaciones.

Los programas con el bit **SUID** tienen el mismo problema. Igual que un usuario normal no puede crear ficheros de dispositivo, tampoco puede crear ficheros con este bit y propietario *root*. Pero al igual que podía introducir un fichero de dispositivo en un medio externo, también puede insertar un pequeño fichero con el **SUID** de *root* que ejecutará una *shell*.

> Las shells más comunes como *bash*, son reacias a ser iniciadas como procesos **SUID**. Sin embargo, un atacante puede traer su propia shell con este código neutralizado.

La forma que tiene Linux de evitar este problema es básicamente el mismo que en el caso de los ficheros de dispositivo, las opciones *user* y *users* implican la opción *nosuid*, que evita la ejecución de programas **SUID** en el sistema de ficheros en cuestión.

> Para ser más precisos, *nosuid* no previene de la ejecución de programas, sino que ignora los bits **SUID** y **SGID**.

> La opción `noexec mount` puede prevenir la ejecución de programas desde el sistema de ficheros en cuestión. Esta opción, también está implicita en las opciones *user* y *users*. Sin embargo, un usuario siempre puede copiar el fichero a su directorio personal y ejecutarlo desde allí.

Como administradores podemos realizar un escaneo del sistema en búsqueda de ficheros, usando el comando `find`.

	# find / -perm /06000 -type f -print

podemos usar `-ls` en lugar `-print`, para ver más información acerca de los ficheros. Podemos crear una lista y luego compararla con otra generada más tarde, y ver las diferencias (con *cron* por ejemplo). Algunas distribuciones realizan esta tarea de forma automática.

También podemos localizar ficheros de dispositivo.

	# find / \( -type b -o -type c \) -ls

Por supuesto, parece tener poco sentido incluir el directorio `/dev` en la búsqueda. Desafortunadamente, no puede ser excluido explícitamente. Todo lo que podemos hacer es decirle a `find` que se detenga cuando encuentre el directorio `/dev`.

	# find / -path /dev -prune -o \( -type b -o -type c \) -ls

o, también podemos crear una *tubería* hacia un `grep` para excluir las entradas que comiencen por `/dev`:

	# find / \( -type b -o -type c \) -print | grep -v ^/dev

Podemos usar herramientas, como Tripwire o AIDE, para realizar auditorías de nuestro sistema.

## Usuarios y ficheros.

En ocasiones puede resultar útil saber quién está conectado a nuestro equipo. El comando más simple que podemos utilizar es `who`:

	$ who

La primera columna muestra el nombre de usuario, y la segunda, la terminal a través de la cual se ha iniciado sesión. «:0» se refierea al número de pantalla en el servidor X; otros nombres, como «pts/0» hacen referencia a un fichero de dispositivo bajo `/dev`. El resto de la línea muestra la fecha y la hora de inicio de la sesión. Y a continuación, el extremo remoto de la sesión.

> `who` soporta algunas opciones: `-H` muestra una cabecera en las columnas; `-b` imprime la fecha y hora del último reinicio, y `-r` el nivel de ejecución actual. `-a` muestra toda la información al mismo tiempo. Con `-m` muestra sólo el usuario conectado al mismo terminal de ejecución del comando.

> «$ who am i» no confundir con el comando «whoami».

El comando `w` es una aproximación de` who`:

	# w

En la primera línea se muestra la hora actual y el tiempo «uptime», o sea, el tiempo transcurrido desde el último inicio, el número de usuarios activos y la carga del sistema. A continuación, hay una línea por cada usuario con sesión iniciada, similar a `who`. La columna **LOGIN**@ muestra la hora de conexión. **IDLE**, es el tiempo que el usuario ha estado inactivo. **JCPU** es el tiempo total de CPU utilizado por todos los procesos en esta sesión, y **PCPU** es el tiempo de CPU usado por el proceso actual en ejecución. **WHAT** es la línea de comandos actual.

Mientras que los comandos `who` y `w` están relacionados con el tiempo presente, el comando `last` puede mostrar información del pasado.
	
	# last

En su salida también se muestra el usuario, la terminal, el extremo remoto, la fecha y hora de inicio y la duración para cada sesión. Si la hora de fin es «crash», significa que el ordenador no ha tenido la oportunidad de registrar el fin de sesión. Las líneas «reboot» indican un reinicio del sistema, y en este caso, la tercera columna muestra el nombre del kernel utilizado en dicho reinicio.

> `last` examina el fichero `/var/log/wtmp`, a no ser que utilicemos la opción `-f` para especificar otro fichero.

En ciertas ocasiones, nos puede interesar conocer los procesos o usuarios que están haciendo uso de un determinado recurso. Para consultar esta información, debemos utilizar el comando `fuser`.

	$ cd /home/user
	$ (echo hola; sleep 600) > test.txt

En una ventana diferente

	$ fuser /home/user /home/user/test.txt
	...

La salida de `fuser` es por defecto, una lista de PID’s seguida por una letra que indica el modo de uso del recurso.

> La opción `-v` muestra más información. Y la opción `-m` muestra cualquier fichero o directorio situado en el mismo sistema de ficheros que el indicado como argumento. Es útil para poder desmontar un sistema de ficheros.

Para poder solucionar el problema «Quiero desmontar este sistema de ficheros, y Linux no me deja» podemos hacer uso de la opción `-k` para enviar una señal a los proceso implicados:

	# fuser -mk TERM /home; sleep 10; fuser -mk /home

La opción `-w` hace que se aplique sólo a los procesos que escriben en el recurso, y con `-i` nos preguntará antes de finalizar cada uno de los procesos afectados.

También podemos identificar puertos **TCP** y **UDP**:

	# fuser -n tcp ssh

### Fortalecer contraseñas

La gestión de las contraseñas y sus ficheros se han tratado en temas anteriores, pero existen cuestiones adicionales que debemos conocer para una correcta gestión de las cuentas y mantener nuestro sistema seguro.

Aunque en la actualidad todas las contraseñas de los usuarios locales de almacenan en el fichero `/etc/shadow`, en el cual solo tiene permiso de escritura el usuario *root*, podría ocurrir que tengamos que realizar un volcado de alguna contraseña almacenada en el fichero `/etc/passwd`, que tiene permiso de lectura cualquier usuario del sistema. Para ejecutar ese volcado hacia `shadow` haremos uso del comando `pwconv`.

En ocasiones tendremos que atender solicitudes de usuarios que no son capaces de acceder correctamente al sistema. Si el usuario es de reciente creación, o es su primer acceso al sistema, debemos comprobar el estado de sus datos de acceso. En ocasiones, los administradores dan de alta nuevos usuarios con el comando `useradd`, pero se olvidan de establecer una contraseña con el comando `passwd`. Podemos hacer uso de los comandos `grep` o `getent` para comprobar sus registros en los ficheros `/etc/passwd` y `/etc/shadow`.

	$ sudo getnet passwd linus
	linus:x:1005:1005::/home/linus:/bin/bash

	$ sudo getent shadow linus
	linus:!:17806:0:99999:7:::

La exclamación (!) que se observa en el fichero `shadow` indica que no se ha creado ninguna contraseña para el usuario *linus*.

También puede ocurrir que la cuenta del usuario se haya bloqueado, algo que podemos comprobar de la siguiente manera:

	$ sudo passwd -S linus
	linus L 01/02/2019 0 99999 7 -1

	$ sudo getent shadow linus
	linus:!$6$[...]0:17898:0:99999:7:::

La letra L que aparece en la salida del comando `passwd` ya nos indica que la cuenta se encuentra bloqueada, aunque esta información también se muestra para aquellas que carecen de contraseña. Para comprobarlo debemos hacer uso del comando `getent`. Para desbloquear la cuenta de un usuario debemos ejecutar el comando `usermod -U` o `passwd -u`.

La cuenta también pudo haber caducado. Algo típico para usuarios temporales que pertenecen a entidades externas que realizan tareas puntuales. Para comprobar esta situación debemos ejecutar el comando `chage`.

	$ sudo chage -l linus
	[...]
	Account expires		: Jan 01, 2019
	[...]

Si ha ocurrido un error en el establecimiento de la fecha de caducidad, y deseamos recuperar el acceso para este usuario, ejecutamos el comando `chage -E`.

## Límites de recursos

Cuando un sistema es utilizado por varios usuarios, debemos prevenir que un sólo usuario acapare los recursos del sistema. Y aunque sólo haya un usuario conectado, también es necesario tratar de evitar esta circunstancia. Linux nos permite establecer límites superiores para varios recursos que los usuarios pueden consumir. Con el comando `ulimit` podemos obtener una visión global de los recursos en cuestión (la opción `-a` muestra todos los límites de recursos y su configuración actual):

	$ ulimit
	$ ulimit -a

Para cada uno de estos recursos hay un «límite soft» y un «límite hard». La diferencia está en que para elevar el límite *hard* hay que tener privilegios de administrador (aunque siempre podremos bajarlo).

Los usuarios pueden establecer el valor para límite *soft*, pero solo hasta el valor del límite *hard*. En la shell, los límites *hard* son modificados usando `ulimit -H` y los límites *soft* con `-S`, si no se especifica alguno de ellos, ambos son configurados al mismo valor.

Cuando no se indica ningún valor para un recurso, se muestra el valor actual.

	$ ulimit -n 512	      # Permite abrir 512 ficheros por proceso
	$ ulimit -n

Los límites de recursos son atributos del proceso (al igual que el directorio de trabajo actual, o el entorno del proceso) y son heredados por sus procesos hijo. Veamos, a continuación algunos de esos límites:

* **core file size** - Es utilizado por los programadores - «core dumps» son creados cuando un programa es parado de forma inesperada por una señal y permite investigar la causa. El valor por defecto es 0, que significa que no serán creados *core dumps*, si los necesitamos debemos usar un comando como el siguiente:

	$ ulimit -c unlimited

* **data seg size** - Cuando se alcanza el límite *soft* de este recurso, un proceso no será capaz de obtener más memoria del sistema operativo. Debería liberar parte de la memoria que ya no está utilizando.

* **scheduling priority** - Este es el límite más bajo para el valor *nice* de un proceso. Debido a que `ulimit` no acepta parámetros negativos, el valor que se usará cuando escribamos `ulimit -e n` es igual a *20-n*. Esta forma de indicar valores de *nice* es un poco «retorcida», pero permite a los usuarios normales indicar un valor de *nice* inferior a 0:
```
	# ulimit -e 30
	# /bin/su - debian
	$ nice --5 /bin/sleep 10
	$ nice --15 /bin/sleep 10
```
* **file size** - Cuando un proceso intenta incrementar el tamaño de un fichero a un valor por encima de este límite, se le envía una señal **SIGXFSZ**. Esta señal normalmente finaliza el proceso, pero si el proceso captura esta señal, la operación de escritura finaliza con un código de error, y el proceso puede continuar realizando otras tareas.

* **max memory size** - Este valor podría parecer tentador. Pero a partir del kernel 2.4.30 es ignorado.

* **open files** - El número de descriptores de fichero que un proceso puede mantener abiertos al mismo tiempo. Cuando se alcanza el límite y un proceso intenta abrir otro fichero, ese intento falla.

* **stack size** - El tamaño de la pila de procesos en Kbits.

> Además, a partir de Linux 2.6.23, también indica el tamaño del espacio usado para los argumentos de la línea de comandos y variables de entorno. Linux asigna inicialmente un cuarto del límite para cada proceso -por lo menos 32 páginas de memoria, que en arquitecturas x86, dónde Linux usa páginas de 4Kb, da un total de 128Kb. Con el valor por defecto para el límite de este recurso, la cantidad normal es ahora de 2Mb.

* **cpu time** - Cuando un proceso alcanza el valor *soft* del límite de recurso, se le envía una señal **SIGXCPU**. Esta señal es repetida una vez por segundo hasta se alcanza el límite *hard*, que es cuando se le envía una señal **SIGKILL**.

* **max user processes** - Este límite difiere de los otros en que es aplicado por usuario, no por proceso o fichero. Especifica el número máximo de procesos que un usuario puede tener en ejecución. Si se alcanza, cualquier intento de crear más procesos, fallará.

Todos los límites de recursos pueden ser configurados interactivamente usando el comando shell `ulimit`. De este modo, estos límites se aplican a los comandos introducidos en la shell, y a todos los procesos iniciados por ésta (y a sus hijos).

Un clásico del género es el «fork bomb»:

	:(){ :|: & };:

(o escrito de forma más legible)

	f() {
		f | f &
	}
	f

Esta línea de comando lanza un proceso que crea dos copias de sí mismo en segundo plano. Cada uno de los hijos, crea a su vez dos nuevos procesos, y así de forma infinita. El problema es que este tipo de programas, si se deja ejecutar de forma incontrolada, ocupan toda la tabla de procesos del sistema, de tal forma que el administrador no puede lanzar nuevos procesos para controlar la situación (incluso, aunque quedara algún hueco libre en la tabla de procesos, enseguida sería cubierto por algún proceso hijo del «fork bomb»)

Podemos evitar este problema, limitando el número máximo de procesos que puede lanzar un usuario. Por ejemplo:

	ulimit -u 128

(en contraste con el valor por defecto 8191). 

Si a pesar de todo, nuestro sistema se ve afectado por un «fork bomb» mientras tenemos una shell de *root* en ejecución, podemos intentar finalizar con los procesos. Debemos ser cautelosos, porque si usamos `kill`, cado proceso que finalizamos será reemplazado por una nueva copia. En su lugar, intentaremos pararlos. Esto nos previene del efecto multiplicador:

	# killall -STOP nombre_procesos
	# killall -KILL nombre_procesos

> Cuando nuestro amigo usuario de Windows se ría de lo sencillo que es bloquear un equipo en Linux, con un sólo comando. Debemos responderle que pruebe el siguiente comando en su sistema:<br>

	%0|%0

> Son cinco caracteres, contra trece !!.

Para establecer límites de recursos a los usuarios, hay que configurarlos antes de que los usuarios tomen el control, y de tal forma que no puedan cambiar eses límites (por lo tanto el fichero `.bash_profile` no es una buena opción). Una alternativa podría ser `/etc/profile`, pero tendríamos que asumir que todos nuestros usuarios usen *bash* como shell de login. La solución elegante podría ser usar un módulo PAM llamado `pam_limits`. Este módulo es parte del proceso de inicio de sesión y permite configurar límites de recurso de forma detallada para usuarios y grupos en el fichero `/etc/security/limits.conf`.

Supervisa el número de procesos creados con el siguiente comando:<br>
	`# watch 'ps auxw | grep forkbomb.sh | grep -v “\(watch\|grep\)”'`
<br>Finalmente, mata el «fork bomb» como se ha descrito anteriormente, con el comando `killall`.
4. Ejecuta el «fork bomb» sin el comando `sleep`. Para proteger al sistema de un reinicio casi seguro, podemos ejecutar previamente el siguiente comando:<br>
	`# (sleep 15 & exec /usr/bin/killall -STOP forkbomb.sh) &`
<br>además de establecer un límite inferior para el número máximo de procesos.
5. Introduce un límite de 1000 en el número de ficheros abiertos para el usuario sin privilegios de tu sistema, haciendo uso del fichero `/etc/profile`. Hazlo también para un grupo determinado de usuarios.

## Privilegios de administrador con sudo

El comando `su` permite a los usuarios ordinarios asumir la identidad de *root* (si conocen su contraseña). El sistema guarda un registro de que el comando `su` ha sido usado. Si esto se hace con la idea de que este usuario realice la copia semanal, no es una gran idea. Sería mejor permitir a ciertos usuarios ejecutar ciertos comandos como *root*. Esto es exactamente lo que hace `sudo`. Usando este comando, la cuenta de *root* no se utiliza directamente. En su lugar, los usuarios pueden usar comandos del siguiente modo:	

	$ sudo passwd sue

para ejecutar comandos como *root*. `sudo` solicita la contraseña del usuario (no el de *root*), y una respuesta correcta es recordada durante un tiempo.

> Algunas distribuciones -Ubuntu, Debian GNU/Linux...- extienden los privilegios de `sudo` al primer usuario creado durante la instalación y deshabilita los logins como *root*. De esta forma se previene a los usuarios trabajar como *root* todo el tiempo.

> Por defecto la contraseña es recordada durante 15 minutos. El comando `sudo -k` (que no requiere contraseña) reinicia la contraseña a recordar, por lo que la próxima vez que usemos `sudo`, nos solicitará de nuevo la contraseña.

Todos los comandos ejecutados a través de `sudo` se registran usando el mecanismo *syslog*. El fichero `/etc/sudoers` controla quien puede usar `sudo` y para qué. Sólo se verán algunas opciones sencillas.

> Podemos usar el comando `visudo` para editar el fichero `/etc/sudoers`. Tiene ciertas ventajas, como que realiza un chequeo de la sintaxis, evitando errores que puedan comprometer la seguridad, y evitar que sea editado por más de un usuario.

El núcleo del fichero `/etc/sudoers` está formado por líneas que contienen reglas como:

	debian ALL = /usr/bin/cancel [a-z]*

Esta regla permite al usuario *debian* ejecutar el comando `cancel`. 

	$ sudo cancel lp-123

La segunda parte `[a-z]*` obliga a que el parámetro empiece por una letra, evitando de este modo el uso de opciones, que comiencen por el carácter «-».

> La parte ALL significa que esta regla se aplica a todos los hosts de la red. Por ejemplo, si al usuario *debian* sólo le estuviera permitido el uso de `cancel` en el host *red*, la regla sería:<br>
	debian red = /usr/bin/cancel [a-z]*

> El hecho de que el fichero `sudoers` pueda contener reglas para todos los hosts en una red, no significa que el fichero sea preciso mantenerlo en un sólo host. Como administrador, eres responsable de que esté accesible en cualquier host que se pueda necesitar.

Si especificamos tan solo el comando (sin parámetros), se permite el uso de opciones:

	debian ALL = /usr/bin/cancel

Si no queremos que se introduzca ningún parámetro, debemos indicarlo con una cadena vacía:

	debian ALL = /usr/bin/passwd ""

**¿Cuál es el problema de esta regla?**

> Hay que tener mucho cuidado con el uso de `vi`, ya que permite a los usuarios ejecutar comandos de la shell. Estes comandos también serán ejecutados con permisos de *root*. Para prevenir esto, podemos insertar la siguiente regla.<br>
	`debian ALL = NOEXEC: /usr/bin/vi`
> <br>Esta linea impide que `vi` ejecute procesos hijo.

También podemos especificar varios comandos:

	debian ALL=/usr/bin/cancel [a-z]*,/usr/bin/cups enable [a-z]*

permite el uso de `sudo cancel` y `sudo cups enable`, ambos con al menos un parámetro.

Es más práctico unir los comandos en grupos. Podemos usar alias de comandos:

```
Cmnd_Alias PRINTING = /usr/bin/cancel [a-z]*, \
				/usr/bin/cups enable [a-z]*, \
				/usr/bin/cups disable [a-z]*
debian ALL = PRINTING
```

> Si indicamos un nombre de directorio, permite el uso de los ejecutables situados en dicho directorio, pero no de los subdirectorios <br>
	`debian ALL = /usr/bin/`

También se pueden mezclar grupos con comandos individuales:

	debian ALL = PRINTING, /usr/sbin/accept [a-z]*

Todos los comandos:	

	debian ALL = ALL

El carácter «!» puede usarse para excluir comandos que se podrían haber incluido en la configuración:

	debian ALL = /usr/bin/passwd [a-z]*, !/usr/bin/passwd root

Hay que tener cuidado y no ahorrar en el uso de «!». Por ejemplo, en la siguiente regla:

	debian ALL = /bin/, !/bin/sh, !/bin/bash

el usuario puede ejecutar comandos en `/bin`, pero no puede ejecutar las shells, nada le impide copiar la `sh` con:

	sudo cp /bin/sh /bin/mish
	sudo mish

Todas estas barreras son finalmente eliminadas con una regla como la siguiente:

	debian ALL = NOPASSWD: ALL

que permite al usuario *debian* ejecutar todos los comandos como *root* sin preguntarle la contraseña.

Por defecto, `sudo` ejecuta todos los comandos como *root*, pero podemos indicar otro usuario:

	debian ALL = (mysql) /usr/bin/mysqladmin

*debian* puede ejecutar el comando `mysqladmin` como si fuera el usuario *mysql*. Por ejemplo:

	$ sudo -u mysql mysqladmin flush-privileges

No solo existen alias para los comandos, también los podemos usar con los nombres de host y usuarios:

	User_Alias WEBMASTERS = debian, tux1
	Host_Alias WEBHOSTS = www1, www2
	....
	WEBMASTERS WEBHOSTS = NOPASSWD: /usr/bin/apache2ctl graceful

También podemos indicar direcciones IP, o direcciones de red.

	Host_Alias FILESERVERS = red, 192.168.17.1
	Host_Alias DEVNET = 192.168.17.0/24

Podemos indicar el nombre de un grupo, con el carácter «%» antes de su nombre

	%operadores ALL = /usr/local/bin/do-backup

Un truco extra es `sudo -e` (o `sudoedit`). El comando:

	$ sudo -e /etc/hosts

es equivalente a:

	$ sudo cp /etc/hosts /tmp/hosts.$$
	$ sudo chown $USER /tmp/hosts.$$
	$ vi /tmp/hosts.$$
	$ if ! sudo cmp --quiet /etc/hosts /tmp/hosts.$$ then
	       sudo cp /tmp/hosts.$$ /etc/hosts && rm -f /tmp/hosts.$$
	fi

Esto significa que el comando crea una copia temporal del fichero a editar y le envía una copia al editor predeterminado. A continuación compara los dos ficheros en búsqueda de cambios, si es afirmativo, se reemplaza el fichero original. Este método tiene algunas ventajas, como que el editor se ejecuta con los privilegios del usuario, sino que impide el lanzamiento de una shell desde el editor.

## Seguridad básica de la red

Podemos ver nuestro equipo como una casa que proteger. Si en cada una de las habitaciones tenemos una puerta con acceso al exterior, está claro que todas deben ser protegidas, o en su caso evaluar las medidas de protección de cada una de ellas. En términos de seguridad, debemos mantener las cerraduras en buen estado y con revisiones periódicas para su mantenimiento.

Cómo saber si nuestro equipo es una amenaza quizás para otros equipos de la red al estar siendo usado por una red de ciberdelincuentes, etc. Lo primero que debemos realizar es comprobar que nuestro equipo no está ofreciendo servicios que desconocemos, haciendo uso del comando:

```
# netstat --tcp --listening
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State      
tcp        0      0 0.0.0.0:sunrpc          0.0.0.0:*               LISTEN     
tcp        0      0 0.0.0.0:ssh             0.0.0.0:*               LISTEN     
tcp6       0      0 [::]:sunrpc             [::]:*                  LISTEN     
tcp6       0      0 [::]:ssh                [::]:*                  LISTEN
```

que nos presentará una lista de los puertos abiertos. 

> Como este comando se encuentra en estado *deprecated*, en cualquier nueva versión de nuestra distribución puede no estar disponible. La utilidad que lo puede reemplazar es el comando `ss`.

La utilidad Network Mapper (`nmap`) se suele utilizar para pruebas de seguridad, y escaneo de puertos. En este caso nos resultará de utilidad para comprobar el estado de los puertos TCP IPv4 están activos en nuestro equipo:

```
$ nmap -sT 127.0.0.1
Starting Nmap 7.70 ( https://nmap.org ) at 2021-02-07 10:52 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00037s latency).
Not shown: 998 closed ports
PORT    STATE SERVICE
22/tcp  open  ssh
111/tcp open  rpcbind

Nmap done: 1 IP address (1 host up) scanned in 0.11 seconds
```

De la información anterior sabemos que el puerto 22 está abierto y acepta conexiones TCP para el servicio OpenSSH (`ssh`).

Si tenemos acceso a un equipo externo a nuestra red local, podemos usar `nmap` para comprobar cómo se verá nuestro ordenador desde Internet. Debemos realizar un escaneo tanto de los protocolos TCP como UDP.

Si en alguna de las salidas de estes comandos vemos algo extraño podemos actuar de la siguiente forma:

Desactivando el servicio. Comentando la línea indicada del fichero `/etc/inetd.conf`. Si estamos usando *xinetd*, añadir la línea siguiente a la sección de configuración de los servicios que no queramos ejecutar:

	disable = yes

Los servicios que no son tan obvios de suprimir, podemos limitar el ámbito de conexión. Podemos indicar, por ejemplo, sólo la posibilidad de conexión por parte del *localhost*. Por ejemplo, podemos hacer que el envío de mensajes se realice a través del servicio SMTP del host, limitando el uso a la dirección 127.0.0.1, puerto 25. Debemos tener especial cuidado en la autenticación basada en la dirección IP, porque un atacante puede falsear su dirección de origen.

## Explorar sockets con ss y systemd.socket

El comando `ss` llega para reemplazar al obsoleto `netstat`. La opción `ltu` muestra los sockets a la escucha (-l) de tipo TCP (-t) y UDP (-u) tanto para IPv4 como IPv6.

```
$ ss -ltu
Netid            State             Recv-Q            Send-Q                         Local Address:Port                           Peer Address:Port            
udp              UNCONN            0                 0                                  127.0.0.1:323                                 0.0.0.0:*               
udp              UNCONN            0                 0                                    0.0.0.0:sunrpc                              0.0.0.0:*               
udp              UNCONN            0                 0                                      [::1]:323                                    [::]:*               
udp              UNCONN            0                 0                                       [::]:sunrpc                                 [::]:*               
tcp              LISTEN            0                 128                                  0.0.0.0:sunrpc                              0.0.0.0:*               
tcp              LISTEN            0                 128                                  0.0.0.0:ssh                                 0.0.0.0:*               
tcp              LISTEN            0                 128                                     [::]:sunrpc                                 [::]:*               
tcp              LISTEN            0                 128                                     [::]:ssh                                    [::]:*               
```

Un método novedoso para la creación de sockets de red (así como de otro tipo) es a través de *systemd*. Utiliza un método de activación que hace posible el inicio del socket así como el servicio asociado, lo que hace el proceso mucho más rápido.

Para poder realizar una auditoría de los sockets de red, disponemos de varios métodos pero un buen principio es a través del comando `systemctl`. El siguiente comando nos muestra un resultado de fácil lectura:

```
$ systemctl list-sockets --all --no-pager --full
LISTEN                            UNIT                            ACTIVATES
/run/dbus/system_bus_socket       dbus.socket                     dbus.service
/run/initctl                      systemd-initctl.socket          systemd-initctl.service
/run/rpcbind.sock                 rpcbind.socket                  rpcbind.service
/run/systemd/coredump             systemd-coredump.socket        
/run/systemd/journal/dev-log      systemd-journald-dev-log.socket systemd-journald.service
/run/systemd/journal/socket       systemd-journald.socket         systemd-journald.service
/run/systemd/journal/stdout       systemd-journald.socket         systemd-journald.service
/run/systemd/journal/syslog       syslog.socket                   rsyslog.service
/run/udev/control                 systemd-udevd-control.socket    systemd-udevd.service
/var/run/.heim_org.h5l.kcm-socket sssd-kcm.socket                 sssd-kcm.service
0.0.0.0:111                       rpcbind.socket                  rpcbind.service
0.0.0.0:111                       rpcbind.socket                  rpcbind.service
[::]:111                          rpcbind.socket                  rpcbind.service
[::]:111                          rpcbind.socket                  rpcbind.service
audit 1                           systemd-journald-audit.socket   systemd-journald.service
kobject-uevent 1                  systemd-udevd-kernel.socket     systemd-udevd.service

16 sockets listed.
```

En la salida anterior también se muestran sockets que no son de red, los cuales se pueden identificar a través de algunos servicios conocidos para nosotros.

Para localizar los potenciales ficheros de configuración de los sockets de red, vamos a usar el comando `systemctl` con otras opciones:

```
$ sudo systemctl list-unit-files --type=socket --no-pager
UNIT FILE                       STATE   
dbus.socket                     static  
rpcbind.socket                  enabled 
sshd.socket                     disabled
sssd-autofs.socket              disabled
sssd-kcm.socket                 enabled 
sssd-nss.socket                 disabled
sssd-pac.socket                 disabled
sssd-pam-priv.socket            disabled
sssd-pam.socket                 disabled
sssd-ssh.socket                 disabled
sssd-sudo.socket                disabled
syslog.socket                   static  
systemd-coredump.socket         static  
systemd-initctl.socket          static  
systemd-journald-audit.socket   static  
systemd-journald-dev-log.socket static  
systemd-journald.socket         static  
systemd-rfkill.socket           static  
systemd-udevd-control.socket    static  
systemd-udevd-kernel.socket     static  

20 unit files listed.
```

La salida anterior muestra si el socket de red se encuentra gestionado por *systemd*. Un valor `disabled` solo significa que *systemd* no se encarga de ese socket en particular; no quiere decir que está deshabilitado. Para los sockets habilitados, es preciso realizar un examen más completo a través de la revisión del contenido de los ficheros de las *units*.

```
$ systemctl cat rpcbind.socket
# /usr/lib/systemd/system/rpcbind.socket
[Unit]
Description=RPCbind Server Activation Socket
DefaultDependencies=no
Wants=rpcbind.target
Before=rpcbind.target

[Socket]
ListenStream=/run/rpcbind.sock

# RPC netconfig can't handle ipv6/ipv4 dual sockets
BindIPv6Only=ipv6-only
ListenStream=0.0.0.0:111
ListenDatagram=0.0.0.0:111
ListenStream=[::]:111
ListenDatagram=[::]:111

[Install]
WantedBy=sockets.target
```

## Auditoría de ficheros abiertos con lsof y fuser

Como sabemos, Linux trata las conexiones de red y sockets como ficheros, por lo tanto aparecerá en los resultados del comando `lsof`. Como la salida es muy extensa, será necesario aplicar algún filtro. Por ejemplo, si estamos interesados en auditar los sockets y conexiones UDP, debemos usar la opción `-iUDP`.

	$ sudo lsof -iUDP
	COMMAND   PID   USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
	systemd     1   root   82u  IPv4  17591      0t0  UDP *:sunrpc 
	systemd     1   root   84u  IPv6  17593      0t0  UDP *:sunrpc 
	rpcbind   586    rpc    5u  IPv4  17591      0t0  UDP *:sunrpc 
	rpcbind   586    rpc    7u  IPv6  17593      0t0  UDP *:sunrpc 
	NetworkMa 651   root   22u  IPv4  21984      0t0  UDP localhost.localdomain:bootpc->_gateway:bootps 
	chronyd   660 chrony    6u  IPv4  20413      0t0  UDP localhost:323 
	chronyd   660 chrony    7u  IPv6  20414      0t0  UDP localhost:323

En el siguiente ejemplo muestra los sockets de red en estado de escucha de paquetes TCP:

	$ sudo lsof -iTCP -sTCP:LISTEN
	COMMAND PID USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
	systemd   1 root   81u  IPv4  17590      0t0  TCP *:sunrpc (LISTEN)
	systemd   1 root   83u  IPv6  17592      0t0  TCP *:sunrpc (LISTEN)
	rpcbind 586  rpc    4u  IPv4  17590      0t0  TCP *:sunrpc (LISTEN)
	rpcbind 586  rpc    6u  IPv6  17592      0t0  TCP *:sunrpc (LISTEN)
	sshd    678 root    4u  IPv4  21294      0t0  TCP *:ssh (LISTEN)
	sshd    678 root    6u  IPv6  21303      0t0  TCP *:ssh (LISTEN)

También podemos consultar información relativa al tráfico de un puerto determinado y sus conexiones establecidas:

	$ sudo lsof -i tcp:22
	COMMAND  PID    USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
	sshd     678    root    4u  IPv4  21294      0t0  TCP *:ssh (LISTEN)
	sshd     678    root    6u  IPv6  21303      0t0  TCP *:ssh (LISTEN)
	sshd    2120    root    5u  IPv4  28419      0t0  TCP localhost.localdomain:ssh->_gateway:38216 (ESTABLISHED)
	sshd    2135 vagrant    5u  IPv4  28419      0t0  TCP localhost.localdomain:ssh->_gateway:38216 (ESTABLISHED)

Otra utilidad que permite realizar auditorías es el comando `fuser`. Nos mostrará el programa o PID que está haciendo uso del protocolo y puerto. El siguiente ejemplo muestra los PID's que están usando TCP y un puerto concreto:

```
# fuser -vn tcp 22
                     USER        PID ACCESS COMMAND
22/tcp:              root        678 F.... sshd
                     root       2120 F.... sshd
                     vagrant    2135 F.... sshd
```

## Deshabilitando servicios

Una vez realizado el análisis de la actividad de la red, si es necesario deshabilitar alguno de los servicios, tendremos que comprobar en primer lugar el método de nuestro sistema para gestionar el inicio.

Para sistemas basados en *systemd*, tan solo tendremos que ejecutar unas pocas tareas.

	systemctl stop SERVICIO
	systemctl status SERVICIO

Si además de parar el servicio en este momento, deseamos que no se inicie en el siguiente arranque del sistema, debemos ejecutar el siguiente comando:

	systemctl disable SERVICIO

Si estamos ante un sistema basado en SysVinit, debemos parar el servicio y luego comprobar su nuevo estado:

	service SERVICIO stop
	service SERVICIO status

En el caso de un sistema basado en Red Hat, ejecutamos el siguiente comando con privilegios de administrador:

	chkconfig SERVICIO off
	chkconfig --list SERVICIO

Para deshabilitar un servicio de red en sistemas basados en Debian, ejecutamos el siguiente comando:

	update-rc.d -f SERVICIO remove

La opción `-f` solo es necesaria si existe un script para el servicio en el directorio `/etc/init.d`.

## Comandos vistos en el tema

| Comando | Descripción |
| --- | --- |
| sudo | Permite a los usuarios normales ejecutar ciertos comandos con privilegios de administrador |
| sudoedit | Permite a los usuarios normales editar ficheros (equivalente a `sudo -e`) |
| ulimit | Configura límites de recurso para los procesos |
| visudo | Permite edición exclusiva de `/etc/sudoers`, incluyendo un chequeo de la sintaxis |
