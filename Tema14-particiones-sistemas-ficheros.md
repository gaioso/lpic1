# Tema 14 - Particiones y sistemas de ficheros

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Particionado básico](#particionado-básico)
- [El método tradicional (MBR)](#el-método-tradicional-mbr)
- [El método actual (GPT)](#el-método-actual-gpt)
- [Particionado de discos. Fundamentos](#particionado-de-discos-fundamentos)
- [Crear una nueva partición. Comando fdisk.](#crear-una-nueva-partición-comando-fdisk)
    - [Comando gdisk](#comando-gdisk)
- [Crear particiones con `parted`](#crear-particiones-con-parted)
- [Herramientas gráficas](#herramientas-gráficas)
- [Los sistemas de ficheros](#los-sistemas-de-ficheros)
- [Crear sistemas de ficheros](#crear-sistemas-de-ficheros)
- [Crear un sistema de ficheros `tmpfs`](#crear-un-sistema-de-ficheros-tmpfs)
- [Espacio de intercambio (swap)](#espacio-de-intercambio-swap)
- [Reparando sistemas de ficheros](#reparando-sistemas-de-ficheros)
- [Montaje del sistema de ficheros](#montaje-del-sistema-de-ficheros)
- [Estadísticas de los sistemas de ficheros](#estadísticas-de-los-sistemas-de-ficheros)
- [Etiquetas y UUID's](#etiquetas-y-uuids)
- [El comando `dd`](#el-comando-dd)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos

* Ser capaces de particionar discos duros y generar sistemas de ficheros
* Conocer las herramientas de mantenimiento de sistemas de ficheros
* Montar sistemas de ficheros locales
* Uso del comando `dd`

## Particionado básico

Con comandos como fdisk, los discos duros pueden ser divididos en segmentos más pequeños, llamados particiones. No es obligatorio tener varias particiones, pero desde luego, es una muy buena idea.

* Se pueden separar los datos personales, de los datos propios del sistema operativo. Protegiéndolos de actualizaciones o fallos del sistema.
* Podemos adaptar la estructura al contenido. Con un tamaño de bloque de 4K, si los ficheros son de 500 bytes, sólo ocuparía 1/8 del bloque, el resto es «basura». Por ejemplo, un servidor de correo trabaja con ficheros pequeños. Por otro lado, hay servidores de bases de datos que trabajan con discos en modo plano, que ellos mismos gestionan.
* Aquellos procesos que puedan estar mal configurados, o usuarios incautos, pueden provocar una ocupación de todo el espacio disponible. Por eso es importante «enjaular» los datos del usuario (trabajos de impresión, correo no leído, etc.) para evitar problemas en el sistema.

Actualmente coexisten dos sistemas para particionar un disco duro. El método tradicional, que se remonta a los años 80, y el más reciente que intenta solucionar las limitaciones de aquel.

## El método tradicional (MBR)

Este método almacena la información de las particiones dentro del «master boot record» (MBR), el primer sector (número 0) de un disco duro. El espacio disponible (64 bytes que comienzan en el offset 446) es suficiente para cuatro particiones primarias. Si necesitamos más, debemos convertir una de esas primarias en *extendida*. Y que nos sirve como contenedor de *particiones lógicas*. La información de la partición lógica, ya no reside en la tabla de particiones, sino al principio de la partición extendida.
La entrada de una partición almacena el número del sector de commienzo, así como el tamaño en sectores. Debido a que estes números se representan con 32 bits, y el tamaño de un sector suele ser de 512 bytes, resulta que el tamaño máximo de la partición es de 2 TiB.

	4,294,967,295 (232−1) sectores × 512 (29) bytes por sector

> Hoy en día nos podemos encontrar cos discos de más de 2 TiB. Para poder utilizarlos con MBR, podemos adquirir discos etiquetados como 4Kn, ya que tienen sectores de 4096 bytes, en lugar de 512. Aunque también existen discos tipo «512e», que tiene sectores de 4KiB internos, pero son accesibles como si fuesen de 512 bytes.

## El método actual (GPT)

A finales de los años 90, Intel desarrolló un nuevo método de particionado para solucionar las limitaciones de MBR, que denominó «GUID Partition Table», o GPT.

> GPT ha sido desarrollado junto con UEFI, y hoy es una parte de dicha especificación. Utiliza 64 bit para los números de sector, lo que permite un tamaño máximo de disco de 8 ZiB (270 bytes)

En GPT, el primer sector del disco permanece reservado como «protective MBR». De esta forma, si un ordenador no es capaz de gestionar GPT, verá el disco como una única partición tipo MBR. Y el segundo sector, contiene la «cabecera GPT» que guarda información sobre el disco. La información de las particiones se guarda a continuación.

## Particionado de discos. Fundamentos

Antes de particionar el disco, debemos diseñar un esquema del diseño final del disco. Los cambios a posteriori serán complejos, o incluso imposibles de realizar, por lo que debemos invertir una parte importante de nuestro esfuerzo en la etapa de diseño de las particiones. Algunas sugerencias:

* Aparte de la partición con la raíz del sistema «/», debemos crear al menos otra partición con el directorio «/home». Dentro de este esquema, debemos considerar la posibilidad de crear enlaces simbólicos para «/usr/local» y «/opt», a (por ejemplo), «/home/usr-local» y «/home/opt». 
* Aunque sea posible instalar un sistema Linux en una partición de 2GiB, es recomendable utilizar un mínimo de 30Gib, sobre todo teniendo en cuenta el precio actual de los discos.
* En servidores, podemos separar los directorios «/tmp», «/var» e incluso «/srv» en particiones diferentes.
* Se debe activar un espacio de «swap» con un tamaño igual al de la memoria RAM, con un valor máximo de 8GiB. En Linux, RAM y «swap» se suman en una sola instancia.
* Si disponemos de varios discos, podemos distribuír el sistema de ficheros entre todos ellos, con la intención de incrementar la velocidad de acceso. La excepción en este punto es el espacio de «swap», que en caso de utilizar varios discos, Linux siempre utilizará el disco menos usado de todos ellos. 

## Crear una nueva partición. Comando fdisk.

Debemos seguir los siguientes pasos:

1. Respaldar los datos.
2. Particionar el disco usando `fdisk` (o similar)
3. Crear un sistema de ficheros (formatear)
4. Incluir el sistema de ficheros en el árbol de directorios, usando `mount` o `/etc/fstab`

Los datos y los sectores *boot* pueden respaldarse usando el comando `dd`:

	# dd if=/dev/sda of=/dev/st0

Al comando `fdisk` se le pasa como parámetro, el nombre del disco a gestionar (`/dev/sda`, por ejemplo).

Algunos comandos importantes son:

* **d** elimina la partición seleccionada, indicada con un número.
* **m** muestra un resumen de los comandos
* **n** crea una nueva partición y solicita al usuario información relevante.
* **p** muestra la actual tabla de particiones.
* **q** finaliza el programa sin guardar los cambios en disco.
* **w** escribe la nueva tabla de particiones en disco y finaliza el programa

También reconoce algunas opciones de la línea de comandos.

**-l** muestra la tabla de particiones del disco en cuestión, y sale.
**-u** (“units”) muestra los tamaños de las particiones en sectores en lugar de cilindros.

### Comando gdisk

Si estamos trabajando con particiones tipo GPT, debemos usar el programa `gdisk`.

	$ sudo gdisk /dev/sda

El comando `gdisk` es capaz de reconocer el tipo de formato usado en la unidad. Si no hace uso del método GPT, nos ofrecerá la posibilidad de convertirlo.

El uso del comando `gdisk` es muy similar a `fdisk` incluyendo las opciones más frecuentes.

## Crear particiones con `parted`

Otro programa popular para el particionado de discos es el proyecto GNU `parted`. Aunque es similar a `fdisk`, tiene algunas características de interés.

	# parted /dev/sdb

Podemos crear nuevas particiones con `mkpart`.

	(parted) mkpart
	Partition name? []? Test
	File system type? [ext2]? Ext4
	Start? 211MB
	End? 316MB

## Herramientas gráficas

Algunas de las herramientas para la gestión del particionado también están disponibles en un entorno gráfico. La más común de todas ellas es *gparted* (GNOME Partition Editor).

![image](/img/T12_01.png)

La ventana principal de `gparted` muestra todas las unidades conectadas al sistema, así como todas las particiones incluídas en las mismas. Con el botón derecho podemos realizar operaciones de montaje, desmontaje, formateo, borrado o redimensionar la partición.

## Los sistemas de ficheros

El almacenamiento de los datos en un disco requiere de un método de organización para que sea eficaz. Un sistema de ficheros gestiona un mapa que le permite localizar cada uno de los ficheros almacenados.

Si estamos acostumbrados a la gestión de ficheros y directorios por parte de otros sistemas operativos, como puede ser Windows, sabremos que se asigan letras de unidad a cada dispositivo de almacenamiento. Por ejemplo, *C:* para el disco principal, o *:E* para una memoria USB. Por lo tanto, las rutas a ficheros tienen el siguiente formato:

	C:\Users\paul\Documents\memory.docx

Sin embargo, en Linux se usa una estructura de *directorios virtuales*, que contiene rutas a los ficheros que se encuentran en cada uno de los dispositivos conectados al sistema, y que se anidan bajo una estructura única. Esta estructura dispone de un directorio *base*, que se conoce como *root directory* (directorio raíz). Por lo tanto, la ruta a un fichero en Linux será algo similar a lo siguiente:

	/home/paul/documents/memory.odt

La primera diferencia radica en los caracteres que separan los directorios; en Linux «/», y en Windows «\». Por otra parte, la ruta al fichero `memory.odt` no aporta ninguna información referente a la localización del fichero, es decir, no sabemos en que dispositivo se encuentra almacenado. Linux permite el acceso a los dispositivos físicos a través de *puntos de montaje* presentes en el directorio virtual.

Antes de poder asignar una partición a un punto de montaje de nuestro directorio virtual, debemos darle formato a través de un sistema de ficheros (*filesystem*). debemos crear la estructura para poder almacenar ficheros y directorios. Hay diferentes sistemas de ficheros, algunos creados para Linux, como los **ext**, o el **Reiser**. Por supuesto, también soporta los sistemas de ficheros de DOS, Windows, o MacOS, así como *NFS* o *SMB*, que son accesibles a través de una red local.

Cuando creamos un sistema de ficheros para su uso en un sistema Linux, puede elegir de entre cuatro sistemas principales:

* *btrfs* Un sistema de ficheros de reciente publicación, orientado al alto rendimiento, que soporta ficheros de hasta 16 exbibytes (EiB), al igual que el total del sistema de ficheros (16 EiB). Implementa su propio RAID, así como gestión de LVM. Soporta instantáneas para realizar copias de seguridad, tolerancia a fallos y compresión de datos en tiempo real.
* *ecryptfs* Este sistema aplica un protocolo de cifrado a los datos antes de ser guardados en el dispositivo. Solo el sistema operativo que ha guardado los datos será capaz de acceder a ellos.
* *ext3* Conocido también como *ext3fs*, es un descendiente del sistema *ext* original de Linux. Soporta *journaling*, así como un arranque y recuperación muy veloces.
* *ext4* Es la versión actual del sistema de ficheros de Linux. También soporta *journaling*, e implementa mejoras en el rendimiento respecto a *ext3*.
* *reiserFS* Fue creado con anteriorid a *ext3*, y se ha usado en algunos sistemas Linux en el pasado. Sus características ya se encuentran disponibles en *ext3* y *ext4*, por lo que se ha eliminado el soporte en las últimas versiones de Linux.
* *swap* El sistema de ficheros de intercambio permite el uso de memoria virtual haciendo uso del espacio en disco.

## Crear sistemas de ficheros
 
En los sistemas de ficheros de Linux es frecuente disponer de un *superbloque* al principio del sistema de ficheros, que contiene información del sistema de ficheros como un todo -por ejemplo, cuando fue la última vez que se ha montado o desmontado, si fue desmontado de forma limpia, o debido a un fallo del sistema. También almacena información de las listas de inodos, o los bloques ocupados y libres. Es habitual almacenar varias copias del superbloque.

Los sistemas de ficheros se crean con el comando `mkfs`. Es independiente del sistema de ficheros deseado, ya que hace una llamada a la rutina específica. Otra forma, es usar la opción `-t ext2`. O bien, otro nombre para *ext2* y *ext3*, como `mke2fs`.

	# mke2fs -n 2048 /dev/hdb1

Opciones importantes de mke2fs:

* **-b <size>** determina el tamaño del bloque. Valores típicos son 1024, 2048 o 4096
* **-c** comprueba la partición. Busca bloques dañados y los marca.
* **-i <count>** determina la densidad de inodos; un inodo es creado para cada *<count>* bytes de espacio en el disco. El valor mínimo es 1024, el valor por defecto es el actual tamaño de bloque.
* **-m <porcentaje>** el porcentaje de bloques de datos reservados para root (5%)
* **-j** crea un *journal*, y por lo tanto un sistema ext3.

La densidad determina el número de ficheros que se pueden crear, ya que todo fichero requiere un inodo. También hay que tener en cuenta que hay inodos utilizados por dispositivos, por lo que se agotarán antes los inodos que los bloques. Una vez formateado el sistema de ficheros, se pueden modificar algunos parámetros. Usando el comando tune2fs, que debe ser usado con mucho cuidado.

Algunas opciones de este comando:

* **-c <count>** el número máximo de veces que el sistema de ficheros puede ser montado entre dos rutinas de chequeo. El valor por defecto es un número aleatorio cercano a 30. El valor 0, es infinito.
* **-C <count>** configura la cuenta actual. Podemos utilizar esta opción para forzar un chequeo en el siguiente arranque.
* **-e <behaviour>** define el comportamiento del sistema en caso de errores. *continue*, *remount-ro*, o *panic*
* **-i <interval> <unit>** el máximo tiempo entre dos rutinas de chequeo.
* **-l** muestra la información del superbloque
* **-m <porcentaje>** configura el porcentaje de bloques reservados para root. (5%)
* **-L <nombre>** configura el nombre de la partición.

## Crear un sistema de ficheros `tmpfs`

No hay forma especial de crearlos, simplemente usamos la orden mount:

	# mkdir /scratch
	# mount -t tmpfs -o size=1G,mode=0700 tmpfs /scratch

crea un `tmpfs` con un tamaño máximo (dinámico) de 1G bajo el nombre de *scratch*, que sólo puede ser accedido por el propietario del directorio `/scratch`.

A continuación, copiar un fichero de 512MiB al directorio `/scratch`, y comprobar el uso de memoria antes y después (`free`).

## Espacio de intercambio (swap)

Además de las particiones del sistema de ficheros, debemos crear siempre una partición de *swap*. Linux la usará para almacenar parte del contenido de la RAM del sistema. Haciendo de esta forma que la memoria total del sistema sea mucho mayor. Cuando la memoria era más cara y escasa, era recomendable que la partición de intercambio fuera al menos el doble de la RAM. Por ejemplo, con RAM de 32Mb, una swap de 64Mb. Hoy en día esto implicaría áreas de varios gigas, que no tendrá mucho sentido. En un sistema de 4Gb, no es preciso una swap de 8Gb, sino que con uno o dos gigas será suficiente.

Hay una razón para crear un espacio de intercambio del mismo tamaño que la RAM, y es el uso del modo suspendido, en el que el equipo debe almacenar el contenido de la memoria en disco, para luego ser restaurado. Puede haber problemas con los drivers de los dispositivos, además del tiempo de mover varios gigas a disco.

Debemos formatear la partición antes de usarla:

	# mkswap /dev/sdb5
	# swapon /dev/sdb5
	# cat /proc/swaps
	# swapoff /dev/sdb5

Se pueden llegar a tener 32 particiones swap en paralelo. No hay tamaño máximo, pero hoy en día suele ser suficiente con un tamaño no superior a 2Gb. Se podría llegar a distribuir este espacio a través de varios discos, incrementando la velocidad. También es posible utilizar un fichero como espacio de intercambio:

	# dd if=/dev/zero of=swapfile bs0=1M count=256

Ver los pasos restantes para poder utilizar este fichero como espacio de intercambio.

## Reparando sistemas de ficheros

Cuando el equipo se ha apagado de forma brusca, o ha ocurrido un fallo grave en el sistema, puede haber inconsistencias en los ficheros. Las operaciones a disco se realizan en varios pasos, y alguno de ellos puede no completarse. Cuando se produce un error de este tipo debemos utilizar los mismos programas que han podido generar dichos fallos, o debe ser el usuario el que debe restablecer la estabilidad en el sistema de ficheros. Sin embargo, hay errores en la estructura que pueden ser localizados y reparados por programas específicos. 

El sistema *ext2*, permite la reparación de los siguientes fallos:

* Entradas de directorio erróneas.
* Entradas de inodo erróneas.
* Ficheros que no están en ningún directorio.
* Bloques de datos que pertenecen a varios ficheros.

Algunas correciones implican la pérdida de datos. Si el fichero afectado no está en ningún directorio, se traslada al directorio `lost+found`, con el número de inodo como nombre. Cuando se inicia el sistema se comprueba si se ha desmontado correctamente el sistema de ficheros. Para ello hay una marca en el superbloque indicando este hecho. Y si esta bandera está activada se ejecutará un chequeo antes del montaje.

Podemos iniciar una comprobación sin tener que esperar al próximo reinicio. Sin embargo, sólo podrá ser reparado cuando lo desmontemos, porque pueden producirse colisiones entre el kernel y nuestra aplicación de reparación.	El programa `fsck` (cada sistema de ficheros usa su propio programa), lanza a su vez, el programa adecuado para cada sistema de ficheros, de forma similar a `mkfs`.

	# fsck /dev/sdb1

Ejecutando el comando, sin opciones:

	# fsck

realiza una comprobación de los sistemas presentes en el fichero `/etc/fstab` (que tengan un valor diferente a cero en la sexta columna).

También admite la opción `-t` pero con un comportamiento diferente al utilizado en `mkfs`.

	# fsck -t ext3

comprueba todos los sistemas marcados como *ext3* presentes en `/etc/fstab`.

Las opciones más importantes son:

* **-A** (all) comprueba todos los sistemas de ficheros presentes en `/etc/fstab`
* **-R** no comprueba el sistema raíz, cuando usamos `-A`
* **-N** muestra las tareas a realizar, pero sin ejecutarlas.

Justo después de los dispositivos a comprobar, se pueden insertar opciones propias del programa de chequeo. Por ejemplo, las opcións `-a`, `-f`, `-p` y `-v` son soportadas por la mayoría de programas.

El siguiente comando:

	# fsck /dev/sdb1 /dev/sdb2 -pv

comprueba ambas particiones, con una reparación automática de los posibles errores encontrados, y una visualización detallada de las operaciones. 

> Consultar los códigos que devuelve este proceso, su significado y el tratamiento cuando se analizan varios dispositivos.

`e2fsck` es el programa de comprobación de consistencias para el sistema de ficheros *ext* (para que funcione, debe existir el fichero `fsck.ext2`). Algunas opciones:

* **-f** fuerza la comprobación de un sistema, aunque no tenga activada la marca.
* **-c** busca en el sistema bloques defectuosos.

Linux usa el paquete de herramientas *e2fsprogs* para poder trabajar con sistemas de ficheros *ext*. Algunas de las utilidades más populares son:

* `blkid` Muestra información de los dispositivos de bloques.
* `chattr` Modifica los atributos de los ficheros en el sistema de ficheros.
* `debugfs` Muestra y modifica la estructura del sistema de ficheros, pudiendo recuperar ficheros borrados o corruptos.
* `dumpe2fs` Muestra información relativa a los bloques y al superbloque.
* `e2label` Cambia la etiqueta del sistema de ficheros.
* `resize2fs` Aumenta o disminuye el tamaño del sistema de ficheros.
* `tune2fs` Modifica los parámetros del sistema de ficheros.

El sistema de ficheros XFS también dispone de varias utilidades para su configuración y ajuste.

* `xfs_admin` Muestra o modifica los parámetros del sistema de ficheros, como la etiqueta o el UUID asignado.
* `xfs_db` Comprueba y depura un sistema de ficheros XFS.
* `xfs_info` Muestra información de un sistema de ficheros montado, como los tamaños de bloque y sector, o la etiqueta y UUID.
* `xfs_repair` Repara sistemas de ficheros dañados.

## Montaje del sistema de ficheros

Podemos acceder al sistema de forma directa, pero los comandos `cd`, `mv` y similares sólo lo pueden hacer a través del árbol de directorios. Por eso debemos montar el sistema de ficheros en algún punto de nuestro árbol.

**Punto de montaje:** es el lugar donde se ubicará dicho sistema. No tiene porque estar vacío, pero no se podrá acceder al contenido original una vez montado.
 El comando `mount`, es el responsable de montar sistemas de ficheros dentro del árbol de directorios. También puede mostrar los sistemas montados actualmente.

	# mount
	# mount -t ext2 /dev/sdb1 /home

El fichero `/etc/fstab` describe la composición del sistema de ficheros completo, donde hay varios sistemas de ficheros montados en diferentes puntos de montaje. Además de los nombres de dispositivo y los correspondientes puntos de montaje, podemos especificar varias opciones a la hora de montar los sistemas de ficheros. Las opciones disponibles dependen del sistema de ficheros. La tercera columna describe el tipo de sistema de ficheros en cuestión. Las entradas `ext3` o `iso9660`, hablan por sí solas, `swap` se refiere al espacio de intercambio (que no requiere de montaje), y `auto` significa que mount debe intentar averiguar el tipo del sistema de ficheros.

> `mount` utiliza el contenido del fichero `/etc/filesystems`, o si no existe, el fichero `/proc/filesystems`. En cualquier caso, sólo procesa las líneas no marcadas con nodev. El kernel genera de forma dinámica el fichero `/proc/filesystems` basándose en los sistemas de ficheros para los cuales dispone de drivers. El fichero `/etc/filesystems` es útil si quieres establecer un orden para `mount` diferente al establecido en `/proc/filesystems`, el cual no podemos modificar.

> Antes de que `mount` utilice `/etc/filesystems`, prueba suerte con las librerias `libblkid` y `libvolume_id`, las cuales (aparte de otras) son capaces de determinar qué tipo de sistema de ficheros está creado en un medio. Podemos usar estas librerías con los programas `blkid` y `vol_id`.

La cuarta columna contiene las opciones, incluyendo:

* **defaults** En realidad no es una opción, sino un lugar donde situar las opciones estándar.
* **noauto** Previene que un sistema sea montado automáticamente cuando se inicia el sistema.
* **user** En principio, sólo *root* puede montar dispositivos de almacenamiento, a menos que la opción `user` esté activada. En este caso, los usuarios normales puede ejecutar `mount <device>` o `mount <mount point>`. Esta opción permitirá que el usuario que ha realizado el montaje también lo desmonte; hay una opción similar, `users`, que permite a cualquier usuario desmontar el dispositivo.
* **sync** Las operaciones de escritura no se guardan en la RAM, sino que se envían directamente al medio. El final de la operación de escritura sólo se le envía a la aplicación una vez que los datos han sido escritos en el medio. Esto es útil para disquetes, que de otro modo podrían ser retirados de la unidad mientras que los datos todavía están en el buffer.
* **ro** El sistema de ficheros será montado para sólo lectura, no escritura (en oposición a `rw`).
* **exec** Pueden ejecutarse los ficheros ejecutables presentes en el medio. Lo contrario es `noexec`. La opción `user`, implica la opción `noexec` (aparte de otras).

## Estadísticas de los sistemas de ficheros

Como administradores de un sistema Linux, en algún momento será preciso que revisemos el rendimiento y uso de los discos del sistema. Disponemos de varias herramientas para ayudarnos en esta tarea:

* *df* Muestra el uso por partición
* *du* Muestra el uso por directorio
* *iostat* Muestra un gráfico en tiempo real de las estadísticas de uso por partición
* *lsblk* Muestra el tamaño actual de las particiones y los puntos de montaje

Además de las utilidades anteriores, el kernel hace uso de los directorios `/proc` y `/sys` para almacenar diferentes estadísticas del sistema. Hay dos ficheros que resultarán útiles a la hora de trabajar con los sistemas de ficheros; `/proc/partitions` y `/proc/mounts`. Por otro lado, el directorio `/sys/block` contiene varios directorios para cada montaje, con sus particiones y estadísticas del kernel.

## Etiquetas y UUID's

La referencia a sistemas de ficheros a través de rutas como `/dev/sda2` pueden provocar errores en el futuro, cuando la partición sea movida dentro del disco, o incluso se cambie la conexión del disco entero. Si hablamos de medios externos, como USB, el problema se agrava. Para tratar de solucionar esta situación, cabe la posibilidad de introducir una *etiqueta*, de 16 caracteres ,en el superbloque del sistema de ficheros.

	# e2label /dev/sdb2 home

de este  modo podemos hacer referencias a la partición:

	# mount -t ext2 LABEL=home /home

E incluso, otras utilidades también pueden hacer referencia a un dispositivo a través de su etiqueta:

	# tune2fs -L home /dev/sda3

> Averiguar como se gestionan las etiquetas en un sistema Btrfs.

Pero, si tenemos una cantidad elevada de discos, las etiquetas no serán suficientes para diferenciar los discos/particiones. En este caso, debemos utilizar un sistema UUID (universally unique identifier).

	$ uuidgen

Los valores UUID son generados de forma aleatoria, por lo que se puede asegurar que dos sistemas de ficheros nunca tendrán dos identificadores iguales.

	# tune2fs -U random /dev/sdb3

Podemos visualizar el UUID de un sistema de ficheros con el comando:

	# tune2fs -l /dev/sdb3 | grep UUID

> Probar con los comandos `blkid` y `lsblk`.

Tanto las etiquetas como los UUID's pueden utilizarse en el fichero `/etc/fstab`.

> Usando la opción `loop`, podemos montar ficheros que contienen sistemas de ficheros. Por ejemplo, una imagen de un CD-ROM, sin tener que *quemarlo* en un medio físico.

>`$ mkisofs -o micd.iso dir`<br/>
>`$ mount -o loop,ro micd.iso /testcd`

## El comando `dd`

El comando `dd` es un comando para copiar ficheros *por bloques*. Se suele utilizar para crear imágenes, es decir, copias completas de un sistema de ficheros.
 `dd` (“copy and convert”) lee datos bloque a bloque de un fichero de entrada y los escribe sin ninguna modificación en un fichero de salida. No importa si los ficheros son regulares o dispositivos. De esta forma podemos crear rápidamente un backup de la partición del sistema:

	# dd if=/dev/hdb1 of=/data/hdb1.dump

Esta línea guarda el contenido de la partición `/dev/hdb1` en el fichero `hdb1.dump`, que debe residir en un disco diferente, y que podemos recuperar del siguiente modo:

	# dd if=/data/sdb1.dump of=/dev/sdb1

(Si `hdb1` es nuestro disco de sistema, debemos arrancar desde un medio de rescate)

A mayores, necesitamos que la tabla de particiones sea igual en ambos discos (defectuoso y nuevo), por lo que podemos copiarla del siguiente modo:

	# dd if=/dev/sdb1 of=/media/floppy/mbr_sdb.dump bs=512 count=1

Copiamos el MBR al disquete, matando dos pájaros de un tiro: la etapa del gestor de arranque también finaliza en el disco duro después de restaurar el MBR.

	# dd if=/media/floppy/mbr_sdb.dump of=/dev/sda

> `dd` también puede ser usado para hacer que el contenido de CDROM's o DVD's sea accesible de forma permanente en el disco duro. El comando:

	# dd if=/dev/cdrom of=/data/cdrom1.iso

coloca el contenido del CDROM en el disco. Como es una imagen ISO, el kernel de Linux puede interpretarlo, y por supuesto, ser montado con:

	# mount -o loop,ro /data/cdrom1.iso /mnt

y así acceder al contenido de la imagen. Por supuesto, podemos hacer esto permanente usando `/etc/fstab`.

## Comandos vistos en el tema

Comando | Descripción
-- | --
blkid | Localiza y visualiza los atributos de un dispositivo de bloque
dd | «copy and convert», copia ficheros bloque a bloque
e2fsck | Comprueba el estado de los sistemas de ficheros *ext2* y *ext3*
fsck | Organiza las comprobaciones de un sistema de ficheros
mk2fs | Crea sistemas de ficheros *ext2* y *ext3*
mkswap | Inicializa un fichero o partición de intercambio
mount | Añade un sistema de ficheros en el árbol de directorios
swapon | Activa un espacio de intercambio
swapoff | Desactiva un espacio de intercambio
tune2fs | Ajusta los parámetros de un sistema de ficheros *ext2/ext3*
