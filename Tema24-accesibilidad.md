# Tema 24 - Accesibilidad

## Introducción

En un entorno gráfico, la accesibilidad está relacionada con la capacidad de un usuario para hacer uso del entorno de escritorio. Se trata de que el entorno de escritorio sea accesible para todos los usuarios potenciales, más allá de sus posibles limitaciones en cuanto a visión o uso del ratón, por ejemplo. Por lo tanto, es importante tener conocimiento de las configuraciones de escritorio que proporcionan ayuda a los usuarios.

Aunque cada entorno de escritorio puede incorporar métodos ligeramente diferentes, la mayoría de las configuraciones se pueden aplicar a través de paneles de control, como el panel de Acceso Universal de GNOME Shell.

A continuación, se muestran algunos de los ajustes para ayudar en los problemas de visión:

Nombre | Descripción
--- | ---
Cursor parpadeante | Modifica la velocidad de parpadeo del cursor para facilitar su localización
Tamaño de cursor | Modifica el tamaño del cursor
Alto contraste | Aumenta el brillo de las ventanas y los botones, y se oscurecen los bordes, así como el texto y el cursor.
Texto grande | Modifica el tamaño de la fuente. A menudo se conoce como *lupa de pantalla*.
Lector de pantalla | Utiliza un lector de pantalla para leer en voz alta. Por ejemplo, el lector de pantalla Orca y Emacspeak.
Teclas de sonido | Las teclas emiten un sonido cuando se activa el bloqueo de mayúsculas o el bloqueo de teclado numérico.
Zoom | Amplifica una área de la pantalla aplicando diferentes magnitudes.

Si un usuario ciego tiene acceso a una pantalla *braille*, puede instalar la aplicación Britty que está disponible en la mayoría de los repositorios de las distribuciones Linux. Funciona como un demonio y proporciona un acceso de consola (modo texto) a través de una pantalla braille.

Para aquellos usuarios con dificultades en dedos y/o manos, existen diferentes configuraciones para permitir un acceso completo al sistema.

Nombre | Descripción
--- | ---
Teclas rebote | Opción de teclado que ayuda a compensar las teclas que se pueden presionar accidentalmente varias veces.
Retardo doble clic | Modifica la cantidad de tiempo permitido entre dobles clic de ratón.
Gestos | Activa combinaciones de clic de ratón con pulsaciones de teclado.
Clic *Hover* | Ejecuta un clic de ratón al pasar el puntero sobre un objeto.
Teclas de ratón | Permite usar el teclado para emular las funciones del ratón.
Teclas repetidas | Opción de teclado que modifica el tiempo de pulsación de una tecla, así como el retardo para reconocer dicha tecla. También se conoce como tasa de repetición del teclado.
Teclado en pantalla | Muestra un teclado *visual* que puede ser manipulado por el ratón, o por otro elemento que emula pulsaciones de teclado.
Clic secundario simulado | Activa una tecla que debe ser pulsada junto con un clic de ratón para emular un clic secundario del ratón.
Teclas lentas | Modifica el tiempo que una tecla debe ser presionada para aceptar la entrada.
Teclas *pegajosas* | Esta opción permite que algunas teclas especiales, como Shift o Ctrl permanezcan activas después de ser pulsadas.

