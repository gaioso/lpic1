# Tema 18 - Control de acceso

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [El sistema de control de acceso de Linux](#el-sistema-de-control-de-acceso-de-linux)
- [Control de acceso para ficheros y directorios](#control-de-acceso-para-ficheros-y-directorios)
- [Propiedad de los procesos](#propiedad-de-los-procesos)
- [Permisos especiales para ficheros ejecutables.](#permisos-especiales-para-ficheros-ejecutables)
- [Permisos especiales para los directorios.](#permisos-especiales-para-los-directorios)
- [Atributos de fichero.](#atributos-de-fichero)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)
- [Resumen](#resumen)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos

* Comprender los mecanismos de control de acceso y privilegios de Linux.
* Asignar permisos a ficheros y directorios.
* Utilizar «umask», SUID, SGID y el «bit pegajoso».
* Atributos de fichero en el sistema de ficheros ext.

## El sistema de control de acceso de Linux

Cuando tenemos a varios usuarios accediendo al mismo equipo, debe existir un control de acceso para los procesos, ficheros y directorios, para asegurar que el usuario A no pueda acceder a los ficheros privados del usuario B. Para conseguir esta funcionalidad, Linux implementa el sistema estándar de privilegios propio de UNIX. En UNIX, cada fichero y directorio se asigna a un solo usuario (su propietario), y a un grupo. Cada fichero soporta privilegios separados para su propietario, los miembros del grupo al que pertenece («el grupo»), y para el resto de usuarios («others»). Los permisos de lectura, escritura y ejecución se pueden asignar de forma individual para cada uno de esos tres conjuntos de usuarios.

El propietario puede fijar los permisos de acceso al fichero. El grupo y el resto de usuarios sólo pueden acceder al fichero si el propietario ha asignado los permisos adecuados. La suma total de los permisos de acceso al fichero también se llama «modo de acceso».

> Diferentes distribuciones (como Ubuntu y CentOS) asocian a cada usuario a un grupo independiente con el mismo nombre que el del usuario. De esta forma se evitan problemas de seguridad a la hora de compartir ficheros, pero puede resultar confuso a la hora de consultar los propietarios de un fichero al ver en dos columnas el mismo nombre. Se debe tener en cuenta esta situación.

## Control de acceso para ficheros y directorios

**Conceptos básicos**. El usuario que posea permiso de lectura puede examinar el contenido del fichero, el que posea permiso de escritura puede modificar su contenido, y el permiso de ejecución es necesario para ejecutar el fichero como un proceso.

> Aquellos ficheros que contienen «lenguaje máquina», requieren del permiso de ejecución. En los ficheros que contienen scripts de shell, u otro programa interpretado, tan sólo precisamos de permiso de lectura.

Para los directorios, las cosas pueden parecer algo diferentes. El permiso de lectura es necesario para examinar el contenido del mismo (comando `ls`). Se precisan de permisos de escritura para crear, eliminar o renombrar ficheros en el directorio. El permiso de ejecución da la posibilidad de usar el directorio para, por ejemplo, cambiarnos a él con el comando `cd`, o usar su nombre en las rutas que se refieren a ficheros localizados más abajo en el árbol de directorios.

> En los directorios donde sólo tienes permiso de lectura, podemos leer los nombres de los ficheros, pero no podemos saber nada más acerca de los ficheros. Si sólo tenemos permiso de ejecución, puedes acceder a los ficheros tan sólo si conoces los nombres de los ficheros.

> Debe entenderse que la operación de borrado de un fichero, es una operación de directorio, y por lo tanto no importa que tengamos o no permisos de escritura sobre dicho fichero, tan sólo importa si tenemos permiso de escritura en el directorio donde se encuentra dicho fichero.

> Si tenemos permisos de escritura sobre el fichero, pero no sobre el directorio, no se puede eliminar completamente el fichero. Sin embargo, podemos eliminar su contenido, aunque el fichero siga existiendo.

Para cada usuario, Linux determina los derechos de acceso más apropiados. Por ejemplo, si los miembros del grupo del fichero no tienen permisos de lectura para el fichero, pero los otros sí, entonces los miembros del grupo no pueden leer del fichero. No se aplica el razonamiento de que si los miembros del grupo también forman parte del resto, puedan heredar sus permisos.

**Inspeccionando y modificando los permisos de acceso** Se puede obtener información acerca de los permisos, usuario y grupo que tiene un fichero, con el comando `ls -l`.
La primera columna muestra información de los permisos, luego el propietario y el grupo. Los permisos se designan con las letras `r`, `w` y `x`, y con `-` la ausencia de permiso. Como propietario del fichero, se pueden modificar esos valores, con el comando `chmod` (change mode). Se pueden especificar las tres categorías con las abreviaturas **u** (usuario), **g** (grupo) y **o** (otros).	Usando los operadores `+`, `-` y `=` se añade un permiso a los existentes, se elimina o se reemplaza.
Algunas de las opciones más importantes de `chmod` son:
* **-R** Si le indicamos un nombre de directorio se modifican los permisos de los ficheros y directorios que están dentro del mismo, y de forma recursiva hacia el fondo del árbol.
* **--reference=<name>** Usa los permisos del fichero con nombre `<name>`. En este caso no se indican permisos en el comando `chmod`.

> Los permisos también se pueden representar mediante simbología numérica, en lugar de letras. Se representa por tres dígitos para cada uno de los conjuntos de usuarios. El primer dígito es para el propietario, y es la suma de los permisos: 4 para lectura, 2 para la escritura y 1 para la ejecución.<br>

```
rw-r--r--	644
r--------	400
rwxr-xr-x	755
```

> Usando el modo numérico, deben indicarse los permisos en un sólo paso, no se pueden añadir o quitar como en la notación de letras. Sólo podemos usar los operadores `+` y `-` en la notación de letras.

	$ chmod 644 file

> es equivalente a:

	$ chmod u=rw,go=r file

**Especificando el propietario y los grupos; `chown` y `chgrp`**. El comando `chown` permite especificar el propietario y el grupo de un fichero o directorio. Se le pasan como parámetros los nombres de un usuario y un grupo. Si se indican ambos, se modifican. Si sólo se le indica un nombre seguido por *dos puntos*, el fichero es asignado al grupo del usuario. Y si solo se le pasa un grupo precedido por *dos puntos*, el propietario sigue sin cambios. *chown* también soporta la notación obsoleta de separación mediante un punto.

Para *entregar* ficheros a otro usuarios, o grupos debes ser el usuario *root*. La principal razón para evitar esta situación, es que los usuarios se podrían *molestar* unos a los otros en un sistema con cuotas (donde cada usuario dispone de una cantidad limitada de espacio en disco). Usando el comando `chgrp`, se puede cambiar el grupo de un fichero, incluso como un usuario normal -siempre que seas el propietario y seas miembro del grupo *nuevo*. `chown` y `chgrp` también soportan la opción `-R`, para aplicar los cambios de forma recursiva.

**Umask** Los nuevos ficheros son creados usando el modo de acceso 666, lectura y escritura para todo el mundo, y a los nuevos directorios se les asigna el modo de acceso 777. Debido a que esto no es lo deseado, Linux ofrece un método para eliminar algunos de los permisos. El *umask*, es un número octal con cuyo complementario se realiza un *AND* bit a bit con el modo estándar de acceso -666 o 777- para obtener el nuevo modo de acceso al fichero o directorio. De otro modo, *umask* contendrá aquellos permisos que el nuevo fichero o directorio **NO** contendrá.

Valores de muestra de umask y sus efectos:

Umask | Ficheros nuevos | Directorios nuevos
--- | --- | ---
000 | 666 (rw-rw-rw-) | 777 (rwxrwxrwx)
002 | 664 (rw-rw-r--) | 775 (rwxrwxr-x)
022 | 644 (rw-r--r--) | 755 (rwxr-xr-x)
027 | 640 (rw-r-----) | 750 (rwxr-x---)
077 | 600 (rw-------) | 700 (rwx------)
277 | 400 (r--------) | 500 (r-x------)

Veamos un ejemplo, con *umask* a 027:

```
1. valor umask                          027     ----w-rwx
2. complemento de umask                 750     rwxr-x---
3. modo de acceso de fichero nuevo      666     rw-rw-rw-
4. resultado (2 AND 3)                  640     rw-r-----
```

El valor de *umask* se configura mediante el comando `umask`, o llamándolo desde alguno de los ficheros de inicio (`.profile`, `bash_profile` o `.bashrc`). El valor de *umask* tiene un comportamiento similar al de las variables de entorno, los cambios en los procesos hijo, no afectan al proceso padre.

Se le puede indicar el valor en octal, o en letras, teniendo en cuenta que en la notación simbólica, los valores son el complementario, es decir, aquellos que quedan:

	$ umask 027

es equivalente a:

	$ umask u=rwx,g=rx,o=

Sin parámetros se muestra el valor actual. Y con la opción `-S`, se muestra en modo simbólico.

	$ umask
	0027
	$ umask -S
	u=rwx,g=rx,o=


Indicar que con *umask* sólo se pueden eliminar permisos, por lo tanto, no puedo asignar por defecto permiso de ejecución en los ficheros nuevos. *umask* sólo influye en el comando `chmod`. Si usamos `chmod` con el operador «+”» sin hacer referencia al propietario, grupo u otros, es entendido como «a+», pero los permisos configurados en *umask* no son modificados. Consideremos el siguiente ejemplo:

	$ umask 027
	$ touch file
	$ ls -l file
	$ chmod +x file
	$ ls -l file

No se ha aplicado el permiso de ejecución para *otros*, porque *umask* tiene activado su bit. De este modo se puede prevenir la asignación excesiva de permisos a los ficheros.

## Propiedad de los procesos

No sólo los datos almacenados en un medio son objetos que pueden tener propietarios. Los procesos del sistema también pueden tener propietarios. Muchos comandos crean un proceso en la memoria del sistema. Durante el uso normal, hay varios procesos que el sistema protege de los demás. Todos los procesos junto con sus datos en su espacio virtual es asignado a un usuario, su propietario. Lo habitual es que el propietario sea aquel usuario que ha iniciado el proceso. Los propietarios de los procesos se visualizan con el comando `ps -u`.

## Permisos especiales para ficheros ejecutables.

Al igual que con los permisos anteriores, los permisos especiales se gestionan con el comando `chmod`. En la siguiente tabla podemos ver un resumen de los permisos especiales que podemos aplicar tanto a ficheros como a directorios:

Nombre | Número | Símbolo
--- | --- | ---
UserID bit | 4 | u+s
GroupID bit | 2 | g+s
Sticky bit | 1 | o+t
 
Cuando se muestran los ficheros con el comando `ls -l`, a veces podemos encontrar permisos que difieren del tradicional `rwx`, por ejemplo:

	$ ls -l /usr/bin/passwd
	-rwsr-xr-x 1 root root 68208 Mai 28  2020 /usr/bin/passwd

¿Qué significa esto?. Vamos por partes:

Asumamos que el fichero tiene el modo de acceso usual `(-rwxr-x-r-x)`. Un usuario normal (sin privilegios), quiere modificar su *password*, y ejecuta este programa. Entonces, recibe el mensaje `permission denied`. ¿Cuál es la razón?
El proceso `password` intenta abrir el fichero `/etc/shadow` para escritura y le devuelve un error, debido a que solo *root* puede escribir en ese fichero. Esto debe ser así porque sino cualquier usuario puede, por ejemplo, modificar el *password* de *root*.

A través del uso del bit *set-UID* (SUID bit), un programa puede ser iniciado no con los privilegios del usuario, sino con los del propietario, en este caso, *root*. De esta forma, puede modificar el fichero `/etc/shadow`. Es responsabilidad del programador de la aplicación comprobar que no se realizan otras tareas con otros ficheros, o con otras entradas que no sean las del usuario. Este mecanismo sólo funciona con los binarios, no con los scripts de shell, o similares.

De forma análoga, existe un bit *SGID*, que provoca que un programa sea ejecutado con los privilegios del grupo del fichero (normalmente para acceder a otros ficheros del grupo), en lugar de los del grupo del usuario que ha ejecutado el programa. Ambos bits, se pueden modificar con el comando `chmod`. Con `u+s`, se activa el bit *SUID*, y con `g-s`, se elimina el bit *SGID*. También se pueden indicar en el modo octal, añadiendo un dígito a la izquierda, por ejemplo:

	4755	# el bit SUID tiene el valor 4, y el SGID el valor 2.

## Permisos especiales para los directorios.

Hay otra excepción para el principio de asignación de ficheros al propietario de acuerdo con la identidad de su creador. El propietario de un directorio puede *decretar* que los ficheros creados en ese directorio pertenezcan al mismo grupo que el del directorio. Esto se especifica indicando el bit *SGID* en los permisos del propio directorio. Los permisos de acceso al directorio no son modificados con este bit. Debemos tener permisos de escritura en dicho directorio. Un fichero creado en un directorio con *SGID*, pertenecerá al grupo aunque el usuario que lo ha creado no pertenezca.

La típica aplicación para el bit *SGID* en un directorio, es cuando éste será utilizado para almacenar ficheros de un proyecto de grupo. Sólo los miembros de dicho grupo podrán acceder y modificar a los ficheros en el directorio, así como crear nuevos ficheros. Esto significa que necesitamos añadir los usuarios al grupo *equis*, y luego crear el directorio y asignarlo al nuevo grupo. Al propietario y al grupo se le conceden todos los permisos, a los *otros* ninguno, y se debe activar el bit *SGID*.

```
# groupadd proyecto
# usermod -a -G proyecto debian
# mdkir /home/debian/proyecto
# chgrp proyecto /home/debian/proyecto
# ls -ld /home/debian/proyecto
# chmod u=rwx,g=srwx /home/debian/proyecto
# ls -ld /home/debian/proyecto
```

Ahora si creamos un fichero en el directorio *proyecto*, debe ser asignado al grupo *proyecto*:

```
$ id
$ touch /tmp/file.txt
$ ls -l /tmp/file.txt
$ ---
$ touch /home/debian/proyecto/file.txt
$ ls -l /home/debian/proyecto/file.txt
$ ---
```

Aún nos queda algo que arreglar, porque si observamos la última linea, el fichero pertenece al grupo correcto, pero los miembros del grupo sólo pueden leer el fichero. Si deseamos que los miembros del grupo también puedan escribir, debemos cambiar los permisos con `chmod`, o bien aplicar otra solución más *razonable*. El modo *SGID* sólo cambia el comportamiento del sistema cuando se crean nuevos ficheros. Los ficheros existentes se comportan como si no estuviera activado este bit. Esto significa que un fichero creado fuera, conservará sus permisos al ser movido dentro del directorio proyecto.

El comando `chgrp` trabaja como siempre en los directorios *SGID*, el propietario del fichero puede asignarle cualquier grupo al que pertenezca. Si el propietario no es miembro del grupo del directorio, no puede asignar ese grupo al fichero a través del comando `chgrp` -debe crearlo de nuevo dentro del directorio.

Linux soporta otros modos especiales para directorios, dónde sólo el propietario de un fichero puede eliminar ficheros dentro del directorio.

	$ ls -ld /tmp

El modo *t*, el «bit sticky», puede usarse para resolver un problema que surge cuando se comparten directorios públicos. Los permisos de escritura en un directorio, permiten a un usuario eliminar ficheros de otros usuarios, sin tener en cuenta su modo de acceso o propietario. Por ejemplo, el directorio `/tmp` es una zona común, y muchos programas crean ahí sus ficheros temporales. Por lo tanto, todos los usuarios tienen permiso de escritura. Eso implica que cualquier usuario puede eliminar el contenido del directorio.

Si el «bit sticky» está activado en ese directorio, un fichero sólo puede ser eliminado por su propietario, el propietario del directorio o *root*. Puede ser añadido o removido especificando los modos simbólicos `+t` y `-t`; en la representación octal, tiene un valor **1** en la misma posición que *SUID* y *SGID*.

## Atributos de fichero.

Además de los permisos de acceso, los sistemas de ficheros *ext2* y *ext3* soportan atributos de fichero permitiendo el acceso a características especiales del sistema de ficheros. Las más interesantes son «append-only» e «immutable», que pueden ser usadas para proteger los ficheros de *log* y los ficheros de configuración de posibles modificaciones. Sólo *root* puede modificar estos atributos, y una vez configurados también se aplican a los procesos ejecutados por el usuario *root*. El atributo *A* puede también ser útil; puede usarse en portátiles para evitar que el disco esté siempre funcionando, y ahorrar de este modo, energía. A mayores, se pueden consultar los atributos `c`, `s` y `u`.

Se pueden asignar o retirar los atributos mediante el comando `chattr`. Funciona de forma parecida a `chmod`, y también incluye la opción `-R`.

	# chattr +a /var/log/messages
	# chattr -R +j /home/debian/proyecto

Con el comando `lsattr` se pueden revisar los atributos de un fichero. Es similar a `ls -l`:

	# lsattr /var/log/messages
	# lsattr -a /var/log
	# lsattr -aR /var/log
	# lsattr -d /var/log

Verificamos que la opción `-a` funciona en `/var/log/messages`:

	# tail -f /var/log/messages

	# echo “hola” >/var/log/messages
	# echo “hola” >>/var/log/messages

## Comandos vistos en el tema

| Comando | Descripción |
| --- | --- |
| chattr | Configura los atributos de fichero, en *ext2* y *ext3* |
| chgrp | Configura el grupo de un fichero o directorio |
| chmod | Configura el modo de acceso a ficheros y directorios |
| chown | Configura el propietario y/o asigna el grupo de un fichero o directorio
| lsattr | Lista los atributos de fichero |

## Resumen

* Linux soporta permisos de lectura, escritura y ejecución, que pueden asignarse de forma separada para el *propietario*, *grupo* y *resto de usuarios*.
* La suma total de los permisos de un fichero, también se conoce como «modo de acceso».
* Cada fichero (y directorio) posee un propietario y un grupo. Sólo el propietario puede modificar los permisos de acceso.
* El usuario *root*, puede leer o escribir cualquier fichero.
* Los permisos de un fichero se manejan a través del comando `chmod`.
* Con `chown`, el administrador puede modificar el usuario y grupo de cualquier fichero.
* Los usuarios normales pueden usar `chgrp` para asignar sus ficheros a otros grupos.
* `umask` puede ser usado para limitar los permisos estándar para los ficheros y directorios recién creados.
* Los bits *SUID* y *SGID* permiten la ejecución de programas con los privilegios del propietario, o el grupo, en lugar de los del usuario que los ejecuta.
* El bit *SGID* en un directorio provoca que los nuevos ficheros de ese directorio sean asignados al grupo del directorio.
* El «bit sticky» en un directorio permite que sólo el propietario (y *root*) puedan eliminar ficheros.
* El sistema de ficheros *ext* soporta atributos de fichero especiales.

