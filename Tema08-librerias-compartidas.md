# Tema 8 - Librerías compartidas

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Compilando e instalando software](#compilando-e-instalando-software)
- [Librerías dinámicas en la práctica.](#librerías-dinámicas-en-la-práctica)
- [Instalando y localizando librerías dinámicas.](#instalando-y-localizando-librerías-dinámicas)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)
- [Resumen](#resumen)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos

* Ser capaz de identificar librerías compartidas.
* Saber donde se almacenan habitualmente las librerías compartidas.
* Gestionar librerías compartidas.


## Compilando e instalando software

Las funciones comunes y compartidas por los diferentes programas se guardan en ficheros denominados «bibliotecas». Para la compilación de un programa, es necesario que cada componente pueda localizar las bibliotecas del sistema y cree un vínculo entre sus propias funciones y las funciones de las bibliotecas. El vínculo puede ser estático o dinámico. Es decir, pueden estar incluídas en el programa compilado, o solamente un vínculo externo a las bibliotecas. Los programas estáticos no dependen de ficheros externos, pero son de mayor tamaño que los programas dinámicos.

En el siguiente ejemplo, compilaremos el código del fichero <a href="files/senodepi.c" target="_blank">senodepi.c</a>:

```
/* senodepi.c */
#include <math.h>
#include <stdio.h>

int main(void)
{
	printf("Resultado: %g\n" , sin(3.141592654/2.0));
	return 0;
}
```

	$ gcc senodepi.c -lm -o senodepi.o
	$ gcc -static senodepi.c -lm -o senodepi-static.o
	$ ls -l senodepi*

Las ventajas del uso de librerías son:

* reemplazo de las librerías, sin tener que modificar el programa.
* si, «n» programas usan la librería, no es necesario disponer de «n» veces el espacio en disco que ocupa esa librería.
* al mismo tiempo, es suficiente disponer de una copia en memoria.

Desventaja:
 
* la distribución de los programas se hace más compleja.

## Librerías dinámicas en la práctica.

La mayoría de los programas en un sistema Linux están enlazadas dinámicamente. Podemos visualizarlo a través del comando `file`:

	$ file senodepi.o
	$ file senodepi-static.o

Para saber qué librerías está utilizando un programa, hacemos uso del comando `ldd`:

	$ ldd /usr/bin/vi
	$ ldd senodepi.o

Como `vi` es un programa sencillo, requiere pocas bibliotecas. Analizando la salida, se comprueba que se han localizado todas las bibliotecas necesarias, por lo tanto, el programa se ejecutará correctamente.

Si copiaramos ese mismo programa desde otra ubicación, donde se compiló con otras bibliotecas, el programa no funcionará correctamente. Para solucionar esto, debemos instalar el programa apropiado para la distribución utilizada, aunque podemos llegar a tener que hacer la instalación manualmente.

## Instalando y localizando librerías dinámicas.

El programa responsable de cargar la biblioteca y vincularla al programa que depende de ésta es `ld.so`, que es llamado por un programa cada vez que éste necesita de una función localizada en una biblioteca. El programa `ld.so` consigue localizar la biblioteca con la ayuda del mapeo que almacena en el fichero `/etc/ld.so.cache`. Las ubicaciones estándar del sistema son `/lib` y `/usr/lib`. Los directorios con bibliotecas adicionales deben incluirse en el fichero `/etc/ld.so.conf`. En algunas distribuciones existe el directorio `/etc/ld.so.conf.d/` que puede tener otros ficheros con la localización de bibliotecas.

Por lo tanto, cuando un programa está usando una función compartida, el sistema debe realizar la búsqueda en los directorios almacenados en

1. La variable **LD_LIBRARY_PATH**
2. La variable **PATH**
3. El directorio `/etc/ld.so.conf.d/`
4. El fichero `/etc/ld.so.conf`
5. Los directorios `/lib/*` y `/usr/lib*/`

La ejecución del comando `ldconfig` actualiza las bibliotecas localizadas en `ld.so.conf` en el fichero `ld.so.cache`, para que puedan ser utilizadas por `ld.so`. Otro modo de almacenar una localización al alcance de `ld.so`, es agregar su respectiva ruta a la variable de entorno `LD_LIBRARY_PATH`, con el comando:

	export LD_LIBRARY_PATH=”ruta_a_biblioteca”

Sin embargo, esto no funcionará más allá del alcance de la variable de entorno, pero es un método útil si no queremos modificar el fichero `/etc/ld.so.conf`, o para la ejecución puntual de un programa.

Si tenemos problemas con la librería, podemos comprobar la lista de ficheros que forman parte de la biblioteca de librerías con el comando `ldconfig -v`. Por ejemplo, si queremos la existencia de una librería determinada ejecutamos el siguiente comando:

	$ ldconfig -v 2> /dev/null | grep libmysqlclient
		libmysqlclient.so.21 -> libmysqlclient.so.21.1.22

## Comandos vistos en el tema

Comando | Descripción
-- | --
ldconfig | Construye la caché de librerías dinámicas
ldd | Muestra las librerías dinámicas usadas por un programa

## Resumen

* Las librerías ofrecen funcionalidades que se utilizan frecuentemente de forma estandarizada por varios programas.
* Las librerías dinámicas ahorran memoria y espacio de disco, y facilita las tareas de mantenimiento de los programas.
* El comando `file`, permite saber si un programa está enlazado de forma dinámica.
* `ldd` muestra los nombres de las librerías dinámicas que usa un programa.
* Las librerías dinámicas están localizadas en `/lib` y `/usr/lib`, y los directorios indicados en `/etc/ld.so.conf`. El comando `ldconfig` construye un índice.
