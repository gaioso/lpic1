# Prácticas
## Comandos básicos en Bash

1. ¿Qué es una shell? Elige dos respuestas

  1. Un intérprete de comandos
  2. Un lenguaje de programación
  3. Una herramienta de gestión de ficheros
  4. Una consola en el interior de una interfaz gráfica

2. ¿Cuál es la shell por defecto en la mayoría de distribuciones de GNU/Linux?

  1. ksh
  2. sh
  3. bash
  4. csh

3. A partir de la siguiente línea: `linus@eeepc:~/Documentos/proyecto$`, ¿qué información se puede obtener?

4. La siguiente línea: `echo -n "hola, la fecha es" ; date ; exit`:

  1. Visualiza el texto y luego la fecha, y espera una inserción de teclado.
  2. No hace nada; sale de la sesión
  3. Sale únicamente si el comando `date` funciona
  4. Como A, pero al final deja la shell

5. ¿Cómo se puede ejecutar el último comando del historial con el comando `fc`?

6. Independientemente de la ubicación en el árbol de directorios, ¿cuál es el efecto del comando `cd` sin parámetros?

7. ¿Cuál es el resultado del comando `>fic`?

8. Si ejecuto el siguiente comando como un usuario normal: `ls -R / >lista`, ¿qué se visualizará por pantalla? ¿y que contendrá el fichero `lista`?

9. Ha introducido un comando que no se detiene (p. ej. `$ yes linus`), ¿qué puedo hacer para finalizar el trabajo?

10. Ha ejecutado un comando que monopoliza la consola sin que muestre nada, ya que tiene una carga elevada de trabajo de CPU. ¿Cómo se puede enviar a segundo plano?

11. Ejecute el comando `sleep 100 &`. Luego finalice correctamente el proceso.

12. Elimine un fichero llamado `fic`. Si el borrado tiene éxito, haga que se muestre un 'OK'. En caso contrario, haga que se muestre 'ERROR' y haga un `ls -l` de ese fichero.

13. Modifique el PATH por defecto añadiendo `/opt/bin` en primera posición.

14. Quiere crear, de la forma más sencilla posible, la ruta `fuentes/C/backup` en su directorio personal, sabiendo que no existe ninguno de ellos. ¿Qué comando debo utilizar?

15. ¿Cómo eliminar el directorio `C` del ejercicio anterior?
