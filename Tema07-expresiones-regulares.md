# Tema 7 - Exprexiones regulares

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Conceptos básicos](#conceptos-básicos)
- [Expresiones regulares: Extras](#expresiones-regulares-extras)
- [Búsqueda en fichero de texto](#búsqueda-en-fichero-de-texto)
- [El comando sed](#el-comando-sed)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)
- [Resumen](#resumen)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos

* Comprender y ser capaz de formular expresiones regulares simples y complejas.
* Conocer el programa `grep` y sus variantes, `fgrep` y `egrep`.

## Conceptos básicos

Muchos comandos de Linux son usados para procesar texto «haz xyz para todas las líneas como ésta». Una herramienta muy potente para describir los bits de un texto son las denominadas expresiones regulares. En un primer vistazo, las expresiones regulares se parecen a los patrones usados en la búsqueda de ficheros en la shell, pero trabajan de una forma diferente y ofrecen muchas más posibilidades.

Normalmente, se construyen de forma recursiva usando *primitivas* que también se consideran expresiones regulares. La expresión regular más simple son las letras, números y otros caracteres del conjunto habitual de caracteres, que se representan a sí mismos. Por ejemplo `a` es una expresión regular que concuerda con el caracter «a», la expresión regular `abc` equivale a la cadena «abc». Las clases de caracteres pueden definirse de un modo similar a los patrones de búsqueda de la shell, la expresión `[a-e]` corresponde a un carácter desde la «a» hasta la «e». Y `a[xy]b` corresponde con «axb» o «ayb».

Estos intervalos pueden ser concatenados; `[A-Za-z]` equivale a todas las letras mayúsculas y minúsculas, pero en este caso, el complementario de un rango se construye de la siguiente forma:

	[^abc]

concuerda con todos los caracteres excepto «a», «b» y «c». El punto «.» corresponde con el signo de interrogación «?» de la shell, es decir un carácter arbitrario - a excepción del carácter de nueva línea «\n».

`a.c` concuerda con «abc», «a/c», pero no con la multilinea:
a
c

Esto se debe a que la mayoría de los programas trabajan en modo línea, y el modo multilínea es muy difícil de procesar.

Mientras que en la shell los patrones concuerdan con el principio del nombre de los ficheros, en programas que trabajan en la selección de líneas, suele concordar en cualquier parte de la línea. Sin embargo, esto se puede restringir. Una expresión regular que comienza con el símbolo `^` concuerda sólo con el principio de la línea, y una expresión regular que finaliza con `$`, concuerda sólo con el final de la línea. En este caso el carácter de final de línea no se tiene en cuenta, por lo que podemos usar:

	xyz$

para seleccionar todas las líneas finalizadas en «xyz», en lugar de escribir:

	xyz\n$

Finalmente, podemos usar el asterisco para indicar que la expresión regular puede repetirse muchas veces (o ninguna). El asterisco no espera ningún carácter en la entrada, sólo modifica la expresión regular precedente. La repetición tiene precedencia sobre la concatenación. La expresión `ab*` concuerda con una «a» seguida de varias «b» (o ninguna). No varias repeticiones de «ab».

## Expresiones regulares: Extras

Las explicaciones anteriores se aplican a casi todos los programas de Linux que trabajan con expresiones regulares. Pero existen programas que soportan diferentes extensiones, tanto en las reglas de notación como en funcionalidades adicionales. Las implementaciones más avanzadas se encuentran en los lenguajes de script modernos, como Tcl, Perl o Python.

* **Word brackets** (limitador de palabras) `\<`, este patrón debe estar al principio de una palabra (un lugar donde un carácter 'no-letra' precede a una letra), de forma análoga `\>` debe estar al final de una palabra (una letra es seguida por un carácter 'no-letra')
* **Agrupamiento** Los paréntesis permiten la repetición de concatenaciones de expresiones regulares:

`a(bc)*` concuerda con una «a» seguida de cero o más repeticiones de «bc».
* **Alternativa** Con la barra vertical `|` se puede seleccionar entre varias expresiones regulares. La expresión: `motor (bike|cycle|boat)` concuerda con «motor bike», «motor cycle» y «motor boat», y nada más.
* **Expresión opcional** El símbolo interrogante (?) hace que la expresión precedente sea opcional, es decir, debe aparecer una o cero veces: `super(man)?` - concuerda con «super» y «superman»
* **Repetición al menos una vez** El signo más (+) corresponde con el operador asterisco, excepto que la expresión debe aparecer al menos una vez.
* **Repeticiones de un número dado** Se puede especificar un número mínimo y máximo de repeticiones entre corchetes: `ab{2,4}` concuerda con «abb», «abbb» y «abbbb», pero no con «ab» ni con «abbbbb». Se puede omitir el valor mínimo y máximo, que serán cero e infinito respectivamente.

## Búsqueda en fichero de texto

El programa `grep` es posiblemente el más importante en cuanto al uso de expresiones regulares. Busca en un fichero (o más) las líneas que concuerdan con una expresión regular determinada. Las líneas concordantes se envían a la salida, las no concordantes son descartadas. Hay dos variantes de `grep`: de forma tradicional, la versión reducida al mínimo `fgrep` (*fixed*) no permite expresiones regulares -sólo trabaja con cadenas de caracteres- pero es muy rápido. Y la versión *extendida* `egrep` ofrece operadores regulares, pero es más lento y necesita más memoria.

La sintaxis de `grep` precisa al menos una expresión regular para buscar, y a continuación el fichero en el que buscar. Si no hay fichero, espera por la entrada estándar. La expresión regular se debe colocar entre comillas simples, para prevenir que la shell intente expandirla, especialmente si es una expresión compleja, y por supuesto si es similar a un patrón de búsqueda de la shell. Además de las expresiones y los ficheros, hay opciones:

* **-f** (file), el patrón de búsqueda se puede leer del fichero. Si el fichero contiene varias líneas, el contenido de cada línea será considerado un patrón de búsqueda. Esto puede simplificar el trabajo para los patrones usados con frecuencia.

En el fichero de ejemplo `frog.txt`, está la versión en inglés del cuento «Frog King». Todas las líneas que contienen la secuencia de caracteres *frog* se pueden localizar de la siguiente forma:

	$ grep frog frog.txt

Para encontrar todas las líneas que contienen la palabra *frog* (sin combinaciones, como «bullfrog» o «frogspawn»), necesitamos usar el limitador de palabras:

	$ grep '\<frog\>' frog.txt

¿Cómo localizamos todas las líneas que comiencen por *frog*?

Otro ejemplo diferente:

El fichero `/usr/share/dict/words` contiene una lista de palabras del idioma inglés («diccionario»). Nos interesan todas las palabras que contengan tres o más letras «a»:

	$ grep -n 'a.*a.*a' /usr/share/dict/words

Con expresiones regulares mucho más complejas, podemos no entender porqué `grep` muestra en su salida una linea y otras no. Esto se puede resolver con la opción `--color`, mostrando la parte concordante en un color diferente:

	$ grep --color root /etc/passwd

## El comando sed

Otra herramienta de gran utilidad es el *stream editor*. En ciertas ocasiones debemos modificar un fichero de texto sin que para ello tengamos que abrir un editor de texto. Un *stream editor* modifica el texto que se le envía a través de un fichero o como salida de una tubería.

El comando que debemos usar es `sed`. Es muy rápido ya que ejecuta una única pasada a través del texto para poder ejecutar las modificaciones indicadas en las opciones. Por defecto, se usará el texto que obtiene desde STDIN, por ejemplo:

```
	$ echo "Me gustan las nueces" | sed 's/nueces/avellanas/'
	Me gustan las avellanas
```

Debemos tener presente que el comando `s` no sustituye todas las ocurrencias de la palabra buscada (nueces). Para que esa búsqueda sea *global*, debemos hacer uso de un segundo comando; `g`:

	$ echo "Me gusta comer nueces y mas nueces" | sed 's/nueces/almendras/'
	Me gusta comer almendras y mas nueces
	$ echo "Me gusta comer nueces y mas nueces" | sed 's/nueces/almendras/g'
	Me gusta comer almendras y mas almendras

El comando `sed` también es capaz de procesar ficheros de texto.

```
$ cat frutos.txt 
A Miguel le gustan las nueces
A Juan solo le gustan los pasteles de nueces
A Tom no le gustan las nueces

$ sed 's/nueces/avellanas/' frutos.txt 
A Miguel le gustan las avellanas
A Juan solo le gustan los pasteles de avellanas
A Tom no le gustan las avellanas

$ cat frutos.txt                                    
A Miguel le gustan las nueces
A Juan solo le gustan los pasteles de nueces
A Tom no le gustan las nueces
```

De momento, hemos visto como modificar el contenido de las líneas del fichero de texto siguiendo el SCRIPT que se añade como opción al comando `sed`. A continuación, veremos como eliminar líneas del fichero que cumplan con un criterio dado.

	$ sed '/Juan/d' frutos.txt
	A Miguel le gustan las avellanas
	A Tom no le gustan las avellanas

En el siguiente ejemplo, veremos como modificar una línea completa. Usaremos la sintaxis *ADDRESScNEWTEXT*. Donde *ADDRESS* hace referencia al número de línea, y *NEWTEXT* es el texto que deseamos incorporar en dicha línea.

	$ sed '4cSoy una nueva línea' frutos.txt
	A Miguel le gustan las nueces
	A Juan solo le gustan los pasteles de nueces
	A Tom no le gustan las nueces
	Soy una nueva línea

Y por último, veremos como hacer uso de varios scripts en la misma línea de comandos:

	$ sed -e 's/nueces/almendras/ ; s/gustan/encantan/' frutos.txt
	A Miguel le encantan las almendras
	A Juan solo le encantan los pasteles de almendras
	A Tom no le encantan las almendras

## Comandos vistos en el tema

| Comando | Descripción |
| --- | --- |
| egrep | Busca líneas en ficheros que concuerdan con expresiones regulares; se permiten expresiones regulares extendidas. |
| fgrep | Busca líneas en ficheros con un contenido específico; no se permiten expresiones regulares. |
| grep | Busca líneas en ficheros que concuerdan con una expresión regular. |

## Resumen

* Las expresiones regulares son un potente método para describir conjuntos de cadenas de caracteres.
* `grep` y sus semejante buscan aquellas líneas que concuerdan con una expresión regular dentro del contenido de un fichero. 
