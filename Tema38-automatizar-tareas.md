# Tema 38 - Automatizar tareas

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Introducción](#introducción)
    - [at y batch](#at-y-batch)
- [Operaciones periódicas](#operaciones-periódicas)
    - [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Ejecutar comandos en el futuro con el comando `at`
* Ejecutar comandos de forma periódica usando `cron`

## Introducción

Un importante componente de la administración de un sistema consiste en la automatización de procedimientos repetitivos. Una tarea candidata podría ser que el servidor de correo interno en una empresa se conectara con el ISP periódicamente para comprobar si hay nuevos mensajes entrantes. Además, todos los miembros de un proyecto en grupo, podrían recibir un mensaje recordatorio media hora antes de celebrar una reunión. Tareas administrativas, como la comprobación del sistema de ficheros o backups del sistema puede resultar rentable ejecutarlas por las noches cuando la carga del sistema es mucho menor.

Para conseguir estos resultados, Linux ofrece dos servicios que se verán a continuación.

### at y batch 

Usando el servicio `at`, podemos ejecutar comandos una sola vez en el futuro. Si los comandos deben ser ejecutados repetidamente, es preferible usar cron.
La idea detrás del uso de `at` es especificar una hora a la cual el comando o secuencia de comandos deben ejecutarse. Como esto:

```
$ at 01:00
at> tar czvf /dev/st0 $HOME
at> echo "Backup done" | mail -s Backup $USER
at> CTRL+D
```

que realizaría un backup del directorio home a la primera unidad de cinta a la una de la madrugada, y luego envía un correo notificando su finalización.

El comando `atd` escanea el directorio `/var/spool/at` en busca de trabajos enviados a través del comando `at`. Por defecto, se realiza una comprobación cada 60 segundos, y si alguno de los trabajos debe ejecutarse, el comando `atd` lo inicia.

Tipo de referencia | Sintaxis
-- | --
Fija | HH:MM
| Noon
| Midnight
| Teatime
| MMDDYY o MM/DD/YY o MM.DD.YY
Relativa | now
| now + valor - `now + 5 minutes/hours/days`
| today - Se puede combinar `2 pm today`
| tomorrow - `2 pm tomorrow`

El argumento de `at` es la especificación de hora para el/los comando(s). Horas como **HH:MM** denotan la próxima vez que sea posible. Si el comando `at 14:00` se ejecuta a las 8AM, se refiere al mismo día, si es a las 4PM, será el día siguiente.

> Podemos hacer esas horas únicas, añadiendo today o tomorrow: "at 14:00 today", antes de las 2 PM, se refiere a hoy, "at 14:00 tomorrow", a mañana.

También se puede incluir la notación anglo-sajona, tales como **01:00am** o **14:20pm**, así como los nombres simbólicos **midnight** (12AM), **noon** (12PM), y **teatime** (4PM). Además de la hora, el comando `at` también interpreta fechas en el formato MMDDYY y MM/DD/YY, así como DD.MM.YY (para europeos).

También se admite "«nombre mes» «día»" y "«nombre mes» «día» «año»". Si le indicamos una fecha, será ejecutado ese día a la hora actual. Podemos combinar hora con fecha, pero primero la hora y luego la fecha.
```
$ at 00:00 January 1 2005
at> echo "Feliz año nuevo"
at> CTRL+D
```
Aparte de indicar de forma explícita la fecha y la hora, podemos indicar horas y fechas relativas, indicando un intervalo después de un momento dado:
```
$ at now + 5 minutes
```
mientras que,
```
$ at noon + 2 days
```

se refiere a las 12PM del día después de mañana (siempre que lo ejecutemos antes de las 12PM de hoy). 
`at` soporta las unidades:** minutes**, **hours**, **days** y **weeks**.

> No se permiten las combinaciones de unidades, por ejemplo:
>```
>$ at noon + 2 hours 30 minutes
>$ at noon + 2 hours + 30 minutes
>```
>por supuesto, siempre podremos indicar el intervalo en minutos.

`at` lee los comandos de su entrada estándar, con la opción `-f` (file) podemos especificar un fichero.

>`at` intenta ejecutar el comando en un entorno igual al actual siempre que sea posible. El directorio de trabajo actual, el umask, y las variables de entorno (excepto TERM, DISPLAY y _) son guardadas y reactivadas antes de ejecutar los comandos.

Cualquier salida de los comandos ejecutados por `at` -salida estándar y salida de error estándar- son enviadas al usuario por correo.

>Si hemos asumido el papel de otro usuario mediante el comando `su`, los comandos serán ejecutados usando esa identidad. Pero los correos de salida serán enviados hacia el usuario.

Mientras que podemos usar `at` para ejecutar comandos en un punto determinado del tiempo, el comando `batch` ejecuta una secuencia de comandos tan pronto como sea posible. Que dependerá de la carga del sistema, si el sistema está muy ocupado, entonces los trabajos _batch_ deben esperar.

>`batch` no puede utilizarse en entornos en donde los usuarios compiten por los recursos tales como el tiempo de CPU. Deben utilizarse otros sistemas.

El sistema añade los comandos enviados por `at` a una cola. Podemos ver el contenido de esa cola usando atq (cada usuario sólo verá sus propios trabajos, a no ser que seas root):
```	
$ atq
123	2003-11-08 01:00 a debian
124	2003-11-11 11:11 a debian
125	2003-11-08 21:05 a debian
```

>La letra «a» en la lista indica la «clase del trabajo», una letra entre «a» y «z». Podemos indicar la clase del trabajo usando la opción `-q` en `at`; los trabajos con letras _mayores_ son ejecutados con un valor `nice` más alto. Por defecto, se usa «a» para trabajos `at`, y «b» para trabajos _batch_.

>Un trabajo que está siendo ejecutado en ese momento, se denota con «=».

Podemos usar `atrm` para cancelar un trabajo. Debemos indicar el número de trabajo, que aparece una vez lanzado, o bien a través de la salida de `atq`. Si queremos ver lo que se ejecutará con ese trabajo podemos usar la opción `-c` con el número de trabajo.
El demonio `atd` es el encargado de ejecutar los trabajos. Se suele inicar en el arranque del sistema y espera en segundo plano por trabajo. Cuando se inicia, podemos indicarle varias opciones:

* **-b ("batch")** - Indica el intervalo mínimo entre dos ejecuciones de trabajos batch. Por defecto, son 60 segundos.
* **-l ("load")** - Indica un límite para la carga del sistema, por encima de la cual los trabajos _batch_ no serán ejecutados. El valor por defecto es 0.8
* **-d ("debug")** - Activa el modo «debug», los mensajes de error no se enviarán a _syslogd_, sino que se escribirán en la salida de error estándar.

Los ficheros `/etc/at.allow` y `/etc/at.deny` determinan quien puede enviar trabajos usando `at` y `batch`.
Si existe el fichero `/etc/at.allow` , sólo los usuarios en ese listado pueden enviar trabajos. Si no existe el fichero `/etc/at.allow`, los usuarios no listados en `/etc/at.deny` pueden enviar trabajos. 
Si no existe ninguno de los archivos, solo puede ejecutar `at` y `batch` el usuario `root`.

> Un típico fallo es un fichero `/etc/deny` vacío, por lo que cualquier usuario puede enviar trabajos.

## Operaciones periódicas

Al contrario que el comando `at`, el propósito del demonio `cron` es ejecutar trabajos en intervalos periódicos. `cron`, al igual que `atd`, debe ejecutarse durante el arranque del sistema usando un script de inicio. No debemos preocuparnos por ellos, porque son parte esencial de Linux y la mayoría de las distribuciones los incorporan.
Cada usuario tiene su propia lista de tareas (llamada _crontab_), que está almacenada en el directorio `/var/spool/cron/crontabs` bajo el nombre de usuario. Los comandos son ejecutados con los permisos del usuario.

>Los usuarios no tienen acceso directo a las listas de tareas en el directorio `crontabs`, por lo tanto debemos usar el comando `crontab`.

Los ficheros crontab son organizados por líneas; cada línea describe un punto en el tiempo (recurrente) y un comando que debe ser ejecutado en ese momento. 
Tanto las líneas vacías como los comentarios (#) serán ignorados. El resto de la línea está formado por cinco campos y el comando a ejecutar; los campos de tiempo describen el **minuto** (0-59), **hora** (0-23), **dia de mes** (1-31), **mes** (1-12, o nombre inglés) y **día de semana** (0-7, 0 es domingo, o nombre inglés), que indican cuando se ha de ejecutar el comando. 
Alternativamente, un asterisco está permitido, e indica «todo momento». Por ejemplo:

`58 17 * * * echo "Los noticias estan a punto de llegar"`

indica que el comando será ejecutado todos los días a las 17:58.

>El comando será ejecutado cuando la hora, minuto y mes coincidan exactamente, y al menos uno de los dos indicadores para el día -día de mes y de semana. Por ejemplo:

>`1 0 13 * 5 echo "Justo despues de la medianoche"`

>el mensaje se mostrará el 13 de cada mes, así como cada viernes, no justo cada viernes 13.

>El final de cada línea debe finalizar con un fina de línea, para que no sea ignorada.

En los campos de tiempo, `cron` no sólo acepta números simples, sino listas de comandos separados por coma. Por ejemplo, "0,30" en el campo de los minutos, indica que el comando debe ser ejecutado cada media hora.

También se pueden indicar rangos: "8-11" es equivalente a "8,9,10,11", "8-10,14-16", equivale a "8,9,10,14,15,16".
También se permite un salto en los rangos "0-59/10" es equivalente a "0,10,20,30,40,50".  "\*/10" de esta forma se ejecutará cada 10 minutos.
Los nombres permitidos en el mes y día de la semana consiste en las tres primeras letras de los nombres ingleses. (may, oct, sun o wed). No se permiten rangos ni listas.
El resto de la línea indica el comando a ejecutar, que será pasado por cron a `/bin/sh` (o a la shell especificada en la variable **SHELL**, que luego veremos)

>Los símbolos % en el comando deben ser 'escapados' por un backslash (\), para que no sean convertidas en caracteres de nueva línea. En ese caso, el comando es considerado que se extiende despues del primer símbolo %; las siguientes líneas serán enviadas al comando como entrada estándar.

>A propósito, como administrador si no desea que la ejecución de un comando sea controlada usando syslogd (que es lo que hace por defecto), podemos evitar esto colocando un carácter "-", como primer carácter de la línea.

También se pueden incluir asignación de valores a variables de entorno. De la forma `variable=valor` (donde, a diferencia de la shell puede haber espacios en blanco antes y despues del simbolo «=»). Si el valor contiene espacios debe ser encerrado entre comillas.

Las siguientes variables se configuran automáticamente:

* **SHELL**. Esta shell es usada para ejecutar los comandos. El valor por defecto es `/bin/sh`, pero se admiten otras shells.
* **LOGNAME**. El nombre de usuario es tomado de `/etc/passwd` y no puede ser modificado.
* **HOME**. El directorio `home` también es tomado de `/etc/passwd`. Sin embargo, sí podemos cambiar su valor.
* **MAILTO**. cron envía correos con la salida de los comandos a esta dirección de correo (por defecto, van al propietario del fichero crontab). Si cron no debe enviar mensajes a nadie, la variable tendrá asignado un valor nulo (MAILTO=””).

Además de las listas de tareas específicas de los usuarios, existen también listas de tareas del sistema. 
Estas residen en `/etc/crontab` y pertenecen a `root`, que es el único usuario con permisos para cambiarlo. La sintaxis de este fichero es algo diferente a los ficheros específicos de los usuarios; entre los campos del tiempo y el comando está el nombre del usuario con cuyos privilegios debe ejecutarse el comando.

>Algunas distribuciones incorporan un directorio `/etc/cron.d`, que puede contener ficheros que son considerados 'extensiones' de `/etc/crontab`. Paquetes de software instalados via el gestor de paquetes encuentran así un modo sencillo de usar cron sin tener que añadir o eliminar lineas en `/etc/crontab`.

>Otra extensión popular son los ficheros llamados `/etc/cron.hourly`, `/etc/cron.daily` y demás. En eses directorios, los paquetes software (o el administrador) puede guardar ficheros cuyo contenido será ejecutado cada hora, día, etc. Eses ficheros suelen ser scripts de shell en lugar de ficheros del estilo crontab.

`cron` lee las diferentes listas de tareas, una vez al comienzo y luego las almacena en memoria. Sin embargo, el programa comprueba cada minuto si algún fichero crontab ha cambiado. El 'mtime', last-modification time, es usado para esto. Si `cron` detecta alguna modificación, la lista de tareas es reconstruida automáticamente, en este caso, no es preciso un reinico del demonio.

Los usuarios que pueden trabajar con `cron`, también están listados en dos ficheros (de forma similar a at). `/etc/cron.allow` y `/etc/cron.deny`. 
Si no existe el primero (`allow`), pero si el `deny`, entonces el fichero `deny` almacena los usuarios que no pueden ejecutar trabajos. Si no existe ninguno de los ficheros, dependerá de la configuración si sólo tiene permiso el root, o por el contrario, todo el mundo tiene permiso.

Los usuarios individuales no pueden modificar sus ficheros crontab manualmente, porque el sistema les oculta esos ficheros. Sólo la lista de tareas del sistema `/etc/crontab` puede ser editado por root.
En lugar de utilizar un editor, los usuarios pueden usar el comando `crontab`. Esto les permite crear, inspeccionar, modificar y eliminar listas de tareas.

`$ crontab -e`

podemos editar el fichero crontab usando el editor por defecto. Después de cerrar el editor, el fichero crontab es automáticamente instalado. Además de la opción `-e`, también podemos indicar el nombre de un fichero cuyo contenido será instalado como una lista de tarea. El nombre de fichero `-`, se interpreta como entrada estándar.
Con la opción `-l`, crontab envía el fichero crontab hacia su salida estándar; con la opción `-r`, borramos las tareas existentes.

>Con la opción `-u` (user name), nos podemos referir a otro usuario (siendo `root` para hacerlo, claro). Esto es importante si estamos haciendo uso del comando `su`; en este caso debemos usar la opción `-u` para asegurarnos de que estamos trabajando con el fichero correcto.

### Comandos vistos en el tema

Comando | Descripción
--- | ---
at | Registra comandos para ejecutar en un momento del futuro
atd | Demonio que ejecuta comandos en el futuro, usando at
atq | Consulta la cola de comandos que serán ejecutados en el futuro
atrm | Cancela comandos que serían ejecutados en el futuro
batch | Ejecuta comandos tan pronto como la carga del sistema lo permita
crontab | Gestiona los comandos que se ejecutarán en intervalos regulares
