# Tema 43 - Introducción a GnuPG

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Criptografía asimétrica y el Web of Trust](#criptografía-asimétrica-y-el-web-of-trust)
- [Generando y gestionando llaves públicas](#generando-y-gestionando-llaves-públicas)
- [Cifrando y descifrando datos](#cifrando-y-descifrando-datos)
- [Firmar ficheros y verificar firmas](#firmar-ficheros-y-verificar-firmas)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Comprender GnuPG
* Generar y gestionar claves GnuPG
* Cifrar y descifrar ficheros

## Criptografía asimétrica y el Web of Trust

GnuPG permite cifrar y firmar ficheros y mensajes de correo electrónico. Un fichero cifrado es un fichero que contiene los datos modificados, de tal forma que sólo las personas autorizadas pueden verlo o procesarlo. Las personas autorizadas tienen que descifrarlo con la llave correcta para acceder a él. Mediante la firma de datos, podemos documentar el hecho de que somos nosotros los que hemos creado o aprobado esa información. Si los datos son modificados más tarde, la firma se vuelve inválida. Las firmas GnuPG están incluidas como parte de los paquetes *apt* o *rpm*, y juegan un papel muy importante en la verificación de dichos paquetes.

> El cifrado usado por GnuPG no es fácil de romper. Incluso las agencias de inteligencia más modernas carecen de la potencia de computación necesaria para descifrar un fichero de texto plano sin conocer la clave. Esta es la razón por la que en varios paises el uso de software como GnuPG es ilegal. Un país que reguló el uso de este tipo de software fué Francia, que en el año 1996, lo prohibió, aunque hoy en día ya se ha levantado dicha prohibición.

> Está en nuestras manos evaluar las firmas digitales de GnuPG. Los desarrolladores de paquetes Debian GNU/Linux firman sus paquetes antes de subirlos a los servidores FTP, donde quedan accesibles para el público. La firma no nos dice nada acerca de la calidad del paquete. Sólo se hace para asegurarse de que no se han introducido paquetes con software malicioso por terceras partes.

Para poder explicar los principios que están detrás de GnuPG, debemos hacer un poco de historia. Existen dos métodos básicos de criptografía. El primero de ellos es la llamada «criptografía simétrica». Usa la misma llave para cifrar y descrifrar los datos. Cuando Alice y Bob desean intercambiar información privada usando el método simétrico, han de crear primero una llave. La misma llave ha de ser conocida por ambos. Cuando Charlie quiere unirse, se precisan tres llaves: una para la comunicación entre Alice y Bob, una para Alice y Charlie, y otra para Bob y Charlie. Cuatro participantes necesitan seis llaves, diez, 55 llaves, 100, 5050 llaves, etc. Todas las llaves son secretas, por lo que se han de distribuir de tal forma que un tercero no las pueda obtener.

El principal problema de la criptografía simétrica radica en la distribución de la llave. En primer lugar, las llaves han de ser transmitidas a través de un medio seguro, por lo que debemos usar la criptografía para distribuirlas, y en segundo lugar, con el número de potenciales usuarios en Internet, el número de llaves necesarias alcanzaría cifras astronómicas, con los problemas de almacenamiento consiguientes. Ambos problemas se resuelven mediante la «criptografía asimétrica».

La criptografía asimétrica usa diferentes llaves para cifrar y descifrar los datos. Un conjunto de llaves asimétricas se llama un *par de llaves*. Cada par de llaves está formado por una llave pública y una llave privada. La llave pública puede ser conocida por cualquiera, mientras que la privada debe mantenerse en secreto.

Ahora, Alice puede enviar un mensaje privado a Bob cifrándolo con la llave pública de Bob. Bob, entonces, usará su llave privada para descifrar el mensaje. Debido a que sólo Bob conoce su llave privada, el mensaje se mantiene seguro durante la transimisión.

> Hemos ignorado algunos detalles. El más importante es que todos los métodos de cifrado asimétrico conocidos se vuelven muy lentos e incómodos para transferir grandes cantidades de información. Esta es la razón por la cual a menudo se usan los métodos asimétricos para intercambiar las llaves que se usarán en un modelo simétrico. (La criptografía simétrica es más rápida y eficiente, y puede manejar grandes cantidades de datos).

> Un ejemplo concreto: Cuando Alice quiere comunicarse con Bob de forma confidencial, primero genera una llave aleatoria para el método simétrico, cifra esta llave con la llave pública de Bob, y envía el resultado a Bob. Bob usará su llave privada para obtener la llave de Alice. En este momento Alice y Bob puden usar la clave simétrica generada por Alice para intercambiar información confidencial.

Las firmas trabajan de otra manera. Alice firma un fichero cifrándolo con su llave privada. Bob puede verificar que Alice ha firmado el fichero, descifrándolo con la llave pública de Alice (que es fácil de obtener, porque es pública).

> En la práctica, no cifraremos el fichero usando un método asimétrico, porque, como dijimos, es un método lento e incómodo. En su lugar, debemos generar un «resumen del mensaje cifrado» del fichero y firmarlo. Los métodos más frecuentes en informática son MD5 y SHA1. Usando un resumen es tan seguro como cifrar el fichero completo, porque si el fichero cambia, el resumen también lo hará.

La criptografía asimétrica soluciona el problema de la distribución de la llave, pero introduce otro problema. ¿Cómo sabe Bob que la llave que usa para verificar la firma de Alice es realmente la llave pública de Alice?. Nos hace falta un método para verificar la autenticidad de las llaves públicas.

Las infraestructuras SSL y TLS, usadas, por ejemplo, por las webs seguras, solucionan este problema mediante la introducción de una jerarquía de autoridades certificadoras. Toda la construcción se denomina «public key infrastructure» (PKI). Las autoridades de certificación aseguran que una llave determinada pertenece a cierta persona, y da fe de esta afirmación mediante una firma digital. Un conjunto de una llave pública, el propietario de la firma digital, y la firma y nombre de la autoridad certificadora, se denomina un *certificado*. Como usuario puedes verificar la firma del certificado usando la llave pública de la autoridad de certificación (que podemos obtener de una fuente de confianza -normalmente, los más importantes vienen con el navegador).

Cuando los detalles técnicos se han dejado atrás, la única duda que puede rodear a la confianza que depositemos en la firma se reduce a fiarnos de los empleados, y al trabajo de la autoridad de certificación. *GnuPG* no usa la estructura jerárquica aquí explicada, sino que usa un método que es conocido como «web of trust» (anillo de confianza). No hay autoridades de certificación central en la que todos los usuarios deben confiar por obligación. En su lugar, se asume que los usuarios de *GnuPG* firman con la llave pública de la gente a la que conocen personalmente, de este modo se comprueba que la llave pertenece a dicha persona. Si queremos enviar un mensaje a Charlie, a quien no conoces en persona. En algún lugar de las profundidades de Internet, obtenemos una llave GnuPG pública que pertenece a Charlie.

El hecho de que confíes o no en esta llave depende de si puedes formar una «cadena de confianza» entre tu y la llave en cuestión. Quizás tu hayas firmado con la llave pública de Alice, y Alice ha usado su llave pública para firmar la llave pública de Bob. Bob, a su vez, puede ser un viejo amigo de Charlie, y por lo tanto ha firmado la llave pública de Charlie. En otras palabras, la firma de Bob sobre la llave de Charlie, dice: «Esta llave pertenece a Charlie», por lo que si la firma de Bob es correcta, entonces todo está correcto. La firma de Bob sobre la llave de Charlie puede ser verificada usando la llave pública de Bob, y la autenticidad de la llave pública está comprobada a través de la firma de Alice sobre la llave de Bob. Y por último, la firma de Alice, debe ser verificada con su llave pública. Cuando todas las firmas de un anillo de confianza son válidas, podemos verficar que la llave de Charlie pertenece a Charlie.

> En la práctica, el método descrito es un poco ingenuo. Esto es porque *GnuPG* soporta el concepto llamado confianza en el propietario: para cada llave pública que tenemos disponible podemos recordar el grado de herencia que sus propietarios aplican cuando están usando llaves *GnuPG*. Los rangos van desde «full» (su firma en una clave es tan buena como la nuestra), «marginal» (el propietario usa sus llaves de forma cuidadosa), y «none» (el propietario usa sus claves de forma descuidada).

GnuPG considera una llave pública válida, si:
1. La cadena de confianza que te conecta hasta el propietario de la llave no tiene más de 5 enlaces, y 
2. ha sido firmada por un número suficiente de llaves que tu consideras fiables. Dicho de otra manera:
  * has firmado la llave tu mismo, o
  * está firmada por una llave «full», o
  * está firmada por tres llaves «marginal»

Cuando un usuario quiere tomar parte en el anillo de confianza, debe acatar una serie de reglas. La más importante, que debe firmar las llaves de otras personas si tiene la seguridad absoluta acerca de su identidad. Cuando haya dudas, debe requerir de la persona alguna prueba adicional, como puede ser una foto (o dos). **Nunca se deben firmar llaves basándose en rumores**.

## Generando y gestionando llaves públicas

**Generando un par de llaves**. En principio, no necesitas tu propio par de llaves *GnuPG* para enviar mensajes cifrados a otra gente o verificar sus firmas. Todo lo que tienes que hacer es obtener la llave pública en cuestión. Sin embargo, una comprobación correcta de la firma indica que el fichero no ha sido modificado, pero para verificar que la firma ha sido generada por la persona que dice ser, debemos tomar parte en el anillo de confianza. Es el único modo de asegurarnos que la llave es válida de acuerdo con las reglas descritas antes. Por lo que el primer paso para usar *GnuPG* es la generación de tu propio par de llaves. Para generar un par de claves, debemos ejecutar el programa `gpg`:

	$ gpg --gen-key

La opción por defecto, «DSA and Elgamal» genera dos pares de llaves: un par de llaves DSA para firmar datos y un par de llaves Elgamal para cifrar y descifrar datos. 2048 bits es un valor razonable en la actualidad. 1024 es un valor quizás algo escaso, y 4096, algo paranoico.

> Cuando se usa *GnuPG* para comprobar la integridad de un paquete de nuestra distribución, el distribuidor normalmente envía un conjunto de llaves públicas para usar en la verificación. Cuando esas llaves están dentro de un CD-ROM que hemos comprado en un kiosko, es una buena señal de que esas llaves son fiables. Con los CD-ROM's *quemado*' por colegas, las cosas son diferentes.

A continuación, debemos asignar el tiempo de vida de la llave. Los usuarios privados pueden escoger la opción «keys does not expire». El siguiente paso es asignar un «User ID» a tu llave. Este ID consiste en un nombre, una dirección de correo electrónico y opcionalmente, un comentario. Finalmente, debes especificar una contraseña, que será usada para cifrar la llave privada. La mejor elección es una frase larga sin significado aparente. Llegados a este punto, tenemos casi finalizado el trabajo. Debemos considerar la creación de un *certificado de revocación*, que podemos usar para declarar nuestra llave pública inválida en caso de que nuestra clave privada se vea comprometida. Un certificado de revocación, lo creamos así:

	$ gpg --output revoke.asc --gen-revoke “Linus Torvalds”

Después de aceptar la revocación, e indicar un motivo de tal hecho, debemos introducir la contraseña, con lo que quedará almacenado el certificado en el fichero `revoke.asc`. Este certificado debe ser guardado en un medio seguro, y a ser posible, fuera del ordenador. Es una buena idea, guardar también una copia impresa.

> El certificado de revocación generado aquí está creado para usar en el peor de los casos, esto es, en caso de que la llave se convierta en inutilizable. Si decidimos realizar la revocación por una causa diferente, tenemos la libertad de volver a generar dicho certificado de forma que refleje mejor la nueva situación.

**Publicar una llave pública**. El próximo paso es publicar tu llave pública para que esté disponible para nuestros amigos, familiares, etc. Para ello, en primer lugar debemos *exportar* la llave. La opción `--list-keys` hace que *GnuPG* liste todas las claves públicas que se pueden usar:

	$ gpg --list-keys

Tu llave pública se exporta con la opción `--export` de *GnuPG*. Al igual que en el comando de creación del certificado de revocación, la opción `--output` se puede usar para indicar un fichero en el cual escribir. La opción `--armor` genera que la salida se realice en formato ASCII, para ser enviada por correo, o publicada en un sitio web:

	$ gpg --output linus.gpg --armor --export “Linus Torvalds”
	$ cat linus.gpg

> Una forma más eficiente de publicar tu llave pública es subirla a un servidor de llaves. Lo podemos realizar con la ayuda del programa `gpg`:<br>
`$ gpg --keyserver hkp://subkeys.pgp.net --send-keys 7AA07E27`
<br>La mayoría de los servidores sincronizan sus datos, de forma que es suficiente con subirla a uno de ellos. El ID mágico **7AA07E27** es el «key ID», que puede encontrarse en la salida de `gpg --list-keys`.

**Importando y firmando llaves públicas**. Cuando encontramos una llave pública en un correo electrónico o en un sitio web, debemos importarla en nuestro «anillo de llaves» antes de poder usarla.

	$ gpg --import /tmp/linus.gpg
	$ gpg --list-keys

Ahora, la nueva llave forma parte del anillo, pero ¿cómo sabemos que realmente es la llave pública de Linus Torvalds?

La mejor forma de hacerlo es generar la *huella* y preguntarle personalmente a Linus si es correcta:

	$ gpg --fingerprint “Linus Torvalds”

Una vez verificada la autenticidad de la llave, podemos firmala:

	$ gpg --sign-key “Linus Torvalds”

Las firmas sobre una llave pueden ser listadas y comprobadas al mismo tiempo:

	$ gpg --check-sigs “Linus Torvalds”

La exclamación indica que la firma se ha verificado correctamente. Cuando una firma es *mala*, aparece un signo «-».

Para gestionar las llaves, debemos usar la opción `--edit-key`:

	$ gpg --edit-key “Linus Torvalds”

Ahora podemos introducir diferentes comandos, `fpr` muestra la *huella* de la llave, y `sign` firma una llave. Hay otros comandos, con `help` podremos ver un resumen. Un comando importante es `trust`, que cambia el nivel de confianza. La diferencia entre «totalmente» (fully) y «absolutamente» (ultimately), está entre nosotros y el resto de personas. El resto de personas tendrá confianza «total», y nosotros mismos, «absoluta». Si deseamos revocar nuestra llave pública, tenemos que importar el certificado de revocación a nuestro anillo:

	$ gpg --import revoke.asc

Puedes enviar la llave revocada a los servidores habituales.

## Cifrando y descifrando datos

Para cifrar un fichero usando *GnuPG*, necesitamos la llave pública del destinatario del fichero. Una llave pública es como una caja fuerte. Ciframos los datos colocándolos dentro de la caja, cerrando la puerta y girando la rueda con la combinación de bloqueo. La única persona que puede acceder a los datos es aquella que posee la combinación de la caja. La llave privada es esa combinación necesaria para la apertura. Por lo tanto, si queremos enviarle datos sensibles a Jane, necesitamos su llave pública.

	$ gpg --output text.txt.gpg --encrypt --recipient “Jane” text.txt

Este comando cifra el fichero `text.txt`, y la almacena en `text.txt.gpg`.

> Podemos cifrar un mensaje para varios destinatarios, tan sólo debemos indicar varias opciones `--recipient`. Hay que tener en cuenta que se guardan todas las copias cifradas en el fichero.

Ahora Jane puede descifrar el fichero usando su llave privada:

	jane$ gpg --output text.txt --decrypt text.txt.gpg

Si lo que deseamos es cifrar un fichero para guardarlo nosotros, no necesitamos un cifrado asimétrico. Se usa la misma contraseña para descifrar:

	$ echo “Hola mundo !” | gpg --output hw.gpg --symmetric
	$ gpg --decrypt hw.gpg

## Firmar ficheros y verificar firmas

Podemos usar nuestro par de claves para firmar un fichero. La firma de un fichero marca el contenido del fichero con un sello de tiempo y un resumen. Cuando el fichero es modificado después de firmarlo, la verificación de la firma fallará. Este método se usa para la firma de los paquetes software, de tal forma que se evitan modificaciones que podrían introducir malware. Para firmar un fichero con *GnuPG*:

	$ gpg --output text.txt.sig --sign text.txt

El fichero firmado será comprimido, y escrito en formato binario. Podemos comprobar la firma:

	$ gpg --verify text.txt.sig

para descifrarlo:

	$ gpg --decrypt text.txt.sig

Como hicimos antes, podemos usar la opción `--output` para enviar el resultado hacia un fichero. El formato comprimido no es practicable para usar en los mensajes de correo. En este caso debemos usar la opción `--clearsign`. El fichero resultante contiene tanto los datos firmados como la firma en formato ASCII:

	$ gpg --clearsign text.txt

## Comandos vistos en el tema

| Comando | Descripción |
| --- | --- |
| gpg | Cifra y firma ficheros |
