# Tema 34 - Las Shells

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Shells y scripts de shell](#shells-y-scripts-de-shell)
- [Tipos de shells](#tipos-de-shells)
- [La Shell Bourne-Again](#la-shell-bourne-again)
- [Shells de login y Shells interactivas](#shells-de-login-y-shells-interactivas)
- [Cambios permanentes en la configuración](#cambios-permanentes-en-la-configuración)
- [Mapas de teclado y métodos abreviados](#mapas-de-teclado-y-métodos-abreviados)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Aprender los conceptos básicos de las shells y los scripts
* Ser capaz de distinguir entre los diferentes tipos de shells

## Shells y scripts de shell

La shell te permite interactuar directamente con un sistema Linux. Puedes escribir comandos que serán evaluados y ejecutados por la shell -normalmente a través de la ejecución de programas externos. A la shell también se la conoce como un *intérprete de comandos*.
Además, la mayoría de las shells incluyen características de lenguaje de programación -variables, estructuras de control tales como condicionales, bucles, funciones, etc. 
También se pueden utilizar secuencias complejas de comandos de shell y llamadas a programas externos en ficheros de texto e interpretarlos como scripts de shell. Esto hace más fácil la ejecución de tareas complejas y repetitivas.
Linux utiliza scripts de shell para muchas tareas internas. Por ejemplo, los scripts de init en `/etc/init.rd`, suelen estar implementados como scripts de shell. Esto también afecta a muchos comandos del sistema, ya que Linux puede ocultar el hecho de que un programa de sistema no sea realmente un código máquina ejecutable directamente, sino que sea un script de shell de sus usuarios, al menos a lo que a la llamada concierne -para asegurarnos podemos hacer uso del comando file.

```
$ file /bin/ls
$ file /bin/zgrep
```

## Tipos de shells

La shell más usada en Linux es la bash del proyecto GNU. Es ampliamente compatible con la primera shell de la historia de UNIX (Bourne shell) y con el estándar POSIX. También podemos usar otras shells, tales como la shell de C, la Tenex C shell (tcsh), la shell Korn (ksh) y algunas otras. Existen diferencias entre ellas, tanto en sus características como en la sintaxis.
> Existen dos grandes grupos de shell procedentes de diferentes escuelas, e incompatibles entre ellas, la shells tipo Bourne, y las shells tipo C. La shell Bourne-Again intenta integrar las características más importantes de las C.

Podemos decidirnos por el uso de cualquiera de esas shells, incluso podemos usar alguna de ellas tan sólo con un propósito puntual en un momento dado. Recordar que la shell de login está situada en el fichero `/etc/passwd`. Podemos modificar la shell de login usando el comando `chsh`:

```
$ getent passwd tux1
$ chsh
$ getent passwd tux1
$ chsh -s /bin/csh
$ getent passwd tux
```

> Con `chsh` puede seleccionar alguna de las shells que están listadas en `/etc/shells`, que en principio contiene todas las shells instaladas en el sistema, aunque en algunas distribuciones contiene una lista de todas las shells disponibles, aunque no estén instaladas.

Podemos saber qué shell estamos utilizando en este momento, examinando el valor de la variable `$0`.

```
$ echo $0
```

## La Shell Bourne-Again

Fue creada dentro del proyecto GNU por Brian Fox y Chet Ramey e incluye características de Korn shell y C shell.
Puntos importantes:
* **Variables**. Como la mayoría de las shells, *bash* soporta el uso de variables que pueden ser configuradas y luego reutilizadas. También se usan para configurar diversos aspectos del funcionamiento de la shell, por ejemplo, PATH, PS1...

* **Alias**. El comando `alias`, permite abreviar una secuencia larga de comandos. Por ejemplo:

```
$ alias c='cal -m; date'
$ type c
```

Podemos ver todos los alias definidos haciendo uso del comando sin argumentos, y con `unalias` eliminamos alguno de los alias creados (o todos, con la opción `-a`).

* **Funciones**. Si necesitamos realizar más operaciones que un simple reemplazo como con alias, debemos usar funciones.

* **Comando set**. No sólo muestra todas las variables de la shell, sino que puede modificar las opciones de bash. Por ejemplo, con `set -C` (`set -o noclobber`), previene la sobreescritura de un fichero a través de la redirección de la salida.
Con `set -x` (o `set -o xtrace`) se pueden ver los pasos que toma la shell para conseguir un resultado:

```
$ set -x
$ echo "Mi $HOME es mi castillo"
$ ls -l $(cat /etc/shells)
```
Si en lugar de `-`, usamos `+`, la opción se desactivará (`set +x`).

## Shells de login y Shells interactivas

Las shells pueden ser iniciadas de tres formas diferentes: 

1. shells de login
2. shells interactivas
3. no interactivas

Se diferencian en los archivos de configuración que leen.

* **Shell de login**. Esta shell se obtiene después de realizar login en el sistema. El programa que inicia la shell (`login, su -, ssh...`), le pasa a la shell el caracter `-` como el primero de su nombre de programa. Podemos comprobar si nuestra shell es de login:

```
$ echo $0
-sh
$ bash
$ echo $0
bash
```

De forma alternativa podríamos invocar a la shell usando la opción `-l`, para simular el comportamiento de una shell de login. Las shell tipo Bourne (no sólo bash) ejecutan en primer lugar los comandos en el fichero `/etc/profile`. Esto establece valores globales del sistema, por ejemplo variables de entorno, o umask. Después de esto, la bash busca los ficheros `.bash_profile`, `.bash_login` y `.profile` en el directorio home del usuario. En la mayoría de las distribuciones solo se ejecuta el primero que se encuentre. Si salimos de una shell de login, se procesará el fichero `.bash_logout` del directorio home (si existe).

* **Shell interactiva**. Si ejecutamos bash sin argumentos de nombre de fichero (pero posiblemente con opciones) y su entrada y salida estándar están conectados a un terminal, se define como una shell interactiva. En el momento de iniciar la ejecución, lee el fichero `/etc/bash.bashrc` así como el fichero `.bashrc` del directorio HOME y ejecuta los comandos contenidos dentro de ambos. Cuando salimos de una shell interactiva no se procesa ningún fichero.
	
* **Shell no interactiva**. Una shell no interactiva no procesa ningún fichero ni al comienzo ni al final. Es no interactiva cuando la usamos para ejecutar un script, o si un programa usa una shell para ejecutar otro programa. Esa es la razón de porque comandos como este:
`$ find -exec ll {} \;`
no se ejecutan correctamente, porque `find` inicia una shell para ejecutar el comando `ll`. Aunque `ll` está disponible en muchos sistemas, es tan solo un alias de `ls -l`. Y debe ser definido en cada shell, ya que los alias no los heredan los procesos hijo. Y estas shells no leen ningún fichero de configuración de donde poder activar los alias.

La separación entre las shells de login y las interactivas implica que las opciones de configuración que deseamos aplicar en nuestras shells debemos escribirlas en `.profile` y en `.bashrc`, para que sean efectivas en ambas shells.
Para eliminar este trabajo duplicado, algunas distribuciones tienen en el fichero `.profile` la siguiente línea:	
`test -r ~/.bashrc && . ~/.bashrc`

## Cambios permanentes en la configuración

¿Dónde debemos realizar los cambios para que sean permanentes de los ficheros que hemos visto? ¿En qué fichero debemos realizar los cambios?. Por ejemplo, en el caso de los alias, deben ser configurados de forma individual, por lo que los debemos añadir al fichero `~/.bashrc` (y para que funcionen en las shell de login, también debemos introducirlas en el fichero `~/.profile`)
Otras candidatas para estar en `./bashrc` son las variables que controlan el comportamiento de la shell (PS1, HISTSIZE, etc.), pero que no modifican el entorno en cuanto a la ejecución de los programas, digamos que tan sólo el aspecto. Si queremos iniciar una shell *fresca* de cada vez, podemos resetear estas variables desde el fichero `.bashrc`.

Otro caso diferente es el de las variables de entorno. Normalmente se configuran una sola vez, como la configuración de teclado. Basta con definirlas en el fichero `.profile`.
	
`PATH=$PATH:$HOME/bin` no debería estar en `.bashrc`, ya que la variable podría crecer de forma descontrolada si se ejecuta cada vez que se invoca una shell interactiva.

> Cuando trabajamos con entorno de ventanas, la configuración reside en el fichero `~/.xsession`, o lee el `.profile` desde éste.

**Cambios en el sistema completo**. Como administrador podemos aplicar cambios a todos los usuarios, modificando los ficheros `/etc/profile` y `/etc/bash.bashrc`.	Otro modo de cambios globales es a través del directorio `/etc/skel`. Si colocamos en este directorio un `.bashrc` una linea como la siguiente:
`test -r /etc/bash.local && . /etc/bash.local`
podremos realizar cambios en el fichero `/etc/bash.local` que afectarán a todos los usuarios.

## Mapas de teclado y métodos abreviados

La Shell Bash utiliza varios métodos abreviados para habilitar la edición de la línea de comandos y acceso a características especiales. Las personalizaciones son posibles a través del fichero `.inputrc`, presente en el directorio *home*, así como en el `/etc/inputrc` para todos los usuarios. Este es el fichero de configuración para la librería *readline*, que utiliza bash. Por ejemplo, la librería readline permite la búsqueda en la historia de la línea de comandos con CTRL+r.
Si colocamos estas líneas:

```
Control-t:	tab-insert
Control-e:	“cal\C-m”
```

en el fichero `.inputrc`, cuando pulsemos `CTRL-t` se insertará un carácter tabulador. Con `CTRL+e` inicia una macro.

## Comandos vistos en el tema

Comando | Descripción
--- | ---
bash | La 'Bourne-Again Shell'
chsh | Cambia la shell de login del usuario
file | Averigua el tipo de contenido de un fichero, siguiendo unas reglas
