# Tema 41 - La shell segura

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Introducción](#introducción)
- [Iniciando sesión en hosts remotos usando ssh](#iniciando-sesión-en-hosts-remotos-usando-ssh)
- [Ficheros de configuración](#ficheros-de-configuración)
- [Otras aplicaciones útiles: scp y sftp](#otras-aplicaciones-útiles-scp-y-sftp)
- [Autenticación cliente con clave pública](#autenticación-cliente-con-clave-pública)
- [Autenticación a través de un agente](#autenticación-a-través-de-un-agente)
- [Tunneling](#tunneling)
- [Securizando SSH](#securizando-ssh)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos

* Aprender a usar y configurar la Shell Segura (SSH)

## Introducción

SSH («Secure Shell») es un protocolo de red basado en TCP/IP, que utiliza un par de claves (público/privada) para el cifrado de la información. Permite la transmisión de datos a través de una red pública usando autenticación y cifrado. Sus aplicaciones incluyen, sesiones interactivas, transferencia de ficheros, y la redirección segura de otros protocolos («tunneling»). OpenSSH, que se integra dentro de la mayoría de las distribuciones Linux, es una implementación libre de este protocolo. Contiene algunos clientes SSH, así como un servidor SSH (`sshd`).

> El cifrado es importante para evitar que usuarios no autorizados puedan escuchar en la red, y ser capaces de leer el contenido de la transferencia. La autenticación se asegura, por un lado, como usuario estás hablando con el servidor correcto, y por otro lado, el servidor te permite el acceso a la cuenta correcta.

Usado adecuadamente, SSH puede prevenir los siguientes ataques:
* «DNS spoofing», entradas DNS falsas o adulteradas
* «IP spoofing», donde un atacante envía datagramas desde un host y pretende engañar con que vienen desde otro host.
* «Sniffing» de contraseñas y contenido transmitido en claro entre los hosts presentes en una ruta.
* Manipulación de los datos transmitidos por los hosts a lo largo de la ruta de transmisión.
* Ataques al servidor X11 mediante datos de autenticación no válidos y conexiones falseadas al servidor X11.

> SSH ofrece una sustitución completa para los protocolos inseguros *Telnet*, *Rlogin* y *RSH*. Además, permite que los usuarios copien ficheros desde o hacia los host remotos, sustituyendo a *RCP* y muchas aplicaciones *FTP*.

## Iniciando sesión en hosts remotos usando ssh

Para iniciar sesión en un host usando SSH, ejecutamos el comando `ssh`, por ejemplo:

	$ ssh blue.example.com

`ssh` asume que tu nombre de usuario en el host remoto es el mismo que en el host local. De no ser así, debes indicarlo de la siguiente forma:

	$ ssh usuario@blue.example.com

Para establecer la conexión se llevan a cabo los siguientes pasos:
* Cliente y servidor se envían información acerca de sus llaves, esquemas criptográficos soportados, etc. El cliente comprueba si la clave pública del servidor es la misma que ha estado utilizando, y negocia una clave secreta compartida con el servidor, que usarán para cifrar la conexión. Al mismo tiempo el cliente comprueba la autenticidad del servidor y rompe la conexión si existe alguna duda.
* El servidor comprueba la autenticidad del cliente usando uno de los diferentes métodos (en este caso pregunta por una contraseña). La contraseña es enviada a través de la conexión cifrada y, a diferencia de otros protocolos como FTP o TELNET, no puede ser escuchada por intrusos.

El primer paso es muy importante. El siguiente ejemplo muestra lo que ocurre cuando contactamos con el host remoto por primera vez:

	$ ssh blue.example.com
	The authenticity of host 'blue.example.com (192.168.33.2)' can't be established.
	RSA key fingerprint is 81:24:bf:3b:29:b8:f3:f9:46:57:18:1b:e8:40:5a:09.
	Are you sure you want to continue connecting (yes/no)? _
	
Como el host remoto es desconocido, `ssh` solicita verificar la clave del host. Este paso hay que tomarlo muy en serio. Si obviamos este paso, perdemos la garantía de que nadie esté escuchando en nuestra conexión.

> El peligro está en que alguien intercepte nuestra petición de conexión y pretenda suplantar al host remoto. El atacante puede establecer su propia conexión con el host remoto, y filtrar los envíos que hagamos hacia el host remoto, y por supuesto, todas las respuestas dirigidas hacia nosotros. No notaremos ninguna diferencia, pero el atacante puede leer todo lo que transmitamos. Este ataque se llama «man-in-the-middle».

> Para realizar la comprobación, deberíamos telefonear al administrador del sistema y verificar la clave pública del host remoto. Se puede visualizar a través del comando: `ssh-keygen -l`, y debe ser idéntica a la mostrada por el comando `ssh`.

> El par de claves SSH de un host se pueden encontrar en los ficheros `ssh_host_rsa_key` y `ssh_host_rsa_key.pub`, así como en `ssh_host_dsa_key` y `ssh_host_dsa_key.pub` en el directorio `/etc/ssh`.

> Un par de claves, corresponden a las claves privada y pública. La pública se la puede enviar a todo el mundo, y la privada permanece confidencial. Lo que se cifra con la clave pública sólo puede descifrarse con la privada y viceversa.

Si la clave pública del host remoto es auténtica, respondemos a la pregunta con «yes». Entonces `ssh` almacena la clave pública en el fichero `~/.ssh/known_hosts` para usarlo como base de comparación para futuras conexiones.

OpenSSH guarda la información de las conexiones realizadas en el fichero `~/.ssh/known_hosts`. En este fichero se almacenan las llaves públicas de los servidores remotos. Si en conexiones futuras a alguno de estos servidores obtenemos un mensaje del tipo `WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED`, debemos atenderlo con precaución ya que el equipo remoto pudo ser víctima de un ataque.

En el fichero `/etc/ssh/ssh_config` podemos modificar el valor de la clave `StrictHostKeyChecking` a los siguientes valores:
* **ask** (por defecto)
* **yes** (no acepta ningún host nuevo, debemos tener su clave pública almacenada)
* **no** (acepta todo)

Una vez establecida la conexión, podemos usar el host remoto como si estuvieramos sentados en frente del mismo, y podemos finalizar la conexión usando el comando `exit` o CTRL+D.

A menos que especifiquemos otra cosa, durante las sesiones *ssh* interactivas la tilde (~) será considerado un carácter de escape especial si se encuentra justo después de un carácter de nueva línea. Esto nos permite controlar *ssh* durante la sesión en curso. En particular, la secuencia `~.` finalizará la conexión. Podemos ver más secuencias en `man ssh` (ESCAPE CHARACTERS).

El uso de `ssh` no se restringe a sesiones interactivas, también podemos ejecutar comandos simples:

	$ ssh blue.example.com hostname

## Ficheros de configuración

Con el objetivo de mantener nuestro servicio SSH en buenas condiciones, debemos revisar los ficheros de configuración de OpenSSH para poder mantener seguras las comunicaciones.

Fichero | Descripción
--- | ---
~/.ssh/config | Contiene la configuración del cliente OpenSSH. Se puede sobreescribir con comandos `ssh`
/etc/ssh/ssh_config | Contiene la configuración del cliente OpenSSH. Puede ser sobreescrita con comandos `ssh` o configuraciones en el fichero `~/.ssh/config`
/etc/ssh/sshd_config | Contiene la configuración del demonio OpenSSH (`sshd`)

Si necesitamos realizar cambios en la configuración SSH, debemos conocer los ficheros de configuración a modificar;

* Para una conexión de un usuario en particular a un servidor remoto, creamos y/o modificamos el fichero del lado del cliente `~/.ssh/config`.
* Para las conexiones de todos los usuarios a un sistema remoto, creamos y modificamos el fichero `/etc/ssh/ssh_config`.
* Para las peticiones de conexiones entrantes, modificamos el fichero `/etc/ssh/sshd_config` del lado del servidor.

OpenSSH utiliza multitud de directivas para aplicar diferentes configuraciones. Podemos consultarlas en los manuales para los ficheros `ssh_config` y `sshd_config`. Sin embargo, algunas son vitales para el fichero `sshd_config`:

* `AllowTcpForwarding`: Permite la redirección de puertos SSH
* `ForwardX11`: Permite la redirección X11
* `PermitRootLogin`: Permite a `root` iniciar sesión a través de una conexión SSH (Por defecto, `yes`). Debe establecerse a `no`
* `Port`: Establece el puerto en el que se aceptan peticiones (Por defecto, 22)

Un ejemplo de cambios en la configuración de los ficheros del lado del cliente, es cuando el equipo remoto modifica el puerto de escucha por peticiones. En este caso, el usuario del lado del cliente debe indicar de forma manual el puerto remoto a la hora de iniciar la conexión:

	$ ssh -p 2200 192.168.0.104

## Otras aplicaciones útiles: scp y sftp

Usando `scp` podemos copiar ficheros entre dos hosts via conexión SSH:

	$ scp blue.example.com:hello.c .

La sintaxis está basada en el comando `cp`, y podríamos indicar dos nombres de fichero (fuente y destino) o una lista de nombres de fichero y un directorio de destino. Con la opción `-r`, copiamos directorios de forma recursiva.

También podríamos copiar ficheros entre dos hosts remotos:
	
	scp tux@h1.example.com:hello.c tux2@h2.example.com:hello-new.c

El comando `sftp` está inspirado en los clientes FTP, pero precisa una conexión SSH. No sirve para comunicarse con un servidor FTP. Después de establecer una conexión con el comando:

	sftp user@h1.example.com

podemos hacer uso de los comandos `get`, `put` o `mget` para transferir ficheros entre el host local y el remoto, listar los contenidos de un directorio con el comando `ls`, y cambiar entre diferentes directorios usando el comando `cd`. Al inicio de la sesión serás situado en tu directorio personal del host remoto.

## Autenticación cliente con clave pública

Normalmente el servidor SSH te autenticará como usuario con una contraseña que está asociada a tu cuenta en el servidor (almacenada en el fichero `/etc/shadow`). Debido a que la contraseña es solicitada sólo después de que se haya establecido una conexión cifrada, en principio estamos a salvo de posibles *escuchas*. Sin embargo, puede resultar una molestia el hecho de tener almacenada nuestra contraseña en el servidor -incluso cifrada, el fichero de contraseñas puede caer en manos de crackers. Es deseable no tener ningún secreto nuestro almacenado en un host remoto. Podemos conseguir esto a través del uso de autenticación con clave pública, en lugar de usar la contraseña. Debemos almacenar nuestra clave pública en el servidor, ya que no necesita especial protección (¡es pública!).

El servidor te puede autenticar como el propietario de la llave pública, mediante la generación de un número aleatorio, cifrándolo con tu llave pública, y enviandotela. Tu descifras ese número aleatorio usando la llave privada, y le devuelves el resultado al servidor, que la compara con la original, y en caso de que coincidan sabrá que eres el usuario correcto.

Los diferentes tipos de clave que podemos usar son las siguientes:

* `rsa` (Rivest-Shamir-Adleman) es la más antigua, cuenta con un uso muy extendido y altamente soportado.
* `dsa` (Digital Signature Algorithm) en un estándar de Federal Information Processing. Está obsoleto.
* `ecdsa` (Elliptical Curve Digital Signature Algorithm) es una implementación de *Elliptic Curve* de DSA.
* `ed25519` es una variante del algoritmo EdDSA que ofrece más seguridad que `dsa` y `ecdsa`.

> Es crítico que los ficheros que almacenan las llaves tengan suficiente protección. Los ficheros de llaves privadas debe tener un permiso 0640 y con *root* como propietario. Sin embargo, las llaves públicas pueden ser legibles por todo el mundo.

	$ sudo ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed2551_key

Para usar la autenticación con clave pública, primero debemos generar un par de claves:

	$ ssh-keygen -t rsa

El comando pregunta en primer lugar por la ubicación para almacenar el par de claves. La ubicación por defecto es razonable y simplemente debemos confirmarla. A continuación `ssh-keygen` nos pide una contraseña, que se utiliza para cifrar la clave privada. El resultado son dos ficheros: `id_rsa` y `id_rsa.pub`, situado en el directorio `~/.ssh`. El siguiente paso es almacenar la clave pública, es decir, el contenido del fichero `id_rsa.pub`, en el fichero `~/.ssh/authorized_keys` en la cuenta de usuario del host remoto. Esto lo podemos hacer fácilmente usando el comando `ssh-copy-id`:

	$ ssh-copy-id usuario@blue.example.com
	$ ssh-copy-id -i ~/.ssh/id_rsa.pub usuario@blue.example.com

> Por supuesto, podemos hacer lo mismo *a mano*, usando `scp` y/o `ssh`. Tan sólo debemos asegurarnos de no sobreescribir otras claves existentes en el fichero `~/.ssh/authorized_keys`.

> Si configuramos la entrada `PasswordAuthentication` en el fichero `/etc/ssh/sshd_config` del servidor a «no», y `PubkeyAuthentication` a «yes», entonces los usuarios sólo pueden autenticarse a través del método de clave pública. Esto es una buena idea ya que los crackers se suelen divertir ejecutando programas que prueban contraseñas obvias en los servidores SSH.

## Autenticación a través de un agente

La autenticación con clave pública, si estamos usando una contraseña, no es más recomendable, sino que es más seguro. Si quieres conectarte al mismo host con el mismo usuario de forma repetida, el hecho de introducir la contraseña puede agotarnos. El comando `ssh-agent` está para ayudarnos a descansar. Lo que hace es recordar la contraseña y se la envía a los programas cliente SSH cuando sea solicitada. El programa lo iniciamos con `ssh-agent bash`. De esta forma se abre una nueva shell, en la cual debemos añadir la contraseña con `ssh-add`:

	$ ssh-agent /bin/bash
	$
	$ ssh-add ~/.ssh/id_ecdsa
	Enter passphrase for /home/linus/.ssh/id_ecdsa:
	Identity added: /home/linus/.ssh/id_ecdsa
	(/home/linus/.ssh/id_ecdsa)

A partir de este momento, cualquier ejecución de `scp`, `sftp`, o `ssh` obtendrá automáticamente la contraseña del agente SSH. El agente *olvida* la contraseña una vez que sales de la shell, o indicándoselo con `ssh-add -D`.

## Tunneling

Otro método para proporcionar mayor seguridad a través de OpenSSH es la ejecución de *SSH port forwarding*, también conocido como *SSH tunneling*. Esta práctica nos permite redireccionar una conexión desde un puerto de red concreto al puerto 22, donde el servicio SSH está esperando las peticiones. Esto hace que el tráfico de otro servicio diferente ahora lo haga a través del canal cifrado que proporciona SSH, de forma análoga a lo que hace una VPN.

Por ejemplo, si es preciso el uso de una aplicación gráfica X11, podemos usar OpenSSH para crear un túnel seguro. Es lo que se denomina *X11 forwarding*. En primer lugar, debemos comprobar el estado del fichero de configuración `/etc/ssh/sshd_config` para ver si se permite esta conexión. La directiva *X11Forwarding* debe tener un valor `yes` en el fichero de configuración del host remoto. Podemos comprobarlo con el siguiente comando:

	# grep "X11Forwarding yes" /etc/ssh/sshd_config
	X11Forwarding yes

Una vez comprobada la configuración anterior, y modifica si fuese necesario, debemos usar el comando `ssh -X user@host-remoto`.

## Securizando SSH

Existen algunos consejos para dotar a SSH de un mayor nivel de seguridad:

* User un puerto diferente al 22.
* Deshabilitar las conexiones del usuario *root*.
* Asegurarse del uso del protocolo 2.

Para modificar el puerto por defecto, 22, se debe cambiar el valor de la directiva `Port` en el fichero `/etc/ssh/sshd_config`.

El hecho de que el nombre de usuario del administrador sea genérico (root), se debe proteger de ataques de fuerza bruta. Se debe revisar el valor de la directiva `PermitRootLogin` en el fichero `/etc/ssh/sshd_config`, para que use el valor `no`.

La versión 1 del protocolo OpenSSH se considera insegura, y la debemos evitar. Aunque la mayoría de los sistemas actuales hacen uso de la versión 2, debemos asegurarnos a través de la directiva `Protocol` del fichero `/etc/ssh/sshd_config`. Si tiene un valor 1, debemos cambiarlo a 2 y reiniciar el servicio para aplicar los cambios.

## Comandos vistos en el tema

| Comando | Descripción |
| --- | --- |
| scp | Programa para copiar ficheros basado en SSH |
| sftp | Programa tipo FTP basado en SSH |
| ssh | «Secure Shell», crea una sesión interactiva segura en host remotos |
| ssh-add | Añade claves SSH privadas a `ssh-agent` |
| ssh-agent | Gestiona claves privadas y contraseñas para SSH |
| ssh-copy-id | Copia claves SSH públicas a otros hosts |
| ssh-keygen | Genera y gestiona claves para SSH |
| sshd | Servidor para el protocolo SSH |


