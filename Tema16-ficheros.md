# Tema 16 - Ficheros

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Contenidos](#contenidos)
- [Objetivos](#objetivos)
- [Nombres de ficheros y rutas](#nombres-de-ficheros-y-rutas)
    - [Directorios](#directorios)
- [Listando ficheros y directorios](#listando-ficheros-y-directorios)
- [Crear y eliminar directorios](#crear-y-eliminar-directorios)
- [Patrones de búsqueda de ficheros](#patrones-de-búsqueda-de-ficheros)
    - [Clases de caracteres](#clases-de-caracteres)
- [Copiar, mover y eliminar ficheros](#copiar-mover-y-eliminar-ficheros)
- [Enlaces a ficheros](#enlaces-a-ficheros)
    - [Enlaces duros](#enlaces-duros)
    - [Enlaces simbólicos](#enlaces-simbólicos)
    - [Usos prácticos de los enlaces](#usos-prácticos-de-los-enlaces)
        - [Enlaces a versiones](#enlaces-a-versiones)
        - [Copias de seguridad](#copias-de-seguridad)
        - [Sustitución de comandos](#sustitución-de-comandos)
- [Mostrando el contenido de los ficheros](#mostrando-el-contenido-de-los-ficheros)
- [Buscando ficheros](#buscando-ficheros)
    - [Argumentos necesarios para find.](#argumentos-necesarios-para-find)
- [Encontrar ficheros rápidamente](#encontrar-ficheros-rápidamente)
- [Más comandos](#más-comandos)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Contenidos

* Nombres de ficheros y rutas
  * Nombres de ficheros
  * Directorios
  * Nombres de rutas absolutas y relativas
* Comandos de directorio
  * El directorio actual: `cd` y compañía
  * Listar ficheros y directorios - `ls`
  * Crear y eliminar directorios: `mkdir` y `rmdir`
* Patrones de búsqueda de ficheros
* Ser capaces de crear y modificar ficheros de texto.
* Manejando ficheros
  * Copiar, mover y eliminar - `cp` y amigos
  * Enlazando ficheros - `ln` y `ln -s`
  * Mostrando el contenido de ficheros - `more` y `less`
  * Buscar ficheros - `find`
  * Encontrar ficheros rápidamente - `locate` y `slocate`
  * Más comandos de búsqueda - `which` y `whereis`

## Objetivos

* Familiarizarse con las convenciones de Linux en lo referente a los nombres de ficheros y directorios
* Conocer los comandos más importantes para trabajar con ficheros y directorios
* Ser capaces de usar patrones de búsqueda de ficheros

## Nombres de ficheros y rutas

Una de las tareas más importantes de un S.O. es almacenar información en medios de almacenamiento permanente para luego poder recuperarla. En Linux, los nombres de ficheros pueden contener cualquier carácter que nuestro ordenador pueda visualizar. Pero debemos tener especial cuidado con algunos caracteres especiales, por su significado concreto (para la shell, por ejemplo). Sólo hay dos caracteres ASCII totalmente prohibidos, el slash «/» y el byte «cero» (el ASCII con valor cero). Otros caracteres, como espacios, diéresis, signos dólar, se pueden usar libremente, pero deben precederse del caracter escape «\», o comillas.

Por supuesto, se diferencia entre minúsculas y mayúsculas. El límite del tamaño del nombre viene impuesto por el sistema de ficheros. Un valor típico es de 255 caracteres. Una gran diferencia con sistemas como DOS o Windows es la extensión que designa su tipo. Aquí la extensión forma parte del nombre y la utilizaremos por conveniencia, para que nos sirva de ayuda.

> Hay casos en que sí importa la extensión. Por ejemplo, el compilador `gcc`, la utiliza para diferenciar archivos (código fuente, precompilado, objeto)

Los caracteres que podemos usar libremente son:

	abcdefghijklmnopqrstuvwxyz
	ABCDEFGHIJKLMNOPQRSTUVWXYZ
	0123456789+-._

Es recomendable no sobrepasar los 14 caracteres en el nombre por razones de compatibilidad con sistemas UNIX antiguos. Así como comenzar con letras, o número. Los últimos cuatro caracteres pueden ser usados sin problemas en el interior del nombre.

Algunos ejemplos de nombres de ficheros:

	X-files
	foo.txt.bak
	50.something
	7_of_9

sin embargo, podemos tener problemas con los siguientes nombres:

	-10ºC
	.profile
	3/4-metros
	cigüeñal

Los nombres que comienzan con . son ficheros ocultos para el sistema, y no son listados de forma predeterminada para no molestar al usuario.

El comando `touch` permite la creación de un fichero vacío de forma rápida. Aunque su propósito original era la actualización de las marcas de tiempo, resulta útil para crear ficheros con los que trabajar más tarde.

### Directorios

Debemos separar los ficheros en directorios. Por ejemplo, los de varios usuarios. Y además hacerlo de forma jerárquica y en árbol. Los directorios también son ficheros, aunque no utilizaremos las mismas herramientas para trabajar con ellos, pero sirven las reglas que definimos antes en cuanto a los nombres. Con el carácter «/» (slash) separamos los directorios.

El directorio raíz es especial en Linux (/). No confundir con el directorio del usuario root. Cada fichero se nombra desde el directorio raíz.

	/home/debian/carta.txt

El nombre `carta.txt`, es una abreviatura de «el fichero llamado carta.txt en el directorio actual». y `joe/carta.txt` es «el fichero carta.txt en el directorio joe dentro del directorio actual». Los nombres relativos empiezan su ruta en el directorio actual, y las rutas absolutas empiezan con el carácter «/».

El nombre «..» hace referencia al directorio superior al actual. Y el nombre «.» hace referencia al directorio actual.

> Puede parecer estúpido referirse a un directorio en el cual ya estoy situado, pero se usa para ejecutar un programa que está en el directorio actual, pero dicho directorio no está en la variable `PATH`, que es donde se realiza la búsqueda de los comandos.

Podemos usar el comando `cd` de la shell para cambiar el directorio actual. Simplemente indicamos el directorio como un parámetro:

	$ cd desktop

Si no le indicamos ningún parámetro, nos trasladaremos hasta nuestro directorio personal.

	$ cd
	$ pwd

El comando `cd -` cambia al directorio anteriormente usado. Es útil para alternar entre dos directorios.

## Listando ficheros y directorios

Para encontrar los caminos que existen en el árbol de directorios, es importante ser capaces de ver los ficheros y directorios que hay dentro de un directorio. El comando `ls` realiza esta función. Sin opciones, esta información es mostrada como una tabla multicolumna ordenada por el nombre de fichero. Los nombres de los ficheros se suelen mostrar en varios colores. Afortunadamente, la mayoría de las distribuciones han estandarizado los colores a usar. En los monitores monocromo, las opciones `-F` y `-p` evitan el uso de colores. Añaden un carácter especial dependiendo del tipo de fichero.

Podemos mostrar los ficheros ocultos (cuyos nombres comienzan por punto) usando la opción `-a`(all). Otra opción muy útil es `-l` (long). Si queremos mostrar información extra para un directorio `ls -l /tmp` no nos sirve, porque se muestra el contenido con todos los ficheros del directorio. Usaremos la opción `-d` para evitar esto y obtener la información sobre el directorio `/tmp`.

Para poder visualizar información más detallada, o en un formato diferente, el comando `ls` incorpora algunas opciones de uso frecuente:

Corto | Largo | Descripción
--- | --- | ---
-1 | | Muestra un fichero o directorio por línea
-a | --all | Muestra todos los nombres de fichero y subdirectorios, incluyendo los ocultos
-d | --directory | Muestra la información relativa al directorio en lugar de su contenido
-F | --classify | Clasifica cada tipo de fichero con un código
-i | --inode | Muestra los nombres de fichero y directorio junto con su número de *inodo* asociado
-l | | Muestra los metadatos de ficheros y directorios, que incluye el tipo de fichero, permisos de acceso, total de links *duros*, propietarios, etc.
-R | | Muestra el contenido del directorio, y de cada uno de sus subdirectorios

Debemos tener la precaución de comprobar si nuestra distribución tiene activada algún *alias* para este comando, ya que suelen aplicarse por defecto.

## Crear y eliminar directorios

Para mantener nuestros ficheros ordenados, debemos crear nuevos directorios. Para crear nuevos directorios, disponemos del comando `mkdir`. Requiere uno o más nombres de directorio como argumentos. Para crear directorios anidados en un sólo paso, podemos usar la opción `-p`, de otro modo el comando asume que existen todos los directorios en la ruta, excepto el último. 
Por ejemplo:

	$ mkdir fotos/debian
	-----
	$ mkdir -p fotos/debian
	$ cd fotos
	$ ls -F

El comando `rmdir` se utiliza para eliminar directorios. Debemos indicar al menos la ruta de un directorio. Además, los directorios deben estar vacíos. De nuevo, sólo se eliminará el último nombre de la ruta.

	$ rmdir fotos/debian
	$ ls -F

Con la opción `-p`, todos los subdirectorios vacíos pueden ser eliminados en un sólo paso, empezando por el situado más a la derecha.

	$ mkdir -p fotos/camino/2010
	$ rmdir fotos/camino/2010
	$ ls -F fotos
	---
	$ rmdir -p fotos/camino
	$ ls -F fotos

## Patrones de búsqueda de ficheros

A menudo, aplicaremos un mismo comando a varios ficheros al mismo tiempo. Por ejemplo, si queremos copiar todos los ficheros cuyos nombres comienzan por la letra «p» y acaban con «.c» del directorio `prog1` al `prog2`

	prog1/p*.c

la shell reemplaza en la llamada al comando actual dicho patrón por una lista de nombres de fichero que coinciden con el parámetro. Por ejemplo, nombres de fichero como los siguientes:

	prog1/p1.c
	prog1/polly.c
	prog1/pop-rock.c
	prog1/p.c

*El asterisco también incluye la longitud cero*

> Podemos comprobar los patrones de búsqueda usando el comando `echo` del siguiente modo;: `$ echo prog1/p*.c` de esta forma vemos los ficheros de salida sin modificar nada.

El patrón «\*» se traduce como «todos los ficheros en el directorio actual», excepto los ficheros ocultos cuyo nombre comienza por un punto. Para evitar problemas, los patrones ignoran los ficheros ocultos, a no ser que explícitamente indiquemos que deben ser incluidos por alguna razón: «.\*». El asterisco se puede encontrar en otros sistemas operativos, como DOS o Windows, y se usan para referirse a todos los ficheros en un directorio «*.*». En Linux este patrón se traduciría en: «todos los ficheros cuyo nombre contenga un punto». Es decir, deberíamos usar «\*».

La marca de interrogación «?» equivale a exactamente un caracter cualquiera (excluyendo el slash).
Un patrón como:	`p?.c` concuerda con los siguientes nombres:

	p1.c
	pa.c
	p-.c
	p..c

*En este caso no existe la opción de cadena vacía.*

> Es muy importante entender que la expansión de los patrones es responsabilidad de la shell. Los comandos no saben nada acerca de los patrones, ni les preocupa. Todo lo que ellos ven, es una lista de ficheros, pero sin saber de donde han salido -como si los hubiera escrito el usuario de forma manual.

> ¿Qué ocurre si la shell no encuentra ningún fichero que concuerda con el patrón de búsqueda? En ese caso al comando se le pasa el patrón tal cual. El comando puede interpretar el patrón como un nombre de fichero, pero el «fichero» no existe y se produce un error.

### Clases de caracteres

En el siguiente patrón:

	prog[123].c

los corchetes concuerdan con exactamente aquellos caracteres que están enumerados (ningún otro). El patrón anterior concuerda con:

	prog1.c
	prog2.c
	prog3.c

pero no con:

	prog.c
	proga.c
	prog4.c
	prog12.c

Una notación más interesante, son los rangos:

	prog[1-9].c
	[A-Z]bracadabra.txt

> Hay que tener cuidado con que las letras no están contiguas. El patrón `prog[A-z].c` no sólo concuerda con `progQ.c` sino también con `prog_.c` ¿Porqué? y ¿cómo lo soluciono?

También se pueden indicar clases de caracteres negados, que serán interpretados como «todos los caracteres excepto estos». El siguiente patrón:

		prog[!A-Za-z].c

concuerda con todos los nombres cuyo caracter entre «g» y «.» no sea una letra. De nuevo, el slash es la excepción.

La expansión de las llaves en expresiones como:

	{rojo, amarillo, azul}.txt

equivale a lo siguiente:

	rojo.txt	amarillo.txt	azul.txt

En general, una palabra en la línea de comandos que contiene texto separado por comas y entre llaves, es reemplazada por tantas palabras como palabras hay entre las llaves. Hay que tener en cuenta que este reemplazo está basado en el texto de la línea de comandos y es completamente independiente de la existencia o no de cualquier fichero o directorio. Al contrario que ocurría con los patrones de búsqueda que siempre producían sólo aquellos nombres que actualmente existen como nombres de ficheros en el sistema.

Podemos tener varias llaves en la misma expresión, con lo que se generan todas las posibles combinaciones:
	
	{a,b,c}{1,2,3}.dat

Esto puede resultar útil para la creación de directorios de forma sistemática: los patrones de búsqueda no nos pueden ayudar en este caso, ya que sólo pueden encontrar aquello que existe.

	$ mkdir -p facturas/200{8,9}/T{1,2,3,4}

## Copiar, mover y eliminar ficheros

Podemos copiar ficheros usando el comando `cp` (copy). Si le indicamos a `cp` los nombres de dos ficheros (fuente y destino), entonces realiza una copia del fichero fuente que será colocado en el fichero destino. Normalmente, `cp` no pregunta si puede sobreescribir el fichero destino, en caso de existir. Debemos usar la opción `-i`.

Podemos también indicar un directorio como destino, en lugar de un nombre de fichero.

	$ cp lista1 lista2
	$ cp /etc/passwd .
	$ ls -l

En lugar de un sólo fichero fuente, podemos usar una lista de ficheros fuente (o un patrón de shell). Sin embargo, en este caso, no es posible copiar estos ficheros a un nombre distinto, sino que debemos indicar un directorio destino. En Linux el comando `cp *.txt *.bak` ,normalmente, no funciona. 

Mientras que el comando `cp` hace una copia exacta de un fichero, el comando `mv` sirve para mover un fichero a un lugar diferente o cambiar su nombre. Esta es una operación que afecta a los contenidos de los directorios, a no ser que se mueva el fichero a un sistema de ficheros diferente.

Las opciones `-b`, `-f`, `-i`, `-u` y `-v` de `mv` se comportan igual que las de `cp`.

	$ mv passwd list2
	$ ls -l

Al igual que `cp`, el comando `mv` no pregunta por la confirmación en caso de que el destino exista, sobreescribe el fichero.

El comando para eliminar un fichero se llama `rm` (remove). Para poder eliminar un fichero, debemos tener los permisos adecuados en el directorio correspondiente. Cada usuario puede trabajar «a su gusto» dentro de su directorio personal.

> Los permisos de escritura sobre un fichero, son irrelevantes si lo que deseamos es eliminarlo, así como el grupo al que pertenece. Es una cuestión de permisos para modificar el directorio.

`rm` se comporta igual que sus compañeros a la hora de pedir confirmación para eliminar un fichero. Debemos ser cuidadosos con el uso de comodines. 

> Como administrador de sistema, podemos eliminar por completo todo el sistema de ficheros con un simple comando, como `rm -rf /`, debemos tener cuidado, ya que es fácil equivocarse a la hora de escribir `rm -rf foo*` y realmente introducir `rm -rf foo *`.

Si queremos estar más tranquilos a la hora de eliminar ficheros, podemos crear un alias para `rm -i`. Por ejemplo:

	$ alias del=”rm -i”
	$ del lis*

> Debemos evitar el uso de un alias con el nombre `rm`, ya que nos acostumbraremos a usarlo esperando la confirmación, y cuando estemos trabajando en un equipo que no es el nuestro podemos equivocarnos fácilmente.

> En entornos gráficos como gnome y kde está soportado el concepto de papelera, que recibe los ficheros eliminados desde el gestor de ficheros, y haciendo posible de esta manera su posterior recuperación.

## Enlaces a ficheros

Linux permite crear referencias a ficheros, los llamados «links», y de esta forma asignar varios nombres al mismo fichero ¿Para qué sirve esto?, pues para crear un entorno más seguro y evitar así borrados accidentales, o para ahorrar espacio si tenemos ramas del árbol de directorios que deben almacenar diferentes versiones con muy pocas diferencias, o para evitar rutas muy largas.

### Enlaces duros

![image](/img/T06_01.png)

El comando `ln` asigna un nuevo nombre (segundo argumento) a un fichero, además del que ya tiene (primer argumento).

	$ ln lista1 lista2
	$ ls -l

Ahora, el directorio parece contener un nuevo fichero llamado `lista2`. Hay dos referencias al mismo fichero. Se puede ver en el contador de referencias en la segunda columna de la salida del comando `ls -l`. Su valor es 2, indicando que el fichero tiene dos nombres. Para comprobar que en realidad los dos nombres hacen referencia al mismo archivo sólo lo podemos comprobar viendo la salida de `ls -li`. El valor de la primera columna, que es el número de inodo, es el mismo para los dos nombres.

Cuando estamos creando enlaces duros, debemos tener en cuenta algunas cuestiones:

* El fichero original debe existir antes de ejecutar el comando `ln`.
* El segundo fichero *no* debe existir antes de la ejecución de `ln`.
* Tanto el fichero original como la copia poseen el mismo número de inodo.
* Ambos ficheros comparten los mismos datos.
* El fichero original, y cualquiera de sus copias pueden encontrarse en diferentes directorios.
* Todos los ficheros deben estar en el mismo sistema de ficheros.

Los directorios son simples tablas mapeando los nombres de ficheros a números de inodo. Por supuesto, puede haber varias entradas en una tabla que contienen diferentes nombres pero el mismo número de inodo.

Los enlaces a directorios no están permitidos en Linux. Las únicas excepciones son «.» y «..» que el sistema mantiene en cada directorio.

> Eliminando uno de los ficheros (entradas en el directorio) decrementamos el número de nombres para el fichero. Hasta que ese número no llega a cero, no se borrará realmente el fichero.

> Debido a que los números de inodo son únicos dentro del mismo sistema de ficheros físico, dichos enlaces sólo se pueden utilizar dentro del mismo sistema de ficheros donde reside el fichero inicial.

La explicación acerca del borrado de un fichero no es del todo correcta. Si se elimina el último nombre, ese fichero no se puede volver a abrir, pero si un proceso todavía lo estaba usando, podrá seguir usándolo hasta que lo cierre, o finalice el proceso. Esto es lo que se utiliza para trabajar con archivos temporales, que deben desaparecer cuando el programa finaliza. Creamos el fichero para lectura y escritura, y lo borramos inmediatamente pero sin cerrarlo. Entonces, podemos realizar escrituras en él, y luego más tarde volver al principio del fichero y leer lo escrito. Pero ya no existe en la tabla del directorio, por lo que al finalizar el programa no queda ningún rastro de dicho archivo.

### Enlaces simbólicos

![image](/img/T06_02.png)

En Linux, también existen los llamados «enlaces simbólicos» o «soft links», que son ficheros que contienen el nombre del fichero al que apuntan, junto con una «marca» indicando que los accesos a dicho fichero deben ser redirigidos al fichero destino. A diferencia de los enlaces duros, el fichero destino no sabe nada acerca de los enlaces simbólicos. La creación o borrado de un enlace simbólico no tiene ningún impacto sobre el fichero destino, pero si el fichero destino es eliminado, el enlace simbólico queda huérfano, apuntando a ningún sitio. En esta ocasión, sí se permiten enlaces a directorios, así como a ficheros en sistemas de ficheros diferentes.

Los enlace simbólicos son populares cuando el fichero o directorio cambia su nombre pero se requiere mantener la compatibilidad. Por ejemplo, el correo se almacena en `/var/mail`, pero en versiones anteriores este directorio era `/var/spool/mail`, y muchos programas incorporaban este nombre dentro de su código. Para hacer una transición transparente hacia `/var/mail`, una distribución podrá crear un enlace simbólico con el nombre `/var/spool/mail` que apunte a `/var/mail`. Para crear un enlace simbólico, le debemos pasar la opción `-s` a `ln`:

	$ ln -s /var/log short
	$ ls -l
	$ ls -og
	$ cd short
	$ pwd -P

Para eliminar los enlaces que no se utilicen, se borran mediante el comando `rm` al igual que los ficheros regulares:

	$ rm short
	$ ls

A la hora de crear enlaces simbólicos debemos tener en cuenta los siguientes puntos:

* El fichero original debe existir previamente a la ejecuión del comando `ln -s`.
* El segundo fichero indicado en el comando `ln -s` *no* debe existir antes de su ejecución.
* Un fichero original y su enlace simbólico no comparten el mismo número de inodo.
* Un fichero original y su enlace simbólico no comparten los mismos datos.
* Los ficheros pueden estar presentes en diferentes directorio y/o sistemas de ficheros.

> En ocasiones podemos encontrarnos con enlaces simbólicos que apuntan a otros enlaces simbólicos. Si queremos conocer el fichero final, usaremos el comando `readlink -f` con uno de los enlaces simbólicos como argumento.

> Los enlaces rotos pueden suponer un problema para nuestro sistema. En caso de eliminar el fichero destino, nuestro enlace simbólico queda huérfano y dejará de funcionar correctamente. Puede ocurrir que en un sistema comprometido, un usuario malicioso sustituya el fichero destino por su propia versión. En este caso, podemos usar previamente, y con mucha cautela, el comando `unlink` si también es preciso eliminar el fichero final.

### Usos prácticos de los enlaces

A continuación se detallan algunos de los usos más frecuentes de los enlaces en un sistema Linux:

#### Enlaces a versiones

En ciertas aplicaciones, como `python` o `java`, se usan enlaces simbólicos para hacer uso de una de las versiones instaladas en el sistema, sin que tengamos que saber a cual de ellas apunta el enlace.

```
$ which java
/usr/bin/java
$ readlink -f /usr/bin/java 
/usr/lib/jvm/java-11-openjdk-amd64/bin/java
```

De forma similar a la anterior, también se utiliza para gestionar las versiones de las librerías instaladas en el sistema.

	$ readlink -f /usr/lib/libobs.so
	/usr/lib/libobs.so.0

#### Copias de seguridad

Los enlaces duros serán útiles cuando estamos trabajando en un entorno de desarrollo, y las pruebas pueden provocar borrados accidentales. Para evitar pérdidas no deseadas, creamos enlaces a los ficheros importantes:

	$ ln Importante.txt dev/Importante.txt

En este momento, si se produce un borrado del fichero `Importante.txt`, tendremos una copia a salvo en el directorio `dev`.

#### Sustitución de comandos

Con el paso del tiempo, algunas aplicaciones cambian de nombre, o bien se pueden ejecutar desde diferentes nombres de comando. Un ejemplo de esto es el comando `mkfs` que puede ser usado para dar formato con los tipos `ext2`, `ext3` o `ext4`. Todos los comandos comparten el mismo número de inodo, como se puede comprobar con el siguiente ejemplo:

*CentOS*
```
$ ls -i /sbin/mkfs.ext[234]
17170479 /sbin/mkfs.ext2  17170479 /sbin/mkfs.ext3  17170479 /sbin/mkfs.ext4
```
*Ubuntu*
```
$ ls -i /sbin/mkfs.ext[234]
787745 /sbin/mkfs.ext2  787747 /sbin/mkfs.ext3  787749 /sbin/mkfs.ext4
$ readlink -f /sbin/mkfs.ext[234]
/sbin/mke2fs
/sbin/mke2fs
/sbin/mke2fs
```

Como se puede comprobar, en Ubuntu se trabaja con enlaces simbólicos. Y en ambas distribuciones, el comando `mke2fs` decide que tipo de sistema de ficheros utilizar en base al nombre del comando con el que se ha ejecutado.

## Mostrando el contenido de los ficheros

Una forma de ver los documentos en la pantalla es usando el comando `more`, que permite ver documentos de gran tamaño, página a página. La salida se para después de mostrar una pantalla completa. Como `more` tiene algunas limitaciones, como la imposibilidad de volver hacia el principio de la salida, existe la versión mejorada: `less` (“menos es más”).

## Buscando ficheros

Quién no se ha preguntado por el lugar donde ha guardado un fichero recientemente utilizado. El comando `find` busca en el árbol de directorios de forma recursiva por los ficheros que coincidan con un criterio. Los resultados son las rutas a dichos ficheros localizados, que pueden ser enviados a otros programas.
	
	$ find . -user debian -print

El ejemplo anterior busca en el directorio actual (y todos sus subdirectorios) todos aquellos ficheros que pertenecen al usuario `debian`. El comando `-print` muestra el resultado en la terminal. Si no se especifica nada, ésta es la opción por defecto.


### Argumentos necesarios para find.

* **Directorio de comienzo**. El directorio donde debe comenzar la búsqueda ha de ser seleccionado con cuidado, ya que si indicamos el directorio raíz, la búsqueda será muy profunda y lenta. Por supuesto, sólo podremos buscar allí donde tengamos permiso. También podremos indicar una lista de directorios donde comenzar la búsqueda.

> Si indicamos una ruta absoluta, los ficheros devueltos serán con ruta absoluta, y una ruta relativa para el directorio de comienzo, mostrará rutas relativas.

* **Condiciones de test**. Indicamos los requerimientos que han de cumplir los ficheros. Si se indican varias condiciones, se realiza un `AND` entre ellas, permitiéndose el uso de otro operador lógico. Para evitar errores, esas condiciones múltiples han de encerrarse entre paréntesis:

	`$ find . ( -type d -o -name “A*” ) -print`

* **Acciones**. Hay dos acciones, aparte de `-print`, que son `-exec` y `-ok`. Que ejecutan comandos incorporando los nombres de los ficheros. La opción `-ok` pregunta antes de ejecutar el comando, y el comando `-exec` lo ejecuta directamente.

Hay un par de reglas a tener en cuenta:

* El comando siguiente a `-exec` debe finalizar con punto y coma «;», anteponiendo el carácter escape «\».
* El nombre del fichero será reemplazado en el lugar que se coloquen los caracteres «{}», encerrados entre comillas simples.

```
	$ find . -user debian -name “*.txt” -exec ls -l '{}' \;
	$ find . -atime +13 -exec rm -i '{}' \;
```

## Encontrar ficheros rápidamente

El comando `find` es lento porque recorre todo el árbol de directorios. El comando `locate` lista todos los ficheros cuyos nombres coinciden con un patrón dado.

	$ locate lista.txt

`locate` es más rápido porque utiliza una base de datos, que ha creado anteriormente usando el programa `updatedb`. Por lo que puede no mostrar ficheros que han sido añadidos después de crear la base de datos, o mostrar nombres de ficheros que han sido eliminados recientemente.

El patrón que se emplea con este comando puede resultar algo confuso en los primeros usos que hagamos de la utilidad. Si no indicamos ningún carácter comodín, se añaden por defecto. Es decir, si usamos el patrón **passwd**, en realidad se convierte en ***passwd***. Y si queremos buscar el nombre base `passwd`, de forma exacta, debemos añadir comillas (dobles o simples) y preceder el patrón con el carácter `\`.

```
$ locate -b passwd
/etc/passwd
/etc/passwd-
/etc/pam.d/chpasswd
[...]
/var/lib/dpkg/info/passwd.prerm
$
$ locate -b '\passwd'
/etc/passwd
/etc/pam.d/passwd
[...]
/usr/share/doc/passwd
/usr/share/lintian/overrides/passwd
```

En realidad, es posible el uso de varios patrones de búsqueda:

	$ locate -b '\passwd' '\group'
	/etc/group
	/etc/passwd
	[...]

> Si queremos evitar que algunos ficheros no puedan ser localizados con el comando `locate`, debemos modificar las directivas *PRUNEFS*, *PRUNENAMES* o *PRUNEPATHS* en el fichero de configuración `/etc/updatedb.conf`. Para mayor detalle, consultar la ayuda con `man updatedb.conf`.

> Podemos hacer uso de la opción `-e`, para restringir la búsqueda sólo a ficheros existentes, pero lo hacemos más lento.

## Más comandos

El comando `which` nos devuelve la ruta de un comando externo.

	$ which grep

hace uso de la variable `PATH` para localizar dicho programa. `which` no sabe nada acerca de los comandos internos de la shell. Sólo nos devuelve la ruta de un programa

	$ which test
	$ type test

si usamos `type` tiene preferencia el comando interno.

El comando `whereis` también nos devuelve la localización de la documentación, el código fuente y otros ficheros de interés.

	$ whereis passwd

## Comandos vistos en el tema

Comando | Descripción
-- | --
cd | Cambia el directorio de trabajo de la shell.
cp | Copia ficheros.
find | Busca ficheros que concuerdan con un cierto criterio.
ln | Crea enlaces (duros y simbólicos)
locate | Localiza ficheros por su nombre en una base de datos.
ls | Lista información de un fichero o el contenido de un directorio
mkdir | Crea nuevos directorios
mv | Mueve ficheros a diferentes directorios o los renombra
pwd | Muestra el nombre del directorio de trabajo actual
rm | Elimina ficheros o directorios
rmdir | Elimina directorios vacíos
updatedb | Crea la base de datos para locate
whereis	| Busca programas ejecutables, páginas de manual, y código fuente para los programas.
which | Busca programas a través de PATH.
