# Tema 36 - La Shell como lenguaje de programación

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Variables](#variables)
- [Expresiones aritméticas.](#expresiones-aritméticas)
- [Ejecución de comandos.](#ejecución-de-comandos)
- [Entradas del usuario](#entradas-del-usuario)
    - [Tiempo de espera](#tiempo-de-espera)
    - [Tamaño entrada](#tamaño-entrada)
    - [Entrada secreta](#entrada-secreta)
- [Estructuras de control](#estructuras-de-control)
- [Bucles.](#bucles)
- [Interrupción de bucle.](#interrupción-de-bucle)
- [Funciones.](#funciones)
- [Ejecución sin consola](#ejecución-sin-consola)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos

* Conocer las características del lenguaje de programación de la shell (variables, estructuras de control, funciones)
* Ser capaz de crear scripts de shell simples usando estas características.

## Variables

Las variables son usadas a nivel de programación, y para gestionar diversos aspecto de la shell (variables de entorno). Las variables en la shell son pares formados por un nombre y un valor. Un nombre de variable consta de una serie de letras, números y “\_”, y no puede comenzar por un número. A diferencia de otros lenguajes de programación, la shell no requiere que las variables sean declaradas antes de su uso; simplemente asignamos un valor a una variable.

	$ color=azul
	$ a123=?_/@xz

Debemos asegurarnos de que no haya espacios en blanco al lado del simbolo “=”.

**Sustitución de variables**. Cuando ejecutamos comandos, la shell reemplaza las referencias a variables con el valor en cuestión. Nos referimos a una variable colocando un simbolo “$” antes del nombre. Por ejemplo, podemos visualizar los valores de las variables con `echo`:

	$ echo $OSTYPE

**Variables y comillas**. La sustitución de variables es útil, pero no siempre es lo que deseamos. Podemos evitar este comportamiento de dos formas:

* Colocar un backslash (“\”) delante del signo dólar:

	$ echo \$color tiene el valor $color

* Encerrar entre comillas simples (o dobles) la referencia a la variable:

	$ echo '$color' tiene el valor $color

Las comillas dobles son importantes cuando manejamos variables cuyos valores pueden contener espacios en blanco. Por ejemplo:

	$ mkdir “Mis fotografias”
	$ dir=”Mis fotografias”
	$ cd $dir
	bash: cd: Mis: Not a directory

En la tercera línea, el valor de la variable dir se sustituye antes de que la línea de comandos se divida en palabras. La forma correcta sería:

	$ cd “$d”

**Variables de entorno.**
¡Otra vez aquí!

**Variables especiales**. La shell bash soporta alguna variables especiales, que resultan de interés en los scripts. No pueden ser modificadas. 
Por ejemplo: 

Variable | Contenido
-- | --
$? | Código de retorno del último comando.
$$ | ID de proceso de la shell actual.
$! | PID del último proceso ejecutado en segundo plano.
$0 | Nombre del script de shell actual (o de la propia shell interactiva).
$# | Número de parámetros pasados en la línea de comandos del script.
$\* | Todos los parámetros del script; “$\*” equivale a “$1 $2...$n”
$@ | Todos los parámetros del script; “$@” equivale a “$1” “$2”...“$n”
$n | El parámetro enésimo del script. Para n>9 debemos escribir ${n} o usar el comando `shift`.

Podemos usar el script: `script_param.sh`, para practicar con el uso de las variables especiales.

Como buenos programadores que somos, en lo que se refiere a las variables de los parámetros según su posición, es recomendable encerrarlas entre comillas, ya que no sabemos lo que el usuario puede escribir, y un espacio en blanco extraviado nos puede dar muchos problemas. 

**Métodos especiales de sustitución de variables**. La shell puede realizar más cosas que usar $variable, o ${variable}:

**Asignar un valor por defecto**. Con ${name:=<valor por defecto>}, se asigna el <valor por defecto> a la variable name, en caso de que no tenga ningún valor anterior, o sea la cadena vacía.

	$ unset color
	$ echo Color favorito: ${color:=amarillo}
	Color favorito: amarillo
	$ echo $color
	amarillo
	$ color=rojo
	$ echo Color favorito: ${color=amarillo}
	Color favorito: rojo

**Usar el valor por defecto**. La expresión ${name:-<valor por defecto>} es reemplazada por el valor de name, si tiene un valor diferente a la cadena vacía, en otro caso, se reemplaza por <valor por defecto>. En este caso el valor de name permanece intacto.

	$ unset color
	$ echo Color favorito: ${color:-amarillo}
	Color favorito: amarillo
	$ echo $color
	_

**Mensaje de error si no hay valor**. Usando ${name:?<mensaje>}, podemos comprobar si la variable name tiene un valor diferente a vacío. Si la variable no tiene ningún valor, se muestra el <mensaje>, y si estamos en un script, la ejecución finaliza en ese punto.

	$ cat script
	#!/bin/sh
	echo “${1:?Upps, parece que falta algo}”
	echo “Vamos allá”
	$ ./script foo
	foo
	Vamos allá
	$ ./script
	./script: line 2: 1: Upps, parece que falta algo

**Subcadenas**. Una expresión del tipo ${name:p:l} es reemplazada por un total de l caracteres del valor de la variable name, empezando en el carácter que está en la posición p. Si se omite l, se devuelve todo empezando en p. El primer carácter del valor se considera que está en la posición cero.

	$ abc=ABCDEFG
	$ echo ${abc:3:2}
	DE
	$ echo ${abc:3}
	DEFG

En efecto, p y l son evaluadas como expresiones aritméticas. p puede ser negativo para referirnos a una posición de comienzo desde el final:

	$ echo ${abc:2*2-1:5-2}
	DEF
	$ echo ${abc:0-2:2}
	FG

Las variables $\* y $@ son tratadas como casos especiales. Obtendremos el parámetro de la posición l, empezando en el parámetro p, donde el primer parámetro está en la posición 1.

	$ set foo bar baz quux
	$ echo ${*:2:2}
	bar baz

**Eliminando texto desde el principio**. En una expresión del tipo 
${name#<patrón>}, <patrón> se considera un patrón de búsqueda (pudiendo contener comodines: \*, ?, etc, como en los patrones de búsqueda de ficheros). La expresión es sustituida por el valor de la variable name, pero sin el texto que coincide con el <patrón>, localizado desde el principio:

	$ starter=”EEL SOUP”
	$ echo ${starter}
	EEL SOUP
	$ echo ${starter#E}
	EL SOUP
	$ echo ${starter#E*L}
	SOUP

Si hay varias posibilidades, “#” intenta eliminar la menor cantidad de texto posible. Pero si usamos “##”, se intentará eliminar la mayor cantidad de texto posible.

	$ old=EIEIO
	$ echo ${old#E*I}
	EIO
	$ echo ${old##E*I}
	O

**Eliminando texto desde el final**. Expresiones con el formato ${name%<patrón>}, y ${name%%<patrón>} se comportan de modo similar a los anteriores, excepto que se aplican desde el final, en lugar del principio.

	$ old=OOLALA
	$ echo ${old%L*A}
	OOLA
	$ echo ${old%%L*A}
	OO

## Expresiones aritméticas.

La shell bash soporta ciertas operaciones matemáticas, aunque sólo maneja enteros y no comprueba el overflow. Las expresiones aritméticas se aplican delimitadas por $((...)). Los operandos pueden ser números o variables. La expresión es tratada igual que si estuviese encerrada entre comillas dobles, por lo que se pueden utilizar la expansión de comandos y los formatos especiales de sustitución vistos en el capítulo anterior.

	$ echo $((1+2*3))
	7
	$ echo $(((1+2)*3))
	9
	$ a=123
	$ echo $((3+4*a))		*variable sin $*
	495

## Ejecución de comandos.

Cuando se ejecutan comandos, la shell sigue una serie de pasos ordenados:

1. La línea de comandos se 'trocea' en palabras. Cada carácter que contiene la variable IFS situado fuera de las comillas se considera un separador. Los valores por defecto de IFS, son el espacio, el tabulador y el salto de línea.
2. Se expanden las llaves; a{b,c}d -> abd, acd. El resto de caracteres permanecen intactos.
3. Se sustituye “~” por el valor de HOME.
4. Se realizan las siguientes sustituciones en paralelo, y de derecha a izquierda:
  * Sustitución de variables.
  * Sustitución de comandos.
  * Expansión aritmética.(es decir, todo aquello que comienza por “$”)
5. Si se han hecho sustituciones, se vuelve a trocear en palabras.
6. Al final de este proceso, se sustituyen los posibles patrones (\*, ?) por los ficheros que la shell haya localizado.
7. Por último, las comillas 'no escapadas' y los backslash “\”, son eliminados.

Los únicos pasos del proceso que pueden cambiar el número de palabras en la línea de comandos son la expansión de llaves, división de palabras y expansión de patrones de búsqueda. Todos los demas reemplazan una palabra por otra, con la excepción de “$@”.

## Entradas del usuario

En ocasiones es preciso que nuestro script sea interactivo en tiempo de ejecución, por ejemplo para tomar una decisión basada en la respuesta del usuario. Dentro de Bash disponemos del comando `read` para obtener este tipo de entradas.

El comando `read` recibe los datos desde la entrada estándar (teclado), o desde otro descriptor de fichero. Después de la lectura, el comando `read` guarda los datos en una variable.

	$ cat test.sh
	#!/bin/bash
	# probando el comando read

	echo -n "Dime tu nombre: "
	read name
	echo "Hola $name, bienvenido a Linux."

	$ ./test.sh
	Dime tu nombre: Linus Torvalds
	Hola Linus Torvalds, bienvenido a Linux.

El comando `read` incluye la opción `-p` que nos permite indicar el mensaje directamente en la misma línea.

	$ cat testread.sh
	#!/bin/bash
	
	read -p "Dime tu edad:" edad
	dias=$[ $edad * 365 ]
	echo "Esto resulta en una edad de $dias días."

También es posible hacer uso de más de una variable en la operación de lectura de datos desde la entrada estándar. Por ejemplo:

	$ cat testvar.sh
	#!/bin/bash

	read -p "Dime tu nombre y primer apellido: " name,first
	echo "Comprobando los datos para $name $first..."

Y, por último, si no se hace uso de ninguna variable expresamente para guardar la entrada, quedarán almacenadas por defecto en la variable REPLY.

	$ cat testreply.sh
	#!/bin/bash

	read -p "Introduce un número: "
	factorial=1
	for (( count=1; count <= $REPLY; count++ ))
	do
		factorial=$[ $factorial * $count ]
	done
	echo "El factorial de $REPLY es $factorial"

### Tiempo de espera

En la sección anterior se ha visto como se puede obtener una respuesta desde la entrada estándar, y se ha comprobado que el script se quedará en espera de esa respuesta por un tiempo infinito. Para evitar esta espera tendremos que utilizar la opción `-t`, seguido del número de segundos que el comando `read` debe esperar por la entrada.

	$ cat testtime.sh
	#!/bin/bash

	if read -t 5 -p "Dime tu nombre: " nombre
	then
		echo "Hola $nombre, bienvenido"
	else
		echo "Lo siento, debes ser más rápido"
	fi

Como se puede comprobar, cuando el comando `read` nos devuelva un valor de retorno distinto a cero será porque se ha agotado el tiempo de espera.

### Tamaño entrada

En lugar de establecer un límite al tiempo de espera, se puede establecer un número mínimo de caracteres para aceptar la entrada.

	$ cat testcount.sh
	#!/bin/bash

	read -n1 -p "Desea continuar [S/N]?" resp
	case $resp in
	S | s) echo
	       echo "muy bien, continuamos...";;
	N | n) echo
	       echo "OK, hasta pronto"
		exit;;
	esac

En el ejemplo anterior se hace uso de la opcion `-n1` para que se acepte solo un caracter antes de finalizar la entrada, no siendo necesaria la pulsación de Intro.

### Entrada secreta

En ciertas ocasiones no se debe visualizar por pantalla la entrada que se está escribiendo, por ejemplo para introducir contraseñas. La opción `-s` evita la salida por pantalla de nuestra entrada.

	$ cat testhide.sh
	#!/bin/bash
	
	read -s -p "Introduce tu contraseña: " pass
	echo
	echo "Es esta tu contraseña? $pass"

## Estructuras de control

Todo lenguaje de programación que se digne necesita estructuras de control tales como, condiciones, bucles, etc. Y la shell bash no será menos.

**Un código de retorno como parámetro de control**. Aquí nos encontramos con la primera peculiaridad de las estructuras de control de la shell. La mayoría de los lenguajes de programación usan valores Booleanos (“true” o “false”) para manejar las condiciones o bucles. Pero en bash no es así, sino que tomaremos el código de retorno de un programa. En Linux, cada vez que finaliza un proceso, le dice a su proceso padre si se ha ejecutado con éxito o no. El código de retorno es un valor entre 0 y 255; el valor 0 indica éxito, y cualquier otro implica algún error.

> Un pequeño truco es que podemos colocar una exclamación (“!”) enfrente de un comando. El código de retorno es “negado lógicamente” -el éxito se convierte en error, y el error en éxito.
```
	$ true; echo $?
	0
	$ ! true; echo $?
	1
	$ ! false; echo $?
	0
```
> Sin embargo, esto nos puede hacer perder información, porque los valores mayores de 1, serán convertidos a cero.

Por supuesto, en bash también podemos usar expresiones lógicas para controlar condiciones o bucles. Tan sólo es necesario invocar un comando que evalúe la expresión lógica, y devuelva un código a la shell.

Dicho comando es `test`. Se usa para comparar números o cadenas y comprobar propiedades de ficheros. Algunos ejemplos: 

* `test “$x”` Con un sólo argumento, comprueba si el argumento es “no vacío”, es decir, está formado por uno o más caracteres.
* `test $x -gt 7` Aquí x debe ser un número, test comprueba si valor numérico de x es mayor que 7. -gt significa “mayor que”; otros operadores son -lt (menor que), -ge (mayor o igual que), -le, -eq (igual) y -ne (no igual).
* `test “$x” \> 10` Comprueba si el primer caracter de la cadena x está situado después del de la segunda cadena, en orden léxico. Por lo tanto, “test 7 \> 10” devuelve éxito, y “test 7 -gt 10” error.
* `test -r “$x”` Comprueba si el fichero cuyo nombre contiene x, existe y es legible. Hay otros operadores.

> Aparte de la «notación larga», test también soporta una más corta que consiste en un par de corchetes, por lo tanto, test “$x” equivale a [ “$x” ]

**Condiciones y ramas múltiples**. La shell ofrece abreviaturas para los casos más frecuentes “Haz A sólo si B funciona”, o “Haz A sólo si B no funciona”. Una línea típica en un script es algo como:

	test -d “$HOME/.dir” || mkdir “$HOME/.dir”

De manera similar, el operador && ejecuta el comando siguiente sólo si el comando anterior ha devuelto éxito. Un ejemplo real:

	test -e /etc/default/miprog && source /etc/default/miprog

**Condiciones simples**. El comando `if` ejecuta un comando (a menudo `test`) y decide qué es lo siguiente en ejecutar dependiendo de su código de retorno. Su sintaxis es:

	if <comando test>
	then
		<comandos para éxito>
	[else
		<comandos para error>]
	fi

En la siguiente tabla se muestra un resumen de las condiciones que soporta el comando `test`.

Test | Tipo | Descripción
--- | --- | ---
n1 -eq n2 | Numérico | Comprueba si n1 es igual a n2
n1 -ge n2 | Numérico | Comprueba si n1 es mayor o igual que n2
n1 -gt n2 | Numérico | Comprueba si n1 es mayor que n2
n1 -le n2 | Numérico | Comprueba si n1 es menor o igual que n2
n1 -lt n2 | Numérico | Comprueba si n1 es menor que n2
n1 -ne n2 | Numérico | Comprueba si n1 es diferente a n2
str1 = str2 | Cadena texto | Comprueba si str1 es lo mismo que str2
str1 != str2 | Cadena texto | Comprueba si str1 no es lo mismo que str2
str1 < str2 | Cadena texto | Comprueba si str1 en menor que str2
str1 > str2 | Cadena texto | Comprueba si str1 es mayor que str2
-n str1 | Cadena texto | Comprueba si str1 tiene una longitud mayor que cero
-z str1 | Cadena texto | Comprueba si str1 tiene longitud cero
-d file | Fichero | Comprueba si existe file y es un directorio
-e file | Fichero | Comprueba si existe file
-f file | Fichero | Comprueba si existe file y es un fichero
-r file | Fichero | Comprueba si existe file y se puede leer
-s file | Fichero | Comprueba si existe file y no está vacío
-w file | Fichero | Comprueba si existe file y se puede escribir
-x file | Fichero | Comprueba si existe file y se puede ejecutar
-O file | Fichero | Comprueba si existe file y es propiedad del usuario actual
-G file | Fichero | Comprueba si existe file y el grupo por defecto es el mismo que el usuario actual
file1 -nt file2 | Fichero | Comprueba si file1 es más reciente que file2
file2 -ot file2 | Fichero | Comprueba si file1 es más antiguo que file2

La mejor forma de verlo es con un ejemplo. El siguiente script debemos invocarlo pasándole como primer parámetro nuestro nombre de login, si es el correcto nos devuelve “Es el correcto”, en caso contrario se devuelve un mensaje de error.

```
	#!/bin/bash
	if test “$1” = “$LOGNAME”
	then
		echo “Es el correcto”
	else
		echo “No es tu nombre de usuario”
	fi
	echo “Fin del programa”
```

Por supuesto, no estamos forzados a usar `test` como el comando de prueba, podemos usar cualquier programa cuyo código de retorno sea útil, por ejemplo, `egrep`:

```
	#!/bin/bash
	if df | egrep '(9[0-9]%|100%)' > /dev/null 2>&1
	then
		echo “Un sistema de ficheros está a punto de estallar” | mail -s “peligro” root
	fi
```

**Múltiples ramas**. A diferencia de `if`, el comando `case` permite el uso de ramas múltiples. Su sintaxis es:

```
	case <valor> in
		<patron1>)
			...
			;;
		<patron2>)
			...
			;;
		...
		*)
			...
			;;
	esac
```

`case` compara el valor con los patrones. Si coincide con el primer patrón se ejecutan los comandos hasta los “;;”. Depués finaliza `case`, y no se considera otro patrón. En el script [tcpdump.sh](/scripts/tcpdump.sh) podemos ver un ejemplo de un típico script de init, que será responsable de iniciar o parar un servicio en segundo plano.

## Bucles.

Suele ser útil tener la capacidad de ejecutar un conjunto de comandos varias veces, especialmente si el número de iteraciones no es conocido en el momento de diseño. La shell soporta dos tipos diferentes de bucles:
* **Iteración** sobre una lista de valores predefinidos, tales como los parámetros o todos los nombres de fichero de un directorio. El número de repeticiones es fijado en el momento de iniciar el bucle.
* Un **bucle con prueba**, que ejecuta un comando al inicio de cada repetición. El valor de retorno de dicho comando es tenido en cuenta para iniciar una nueva repetición.

> En la shell no existen iteraciones del tipo “haz del 1 al 10, con saltos de 2 en 2”.

**Repetición usando `for`**. El comando `for` es usado para iterar a través de una lista de valores. Usaremos una variable a la que se irá asignando cada uno de esos valores:

```
	for <variable> [in <list>]
	do
		<comandos>
	done
```

Algunos ejemplos:

	fordemo.sh
	fordemo2.sh
	fordemo3.sh

**Bucles con prueba usando `while` y `until`**. Los comandos `while` y `until` se usan en bucles cuyo número de repeticiones depende de la ejecución actual (y no puede calcularse al inicio del bucle, como en for). 

```
	while <comando prueba>
	do
		<comandos>
	done
```

El siguiente ejemplo muestra los enteros del 1 al 5:

```
	#!/bin/bash
	i=1
	while test $i -le 5
	do
		echo $i
		i=$((i+1))
	done
```

Con el comando `while`, no estamos obligados a usar `test` como comando de prueba, por ejemplo:

```
	#!/bin/bash
	# Nombre: readline
	while read LINE
	do
		echo “--$LINE--”
	done </etc/passwd
```

Implementar otro método para realizar la misma función que en el ejercicio anterior, con el mismo fichero `/etc/passwd`.

El comando `until`, se comporta igual que `while`, sólo que las repeticiones se ejecutan siempre que la prueba sea “falsa” (un valor diferente de cero). El ejemplo anterior, pero ahora usando `until`:

```
	#!/bin/bash
	# Nombre: untildemo
	i=1
	until test $i -gt 5
	do
		echo $i
		i=$((i+1))
	done
```

## Interrupción de bucle.

Puede haber ocasiones en las que es necesario interrumpir un bucle antes de que se cumpla la condición de salida, por ejemplo, si se produce un error. bash soporta esta situación a través de los comandos `break` y `continue`. 

**Interrumpiendo bucles usando break**. El comando `break` aborta la iteración actual, y continúa con la ejecución a partir de `done`.

Por ejemplo:

```
	#!/bin/bash
	for f
	do
      	[ -f $f ] || break      
		echo $f
	done
```

**Terminar iteraciones de bucle usando `continue`**. El comando `continue` no aborta el bucle completo, sino sólo la iteración actual. Por lo tanto, se inicia la siguiente iteración (`for`), o bien se evalúa el comando para comprobar si es necesario ejecutar una siguiente iteración (`while` y `until`).

El script `continuedemo.sh` muestra un ejemplo del uso de este comando.

**Gestión de excepciones**. En los scripts de bash, podemos actuar en función de las señales recibidas, o ante otros eventos no habituales. Esto se hace con el comando `trap`. Veamos un ejemplo, con el siguiente script:

```
	#!/bin/sh
	trap “echo Señal recibida” TERM HUP
	sleep 60
```

Podemos ejecutar este script en segundo plano y enviarle una señal, por ejemplo, **SIGTERM**:

```
	$ sh traptest &
	[2] 11086
	$ kill -TERM %2
	Señal recibida
```

Las señales **SIGSTOP** y **SIGKILL**, por supuesto, no pueden capturarse.

```
	trap “rm /tmp/script.$$” TERM
```

Si ejecutamos este comando en un script, si la shell nos envía una señal **TERM**, se ejecuta el comando `rm /tmp/script.$$`.

> El valor de retorno de un proceso indica si ha sido finalizado mediante una señal. Será 128 más el número de la señal enviada, por ejemplo, 15 para **SIGTERM**.

## Funciones.

Las secuencias de comandos de mayor uso se pueden guardar en «sub scripts», y luego ser invocados desde otros scripts. Aunque en shells como BASH, es posible definir «funciones» directamente.

```
	#!/bin/bash
	function sort-num-rev () {
		sort -n -r
	}
	ls -l | sort-num-rev
```

Una función se comporta como un comando, a efectos de poder hacer uso de la entrada y salidas estándar, así como recibir argumentos. Aunque la palabra reservada «function» se puede omitir, es recomendable utilizarla por claridad del código. Los paréntesis y las llaves son obligatorios.

En una función de shell, los parámetros $1, $2, ..., corresponden con los argumentos que se le pasan a la función, no los que recibe el proceso actual desde la shell. Igualmente, las variables $# y $@ hacen referencia a los argumentos de la función. Desde la función es posible acceder a las variables de entorno del proceso, como podemos ver en el script «panic.sh».

> Incluso en las funciones de shell, la variable $0 corresponde con el nombre del script, y no el de la función. El nombre de la función se guarda en la variable FUNCNAME.

El valor de retorno de una función es el devuelto por el último comando ejecutado. Es posible consultar las funciones disponibles a través del comando `t ypeset -f`. `typeset -F` sólo imprime los nombres.

## Ejecución sin consola

En ciertas ocasiones tendremos que ejecutar un script que debe continuar su actividad incluso después de cerrar la sesión de la terminal. Para ello debemos usar el comando `nohup`.

El comando `nohup` permite la ejecución de otro comando que va bloquear las señales SIGHUP recibidas por el proceso. Se puede combinar con la ejecución en segundo plano.

	$ nohup ./test.sh &
	[1]] 8485
	$ nohup: appending output to 'nohup.out'

Al perderse la conexión con la salida estándar, los datos que produzca (hacia la salida estándar) el proceso se guardarán en el fichero `nohup.out`.


## Comandos vistos en el tema

Comando | Descripción
-- | --
case | Comando de shell para ramas múltiples.
for | Comando de shell para iterar sobre los elementos de una lista.
test | Evalúa expresiones lógicas en la línea de comandos.
until | Comando de shell para crear un bucle que se ejecuta hasta que se cumpla una condición.
while | Comando de shell para crear un bucle que se ejecuta mientras la condición sea verdadera.
