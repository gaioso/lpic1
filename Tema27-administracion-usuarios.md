# Tema 27 - Administración de usuarios

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [¿Por qué necesitamos usuarios?](#%C2%BFpor-qué-necesitamos-usuarios)
- [Grupos y usuarios](#grupos-y-usuarios)
- [Personas y pseudo-usuarios](#personas-y-pseudo-usuarios)
- [Información de usuario y grupo](#información-de-usuario-y-grupo)
    - [El fichero /etc/passwd](#el-fichero-etcpasswd)
    - [El fichero /etc/shadow.](#el-fichero-etcshadow)
    - [El fichero /etc/group.](#el-fichero-etcgroup)
    - [El fichero /etc/gshadow.](#el-fichero-etcgshadow)
    - [El fichero /etc/login.defs](#el-fichero-etclogindefs)
    - [El fichero /etc/default/useradd](#el-fichero-etcdefaultuseradd)
    - [El directorio /etc/skel](#el-directorio-etcskel)
- [Creación de cuentas de usuario](#creación-de-cuentas-de-usuario)
    - [El comando passwd](#el-comando-passwd)
- [Eliminando cuentas de usuario](#eliminando-cuentas-de-usuario)
- [Modificando las cuentas de usuario y la asignación de grupo](#modificando-las-cuentas-de-usuario-y-la-asignación-de-grupo)
- [Cambiar la información de usuario directamente (vipw)](#cambiar-la-información-de-usuario-directamente-vipw)
- [Creación, modificación y eliminación de grupos](#creación-modificación-y-eliminación-de-grupos)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)
- [Resumen](#resumen)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos
* Comprender los conceptos de usuario y grupo en Linux.
* Conocer cómo se almacena la información de usuario y grupo en Linux.
* Ser capaz de usar los comandos de administración de usuario y grupo. 

## ¿Por qué necesitamos usuarios?

Hoy en día cualquier oficina no se concibe sin un ordenador personal, al igual que en la mayoría de hogares. Y mientras que para una familia puede ser suficiente almacenar sus diferentes ficheros del padre, la madre y los hijos en diferentes directorios, esto no es posible en entornos empresariales, o académicos. En estas ubicaciones, el espacio de un disco compartido es proporcionado por un servidor central, y accesible por muchos usuarios, aquí el sistema debe ser capaz de distinguir entre diferentes usuarios y asignarles diferentes permisos a cada uno de ellos. La segunda razón por la cual distinguir entre diferentes usuarios, viene de que hay muchos aspectos del sistema que no deben ser visibles, y mucho menos modificables, sin privilegios especiales.

Linux distingue entre diferentes usuarios a través del uso de «cuentas de usuario». Las distribuciones más usadas, normalmente crean dos cuentas de usuario durante la instalación, una para tareas administrativas (*root*), y otra cuenta para un usuario normal y sin privilegios. Bajo Linux cada cuenta de usuario tiene asignado un número de usuario único, el denominado User ID (**UID**). Cada cuenta de usuario también posee un nombre (debian, root, ...) que es fácilmente recordable por los humanos. Este nombre será el usado en la mayoría de los lugares. Sin embargo, el kernel no sabe nada acerca del nombre de usuario; tan solo utiliza el **UID**. Esto puede conducir a que si un usuario es eliminado mientras que permanecen los ficheros en el sistema, y su **UID** es reasignado a un usuario diferente, ese usuario hereda los ficheros del usuario recién eliminado.

No hay ningún problema «técnico» en que el mismo **UID** sea usado por diferentes usuarios. Eses usuarios tienen el mismo acceso a todos los ficheros que sean propiedad de ese **UID**, pero cada usuario puede tener su propia contraseña de acceso al sistema.

## Grupos y usuarios

Para trabajar con un equipo con Linux, primero necesitamos iniciar sesión (*login*). Esto permite que el sistema le reconozca, y le asigne los permisos de acceso correctos. Además, todos los usuarios tienen su propio directorio personal (*home*), donde ellos pueden almacenar y gestionar sus propios ficheros, y donde otros usuarios normalmente tienen permisos de lectura, pero no de escritura.

> Estas cuentas de usuario son una parte del sistema de control de acceso DAC de Linux. Discretionary Access Control (DAC) es el sistema de control tradicional de Linux, donde el acceso a un fichero, o a cualquier objeto, se basa en la identidad del usuario así como la pertenencia al grupo actual.

En algunas distribuciones se puede realizar un inicio de sesión automático si se configura de ese modo, por ejemplo, durante la instalación de Ubuntu. Por supuesto, es una amenaza a la seguridad del equipo, y debemos usar esta opción con mucha cautela. Varios usuarios que quieren compartir el acceso a ciertos recursos del sistema, o ficheros, pueden formar un grupo. Los grupos también son identificados internamente usando identificadores numéricos (**GID**). Cada usuario pertenece a un grupo principal, y posiblemente a varios grupos secundarios, o adicionales. En un entorno corporativo, por ejemplo, sería posible introducir grupos específicos para un proyecto y asignar a aquellas personas que colaboran en dicho proyecto a un grupo específico que les permita gestionar datos comunes en un directorio sólo accesible por los miembros de dicho grupo.

Hasta la versión 2.4 (incluída), un usuario podía ser miembro de como mucho 32 grupos adicionales, a partir de la versión 2.6 es ilimitado. Podemos saber el **UID** de la cuenta de usuario, el grupo primario y secundarios, y sus correspondientes **GID's** usando el comando `id`:

	$ id
	$ id root

con las opciones `-u`, `-g` y `-G`, el comando `id` permite visualizar tan sólo el **UID**, el **GID** del grupo primario, o los **GID's** de los grupos adicionales. Con la opción adicional `-n` se muestran nombres en lugar de números:

	$ id -G
	$ id -Gn

El comando `groups` muestra el mismo resultado que el comando `id -Gn`.

## Personas y pseudo-usuarios

Detrás de los *seres humanos*, el concepto de usuario y grupo también se usa para asignar permisos a otros componentes del sistema. Esto significa que, además de las cuentas personales de usuarios reales (como tú), hay otras cuentas que no se corresponden con seres humanos, pero que son asignadas internamente a diversas funciones administrativas. Después de instalar Linux, se pueden encontrar varios pseudo-usuarios y grupos en el fichero `/etc/passwd`, y `/etc/groups`. El más importante es el del usuario *root* y su grupo. El **UID** y **GID** de *root* es cero.

> Los privilegios de *root* están asociados al **UID** 0; el **GID** 0 no representa ningún privilegio adicional.

## Información de usuario y grupo

### El fichero /etc/passwd

El fichero `/etc/passwd` es la base de datos de usuarios del sistema. Hay una entrada en este fichero por cada usuario en el sistema. Después de la instalación, el fichero contiene las entradas para la mayoría de los pseudo-usuarios. Las entradas en `/etc/passwd` tienen el siguiente formato:

	<user name>:<password>:<UID>:<GID>:<GECOS>:<home dir>:<shell>

```
$ cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
[...]
rpcuser:x:29:29:RPC Service User:/var/lib/nfs:/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin
rngd:x:994:990:Random Number Generator Daemon:/var/lib/rngd:/sbin/nologin
vagrant:x:1000:1000::/home/vagrant:/bin/bash
```

Como se puede aprecia en la salida anterior, hay siete campos en total:

Campo | Descripción
--- | ---
**user name** | Este nombre debe estar formado por minúsculas y dígitos; el primer carácter debe ser una letra. En los sistemas UNIX sólo se tienen en cuenta los primeros ocho caracteres, pero Linux no tiene esta limitación, pero debemos tener cuidado en redes heterogéneas. Debemos huir de nombres de usuario formados sólo por mayúsculas o números. El primer caso puede dar a sus propietarios problemas a la hora de iniciar sesión, y en el segundo caso puede crear confusión, especialmente si el nombre de usuario no es igual al identificador de la cuenta (UID). Comandos como `ls -l` mostrarán el **UID**, si no existe una entrada correspondiente para ella en el fichero `/etc/passwd`, y no será precisamente sencillo diferenciar los **UID's** de los nombres formados por números.
**password** | Antiguamente, este campo contenía la contraseña de usuario cifrada. Hoy en día, la mayoría de distribuciones, usan contraseñas tipo *shadow*; en lugar de almacenar la contraseña en un fichero que puede leer cualquier usuario, son almacenadas en `/etc/shadow`.
**UID** | El identificador de usuario (un número entre 0 y 2^32-1). Por norma, los **UID's** desde 0 hasta 99 (incluido), están reservados para el sistema, los **UID's** desde 100 hasta 499 son usados por los paquetes software, en caso de que necesiten cuentas de pseudo-usuario. En la mayoría de las distribuciones populares, los **UID's** de usuarios 'reales' empiezan en 500 (o 1000).
**GID** | El **GID** del grupo primario del usuario después de iniciar sesión. Las distribuciones como SUSE asignan un único grupo (*users*) como el grupo compartido de todos los usuarios. Sin embargo, distribuciones como Red Hat o Debian, crean un nuevo grupo cada vez que se crea una nueva cuenta, con el **GID** igual que el **UID**. La idea es permitir asignaciones de permisos más sofisticadas que las conseguidas con la ubicación de todos los usuarios en el mismo grupo. En virtud de la asignación en el fichero `/etc/passwd`, cada usuario debe ser miembro al menos de un grupo. Los grupos secundarios del usuario, son determinados por las entradas en el fichero `/etc/group`.
**GECOS** | Es el campo de comentarios. GECOS significa General Electric Comprehensive Operating System, y no tiene nada que ver con Linux. Este campo contiene varios datos sobre el usuario, su nombre real, y opcionalmente datos como su número de oficina, o su número de teléfono. Esta información es usada por programas como `mail` o `finger`. El comando `chfn` permite a un usuario modificar el contenido de este campo.
**home directory** | Este directorio es el área personal del usuario para almacenar sus propios ficheros. Un directorio *home* recién creado no suele estar vacío, debido a que un usuario nuevo recibe un número de ficheros tipo *profile*, como si fuera su equipamiento básico. Cuando un usuario inicia sesión, la *shell* usa su directorio personal como su directorio actual, por lo tanto es donde se sitúa.
**shell** | El nombre del programa que será lanzado por *login* después de una identificación éxitosa, que normalmente es una shell. Este campo se extiende hasta el final de la línea. El usuario puede cambiar esta entrada con el comando `chsh`. Si un usuario no quiere tener una shell interactiva, podemos indicar un programa con argumentos (`/bin/true`). Si está vacío, se ejecuta `/bin/sh`.

> Algunos de los campos mostrados aquí, pueden estar vacíos. Absolutamente necesarios, son el nombre de usuario, UID, GID y directorio home. 
> Un administrador nunca debe manipular el fichero `/etc/passwd`, sino utilizar alguna de las muchas herramientas que existen para la gestión de las cuentas.

> En el hipotético caso de que nos encontraramos con alguna contraseña guardada en este fichero, debemos moverla cuanto antes al fichero `shadow` con el comando `pwconv`.

### El fichero /etc/shadow.

Por razones de seguridad, casi todas las distribuciones de Linux almacenan las contraseñas de usuario cifradas en el fichero `/etc/shadow`. Este fichero no es legible por los usuarios normales, sólo el usuario `root` puede escribir en él, mientras que los miembros del grupo *shadow* pueden leer. De nuevo, el fichero contiene una línea por cada usuario:

```
$ sudo cat /etc/shadow
root:$6$o3KUah3[...]::0:99999:7:::
bin:*:18397:0:99999:7:::
daemon:*:18397:0:99999:7:::
[...]
vagrant:$6$5mwtCAMPR[...]::0:99999:7:::
```

Este fichero contiene un total de nueve campos, que se detallan a continuación:

Campo | Descripción
--- | ---
**user name** | Debe corresponder con una entrada del fichero `/etc/passwd`. Este campo realiza un *join* entre los dos ficheros.
**password** | La contraseña de usuario cifrada. Un campo vacío, suele indicar que el usuario puede iniciar sesión sin contraseña. Un asterisco, o una exclamación, impide que el usuario inicie sesión, bloqueando a un usuario sin tener que eliminar su cuenta.
**change** | La fecha del último cambio de contraseña, en días desde el 1 de enero de 1970 (Unix Epoch).
**min** | El número mínimo de días que deben transcurrir desde un cambio de contraseña al siguiente.
**max** | El número máximo de días de validez de una contraseña. Después de ese tiempo, el usuario debe modificar su contraseña.
**warn** | El número de días antes de expirar la vida de la contraseña, durante los cuales el usuario será advertido de que debe cambiar su contraseña. Esta advertencia suele aparecer cuando se inicia sesión.
**grace** | El número de días, contando desde la expiración del periodo *<max>*, después del cual la cuenta será bloqueada si el usuario no modifica su contraseña. El usuario puede iniciar sesión.
**lock** | La fecha en la cual, la cuenta fue definitivamente bloqueada, en días desde el 1 de enero de 1970.
**flag** | También conocido como *special flag*. Es un campo para un posible uso en el futuro y que en la actualidad no se usa.

### El fichero /etc/group.

Por defecto, Linux mantiene la información de los grupos en el fichero `/etc/group`. Este fichero contiene una línea por cada grupo en el sistema, y al igual que en el fichero `/etc/passwd` contiene campos separados por *dos puntos*, con el siguiente formato:

	<group name>:<password>:<GID>:<members>

Campo | Descripción
--- | ---
**group name** | El nombre del grupo, para usarlo en los listados de directorios, etc.
**password** | Una contraseña opcional para el grupo. Esto permite a aquellos usuarios que no son miembros del grupo convertirse en miembros del grupo usando el comando `newgrp`. Un asterisco, evita que los usuarios normales puedan cambiarse al grupo en cuestión. Una *x* indica que la contraseña se encuentra en el fichero */etc/gshadow*.
**GID** | El identificador numérico del grupo.
**members** | Una lista de nombres de usuario, separados por coma. Esta línea contiene todos los usuarios que tienen este grupo como grupo secundario, es decir, que son miembros de este grupo, pero tienen un valor diferente en el campo **GID** del fichero `/etc/passwd`.

Al igual que en los **UID's**, los **GID's** son numerados a partir de un valor específico, típicamente 100. Para que una entrada sea válida, al menos se deben cubrir el primero y el tercer campo. Los campos contraseña y/o miembros sólo pueden ser rellenados por aquellos grupos que son asignados a usuarios como grupos secundarios. Los usuarios listados en la lista de miembros no se les requiere la contraseña cuando quieren cambiar el **GID** usando el comando `newgrp`. Si existe una contraseña cifrada, los usuarios sin una entrada en la lista de miembros pueden identificarse usando dicha contraseña, para convertirse en miembros del grupo.

En la práctica, las contraseñas de grupo apenas se utilizan, ya que es más recomendable asignar directamente los grupos a los usuarios, y por otro lado, una única contraseña que debe ser conocida por todos los miembros del grupo, no parece una opción muy segura. Si queremos estar completamente seguros, debemos colocar un asterisco en todos los campos de contraseña de grupo.

### El fichero /etc/gshadow.

Las contraseñas *shadow* de los grupos se almacenan en este fichero, además de información sobre el grupo, como los nombres de los administradores que pueden añadir o eliminar miembros del grupo.

Después de haber instalado una nueva distribución de Linux, tan sólo existirá la cuenta de *root*, y las cuentas de pseudo-usuarios. El resto de cuentas deben ser creadas (aunque en las distribuciones modernas, se creará un usuario sin privilegios en el proceso de instalación).

Antes de ver la sección relacionada con `useradd`, debemos revisar los dos ficheros y el directorio relacionados con dicho proceso de creación de usuarios.

### El fichero /etc/login.defs

El fichero `/etc/login.defs` suele encontrarse en la mayoría de distribuciones Linux. Las directivas de este fichero definen la longitud de la contraseña, si se crea o no un directorio personal por defecto, o el tiempo de renovación de la contraseña.

```
$ grep -v ^$ /etc/login.defs | grep -v ^\#
MAIL_DIR	/var/spool/mail
UMASK		022
HOME_MODE	0700
PASS_MAX_DAYS	99999
PASS_MIN_DAYS	0
PASS_MIN_LEN	5
PASS_WARN_AGE	7
UID_MIN                  1000
UID_MAX                 60000
SYS_UID_MIN               201
SYS_UID_MAX               999
GID_MIN                  1000
GID_MAX                 60000
SYS_GID_MIN               201
SYS_GID_MAX               999
CREATE_HOME	yes
USERGROUPS_ENAB yes
ENCRYPT_METHOD SHA512
```

En la salida podemos observar el valor de la variable `UID_MIN`, que define el UID de los nuevos usuarios. Nos referimos a aquellos que están asociados a seres humanos, en contraposición a los que se pueden asociar a procesos del sistema, que se rigen por la variable `SYS_UID_MIN`.

Otras directivas relevantes de este fichero son las siguientes:

Nombre | Descripción
--- | ---
PASS_MAX_DAYS | Número de días hasta que sea necesario un cambio de contraseña. Es la fecha de caducidad de la contraseña.
PASS_MIN_DAYS | Número de días después de un cambio de contraseña hasta que se pueda cambiar de nuevo.
PASS_MIN_LENGTH | Número mínimo de caracteres requeridos en una contraseña.
PASS_WARN_AGE | Número de días previos a la caducidad de la contraseña durante los cuales se envía un aviso al usuario.
CREATE_HOME | Por defecto se establece a `no`. Si es `yes`, se creará un directorio personal para los nuevos usuarios.
ENCRYPT_METHOD | El método usado para realizar el *hash* de las contraseñas.

### El fichero /etc/default/useradd

El fichero `/etc/default/useradd` es el otro fichero de configuración que está relacionado con la creación de cuentas. Suele tener menor tamaño que el anterior, y a continuación se muestra un ejemplo en CentOS:

```
$ cat /etc/default/useradd
# useradd defaults file
GROUP=100
HOME=/home
INACTIVE=-1
EXPIRE=
SHELL=/bin/bash
SKEL=/etc/skel
CREATE_MAIL_SPOOL=yes

$ sudo useradd -D
GROUP=100
HOME=/home
INACTIVE=-1
EXPIRE=
SHELL=/bin/bash
SKEL=/etc/skel
CREATE_MAIL_SPOOL=yes
```

Tanto con el comando `cat` como con `useradd` son similares en cuanto a la obtención de la información, pero únicamente con `useradd -D` seremos capaces de modificar dichos valores.

Tal y como se observa en la salida anterior, en caso de que en el fichero `/etc/login.defs` la directiva `CREATE_HOME` se encuentre activada (valor `yes`), los directorios personales se crearán en el directorio `/home` (variable `HOME`).

A continuación, se muestra información sobre otras directivas:

Nombre | Descripción
HOME | Directorio base para los directorios personales
INACTIVE | Número de días que deben transcurrir desde la caducidad de una contraseña, y que no hay sido actualizada, para que dicha cuenta se desactive.
SKEL | El directorio *esqueleto*
SHELL | La shell por defecto para las cuentas

### El directorio /etc/skel

En el directorio `/etc/skel` se guardan aquellos ficheros que deben ser copiados de forma predeterminada a los nuevos directorios personales de las cuentas de usuario.

```
ls -a /etc/skel
.  ..  .bash_logout  .bash_profile  .bashrc
```

## Creación de cuentas de usuario

El procedimiento para crear una cuenta de usuario es siempre el mismo (en principio), y consiste en los siguientes pasos:

1. Crear las entradas en el fichero `/etc/passwd` (y en `/etc/shadow`)
2. Si es necesario, una entrada (o varias) en `/etc/group`.
3. Crear el directorio *home*, copiar un conjunto de ficheros dentro del mismo, y transferir la propiedad.
4. Si es necesario, insertar al usuario en otras bases de datos (cuotas).

Todos los ficheros implicados en una nueva cuenta de usuario, son ficheros de texto plano. Podríamos ejecutar cada paso usando un editor de texto. Sin embargo, como ese trabajo se puede volver tedioso, el sistema operativo incorpora herramientas, como el programa `useradd`. En el caso más simple, le pasamos tan solo el nombre del nuevo usuario. Opcionalmente, le podemos indicar algunos parámetros.
 
	useradd <options> <user name>

* **-c <comment>** campo GECOS
* **-d <home directory>** si esta opción no está presente, se asume `/home/username`
* **-e <date>** en esta fecha, la cuenta será desactivada (YYYY-MM-DD)
* **-g <group>** el grupo primario del nuevo usuario (nombre o GID). Debe existir.
* **-G <group, group, ...>** los grupos adicionales (nombres o GID's). También deben existir.
* **-s <shell>** la shell de login del nuevo usuario.
* **-u <UID>** el **UID** numérico del nuevo usuario. No debe estar en uso, a no ser que le indiquemos la opción `-o`.
* **-m** crea el directorio *home* y copia el cojunto básico de ficheros en el mismo. Estos ficheros están en `/etc/skel`, a menos que indiquemos un directorio diferente usando `-k <directorio>`.
* **-M** no crea el directorio personal.
* **-r** crea una cuenta de sistema en lugar de una de usuario.

Por ejemplo:

	# useradd -c “Linus Torvalds” -m -d /home/linus -g devel -k /etc/skel linus

Con la opción -D, podemos configurar los valores por defecto para algunas de las propiedades de las nuevas cuentas. Sin opciones adicionales, se visualizan los valores actuales.

	# useradd -D

Podemos modificar esos valores con las opciones `-g`, `-b`, `-f`, `-e` y `-s` respectivamente:

	# useradd -D -s /usr/bin/zsh

Después de ser creada la cuenta, todavía no es accesible; el administrador debe configurar una contraseña.

Un método para poder realizar consultas sobre los registros almacenados en `/etc/passwd` y `/etc/shadow` es hacer uso de la utilidad `getent`. Tan solo debemos indicarle el nombre de usuario y el fichero, por ejemplo:

```
$ getent passwd vagrant
vagrant:x:1000:1000::/home/vagrant:/bin/bash

$ getent shadow vagrant
[vagrant@localhost ~]$ sudo getent shadow vagrant
vagrant:$6$5mwtCAMPRd8Sba8M$cZ[...]::0:99999:7:::
```

Por supuesto, es posible la creación de una contraseña a través de la utilidad `crypt`, y luego añadirla en el momento de creación de la cuenta a través de la opción `-p` del comando `useradd`, pero no parece una buena práctica. Debemos seguir los pasos que se detallan en la siguiente sección.

### El comando passwd

El comando `passwd` se usa para configurar las contraseñas de los usuarios.

	# passwd linus

Como sabemos, el comando `passwd` también está disponible para que los usuarios puedan modificar sus propias contraseñas.

	$ passwd

Los usuarios normales, deben introducir correctamente su contraseña actual antes de escribir la nueva. Se hace para evitar que cualquier persona que acceda a nuestro equipo, pueda modificarnos la contraseña.

El comando passwd también sirve para manejar varios valores en `/etc/shadow`. Por ejemplo, podemos observar el estado de la contraseña de un usuario, mediante la opción `-S`:

	# passwd -S linus

**PS** o **P** indica que la contraseña está activada, **LK** o **L**, que la cuenta está bloqueada, y **NP** para una cuenta sin contraseña todavía. Los otros campos son:

* la fecha del último cambio de contraseña
* el mínimo, y el máximo intervalo para el cambio de contraseña,
* el intervalo de advertencia de expiración y
* el periodo de gracia antes de que la cuenta se bloquee.

Podemos cambiar algunos de esos valores mediante las opciones `passwd`. Algunos ejemplos:

	# passwd -l linus	bloquea la cuenta
	# passwd -u linus	desbloquea la cuenta
	# passwd -n7 linus	cambio de contraseña al menos cada 7 días
	# passwd -x30 linus	cambio de contraseña como máximo cada 30 días
	# passwd -w3 linus	3 días de gracia

Para modificar el resto de la configuración en `/etc/shadow` precisamos del comando `chage`:

	# chage -E 2010-12-01 linus	bloqueo de cuenta desde el 1 de diciembre de 2010
	# chage -E -1 linus		desbloquea la fecha de caducidad
	# chage -I 7 linus		periodo de gracia de 1 semana
	# chage -m 7 linus		igual que passwd -m
	# chage -M 7 linus		igual que passwd -x
	# chage -W 3 linus		igual que passwd -w

Si no recordamos el nombre de las opciones, ejecutamos `chage` con el nombre del usuario, y nos irá preguntando los valores que deseamos cambiar.

```
$ sudo chage vagrant
Changing the aging information for vagrant
Enter the new value, or press ENTER for the default

	Minimum Password Age [0]: 5
	Maximum Password Age [99999]: 30
	Last Password Change (YYYY-MM-DD) [2018-10-02]:
	Password Expiration Warning [7]: 15
	Password Inactive [-1]: 3
	Account Expiration Date (YYYY-MM-DD) [-1]:
$
```

## Eliminando cuentas de usuario

Para eliminar a un usuario, necesitamos borrar las entradas para el usuario en los ficheros `/etc/passwd`, y `/etc/shadow`, borrar todas las referencias al usuario en `/etc/group`, y eliminar el directorio *home*, así como todos los ficheros creados o que sean propiedad del usuario. Si el usuario posee mensajes en la bandeja de entrada en `/var/mail`, también deben ser eliminados. De nuevo, disponemos de una herramienta para realizar este trabajo. El comando `userdel` elimina una cuenta de usuario:

	userdel [-r] <user name>

La opción `-r` asegura que el directorio personal del usuario (incluyendo su contenido) y su correo en `/var/mail` también será eliminado; el resto de ficheros del usuario deben ser eliminados manualmente. Sin la opción `-r`, sólo eliminamos la información de usuario de las bases de datos.

## Modificando las cuentas de usuario y la asignación de grupo

En lugar de modificar los ficheros `/etc/passwd` y `/etc/group`, podemos utilizar las herramientas `usermod` y `groupmod`. El programa `usermod` acepta casi las mismas opciones que `useradd`, pero realiza los cambios en cuentas existentes.

	usermod -g <grupo> <user name>

Corto | Largo | Descripción
--- | --- | ---
-c | --comment | Modifica el contenido del campo comentario.
-d | --home | Establece un nuevo valor para el directorio personal. Junto con la opción `-m` se moverán los ficheros del directorio actual hacia la nueva ubicación.
-e | --expiredate | Modifica la fecha de caducidad de la cuenta.
-f | --inactive | Modifica el número de días desde que una contraseña caducada y no actualizada debe ser desactivada. Un valor de `-1` significa que nunca se desactivará.
-g | --gid | Cambia el grupo por defecto (primario).
-G | --groups | Actualiza los grupos adicionales. Si solo se indica un nuevo grupo, con la opción `-a` evitamos que se eliminen los anteriores.
-l | --login | Modifica el nombre de usuario, sin cambiar el directorio personal.
-L | --lock | Bloquea la cuenta mediante la inserción de una exclamación al principio del campo de contraseña del fichero `shadow`.
-s | --shell | Cambia la shell de la cuenta
-u | --uid | Modifica el número de identificación UID
-U | --unlock | Desbloquea la cuenta a través de la eliminación del asterisco situado al principio del campo contraseña del fichero `shadow`.

## Cambiar la información de usuario directamente (vipw)

El comando `vipw` llama a un editor para la edición de `/etc/passwd` directamente. El fichero es bloqueado para evitar cambios por parte de otro usuario. Con la opción `-s`, se trabaja con el fichero `/etc/shadow`.

## Creación, modificación y eliminación de grupos

Los grupos se identifican a través de sus nombres y por sus identificadores (GID). Cuando se crea un usuario sin indicar su grupo por defecto, entonces se creará un nuevo grupo. Este grupo tendrá el mismo nombre que el usuario.

En una distribución CentOS, comprobamos la información de grupo de un usuario:

```
$ getent passwd vagrant
vagrant:x:1000:1000::/home/vagrant:/bin/bash

$ sudo groups vagrant 
vagrant : vagrant

$ getent group vagrant
vagrant:x:1000:

$ grep 1000 /etc/group
vagrant:x:1000:
```

Al igual que con las cuentas de usuario, se pueden crear grupos usando varios métodos. Suele ser suficiente con editar el fichero `/etc/group` usando un editor de texto, y añadir una línea (comando `vigr`). Si usamos contraseñas, también debemos modificar el fichero `/etc/gshadow`. Disponemos de los comandos similares a los de gestión de usuarios, pero para grupos: `gropuadd`, `groupmod` y `groupdel`.

	# groupadd -g GID project

	$ getent group project
	project:x:1001:

El nuevo grupo creado `project` no tiene una contraseña de grupo, sin embargo, en la salida del comando `getent` se observa que la `x` del segundo campo solo significa que de existir estará almacenada en el fichero `/etc/gshadow`, por lo que debemos comprobarlo:

	$ sudo getent gshadow project
	project:!::

La exclamación (!) indica que no se ha establecido una contraseña para el grupo `project`.

> Las contraseñas de grupo permiten a los usuarios foráneos acceder a los mismos, y deben ser creadas con el comando `gshadow`. Esta contraseña debe compartirse entre aquellos usuarios que desean acceder al grupo, por lo que parece una mala práctica en cuanto a la seguridad del sistema. Las contraseñas nunca deberían compartirse.

> Cuando creamos un nuevo grupo podemos indicar el **GID** con la opción `-g`, hasta 99 están reservados, si no se indica nada, se utiliza el siguiente valor libre. 

Después de la creación del grupo, podemos añadir usuarios al mismo con el comando `usermod`.

	$ sudo usermod -aG project vagrant

	$ sudo groups vagrant
	vagrant: vagrant project

	$ getent group project
	project:x:1001:vagrant

Con el comando `groupmod` es posible modificar el **GID** o el nombre del grupo:

	# groupmod -g GID -n nombre grupo

Con `groupdel` eliminamos el grupo:

	# groupdel nombre

Cuando se elimina un grupo, es importante buscar en el árbol de directorios aquellos ficheros con permisos de acceso para ese grupo. Lo podemos realizar con el comando `find` y el GID del grupo eliminado.

## Comandos vistos en el tema

Comando | Descripción 
-- | --
adduser | Comando para crear cuentas de usuario.
chfn | Permite a los usuarios cambiar el campo GECOS
gpasswd | Permite al administrador de un grupo, cambiar los miembros del grupo, así como su contraseña.
groupadd | Añade grupos de usuarios al sistema.
groupdel | Elimina grupos del sistema.
groupmod | Cambia entradas de grupo.
groups | Muestra los grupos a los cuales pertenece un usuario.
id | Muestra los UID's y GID's
useradd | Añade nuevas cuentas de usuario
userdel | Elimina cuentas de usuario
usermod | Modifica la base de datos de usuarios
vigr | Permite editar los ficheros `/etc/group` o `/etc/gshadow`

## Resumen

* El acceso al sistema está controlado por las cuentas de usuario.
* Una cuenta de usuario tiene un **UID** numérico y (al menos) un nombre de usuario alfanumérico.
* Los usuarios pueden formar grupos, que también poseen su **GID** numérico y nombre.
* Los *pseudo-usuarios* sirven para perfeccionar los permisos de acceso.
* La base de datos central de usuarios se almacena en `/etc/passwd`.
* Las contraseñas cifradas de los usuarios se almacenan en el fichero `/etc/shadow`.
* La información de los grupos se guarda en `/etc/group`.
* Las contraseñas son gestionadas por el comando `passwd`.
* El programa `chage` se usa para gestionar los parámetros de las contraseñas.
* La información de grupo se gestiona con los comandos `groupadd`, `groupmod`, `groupdel` y `gpasswd`.
